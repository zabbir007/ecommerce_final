$(function(){

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(".addcart").click(function(event) {
    var APP_URL = $('meta[name="_base_url"]').attr('content');
    console.log(APP_URL);
	// var productId=parseInt($("#productIdBtn").val());

	var id = $(this).data("id");

	// console.log(productId);

	console.log(id);


	    jQuery.ajax({
	                url: APP_URL+'/user/add-to-cart',
	                method: 'post',
	                data:{id:id},
	                success: function(result){ 

	                	var val = JSON.parse(result);
	                	console.log(val);
						swal({
						  title: "Good job!",
						  text: "Product Add To Cart Successfully Done !!!",
						  icon: "success",
						  button: "ok!",
						});
	            },
	                  error: function() {
	                    alert('Error occurs!');
	                 }
	            });

 });

$(".btnCartProductDelete").click(function(){
	     var element=$(this);
	     var id = element.attr("id");
	     console.log(id);
	     var APP_URL = $('meta[name="_base_url"]').attr('content');
	     swal({
	      title: "Are you sure?",
	      text: "Once deleted, you will not be able to recover this  file!",
	      icon: "warning",
	      buttons: true,
	      dangerMode: true,
	    })
	    .then((willDelete) => {
	      if (willDelete) {

	            
	             jQuery.ajax({
	                url: APP_URL+'/user/delete-cart-value',
	                method: 'post',
	                data:{id:id},
	                success: function(result){


	                    location.reload(true);
	                },
	                  error: function() {
	                    alert('Error occurs!');
	                 }
	            });



	        swal("Poof! Your  file has been deleted!", {
	          icon: "success",
	        });
	      } else {
	        swal("Your  file is safe!");
	      }
	    });
	})



})