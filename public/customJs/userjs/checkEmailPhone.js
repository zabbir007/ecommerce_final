$(function(){

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$("#checkUserEmail").keyup(function() {
    
    var APP_URL = $('meta[name="_base_url"]').attr('content');
    console.log(APP_URL);
    var userEmail = $.trim($("[name=userEmail]").val());
    console.log(userEmail);
    jQuery.ajax({
        url: APP_URL+'/user/check-user-email',
        method: 'post',
        data:{userEmail:userEmail},
        success: function(result){
        	var checkEmail = JSON.parse(result);

        	if(checkEmail=="Found"){
        		 
                swal("This Email Already Used !!!");
        		$("#userRegister").hide();
    		}else{
    			$("#userRegister").show();
    		}
          
        },
          error: function() {
            alert('Error occurs!');
         }
    });

});

$("#checkUserPhone").keyup(function() {
    
    var userPhone = $.trim($("[name=userPhone]").val());
    var APP_URL = $('meta[name="_base_url"]').attr('content');
    jQuery.ajax({
        url: APP_URL+'/user/check-user-phone',
        method: 'post',
        data:{userPhone:userPhone},
        success: function(result){
        	var checkPhone = JSON.parse(result);

        	if(checkPhone=="Found"){
        		 swal("This Phone Number Already Used !!!");

        		 $("#userRegister").hide();
    		}else{
    			$("#userRegister").show();
    		}
          
        },
          error: function() {
            alert('Error occurs!');
         }
    });

});

})