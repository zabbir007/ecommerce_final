$(function(){

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(".insertSubscribeUser").click(function(){

	console.log('working');
	var APP_URL = $('meta[name="_base_url"]').attr('content');
    var subscribeUserEmail = $.trim($("[name=subscribeUserEmail]").val());
    console.log(subscribeUserEmail);
    if (subscribeUserEmail=='') {

        swal({
			  title: "Oh No!",
			  text: "Please Insert Your Email !!!",
			  icon: "warning",
			  button: "ok!",
			});
    }else{

	    	jQuery.ajax({
	        url: APP_URL+'/user/add-subscribe-user',
	        method: 'post',
	        data:{subscribeUserEmail:subscribeUserEmail},
	        success: function(result){
	        	var checkEmail = JSON.parse(result);

	        	if(checkEmail=="Found"){
	        		swal({
					  title: "Oh No!",
					  text: "This Email Already Used !!!!",
					  icon: "warning",
					  button: "ok!",
					});
	        		
	    		}else if(checkEmail=="Success"){
	    			
	    			
	    			swal({
					  title: "Good job!",
					  text: "Subscribe Successfully Done !",
					  icon: "success",
					  button: "ok!",
					});
	    			$("[name=subscribeUserEmail]").val('');
	    		}
	          
	        },
	          error: function() {
	            alert('Error occurs!');
	         }
	    });
    }
   
	})

})