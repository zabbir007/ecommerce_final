$(function(){

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});


$("#shippingAdditionalPhotoPrice").empty();
 var shippingAdditionalPhotoPrice='<span >'+0+'</span></span><input type="hidden" readonly="" name="setAdditionalPhotoPrice" id="setAdditionalPhotoPrice" value="'+0+'">';
$("#shippingAdditionalPhotoPrice").append(shippingAdditionalPhotoPrice);

$("#shippingPackagePrice").empty();
 var shippingPackagePrice='<span >'+0+'</span></span><input type="hidden" readonly="" name="setPackagePrice" id="setPackagePrice" value="'+0+'">';
$("#shippingPackagePrice").append(shippingPackagePrice);

$("#urgentPrice").empty();
 var urgentPrice='<span >'+0+'</span></span><input type="hidden" readonly="" name="setUrgentPrice" id="setUrgentPrice" value="'+0+'">';
$("#urgentPrice").append(urgentPrice);

$("#stickerPrice").empty();
 var stickerPrice='<span >'+0+'</span></span><input type="hidden" readonly="" name="setStickerPrice" id="setStickerPrice" value="'+0+'">';
$("#stickerPrice").append(stickerPrice);

$("#shippingTotalAmount").empty();
 var shippingTotalAmount='<span >'+0+'</span></span><input type="hidden" readonly="" name="customerSetTotalAmount" id="customerSetTotalAmount" value="'+0+'">';
$("#shippingTotalAmount").append(shippingTotalAmount);


//additional picture
$('input[name="shippingAdditionalPicture"]').click(function(){
  console.log('jabbir');
    if($(this).is(":checked")){
        var radioValue = parseFloat($("input[name='shippingAdditionalPicture']:checked").val());
        var getUrgentPrice=parseFloat($("#setUrgentPrice").val());
        var getStickerPrice=parseFloat($("#setStickerPrice").val());
        var getPackagePrice=parseFloat($("#setPackagePrice").val());
        var finalAmount=radioValue+getUrgentPrice+getStickerPrice+getPackagePrice;
        $("#shippingAdditionalPhotoPrice").empty();
     var shippingAdditionalPhotoPrice='<span >'+radioValue+'</span></span><input type="hidden" readonly="" name="setAdditionalPhotoPrice" id="setAdditionalPhotoPrice" value="'+radioValue+'">';
    $("#shippingAdditionalPhotoPrice").append(shippingAdditionalPhotoPrice);

    $("#shippingTotalAmount").empty();
     var shippingTotalAmount='<span >'+finalAmount+'</span></span><input type="hidden" readonly="" name="customerSetTotalAmount" id="customerSetTotalAmount" value="'+finalAmount+'">';
    $("#shippingTotalAmount").append(shippingTotalAmount);
    }
});


//Package Price
$('input[name="shippingPackagePrice"]').click(function(){
  console.log('jabbir');
    if($(this).is(":checked")){
        var radioValue = parseFloat($("input[name='shippingPackagePrice']:checked").val());
        var getUrgentPrice=parseFloat($("#setUrgentPrice").val());
        var getStickerPrice=parseFloat($("#setStickerPrice").val());
        var getPhotoPrice=parseFloat($("#setAdditionalPhotoPrice").val());
        var finalAmount=radioValue+getUrgentPrice+getStickerPrice+getPhotoPrice;
        $("#shippingPackagePrice").empty();
         var shippingPackagePrice='<span >'+radioValue+'</span></span><input type="hidden" readonly="" name="setPackagePrice" id="setPackagePrice" value="'+radioValue+'">';
        $("#shippingPackagePrice").append(shippingPackagePrice);

    $("#shippingTotalAmount").empty();
     var shippingTotalAmount='<span >'+finalAmount+'</span></span><input type="hidden" readonly="" name="customerSetTotalAmount" id="customerSetTotalAmount" value="'+finalAmount+'">';
    $("#shippingTotalAmount").append(shippingTotalAmount);
    }
});



//urgent ship price
$('input[name="urgentShipPrice"]').click(function(){
    if($(this).prop("checked") == true){
      var getUrgentPrice = parseFloat($("input[name='urgentShipPrice']").val());
      var getStickerPrice=parseFloat($("#setStickerPrice").val());
      var getPhotoPrice=parseFloat($("#setAdditionalPhotoPrice").val());
      var getPackagePrice=parseFloat($("#setPackagePrice").val());
      var finalAmount=getPhotoPrice+getUrgentPrice+getStickerPrice+getPackagePrice;
      $("#urgentPrice").empty();
       var urgentPrice='<span >'+getUrgentPrice+'</span></span><input type="hidden" readonly="" name="setUrgentPrice" id="setUrgentPrice" value="'+getUrgentPrice+'">';
      $("#urgentPrice").append(urgentPrice);

      $("#shippingTotalAmount").empty();
       var shippingTotalAmount='<span >'+finalAmount+'</span></span><input type="hidden" readonly="" name="customerSetTotalAmount" id="customerSetTotalAmount" value="'+finalAmount+'">';
      $("#shippingTotalAmount").append(shippingTotalAmount);
    }
    else if($(this).prop("checked") == false){
        var getUrgentPrice = 0;
        var getStickerPrice=parseFloat($("#setStickerPrice").val());
        var getPhotoPrice=parseFloat($("#setAdditionalPhotoPrice").val());
        var finalAmount=getPhotoPrice+getUrgentPrice+getStickerPrice;
        $("#urgentPrice").empty();
         var urgentPrice='<span >'+getUrgentPrice+'</span></span><input type="hidden" readonly="" name="setUrgentPrice" id="setUrgentPrice" value="'+getUrgentPrice+'">';
        $("#urgentPrice").append(urgentPrice);

        $("#shippingTotalAmount").empty();
         var shippingTotalAmount='<span >'+finalAmount+'</span></span><input type="hidden" readonly="" name="customerSetTotalAmount" id="customerSetTotalAmount" value="'+finalAmount+'">';
        $("#shippingTotalAmount").append(shippingTotalAmount);
        
    }
});


//Fragile Sticker Price..
$('input[name="fragileStickerPrice"]').click(function(){
    if($(this).prop("checked") == true){
      var getUrgentPrice = parseFloat($("#setUrgentPrice").val());
      var getStickerPrice=parseFloat($("input[name='fragileStickerPrice']").val());
      var getPhotoPrice=parseFloat($("#setAdditionalPhotoPrice").val());
      var getPackagePrice=parseFloat($("#setPackagePrice").val());
      var finalAmount=getPhotoPrice+getUrgentPrice+getStickerPrice+getPackagePrice;
      $("#stickerPrice").empty();
       var stickerPrice='<span >'+getStickerPrice+'</span></span><input type="hidden" readonly="" name="setStickerPrice" id="setStickerPrice" value="'+getStickerPrice+'">';
      $("#stickerPrice").append(stickerPrice);

      $("#shippingTotalAmount").empty();
       var shippingTotalAmount='<span >'+finalAmount+'</span></span><input type="hidden" readonly="" name="customerSetTotalAmount" id="customerSetTotalAmount" value="'+finalAmount+'">';
      $("#shippingTotalAmount").append(shippingTotalAmount);
    }
    else if($(this).prop("checked") == false){
        var getUrgentPrice = parseFloat($("#setUrgentPrice").val());
        var getStickerPrice=0;
        var getPhotoPrice=parseFloat($("#setAdditionalPhotoPrice").val());
        var finalAmount=getPhotoPrice+getUrgentPrice+getStickerPrice;
        $("#stickerPrice").empty();
         var stickerPrice='<span >'+0+'</span></span><input type="hidden" readonly="" name="setStickerPrice" id="setStickerPrice" value="'+0+'">';
        $("#stickerPrice").append(stickerPrice);

        $("#shippingTotalAmount").empty();
         var shippingTotalAmount='<span >'+finalAmount+'</span></span><input type="hidden" readonly="" name="customerSetTotalAmount" id="customerSetTotalAmount" value="'+finalAmount+'">';
        $("#shippingTotalAmount").append(shippingTotalAmount);
        
    }
});


// $("select[name='shippingShippingCategory']").change(function(){
//         var id=$.trim($("select[name='shippingShippingCategory']").val());

//         if(id==''){
//           // $('#districtSelectBox').hide();
//           // $('#areaBox').hide();
//           // $('#btnAreaSave').hide();
//         }else{


//           var APP_URL = $('meta[name="_base_url"]').attr('content');
//           // $('#areaBox').hide();

//          jQuery.ajax({
//             url: APP_URL+'/user/get-shipping-shipping-subcategory',
//             method: 'post',
//             data:{id:id},
//             beforeSend: function() {
//               // $('#loadingImg').show();
//             },
//             success: function(result){
//                 // $('#loadingImg').hide();

//               var value = JSON.parse(result);
//               $("#shippingShippingSubCategory").empty();
//               var options='<option value="">----Select----</option>';
//               // $('#districtSelectBox').show();

//                $.each(value, function (key, val) {
                  
                  
//                    //console.log(val.id+" "+val.districtName);

                 

//                  options +='<option value='+val.id+'>'+val.subCategoryName+'</option>';
//                 //options.append(option);

                   
//               });
//                 $("#shippingShippingSubCategory").append(options);
          

//             },
//               error: function() {
//                 alert('Error occurs!');
//              }
//         });


//         }

//    });


})