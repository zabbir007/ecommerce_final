$(function(){

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(".btnPredefinedMarketplaceDelete").click(function(){
	     var element=$(this);
	     var id = element.attr("id");
	     var APP_URL = $('meta[name="_base_url"]').attr('content');
	     swal({
	      title: "Are you sure?",
	      text: "Once deleted, you will not be able to recover this  file!",
	      icon: "warning",
	      buttons: true,
	      dangerMode: true,
	    })
	    .then((willDelete) => {
	      if (willDelete) {

	            
	             jQuery.ajax({
	                url: APP_URL+'/admin/delete-predefined-marketplace',
	                method: 'post',
	                data:{id:id},
	                success: function(result){


	                    location.reload(true);
	                },
	                  error: function() {
	                    alert('Error occurs!');
	                 }
	            });



	        swal("Poof! Your  file has been deleted!", {
	          icon: "success",
	        });
	      } else {
	        swal("Your  file is safe!");
	      }
	    });
	})

 })