$(function(){

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$("#adminCheckCustomerEmail").keyup(function() {
    
    var customerEmail = $.trim($("input:text[name=email]").val());
    var APP_URL = $('meta[name="_base_url"]').attr('content');
    jQuery.ajax({
        url: APP_URL+'/admin/admin-check-customer-email',
        method: 'post',
        data:{customerEmail:customerEmail},
        success: function(result){
        	var checkEmail = JSON.parse(result);

        	if(checkEmail=="Found"){
        		 swal({
	                  title: "This Email Allready Used",
	                  icon: "warning",
	                  dangerMode: true,
	                })

        		 $("#customerRegister").hide();
    		}else{
    			$("#customerRegister").show();
    		}
          
        },
          error: function() {
            alert('Error occurs!');
         }
    });

});

$("#adminCheckCustomerPhone").keyup(function() {
    
    var customerPhone = $.trim($("[name=phone]").val());
    console.log(customerPhone);
    var APP_URL = $('meta[name="_base_url"]').attr('content');
    jQuery.ajax({
        url: APP_URL+'/admin/admin-check-customer-phone',
        method: 'post',
        data:{customerPhone:customerPhone},
        success: function(result){
        	var checkPhone = JSON.parse(result);

        	if(checkPhone=="Found"){
        		 swal({
	                  title: "This Phone Number Allready Used",
	                  icon: "warning",
	                  dangerMode: true,
	                })

        		 $("#customerRegister").hide();
    		}else{
    			$("#customerRegister").show();
    		}
          
        },
          error: function() {
            alert('Error occurs!');
         }
    });

});

//Delete customer
$(".btnAdminCustomerDelete").click(function(){
	     var element=$(this);
	     var id = element.attr("id");
	     var APP_URL = $('meta[name="_base_url"]').attr('content');
	     swal({
	      title: "Are you sure?",
	      text: "Once deleted, you will not be able to recover this  file!",
	      icon: "warning",
	      buttons: true,
	      dangerMode: true,
	    })
	    .then((willDelete) => {
	      if (willDelete) {

	            
	             jQuery.ajax({
	                url: APP_URL+'/admin/admin-delete-customer',
	                method: 'post',
	                data:{id:id},
	                success: function(result){


	                    location.reload(true);
	                },
	                  error: function() {
	                    alert('Error occurs!');
	                 }
	            });



	        swal("Poof! Your  file has been deleted!", {
	          icon: "success",
	        });
	      } else {
	        swal("Your  file is safe!");
	      }
	    });
	})
//Delete Customer

//When edit customer profile then check email and phone start

$("#adminCheckCustomerEmailEdit").keyup(function() {
    
    var customerEmail = $.trim($("input:text[name=email]").val());
    var id = $.trim($("[name=id]").val());
    console.log(customerEmail);
    console.log(id);
    var APP_URL = $('meta[name="_base_url"]').attr('content');
    jQuery.ajax({
        url: APP_URL+'/admin/admin-check-customer-email-edit',
        method: 'post',
        data:{customerEmail:customerEmail,id:id},
        success: function(result){
        	var checkEmail = JSON.parse(result);

        	if(checkEmail=="Found"){
        		 swal({
	                  title: "This Email Allready Used",
	                  icon: "warning",
	                  dangerMode: true,
	                })

        		 // $("#customerRegister").hide();
    		}else{
    			// $("#customerRegister").show();
    		}
          
        },
          error: function() {
            alert('Error occurs!');
         }
    });

});

$("#adminCheckCustomerPhoneEdit").keyup(function() {
    
    var customerPhone = $.trim($("[name=phone]").val());
    var id = $.trim($("[name=id]").val());
    console.log(customerPhone);
    console.log(id);
    var APP_URL = $('meta[name="_base_url"]').attr('content');
    jQuery.ajax({
        url: APP_URL+'/admin/admin-check-customer-phone-edit',
        method: 'post',
        data:{customerPhone:customerPhone,id:id},
        success: function(result){
        	var checkPhone = JSON.parse(result);

        	if(checkPhone=="Found"){
        		 swal({
	                  title: "This Phone Number Allready Used",
	                  icon: "warning",
	                  dangerMode: true,
	                })

        		 // $("#customerRegister").hide();
    		}else{
    			// $("#customerRegister").show();
    		}
          
        },
          error: function() {
            alert('Error occurs!');
         }
    });

});

//When edit customer profile then check email and phone end



})