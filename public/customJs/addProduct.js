$(function(){

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});


//sub category start here

$("select[name='category_id']").change(function(){

	var department_id=$.trim($("select[name='department_id']").val());
	var category_id=$.trim($("select[name='category_id']").val());
	if (department_id=='') {
     
     $('#subCategoryShow').hide();

	}else if (category_id=='') {
     
     $('#subCategoryShow').hide();

	}else{

     
     
	     var APP_URL = $('meta[name="_base_url"]').attr('content');
	        
	       jQuery.ajax({
	          url: APP_URL+'/admin/get-sub-category',
	          method: 'post',
	          data:{department_id:department_id,category_id:category_id},
	          beforeSend: function() {
	            // $('#loadingImg').show();
	          },
	          success: function(result){
	              // $('#loadingImgForPickUpAddress').hide();

	            var subCategory = JSON.parse(result);
	            $("#subCategoryShow").empty();
	            
	            $('#subCategoryShow').show();
                 var options='';
                 options +='<label for="product-category">Sub Categories<span class="text-danger">*</span></label><select class="form-control select2" required="" name="subcategory_id">';
	             $.each(subCategory, function (key, val) {
	               //console.log(val.id+" "+val.districtName);
	               options +='<option value='+val.id+'>'+val.subCategory_type+'</option>';
	              //options.append(option);
	            });
	            options +='</select>';
	              $("#subCategoryShow").append(options);
	        

	          },
	            error: function() {
	              alert('Error occurs!');
	           }
	      });


	}
    

 });

//sub category show end here


//Discount count start here

$("#discount_percent").keyup(function() {
    
    var totalAmount = $.trim($("input:text[name=total_price]").val());
    var discountAmount = $.trim($("input:text[name=discount_percent]").val());
    if (totalAmount=='') {
    	swal({
	      title: "Error?",
	      text: "Please input amount first!",
	      icon: "warning",
	      buttons: true,
	      dangerMode: true,
	    })
    }
    
    if (discountAmount>100) {

    	swal({
	      title: "Error?",
	      text: "Discount less than 100% !",
	      icon: "warning",
	      buttons: true,
	      dangerMode: true,
	    })
    }else{
        
        $("#discount_price").empty();
    	var discount=totalAmount*(discountAmount/100);
    	var finalAmount=totalAmount-discount;
    	console.log(finalAmount);
        var options='';
        options +='<label for="product-price" >Discount price</label><input type="text" readonly="" name="discount_price" value="'+finalAmount+'" class="form-control" placeholder="Discount Price Auto Calculate">';
        $("#discount_price").append(options);

    	
    }
    
});

//Discount count end here

//Delete Product Start Here
$(".btnProductDelete").click(function(){
	     var element=$(this);
	     var id = element.attr("id");
	     var APP_URL = $('meta[name="_base_url"]').attr('content');
	     swal({
	      title: "Are you sure?",
	      text: "Once deleted, you will not be able to recover this  file!",
	      icon: "warning",
	      buttons: true,
	      dangerMode: true,
	    })
	    .then((willDelete) => {
	      if (willDelete) {

	            
	             jQuery.ajax({
	                url: APP_URL+'/admin/delete-product',
	                method: 'post',
	                data:{id:id},
	                success: function(result){


	                    location.reload(true);
	                },
	                  error: function() {
	                    alert('Error occurs!');
	                 }
	            });



	        swal("Poof! Your  file has been deleted!", {
	          icon: "success",
	        });
	      } else {
	        swal("Your  file is safe!");
	      }
	    });
	})
//Delete Product End Here


})