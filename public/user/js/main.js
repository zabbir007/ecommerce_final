
//-----------------Scroll UP----------------------
$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});

//--------------side menu bar-------------------
$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 10000) {
            $('.card-body-menu').fadeIn();
        }else if ($(this).scrollTop() == 0) {
          $('.card-body-menu').fadeIn();
        }
        else {
            $('.card-body-menu').fadeOut();
        }
    });
});

$(document).ready(function () {
  $(".accordionClick").click(function(){
      $('.card-body-menu').fadeIn();
    });
});
$(document).ready(function () {
  $("#accordionClick").click(function(){
      $('.card-body-menu-popup').css('display','block');
    });
});


//---------------Menu bacground---------------
$(document).ready(function(){
  $(window).scroll(function(){
  	var scroll = $(window).scrollTop();
	  if (scroll > 300) {
	    $(".nav-2").css("box-shadow" , "0 1px 2px rgba(34, 25, 25, 0.4)");
	  }
	  else{
		  $(".nav-2").css("box-shadow" , "none");
	  }
  })
})


//-------------------Profile page-----------
$(document).ready(function(){
  $("#orderClick").click(function(){

    $('#personal').hide();
    $('#shipping').hide();
    $('#order').show();
  });

  $("#profileClick").click(function(){

    $('#personal').show();
    $('#shipping').hide();
    $('#order').hide();
  });

  $("#shippingClick").click(function(){

    $('#personal').hide();
    $('#shipping').show();
    $('#order').hide();
  });
});
//==================User Profile===========================
$(document).ready(function(){
  $('ul li a').click(function(){
    $('li a').removeClass("active");
    $(this).addClass("active");
});
});
