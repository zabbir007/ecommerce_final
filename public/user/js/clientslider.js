//Home pages Testimony slider-------------------

var quoteIndex = 1;
showQuotes(quoteIndex);
function plusQuote(n) {
  showQuotes(quoteIndex += n);
}

function currentQuote(n) {
  showQuotes(quoteIndex = n);
}

function showQuotes(n) {
  var i;
  var quotes = document.getElementsByClassName("quoteSlides");
  var qtdots = document.getElementsByClassName("qtdot");
  if (n > quotes.length) {quoteIndex = 1}
  if (n < 1) {quoteIndex = quotes.length}
  for (i = 0; i < quotes.length; i++) {
      quotes[i].style.display = "none";
  }
  for (i = 0; i < qtdots.length; i++) {
      qtdots[i].className = qtdots[i].className.replace(" active", "");
  }
  quotes[quoteIndex-1].style.display = "block";
  qtdots[quoteIndex-1].className += "active";

}
