<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('swiftNumber')->nullable();
            $table->string('userId')->nullable();
            $table->string('userEmail')->nullable();
            $table->string('productType')->nullable();
            $table->string('invoiceNumber')->nullable();
            $table->string('productDescription')->nullable();
            $table->string('packageId')->nullable();
            $table->string('from')->nullable();
            $table->string('trackingNumber')->nullable();
            $table->string('weight')->nullable();
            $table->string('sentTo')->nullable();
            $table->string('status')->nullable();


            $table->string('shippingAdditionalPicture')->nullable();
            $table->string('useSticker')->nullable();
            $table->string('setStickerPrice')->nullable();
            $table->string('useUrgent')->nullable();
            $table->string('setUrgentPrice')->nullable();
            $table->string('boxCondition')->nullable();
            $table->string('personalUse')->nullable();
            $table->string('shippingType')->nullable();
            $table->string('setAdditionalPhotoPrice')->nullable();
            $table->string('setBoxPrice')->nullable();
            $table->string('customerSetTotalAmount')->nullable();
            $table->string('shippingBoxCategory')->nullable();
            $table->string('shippingBoxSubCategory')->nullable();
            $table->string('shippingShippingCategory')->nullable();
            $table->string('shippingShippingSubCategory')->nullable();
            $table->string('bill_name')->nullable();
            $table->string('bill_address1')->nullable();
            $table->string('bill_address2')->nullable();
            $table->string('bill_country')->nullable();
            $table->string('bill_city')->nullable();
            $table->string('bill_state')->nullable();
            $table->string('bill_zip')->nullable();
            $table->string('bill_countryCode')->nullable();
            $table->string('bill_phone')->nullable();
            $table->string('adminShippingCharge')->nullable();
            $table->string('adminServiceCharge')->nullable();
            $table->string('fragileStickerAmount')->nullable();
            $table->string('insuranceAmount')->nullable();
            $table->string('additionalCost')->nullable();
            $table->string('adminFinalAmount')->nullable();
            $table->string('finalAmountStatus')->nullable();
            $table->string('deliveryDate')->nullable();

            $table->string('orderNumber')->nullable();
            $table->string('adminShipmentCharge')->nullable();
            $table->string('adminDiscountAmount')->nullable();
            $table->string('adminInvoiceImage')->nullable();
            $table->string('packageName')->nullable();
            $table->string('packagePrice')->nullable();

            $table->string('adminImage1')->nullable();
            $table->string('adminImage2')->nullable();
            $table->string('adminImage3')->nullable();
            $table->string('adminImage4')->nullable();
            $table->string('adminImage5')->nullable();
            $table->string('adminImage6')->nullable();
            $table->string('adminImage7')->nullable();
            $table->string('adminImage8')->nullable();
            $table->string('adminImage9')->nullable();
            $table->string('adminImage10')->nullable();
            
            $table->string('createTime')->nullable();
            $table->string('quantity')->nullable();
            $table->string('productImage')->nullable();
            $table->string('invoiceImage')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_order');
    }
}
