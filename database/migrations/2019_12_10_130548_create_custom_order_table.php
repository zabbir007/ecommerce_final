<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userId')->nullable();
            $table->integer('orderType')->nullable();
            $table->string('productType')->nullable();
            $table->string('productName')->nullable();
            $table->string('productLink')->nullable();
            $table->longText('productDescription')->nullable();
            $table->string('productEstimatePrice')->nullable();
            $table->string('shippingType')->nullable();
            $table->string('productQuantity')->nullable();
            $table->string('userImage1')->nullable();
            $table->string('userImage2')->nullable();
            $table->string('userImage3')->nullable();
            $table->string('userImage4')->nullable();
            $table->string('userImage5')->nullable();
            $table->string('bill_name')->nullable();
            $table->string('bill_address1')->nullable();
            $table->string('bill_address2')->nullable();
            $table->string('bill_country')->nullable();
            $table->string('bill_city')->nullable();
            $table->string('bill_state')->nullable();
            $table->string('bill_zip')->nullable();
            $table->string('bill_countryCode')->nullable();
            $table->string('bill_phone')->nullable();
            $table->string('initialAmount')->nullable();
            $table->string('adminCommitionPrice')->nullable();
            $table->string('adminOfferPrice')->nullable();
            $table->string('trackingNumber')->nullable();
            $table->string('submitDate')->nullable();
            $table->string('initialAmountStatus')->nullable();
            $table->string('orderNumber')->nullable();
            $table->string('orderStatus')->nullable();
            $table->string('marketPlaceLink')->nullable();
            $table->string('packageSize')->nullable();
            $table->string('packagePrice')->nullable();
            $table->string('useSticker')->nullable();
            $table->string('personalUse')->nullable();
            $table->string('useUrgent')->nullable();
            $table->string('setUrgentPrice')->nullable();
            $table->string('orderDate')->nullable();

            $table->string('adminProductSize')->nullable();
            $table->string('adminOrderDescription')->nullable();
            $table->string('adminOrderCost')->nullable();
            $table->string('adminImage1')->nullable();
            $table->string('adminImage2')->nullable();
            $table->string('adminImage3')->nullable();
            $table->string('adminInvoiceImage')->nullable();
            $table->string('adminProductPrice')->nullable();
            $table->string('adminProductQuantity')->nullable();
            $table->string('adminProductFinalAmount')->nullable();
            $table->string('adminProductFinalAmountStatus')->nullable();

            $table->string('additionalPicture')->nullable();
            $table->string('boxCondition')->nullable();
            $table->string('fragileStickerAmount')->nullable();
            $table->string('setAdditionalPhotoPrice')->nullable();
            $table->string('finalTotalAmount')->nullable();
            $table->string('setBoxPrice')->nullable();
            $table->string('finalInsuranceAmount')->nullable();
            $table->string('boxCategory')->nullable();
            $table->string('boxSubCategory')->nullable();
            $table->string('shippingCategory')->nullable();
            $table->string('shippingSubCategory')->nullable();


            $table->string('adminServicePrice')->nullable();
            $table->string('adminShippingPrice')->nullable();
            $table->string('adminButePrice')->nullable();
            $table->string('adminAdditionalAmount')->nullable();
            $table->string('adminSetProductFinalAmount')->nullable();
            $table->string('adminImage4')->nullable();
            $table->string('adminImage5')->nullable();
            $table->string('adminImage6')->nullable();
            $table->string('adminImage7')->nullable();
            $table->string('adminImage8')->nullable();
            $table->string('adminImage9')->nullable();
            $table->string('adminImage10')->nullable();
            $table->string('adminImage11')->nullable();
            $table->string('adminImage12')->nullable();
            $table->string('adminImage13')->nullable();
            $table->string('adminFinalAmountStatus')->nullable();
           


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order');
    }
}
