<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutsideOrderPaymentSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outside_order_payment_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('serviceCharge')->nullable();
            $table->string('shippingCharge')->nullable();
            $table->string('buteCost')->nullable();
            $table->string('fragileStickerAmount')->nullable();
            $table->string('insuranceAmount')->nullable();
            $table->string('insuranceRange')->nullable();
            $table->string('cancelAmount')->nullable();
            $table->integer('status')->default($value = 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outside_order_payment_setting');
    }
}
