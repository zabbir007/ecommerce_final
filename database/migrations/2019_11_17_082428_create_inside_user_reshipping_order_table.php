<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsideUserReshippingOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inside_user_reshipping_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('totalAmount')->nullable();
            $table->string('boxPrice')->nullable();
            $table->string('boxId')->nullable();
            $table->string('deliveryPriceTotal')->nullable();
            $table->string('deliveryType')->nullable();
            $table->string('serviceCharge')->nullable();
            $table->string('shippingCharge')->nullable();
            $table->string('orderNumber')->nullable();
            $table->string('itemName')->nullable();
            $table->string('itemUrl')->nullable();
            $table->string('size')->nullable();
            $table->string('quantity')->nullable();
            $table->string('insideShippingOrderNumber')->nullable();
            $table->string('userId')->nullable();
            $table->string('createTime')->nullable();
            $table->string('deliveryDate')->nullable();
            $table->string('status')->nullable();
            $table->string('color')->nullable();
            $table->string('price')->nullable();
            $table->string('anyOther')->nullable();
            $table->string('instruction')->nullable();

            $table->string('country')->nullable();
            $table->string('address1')->nullable();
            $table->string('city')->nullable();
            $table->string('company')->nullable();
            $table->string('street')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inside_user_reshipping_order');
    }
}
