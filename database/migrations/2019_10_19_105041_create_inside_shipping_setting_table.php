<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsideShippingSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inside_shipping_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('serviceCharge');
            $table->integer('shippingCharge')->nullable();
            $table->integer('fragileStickerAmount');
            $table->integer('insuranceAmount');
            $table->integer('fastDeliveryTime');
            $table->integer('insuranceRange');
            $table->integer('fastDeliveryPrice');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inside_shipping_setting');
    }
}
