<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutsideShippingSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outside_shipping_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('serviceCharge');
            $table->integer('shippingCharge');
            $table->integer('fragileStickerAmount');
            $table->integer('insuranceAmount');
            $table->integer('fastDeliveryTime');
            $table->integer('insuranceRange');
            $table->integer('fastDeliveryPrice');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outside_shipping_setting');
    }
}
