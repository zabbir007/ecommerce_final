<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomOrderOutsideTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order_outside', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('orderNumber');
            $table->string('orderDate');
            $table->string('productName');
            $table->string('productDescription');
            $table->string('packageId');
            $table->string('quantity');
            $table->string('productLink');
            $table->string('packageSize');
            $table->string('bill_address1');
            $table->string('bill_address2');
            $table->string('bill_country');
            $table->string('bill_city');
            $table->string('bill_state');
            $table->string('bill_zip');
            $table->string('bill_countryCode');
            $table->string('bill_phone');
            $table->string('initialPayment');
            $table->string('initialPaymentStatus');
            $table->string('totalCost');
            $table->string('serviceCost');
            $table->string('status');
            $table->string('invoiceImage');
            $table->string('userImage1');
            $table->string('userImage2');
            $table->string('userImage3');
            $table->string('userImage4');
            $table->string('userImage5');
            $table->string('adminImage1');
            $table->string('adminImage2');
            $table->string('adminImage3');
            $table->string('finalAmount');
            $table->string('finalAmountStatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order_outside');
    }
}
