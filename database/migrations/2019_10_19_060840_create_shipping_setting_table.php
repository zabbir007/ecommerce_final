<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('howShipping');
            $table->longText('shippingAddress');
            $table->longText('insideShipping');
            $table->longText('outsideShipping');
            $table->integer('status')->default($value=1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_setting');
    }
}
