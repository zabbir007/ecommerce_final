<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEcommerceOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('productName')->nullable();
            $table->string('productQun')->nullable();
            $table->string('productPrice')->nullable();
            $table->string('totalAmount')->nullable();
            $table->string('shippingAmount')->nullable();
            $table->string('paidStatus')->nullable()->default('0');
            $table->string('paidMethod')->nullable();
            $table->string('firstName')->nullable();
            $table->string('lastName')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('userId')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('shippingAddress')->nullable();
            $table->string('billingAddress')->nullable();
            $table->string('orderStatus')->nullable()->default('pending');
            $table->string('orderDate')->nullable();
            $table->string('deliveryDate')->nullable();
            $table->string('orderNumber')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_order');
    }
}
