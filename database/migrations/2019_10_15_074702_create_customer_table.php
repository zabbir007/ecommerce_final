<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstName')->nullable();
            $table->string('lastName')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('zip')->nullable();
            $table->string('state')->nullable();
            $table->string('countryCode')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('gender')->nullable();
            $table->string('shippingAddress')->nullable();
            $table->string('billingAddress')->nullable();
            $table->string('bill_name')->nullable();
            $table->string('bill_countryCode')->nullable();
            $table->string('bill_phone')->nullable();
            $table->string('bill_address1')->nullable();
            $table->string('bill_address2')->nullable();
            $table->string('bill_country')->nullable();
            $table->string('bill_city')->nullable();
            $table->string('bill_state')->nullable();
            $table->string('bill_zip')->nullable();
            $table->string('same_billing')->nullable();
            $table->string('dof')->nullable();
            $table->string('hash')->nullable();
            $table->string('swiftNumber')->nullable();
            $table->string('verify')->default($value=0);
            $table->string('status')->default($value=1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
