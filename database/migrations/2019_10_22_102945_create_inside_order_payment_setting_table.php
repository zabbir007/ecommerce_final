<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsideOrderPaymentSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inside_order_payment_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fastDeliveryPrice')->nullable();
            $table->string('fastDeliveryTime')->nullable();
            $table->string('fragileStickerAmount')->nullable();
            $table->string('insuranceAmount')->nullable();
            $table->string('insuranceRange')->nullable();
            $table->string('cancelAmount')->nullable();
            $table->integer('status')->default($value = 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inside_order_payment_setting');
    }
}
