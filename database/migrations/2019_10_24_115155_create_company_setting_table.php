<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanySettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('companyName');
            $table->string('companyLogo');
            $table->string('companyEmail');
            $table->string('companyPhone');
            $table->string('companyAddress');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_setting');
    }
}
