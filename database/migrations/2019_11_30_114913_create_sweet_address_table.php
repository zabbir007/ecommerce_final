<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSweetAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sweet_address', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sweetAddress');
            $table->string('sweetCountry');
            $table->string('sweetCity');
            $table->string('sweetState');
            $table->string('sweetHouse');
            $table->string('sweetRoad');
            $table->string('sweetPhone');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sweet_address');
    }
}
