<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddInsideCustomOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_inside_custom_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('zip')->nullable();
            $table->string('house')->nullable();
            $table->string('road')->nullable();
            $table->string('product_link')->nullable();
            $table->string('productName')->nullable();
            $table->string('price')->nullable();
            $table->string('quantity')->nullable();
            $table->string('totalAmount')->nullable();
            $table->string('userId')->nullable();
            $table->string('createAt')->nullable();
            $table->string('status')->nullable();
            $table->string('finalAmount')->nullable();
            $table->string('firstPaymentStatus')->nullable();
            $table->string('secondPaymentStatus')->nullable();
            $table->string('orderNumber')->nullable();
            $table->string('discount')->nullable();
            $table->string('boxId')->nullable();
            $table->string('secondFinalAmount')->nullable();
            $table->longText('adminNote')->nullable();
            $table->string('boxPrice')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_inside_custom_order');
    }
}
