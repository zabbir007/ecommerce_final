-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2020 at 01:21 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_inside_custom_order`
--

CREATE TABLE `add_inside_custom_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `road` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createAt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finalAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstPaymentStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondPaymentStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boxId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondFinalAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminNote` longtext COLLATE utf8mb4_unicode_ci,
  `boxPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `add_inside_custom_order`
--

INSERT INTO `add_inside_custom_order` (`id`, `address`, `country`, `state`, `city`, `zip`, `house`, `road`, `product_link`, `productName`, `price`, `quantity`, `totalAmount`, `userId`, `createAt`, `status`, `finalAmount`, `firstPaymentStatus`, `secondPaymentStatus`, `orderNumber`, `discount`, `boxId`, `secondFinalAmount`, `adminNote`, `boxPrice`, `created_at`, `updated_at`) VALUES
(1, 'Shahjadpur, Dhaka, Bangladesh', 'Bangladesh', 'Dhaka Division', 'Dhaka', '1215', 'Güstrow, Germany', 'fsdaf', 'https://www.google.com/search?q=laravel+big+string&oq=laravel+big+s&aqs=chrome.1.69i57j0l5.4671j0j7&sourceid=chrome&ie=UTF-8', 'pant', '1259', '2', '360', '1', '2019/11/28', 'pending', NULL, NULL, NULL, '2019112806462175488', '23', '2, 3', '2420.7568', 'asd', '330', NULL, NULL),
(2, 'Dhanmondi, Dhaka, Bangladesh', 'Bangladesh', 'Dhaka Division', 'Dhaka', '1205', 'dshfjh12', 'dshfjh12', 'https://www.daraz.com.bd/?gclid=Cj0KCQjw6eTtBRDdARIsANZWjYZ6wsy9YPz1ary1rCSrCbZdvo8a2jBxOuFN9c2prM55XdtogJvdS_QaAiVDEALw_wcB#', 'pant', '1260', '8', '360', '1', '2019/11/28', 'processing', NULL, NULL, NULL, '2019112810443590758', '2', '2, 3', '10554.6', 'vai apni jata dicen kjdfhk.', '330', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adminType` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`, `adminType`, `created_at`, `updated_at`) VALUES
(1, 'zabbir@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_send_message`
--

CREATE TABLE `admin_send_message` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `messageTitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sendDate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_send_message`
--

INSERT INTO `admin_send_message` (`id`, `userType`, `messageTitle`, `message`, `sendDate`, `created_at`, `updated_at`) VALUES
(1, '1', 'Test Message', 'hello', '2019-11-03', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_type`, `created_at`, `updated_at`) VALUES
(4, 'Childen', NULL, NULL),
(5, 'Woman', NULL, NULL),
(6, 'Man', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_setting`
--

CREATE TABLE `company_setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `companyName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `companyLogo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `companyEmail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `companyPhone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `companyAddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_setting`
--

INSERT INTO `company_setting` (`id`, `companyName`, `companyLogo`, `companyEmail`, `companyPhone`, `companyAddress`, `status`, `created_at`, `updated_at`) VALUES
(1, 'uShopnShip', 'admin/img/2019120213100756012.png', 'hello@ushopnship.com', '+880181167177 , +880181167178', 'Shantinagar Bazar Road, Dhaka, Bangladesh\r\nShirdi, Maharashtra, India', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `coupon_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expire_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `use_per_coupon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `use_per_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `coupon_number`, `discount_amount`, `expire_date`, `department_id`, `use_per_coupon`, `use_per_user`, `created_at`, `updated_at`) VALUES
(1, '5357694112', '10', '2025-01-01', '1', '200', '10', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `firstName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `countryCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingAddress` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billingAddress` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_countryCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `same_billing` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dof` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verify` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `swiftNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `firstName`, `lastName`, `address1`, `address2`, `country`, `city`, `zip`, `state`, `countryCode`, `phone`, `email`, `password`, `gender`, `shippingAddress`, `billingAddress`, `bill_name`, `bill_countryCode`, `bill_phone`, `bill_address1`, `bill_address2`, `bill_country`, `bill_city`, `bill_state`, `bill_zip`, `same_billing`, `dof`, `hash`, `verify`, `status`, `swiftNumber`, `created_at`, `updated_at`) VALUES
(11, 'zabbir', 'Hossain', 'dhaka', 'flat3', 'Bangladesh', 'Dhakaa', '1206', 'Dhaka Division', '880', '1933722564', 'zabbirhossain729@gmail.com', '25d55ad283aa400af464c76d713c07ad', '0', NULL, NULL, 'zabbir', 'Shantinagar Bazar Road, Dhaka, Bangladesh', '4551265685', 'Shantinagar Bazar Road, Dhaka, Bangladesh', 'Shirdi, Maharashtra, India', 'Afghanistan', 'Dhakaa', 'Dhaka Division', '1205', NULL, '2019-12-09', '2019120912330596137', '1', '1', '447895', '2019-12-08 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_order`
--

CREATE TABLE `custom_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `orderType` int(11) DEFAULT NULL,
  `productType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productLink` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productDescription` longtext COLLATE utf8mb4_unicode_ci,
  `productEstimatePrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productQuantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userImage1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userImage2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userImage3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userImage4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userImage5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_countryCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `initialAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `initialAmountStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminCommitionPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminOfferPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trackingNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marketPlaceLink` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderDate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminProductSize` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminOrderDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminOrderCost` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminInvoiceImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminProductPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminProductQuantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminProductFinalAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminProductFinalAmountStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additionalPicture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boxCondition` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fragileStickerAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setAdditionalPhotoPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finalTotalAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setBoxPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finalInsuranceAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boxCategory` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boxSubCategory` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingCategory` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingSubCategory` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminServicePrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminShippingPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminButePrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `packageSize` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `packagePrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useSticker` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `personalUse` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useUrgent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setUrgentPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminAdditionalAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminSetProductFinalAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage11` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage12` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage13` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminFinalAmountStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `submitDate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `custom_order`
--

INSERT INTO `custom_order` (`id`, `userId`, `orderType`, `productType`, `productName`, `productLink`, `productDescription`, `productEstimatePrice`, `productQuantity`, `userImage1`, `userImage2`, `userImage3`, `userImage4`, `userImage5`, `bill_name`, `bill_address1`, `bill_address2`, `bill_country`, `bill_city`, `bill_state`, `bill_zip`, `bill_countryCode`, `bill_phone`, `initialAmount`, `initialAmountStatus`, `orderNumber`, `adminCommitionPrice`, `adminOfferPrice`, `trackingNumber`, `orderStatus`, `marketPlaceLink`, `orderDate`, `adminProductSize`, `adminOrderDescription`, `adminOrderCost`, `adminImage1`, `adminImage2`, `adminImage3`, `adminInvoiceImage`, `adminProductPrice`, `adminProductQuantity`, `adminProductFinalAmount`, `adminProductFinalAmountStatus`, `additionalPicture`, `boxCondition`, `fragileStickerAmount`, `setAdditionalPhotoPrice`, `finalTotalAmount`, `setBoxPrice`, `finalInsuranceAmount`, `boxCategory`, `boxSubCategory`, `shippingCategory`, `shippingSubCategory`, `adminServicePrice`, `adminShippingPrice`, `shippingType`, `adminButePrice`, `packageSize`, `packagePrice`, `useSticker`, `personalUse`, `useUrgent`, `setUrgentPrice`, `adminAdditionalAmount`, `adminSetProductFinalAmount`, `adminImage4`, `adminImage5`, `adminImage6`, `adminImage7`, `adminImage8`, `adminImage9`, `adminImage10`, `adminImage11`, `adminImage12`, `adminImage13`, `adminFinalAmountStatus`, `submitDate`, `created_at`, `updated_at`) VALUES
(6, 11, 1, 'Garments', 'electronics', 'https://www.daraz.com.bd/', 'aita ami kinbo aita onk vlo dita paren apnara.', '459', '2', 'user/orderImage/2019123011544360883.png', 'user/orderImage/2019123011544328743.jpg', 'user/orderImage/2019123011544371702.jpg', 'user/orderImage/2019123011544376028.png', 'user/orderImage/2019123011544365958.jpg', 'zabbir Hossain', 'dhaka', 'flat3', 'Bangladesh', 'Dhakaa', 'Dhaka Division', '1205', '44', '4551265685', '10', '0', '2019123011544398013', '3', '3', '3', 'complete', NULL, '2019-12-30', NULL, 'ha aita onk hoba.', NULL, 'user/orderImage/2019123018114395665.jpg', 'user/orderImage/2019123018114350150.jpg', 'user/orderImage/2019123018114363235.jpg', 'user/orderImage/2019123018203123736.jpeg', '459', '2', '3', '1', '5', 'withoutOrginal', '3', NULL, '3', NULL, '3', NULL, NULL, NULL, 'Daraz', NULL, '3', 'Local Shipping', NULL, 'Basic', '130', '1', '1', '1', '178', NULL, '3', 'user/orderImage/2019123018203142184.jpg', 'user/orderImage/2019123018203178112.jpg', 'user/orderImage/2019123018203159860.jpg', 'user/orderImage/2019123018203199917.jpg', 'user/orderImage/2019123018203125250.png', NULL, NULL, NULL, NULL, NULL, '0', '2019/12/30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_order_outside`
--

CREATE TABLE `custom_order_outside` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `orderNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderDate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `packageId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productLink` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `packageSize` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_countryCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `initialPayment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `initialPaymentStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalCost` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serviceCost` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoiceImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userImage1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userImage2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userImage3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userImage4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userImage5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finalAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finalAmountStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingPayment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingPaymentPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingPaymentPriceStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `custom_order_outside`
--

INSERT INTO `custom_order_outside` (`id`, `orderNumber`, `userId`, `orderDate`, `productName`, `productDescription`, `packageId`, `quantity`, `productLink`, `packageSize`, `bill_name`, `bill_address1`, `bill_address2`, `bill_country`, `bill_city`, `bill_state`, `bill_zip`, `bill_countryCode`, `bill_phone`, `initialPayment`, `initialPaymentStatus`, `totalCost`, `serviceCost`, `status`, `invoiceImage`, `userImage1`, `userImage2`, `userImage3`, `userImage4`, `userImage5`, `adminImage1`, `adminImage2`, `adminImage3`, `finalAmount`, `finalAmountStatus`, `shippingPayment`, `shippingPaymentPrice`, `shippingPaymentPriceStatus`, `created_at`, `updated_at`) VALUES
(2, '2019122619063934858', '11', '2019-12-26', 'pant', 'aita vlo onk', '#1421532', '4', 'https://mail.google.com/mail/', 'small', 'zabbir', 'Shantinagar Bazar Road, Dhaka, Bangladesh', 'Shirdi, Maharashtra, India', 'Afghanistan', 'Dhakaa', 'Dhaka Division', '1205', '44', '4551265685', '5', '0', '580', '120', 'complete', 'user/orderImage/2019122702412961423.jpg', 'user/orderImage/2019122619063956206.jpg', 'user/orderImage/2019122619063957465.jpg', 'user/orderImage/2019122619063970232.jpg', 'user/orderImage/2019122619063961885.jpg', 'user/orderImage/2019122619063981717.jpg', 'user/orderImage/2019122702412994730.jpg', 'user/orderImage/2019122702412933155.jpg', 'user/orderImage/2019122702412944901.jpg', '580', '1', '1', '13', '1', NULL, NULL),
(3, '2019123012285297204', '11', '2019-12-30', 'Pant kinbo', 'aita vlo dakha kinben vai.', '#1421532', '4', 'https://www.daraz.com.bd/', 'small', 'zabbir', 'Shantinagar Bazar Road, Dhaka, Bangladesh', 'Shirdi, Maharashtra, India', 'Afghanistan', 'Dhakaa', 'Dhaka Division', '1205', '44', '4551265685', '5', '0', '580', '120', 'complete', NULL, 'user/orderImage/2019123012285298572.jpg', 'user/orderImage/2019123012285227612.jpg', 'user/orderImage/2019123012285232756.jpg', 'user/orderImage/2019123012285215174.jpg', 'user/orderImage/2019123012285292492.jpg', NULL, NULL, NULL, '580', '1', '1', '13', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_prefix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `department_type`, `currency_type`, `currency_prefix`, `created_at`, `updated_at`) VALUES
(1, 'Bangladesh', 'taka', '৳', NULL, NULL),
(2, 'Saudi', 'dollar', '$', NULL, NULL),
(3, 'Garments', 'dollar', '$', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_order`
--

CREATE TABLE `ecommerce_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `productName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productQun` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paidStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `paidMethod` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingAddress` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billingAddress` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `orderDate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deliveryDate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_order`
--

INSERT INTO `ecommerce_order` (`id`, `productName`, `productQun`, `productPrice`, `totalAmount`, `shippingAmount`, `paidStatus`, `paidMethod`, `firstName`, `lastName`, `phone`, `email`, `address`, `country`, `city`, `userId`, `state`, `zip`, `shippingAddress`, `billingAddress`, `orderStatus`, `orderDate`, `deliveryDate`, `orderNumber`, `created_at`, `updated_at`) VALUES
(1, 'মেনজ টি-শার্ট TS-288, স্টিচড জর্জেট সালোয়ার কামিজ, স্টিচড জর্জেট সালোয়ার কামিজ', '1, 1, 1', '500, 500, 500', '1545', '45', '0', NULL, 'zabbir', NULL, '01566322485', 'zabbirhossain729@gmail.com', 'Shantinagar Bazar Road, Dhaka, Bangladesh', 'Bangladesh', 'Dhaka', '3', 'Dhaka Division', '1205', 'Shah Alam, Selangor, Malaysia', 'Shah Alam, Selangor, Malaysia', 'complete', '2019/12/03', '2019-12-06', '2019120312265745619', NULL, NULL),
(2, 'Sony 43″ KD 4K Smart Android TV -X7500E, স্টিচড জর্জেট সালোয়ার কামিজ, স্টিচড জর্জেট সালোয়ার কামিজ', '1, 1, 1', '1018.71, 400, 500', '1963.71', '45', '1', NULL, 'zabbir', NULL, '01566322485', 'zabbirhossain729@gmail.com', 'Shantinagar Bazar Road, Dhaka, Bangladesh', 'Bangladesh', 'Dhaka', '3', 'Dhaka Division', '1205', 'Shamshabad, Hyderabad, Telangana, India', 'Shamshabad, Hyderabad, Telangana, India', 'complete', '2019/12/05', '2019-12-08', '2019120506273525464', NULL, NULL),
(3, 'Samsung – 32″ J4303 HD Flat Smart Internet TV', '1', '12000', '12045', '45', '0', NULL, 'zabbir', 'Hossain', '33722564', 'zabbirhossain729@gmail.com', 'Shantinagar Bazar Road, Dhaka, Bangladesh', 'Bangladesh', 'Dhakaa', '1', 'Dhaka Division', '1205', 'Shirdi, Maharashtra, India', 'Shantinagar Bazar Road, Dhaka, Bangladesh\r\nShirdi, Maharashtra, India', 'pending', '2019/12/07', NULL, '2019120706044791581', NULL, NULL),
(4, 'Sony 43″ KD 4K Smart Android TV -X7500E', '2', '123', '286', '40', '0', NULL, 'zabbir', 'Hossain', '1933722564', 'zabbirhossain729@gmail.com', 'Shantinagar Bazar Road, Dhaka, Bangladesh', 'Bangladesh', 'Dhakaa', '11', 'Dhaka Division', '1205', 'Shantinagar Bazar Road, Dhaka, Bangladesh,Afghanistan,Dhakaa,Dhaka Division,1205', 'Shantinagar Bazar Road, Dhaka, Bangladesh,Bangladesh,Dhakaa,Dhaka Division,1205', 'pending', '2019/12/15', NULL, '2019121510530482925', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_shipping_cost`
--

CREATE TABLE `ecommerce_shipping_cost` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'free',
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_shipping_cost`
--

INSERT INTO `ecommerce_shipping_cost` (`id`, `type`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'paid', '40', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `forget_password`
--

CREATE TABLE `forget_password` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verifyCode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `forget_password`
--

INSERT INTO `forget_password` (`id`, `email`, `verifyCode`, `created_at`, `updated_at`) VALUES
(13, 'zabbirhossain729@gmail.com', '16376', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inside_box_price`
--

CREATE TABLE `inside_box_price` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `boxSize` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `boxPrice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inside_box_price`
--

INSERT INTO `inside_box_price` (`id`, `boxSize`, `boxPrice`, `created_at`, `updated_at`) VALUES
(1, 'Simple', '120', NULL, NULL),
(3, 'Basic', '130', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inside_order_box_price`
--

CREATE TABLE `inside_order_box_price` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `boxSize` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `boxPrice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inside_order_box_price`
--

INSERT INTO `inside_order_box_price` (`id`, `boxSize`, `boxPrice`, `created_at`, `updated_at`) VALUES
(1, '12*12', '120', NULL, NULL),
(2, '14*14', '130', NULL, NULL),
(3, '18*18', '200', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inside_order_payment_setting`
--

CREATE TABLE `inside_order_payment_setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fastDeliveryPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fastDeliveryTime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fragileStickerAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insuranceAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insuranceRange` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inside_order_payment_setting`
--

INSERT INTO `inside_order_payment_setting` (`id`, `fastDeliveryPrice`, `fastDeliveryTime`, `fragileStickerAmount`, `insuranceAmount`, `insuranceRange`, `cancelAmount`, `status`, `created_at`, `updated_at`) VALUES
(1, '12', '1', '3', '3', '100', '10', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inside_shipping_setting`
--

CREATE TABLE `inside_shipping_setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `serviceCharge` int(11) NOT NULL,
  `shippingCharge` int(11) DEFAULT NULL,
  `fragileStickerAmount` int(11) NOT NULL,
  `insuranceAmount` int(11) NOT NULL,
  `fastDeliveryTime` int(11) NOT NULL,
  `insuranceRange` int(11) NOT NULL,
  `fastDeliveryPrice` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inside_shipping_setting`
--

INSERT INTO `inside_shipping_setting` (`id`, `serviceCharge`, `shippingCharge`, `fragileStickerAmount`, `insuranceAmount`, `fastDeliveryTime`, `insuranceRange`, `fastDeliveryPrice`, `status`, `created_at`, `updated_at`) VALUES
(2, 120, NULL, 3, 3, 1, 100, 178, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inside_user_reshipping_order`
--

CREATE TABLE `inside_user_reshipping_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `totalAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boxPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boxId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deliveryPriceTotal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deliveryType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serviceCharge` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingCharge` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `itemName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `itemUrl` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insideShippingOrderNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createTime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deliveryDate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anyOther` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instruction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inside_user_reshipping_order`
--

INSERT INTO `inside_user_reshipping_order` (`id`, `totalAmount`, `boxPrice`, `boxId`, `deliveryPriceTotal`, `deliveryType`, `serviceCharge`, `shippingCharge`, `orderNumber`, `itemName`, `itemUrl`, `size`, `quantity`, `insideShippingOrderNumber`, `userId`, `createTime`, `deliveryDate`, `status`, `color`, `price`, `anyOther`, `instruction`, `country`, `address1`, `city`, `company`, `street`, `zipcode`, `phone`, `email`, `created_at`, `updated_at`) VALUES
(1, '615', '250', '1, 2', '145', '1', '120', '100', '124589', 'sad', 'as', '12*12', '1', '15614860', '2', '2019/11/19', '2019/11/24', 'pending', 'asd', '1250', 'fdsad', 'sdas', 'Bangladesh', 'Dhanmondi Lake Road, Dhaka, Bangladesh', 'Dhaka', 'tomattos', 'Dhaka Division', '1205', '01933722564', 'zabbirhossain727@gmail.com', NULL, NULL),
(2, '485', '120', '1', '145', '1', '120', '100', '1254654', 'gfg', 'fg', '12*12', '2', '84083336', '4', '2019/11/21', '2019/11/26', 'pending', 'sdf', 'sdf', 'dfs', 'sdf', 'India', 'Deodand, Uttar Pradesh, India', 'Deodand', 'Pipilika', 'UP', '272175', '01933722564', 'zabbirhossain727@gmail.com', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ltm_translations`
--

CREATE TABLE `ltm_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `key` text COLLATE utf8mb4_bin NOT NULL,
  `value` text COLLATE utf8mb4_bin,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_10_03_093221_department', 1),
(5, '2019_10_03_093252_category', 1),
(6, '2019_10_03_093513_coupon', 1),
(7, '2019_10_03_093555_customer', 1),
(8, '2019_10_06_093641_create_admin_table', 1),
(9, '2019_10_10_091651_create_sub_category_table', 1),
(10, '2019_10_12_115429_create_product_table', 1),
(11, '2019_10_15_074702_create_customer_table', 1),
(12, '2019_10_19_060840_create_shipping_setting_table', 1),
(13, '2019_10_19_105041_create_inside_shipping_setting_table', 1),
(14, '2019_10_19_105124_create_outside_shipping_setting_table', 1),
(15, '2019_10_20_070523_create_inside_box_price_table', 1),
(16, '2019_10_20_120756_create_outside_box_price_table', 1),
(17, '2019_10_22_063903_create_order_basic_setting_table', 1),
(18, '2019_10_22_102945_create_inside_order_payment_setting_table', 1),
(19, '2019_10_23_091415_create_inside_order_box_price_table', 1),
(20, '2019_10_23_091524_create_outside_order_box_price_table', 1),
(21, '2019_10_24_070038_create_outside_order_payment_setting', 1),
(22, '2019_10_24_115155_create_company_setting_table', 1),
(23, '2019_10_28_112105_create_user_message_table', 1),
(24, '2014_04_02_193005_create_translations_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `order_additional_photo_price`
--

CREATE TABLE `order_additional_photo_price` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `photoQuantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photoPrice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_additional_photo_price`
--

INSERT INTO `order_additional_photo_price` (`id`, `photoQuantity`, `photoPrice`, `created_at`, `updated_at`) VALUES
(2, '5', '5', NULL, NULL),
(3, '10', '10', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_basic_setting`
--

CREATE TABLE `order_basic_setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `howOrder` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `importProduct` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exportProduct` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_basic_setting`
--

INSERT INTO `order_basic_setting` (`id`, `howOrder`, `importProduct`, `exportProduct`, `status`, `created_at`, `updated_at`) VALUES
(1, '<h2 style=\"text-align: center; margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\"><span style=\"background-color: rgb(255, 255, 0);\">First payment Information</span></h2><ol><li style=\"text-align: left; margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\"><span style=\"color: rgb(108, 117, 125); font-family: Roboto, sans-serif; font-size: 14px;\"><b>The customer has to pay 5 dollars with shipping cost and delivery cost if the product price below 50 dollars</b></span></li><li style=\"text-align: left; margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px;\"><span style=\"color: rgb(108, 117, 125); font-family: Roboto, sans-serif; font-size: 14px;\"><span style=\"font-weight: bolder;\">The customer has to pay 9% dollars from the total amount with shipping cost and delivery cost if the product price above 50 dollars.</span></span></li></ol>', '<h2 style=\"margin-top: 0px; font-family: DauphinPlain; line-height: 24px; color: rgb(0, 0, 0); font-size: 24px; text-align: center; padding: 0px;\"><span style=\"color: rgb(108, 117, 125); font-family: Roboto, sans-serif; font-size: 14px;\"><span style=\"font-weight: bolder;\">Second Payment Information</span></span></h2><h2 style=\"margin-top: 0px; font-family: DauphinPlain; line-height: 24px; color: rgb(0, 0, 0); font-size: 24px; padding: 0px;\"><ol style=\"font-family: Roboto, sans-serif; font-size: 14px;\"><li style=\"text-align: left; margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px;\"><span style=\"color: rgb(108, 117, 125); font-family: Roboto, sans-serif; font-size: 14px;\"><span style=\"font-weight: bolder;\">The customer has to pay 15 dollars with shipping cost and delivery cost if the product price above 100 dollars.</span></span></li></ol></h2>', '<h2 style=\"margin-top: 0px; font-family: DauphinPlain; line-height: 24px; color: rgb(0, 0, 0); font-size: 24px; text-align: center; padding: 0px;\"><span style=\"color: rgb(108, 117, 125); font-family: Roboto, sans-serif; font-size: 14px;\"><span style=\"font-weight: bolder;\">Third Payment Information</span></span></h2><h2 style=\"margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\"><ol style=\"font-family: Roboto, sans-serif; font-size: 14px;\"><li style=\"text-align: left; margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px;\"><span style=\"color: rgb(108, 117, 125); font-family: Roboto, sans-serif; font-size: 14px;\"><span style=\"font-weight: bolder;\">The customer has to pay 10 dollars with shipping cost and delivery cost if the product price below 100 dollars</span></span></li></ol></h2>', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_box_category`
--

CREATE TABLE `order_box_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `categoryName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_box_category`
--

INSERT INTO `order_box_category` (`id`, `categoryName`, `created_at`, `updated_at`) VALUES
(1, 'small', NULL, NULL),
(2, 'Medium', NULL, NULL),
(3, 'Large', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_box_sub_category`
--

CREATE TABLE `order_box_sub_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `categoryId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subCategoryName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_box_sub_category`
--

INSERT INTO `order_box_sub_category` (`id`, `categoryId`, `subCategoryName`, `price`, `created_at`, `updated_at`) VALUES
(1, '1', 'simple packing', '20', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_product_type`
--

CREATE TABLE `order_product_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_product_type`
--

INSERT INTO `order_product_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Garments', NULL, NULL),
(3, 'Electronics', NULL, NULL),
(4, 'Food', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_shipping_category`
--

CREATE TABLE `order_shipping_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `categoryName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_shipping_category`
--

INSERT INTO `order_shipping_category` (`id`, `categoryName`, `created_at`, `updated_at`) VALUES
(2, 'International', NULL, NULL),
(3, 'Local', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_shipping_sub_category`
--

CREATE TABLE `order_shipping_sub_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `categoryId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subCategoryName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_shipping_sub_category`
--

INSERT INTO `order_shipping_sub_category` (`id`, `categoryId`, `subCategoryName`, `created_at`, `updated_at`) VALUES
(1, '2', 'Alibaba', NULL, NULL),
(2, '3', 'Daraz', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `outside_box_price`
--

CREATE TABLE `outside_box_price` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deliveryType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boxSize` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `boxPrice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `outside_box_price`
--

INSERT INTO `outside_box_price` (`id`, `deliveryType`, `boxSize`, `boxPrice`, `created_at`, `updated_at`) VALUES
(3, NULL, 'Simple', '120', NULL, NULL),
(4, NULL, 'Basic', '130', NULL, NULL),
(5, NULL, 'strong', '180', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `outside_order_box_price`
--

CREATE TABLE `outside_order_box_price` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `boxSize` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `boxPrice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `outside_order_payment_setting`
--

CREATE TABLE `outside_order_payment_setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `serviceCharge` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingCharge` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `buteCost` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fragileStickerAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insuranceAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insuranceRange` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `outside_order_payment_setting`
--

INSERT INTO `outside_order_payment_setting` (`id`, `serviceCharge`, `shippingCharge`, `buteCost`, `fragileStickerAmount`, `insuranceAmount`, `insuranceRange`, `cancelAmount`, `status`, `created_at`, `updated_at`) VALUES
(1, '130', '104', '129', '3', '5', '100', '5', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `outside_shipping_setting`
--

CREATE TABLE `outside_shipping_setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `serviceCharge` int(11) NOT NULL,
  `shippingCharge` int(11) NOT NULL,
  `fragileStickerAmount` int(11) NOT NULL,
  `insuranceAmount` int(11) NOT NULL,
  `fastDeliveryTime` int(11) NOT NULL,
  `insuranceRange` int(11) NOT NULL,
  `fastDeliveryPrice` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `outside_shipping_setting`
--

INSERT INTO `outside_shipping_setting` (`id`, `serviceCharge`, `shippingCharge`, `fragileStickerAmount`, `insuranceAmount`, `fastDeliveryTime`, `insuranceRange`, `fastDeliveryPrice`, `status`, `created_at`, `updated_at`) VALUES
(1, 130, 104, 3, 4, 1, 100, 200, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `predefined_marketplace`
--

CREATE TABLE `predefined_marketplace` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `marketPlaceName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marketPlaceUrl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `predefined_marketplace`
--

INSERT INTO `predefined_marketplace` (`id`, `marketPlaceName`, `marketPlaceUrl`, `created_at`, `updated_at`) VALUES
(2, 'amazon', 'https://www.amazon.com/', NULL, NULL),
(3, 'aliexpress', 'https://www.aliexpress.com/', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `productName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productDescription` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `productSummary` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `departmentId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `subcategoryId` int(11) NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discountPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaKey` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `productName`, `productDescription`, `productSummary`, `departmentId`, `categoryId`, `subcategoryId`, `price`, `discount`, `discountPrice`, `status`, `comment`, `image1`, `image2`, `image3`, `metaTitle`, `metaKey`, `metaDes`, `created_at`, `updated_at`) VALUES
(4, 'KONKA 3KDF50X REFRIGERATOR 18.5 CFT', '<p style=\"margin-right: 0px; margin-bottom: 14.5px; margin-left: 0px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\"><strong style=\"font-weight: bold;\">KONKA 3KDF50X REFRIGERATOR</strong>&nbsp; comes with excellent viewing experience and all the latest features. If you think for a better quality Refrigerator at a reasonable price, this one would be the best choice for you.</p><h5 style=\"font-family: Poppins, sans-serif; line-height: 1.4; color: rgb(25, 25, 25); margin-top: 14.5px; margin-bottom: 14.5px; font-size: 16px;\">Key Features</h5><ul style=\"margin-bottom: 14.5px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\"><li>18.5 CFT</li><li>CFC free- the best environmental solution</li><li>Chest Freezer</li><li>A+ Energy Saver</li><li>Deep cooling &amp; quick freezing</li><li>Convenient to store food</li><li>Elegant &amp; exquisite</li><li>Freon free</li><li>Frozen Temperature- 18°C</li><li>Stainless steel interior realizes</li><li>Fast &amp; efficient cooling</li></ul>', '<h3 class=\"title\" style=\"font-family: Poppins, sans-serif; line-height: 1.4; color: rgb(25, 25, 25); margin-top: 0px; font-size: 30px;\">Product Description</h3><p style=\"margin-right: 0px; margin-bottom: 14.5px; margin-left: 0px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">It is always difficult for the general people or customer to get proper information about the product. To help the ordinary customer of the appliance we (familyneeds.net) are provides reviews. So that, our visitors can gets an idea about their desire appliances. Today we will describe about KONKA branded freezer. KONKA has launched new model of freezer. This model is 3KDF50X. KONKA has used latest technology in their new model. Details about this model are given below.&nbsp;Design and outlook&nbsp;It is an ordinary shape chest model freezer. It has unique off white color along with white border line around the freezer that provides extra ordinary look to the freezer. In front of the freezer has a regulator for controlling the temperature of coolness. For easy storing has a basket inside the freezer.</p>', 1, 4, 3, '12000', NULL, NULL, 'online', 'sdafsa', 'admin/product/2019110714461023129.jpg', 'admin/product/2019110714461021528.jpg', 'admin/product/2019110714461091409.jpg', 'KONKA 3KDF50X REFRIGERATOR 18.5 CFT', 'KONKA', 'KONKA', NULL, NULL),
(5, 'Samsung – 32″ J4303 HD Flat Smart Internet TV', '<p style=\"margin-right: 0px; margin-bottom: 14.5px; margin-left: 0px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">Samsung&nbsp;<strong style=\"font-weight: bold;\">32″ Series 4 HD LED TV 32J4303</strong>&nbsp;comes with excellent viewing experience for your home and office!</p><h4 style=\"font-family: Poppins, sans-serif; line-height: 1.4; color: rgb(25, 25, 25); margin-top: 14.5px; margin-bottom: 14.5px; font-size: 20px;\"><strong style=\"font-weight: bold;\">samsung 32” smart tv</strong></h4><h4 style=\"font-family: Poppins, sans-serif; line-height: 1.4; color: rgb(25, 25, 25); margin-top: 14.5px; margin-bottom: 14.5px; font-size: 20px;\"><strong style=\"font-weight: bold;\">KEY FETURES :</strong></h4><ul style=\"margin-bottom: 14.5px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\"><li>Wide Colour Enhancer (Plus)</li><li>Sharp LED Panel</li><li>10W Powerful Sound Output</li><li>Smart HUB</li><li>Wi-fi Built In</li><li>4 Ticks Energy Efficiency to help save utility cost</li><li>Connect Share™ (USB 2.0) : Yes</li><li>HDMI x2, USB x1</li><li>Embedded POP : Yes</li></ul>', 'Samsung 32″ Series 4 HD Flat Smart LED TV 32J4303 at best price in Bangladesh with express shipping at your doorstep. Free WHITE GLOVE installation is available in Dhaka. Panel & Parts Warranty available, please refer to Warranty Policy.', 1, 4, 2, '12000', NULL, NULL, 'online', 'sad', 'admin/product/2019120119373484993.jpg', 'admin/product/2019120119373412751.jpg', 'admin/product/2019120119373447906.jpg', 'sad', 'asd', 'sad', NULL, NULL),
(6, 'Samsung – 32″ J4303 HD Flat Smart Internet TV', '<p style=\"margin-right: 0px; margin-bottom: 14.5px; margin-left: 0px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">Samsung&nbsp;<strong style=\"font-weight: bold;\">32″ Series 4 HD LED TV 32J4303</strong>&nbsp;comes with excellent viewing experience for your home and office!</p><h4 style=\"font-family: Poppins, sans-serif; line-height: 1.4; color: rgb(25, 25, 25); margin-top: 14.5px; margin-bottom: 14.5px; font-size: 20px;\"><strong style=\"font-weight: bold;\">samsung 32” smart tv</strong></h4><h4 style=\"font-family: Poppins, sans-serif; line-height: 1.4; color: rgb(25, 25, 25); margin-top: 14.5px; margin-bottom: 14.5px; font-size: 20px;\"><strong style=\"font-weight: bold;\">KEY FETURES :</strong></h4><ul style=\"margin-bottom: 14.5px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\"><li>Wide Colour Enhancer (Plus)</li><li>Sharp LED Panel</li><li>10W Powerful Sound Output</li><li>Smart HUB</li><li>Wi-fi Built In</li><li>4 Ticks Energy Efficiency to help save utility cost</li><li>Connect Share™ (USB 2.0) : Yes</li><li>HDMI x2, USB x1</li><li>Embedded POP : Yes</li></ul>', 'Samsung 32″ Series 4 HD Flat Smart LED TV 32J4303 at best price in Bangladesh with express shipping at your doorstep. Free WHITE GLOVE installation is available in Dhaka. Panel & Parts Warranty available, please refer to Warranty Policy.', 2, 4, 5, '12000', NULL, NULL, 'online', 'asdasd', 'admin/product/2019120119383985857.jpg', 'admin/product/2019120119383929042.jpg', 'admin/product/2019120119383955644.jpg', 'asd', 'asd', 'asd', NULL, NULL),
(7, 'Sony 43″ KD 4K Smart Android TV -X7500E', '<p style=\"margin-right: 0px; margin-bottom: 14.5px; margin-left: 0px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">SONY&nbsp;<strong style=\"font-weight: bold;\">43″</strong>&nbsp;<strong style=\"font-weight: bold;\">KD- X7500E 4K Android UHD LED TV- 43X7500E&nbsp;</strong>comes with excellent viewing experience and all latest features.If you think for a better quality TV in a reasonable price,this one would be a best choice for you.</p><p style=\"margin-right: 0px; margin-bottom: 14.5px; margin-left: 0px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\"><strong style=\"font-weight: bold;\">Key Features:</strong></p><ul style=\"margin-bottom: 14.5px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\"><li>MODEL YEAR&nbsp; : 2017</li><li>SCREEN SIZE (INCH, MEASURED DIAGONALLY) : 43″ (42.5″)</li><li>SCREEN SIZE (CM, MEASURED DIAGONALLY) : 80.0 cm</li><li>WEIGHT OF PACKAGE CARTON (GROSS) : Approx. 8.0 kg</li><li>HDMI INPUTS TOTAL : 2 (1Side/1Rear)</li><li>USB PORTS : 1 (Side)</li><li>FM RADIO : Yes</li><li>ON/OFF TIMER : Yes</li><li>SLEEP TIMER : Yes</li><li>OPERATING SYSTEM : Linux</li><li>ELECTRONIC PROGRAM GUIDE (EPG)</li><li>CLOSED CAPTIONS (ANALOG/DIGITAL)</li><li>TELETEXT : Yes</li><li>POWER CONSUMPTION (IN OPERATION) : 39 W</li><li>DYNAMIC BACK LIGHT CONTROL : Yes</li></ul>', '<p><span style=\"color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">SONY&nbsp;</span><strong style=\"font-weight: bold; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">43″ KD- X7500E 4K Android UHD LED TV- 43X7500E&nbsp;</strong><span style=\"color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">at best price in Bangladesh with express shipping at your doorstep. Free WHITE GLOVE installation is available in Dhaka. Panel &amp; Parts Warranty available, please refer to&nbsp;</span><a href=\"https://drm.com.bd/warranty-policy/\" style=\"background: rgb(255, 255, 255); color: rgb(25, 25, 25); transition: color 0.3s ease 0s; font-family: Yantramanav, sans-serif;\">Warranty Policy</a><span style=\"color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">.</span><br></p>', 1, 4, 2, '1323', '23', '1018.71', 'online', 'sdfg', 'admin/product/2019120210294364946.jpg', 'admin/product/2019120210294313558.jpg', 'admin/product/2019120210294341531.jpg', 'sdfg', 'fg', 'fdsg', NULL, NULL),
(8, 'Sony 43″ KD 4K Smart Android TV -X7500E', '<p style=\"margin-right: 0px; margin-bottom: 14.5px; margin-left: 0px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">SONY&nbsp;<strong style=\"font-weight: bold;\">43″</strong>&nbsp;<strong style=\"font-weight: bold;\">KD- X7500E 4K Android UHD LED TV- 43X7500E&nbsp;</strong>comes with excellent viewing experience and all latest features.If you think for a better quality TV in a reasonable price,this one would be a best choice for you.</p><p style=\"margin-right: 0px; margin-bottom: 14.5px; margin-left: 0px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\"><strong style=\"font-weight: bold;\">Key Features:</strong></p><ul style=\"margin-bottom: 14.5px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\"><li>MODEL YEAR&nbsp; : 2017</li><li>SCREEN SIZE (INCH, MEASURED DIAGONALLY) : 43″ (42.5″)</li><li>SCREEN SIZE (CM, MEASURED DIAGONALLY) : 80.0 cm</li><li>WEIGHT OF PACKAGE CARTON (GROSS) : Approx. 8.0 kg</li><li>HDMI INPUTS TOTAL : 2 (1Side/1Rear)</li><li>USB PORTS : 1 (Side)</li><li>FM RADIO : Yes</li><li>ON/OFF TIMER : Yes</li><li>SLEEP TIMER : Yes</li><li>OPERATING SYSTEM : Linux</li><li>ELECTRONIC PROGRAM GUIDE (EPG)</li><li>CLOSED CAPTIONS (ANALOG/DIGITAL)</li><li>TELETEXT : Yes</li><li>POWER CONSUMPTION (IN OPERATION) : 39 W</li><li>DYNAMIC BACK LIGHT CONTROL : Yes</li></ul>', '<p><span style=\"color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">SONY&nbsp;</span><strong style=\"font-weight: bold; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">43″ KD- X7500E 4K Android UHD LED TV- 43X7500E&nbsp;</strong><span style=\"color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">at best price in Bangladesh with express shipping at your doorstep. Free WHITE GLOVE installation is available in Dhaka. Panel &amp; Parts Warranty available, please refer to&nbsp;</span><a href=\"https://drm.com.bd/warranty-policy/\" style=\"background: rgb(255, 255, 255); color: rgb(25, 25, 25); transition: color 0.3s ease 0s; font-family: Yantramanav, sans-serif;\">Warranty Policy</a><span style=\"color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">.</span><br></p>', 1, 4, 2, '123', NULL, NULL, 'online', 'stfg', 'admin/product/2019120210311882329.jpg', 'admin/product/2019120210311894949.png', 'admin/product/2019120210311811967.jpg', 'dsafggfg', 'sadfasdgf', 'sdgfvadgh', NULL, NULL),
(9, 'Sony 43″ KD 4K Smart Android TV -X7500E', '<p style=\"margin-right: 0px; margin-bottom: 14.5px; margin-left: 0px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\"><strong style=\"font-weight: bold;\">Key Features:</strong></p><ul style=\"margin-bottom: 14.5px; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\"><li>MODEL YEAR&nbsp; : 2017</li><li>SCREEN SIZE (INCH, MEASURED DIAGONALLY) : 43″ (42.5″)</li><li>SCREEN SIZE (CM, MEASURED DIAGONALLY) : 80.0 cm</li><li>WEIGHT OF PACKAGE CARTON (GROSS) : Approx. 8.0 kg</li><li>HDMI INPUTS TOTAL : 2 (1Side/1Rear)</li><li>USB PORTS : 1 (Side)</li><li>FM RADIO : Yes</li><li>ON/OFF TIMER : Yes</li><li>SLEEP TIMER : Yes</li><li>OPERATING SYSTEM : Linux</li><li>ELECTRONIC PROGRAM GUIDE (EPG)</li><li>CLOSED CAPTIONS (ANALOG/DIGITAL)</li><li>TELETEXT : Yes</li><li>POWER CONSUMPTION (IN OPERATION) : 39 W</li><li>DYNAMIC BACK LIGHT CONTROL : Yes</li></ul>', '<p><span style=\"color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">SONY&nbsp;</span><strong style=\"font-weight: bold; color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">43″ KD- X7500E 4K Android UHD LED TV- 43X7500E&nbsp;</strong><span style=\"color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">at best price in Bangladesh with express shipping at your doorstep. Free WHITE GLOVE installation is available in Dhaka. Panel &amp; Parts Warranty available, please refer to&nbsp;</span><a href=\"https://drm.com.bd/warranty-policy/\" style=\"background: rgb(255, 255, 255); color: rgb(25, 25, 25); transition: color 0.3s ease 0s; font-family: Yantramanav, sans-serif;\">Warranty Policy</a><span style=\"color: rgb(76, 76, 76); font-family: Yantramanav, sans-serif;\">.</span><br></p>', 1, 4, 2, '123', '12', '108.24', 'online', 'sadfadg', 'admin/product/2019120210370240227.jpg', 'admin/product/2019120210370235089.jpg', 'admin/product/2019120210370296740.jpg', 'sadgfg', 'sdagfdfgvb', 'sadgdfg', NULL, NULL),
(10, 'মেনজ টি-শার্ট TS-288', '<div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\">Main Material: Cotton;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Style: Short Sleeve;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">170 GSM;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Fashionable;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Stylish and Trendy;</span></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 1, 4, 2, '123234', NULL, NULL, 'online', 'qwfwef', 'admin/product/2019120210390020278.jpg', 'admin/product/2019120210390022239.jpg', 'admin/product/2019120210390023302.jpg', 'weqt', 'wetfrqewrt', 'wqerfqwe', NULL, NULL),
(11, 'মেনজ টি-শার্ট TS-288', '<div class=\"product-detail-header\" style=\"width: 855.359px; float: left; background-color: rgb(233, 234, 235); padding: 8px; margin-bottom: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"header-text-new\" style=\"float: left; width: 419.672px; font-size: 18px; color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">প্রোডাক্টের বিবরণ</div><div class=\"header-code-text\" style=\"float: left; width: 419.672px; text-align: right; font-family: SolaimanLipi, Helvetica, Verdana; color: rgb(93, 90, 90);\">ডিল কোড:&nbsp;<span id=\"DealCodeLabel\">৯৯৮৮৩২</span></div></div><div class=\"product-details-text-wrapper\" style=\"width: 855.359px; float: left; margin-bottom: 20px; margin-top: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Main Material: Cotton;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Style: Short Sleeve;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">170 GSM;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Fashionable;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Stylish and Trendy;</span></div></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 1, 4, 2, '৩৫০', NULL, NULL, 'online', NULL, 'admin/product/2019120210401732704.jpg', 'admin/product/2019120210401714467.jpg', 'admin/product/2019120210401775819.jpg', 'ASF', 'SADF', 'SDAFDF', NULL, NULL),
(12, 'মেনজ টি-শার্ট TS-288', '<div class=\"product-details-container\" style=\"width: 855.359px; float: left;\"><div class=\"product-details-text-wrapper\" style=\"width: 855.359px; float: left; margin-bottom: 20px; margin-top: 10px;\"><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\">Main Material: Cotton;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Style: Short Sleeve;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">170 GSM;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Fashionable;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Stylish and Trendy;</span></div></div></div><div class=\"cash-back-Condition-wrapper\" style=\"width: 855.359px; float: left;\"><div class=\"cash-back-header\" style=\"width: 855.359px; float: left; background-color: rgb(233, 234, 235); padding: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"cash-back-header-text-new\"></div></div></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 2, 4, 5, '500', NULL, NULL, 'online', 'SADF', 'admin/product/2019120210413376549.jpg', 'admin/product/2019120210413347574.jpg', 'admin/product/2019120210413360415.jpg', 'SDF', 'DSF', 'SADF', NULL, NULL),
(13, 'মেনজ টি-শার্ট TS-288', '<div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\">Main Material: Cotton;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Style: Short Sleeve;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">170 GSM;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Fashionable;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Stylish and Trendy;</span></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 2, 6, 4, '500', NULL, NULL, 'online', 'SADFDF', 'admin/product/2019120210431376072.jpg', 'admin/product/2019120210431315800.jpg', 'admin/product/2019120210431343431.jpg', 'ERTYRET', 'ERTERT', 'SDGERTG', NULL, NULL),
(14, 'মেনজ টি-শার্ট TS-288', '<div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\">Main Material: Cotton;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Style: Short Sleeve;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">170 GSM;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Fashionable;</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Stylish and Trendy;</span></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">&nbsp;প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 1, 4, 2, '500', NULL, NULL, 'online', NULL, 'admin/product/2019120210451730286.jpg', 'admin/product/2019120210451763563.jpg', 'admin/product/2019120210451711978.jpg', 'safsdf', 'sdfgd', 'asf', NULL, NULL),
(15, 'স্টিচড জর্জেট সালোয়ার কামিজ', '<div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\">Fabric: kameez- Cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">salwar-cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">orna- Georgette</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Colour: Blue</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Size: 36, 38, 40, 42, 44</span></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 1, 4, 3, '500', NULL, NULL, 'online', 'wqerfwef', 'admin/product/2019120210464430259.jpg', 'admin/product/2019120210464440810.jpg', 'admin/product/2019120210464475067.jpg', 'wetr', 'wert', 'swfger', NULL, NULL),
(16, 'স্টিচড জর্জেট সালোয়ার কামিজ', '<div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\">Fabric: kameez- Cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">salwar-cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">orna- Georgette</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Colour: Blue</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Size: 36, 38, 40, 42, 44</span></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 1, 4, 1, '900', NULL, NULL, 'online', NULL, 'admin/product/2019120210473878968.jpg', 'admin/product/2019120210473847885.jpg', 'admin/product/2019120210473858160.jpg', 'aSDqadq', 'qwrfqdf', 'asfdwerf', NULL, NULL),
(17, 'স্টিচড জর্জেট সালোয়ার কামিজ', '<div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\">Fabric: kameez- Cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">salwar-cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">orna- Georgette</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Colour: Blue</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Size: 36, 38, 40, 42, 44</span></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 1, 4, 1, '500', NULL, NULL, 'online', 'sdfgbfgh', 'admin/product/2019120210483819398.jpg', 'admin/product/2019120210483816913.jpg', 'admin/product/2019120210483884216.jpg', 'dfhrtyh', 'fdyhrthy', 'dsftgfdh', NULL, NULL),
(18, 'স্টিচড জর্জেট সালোয়ার কামিজ', '<div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\">Fabric: kameez- Cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">salwar-cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">orna- Georgette</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Colour: Blue</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Size: 36, 38, 40, 42, 44</span></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 1, 4, 1, '600', '5', '570', 'online', 'dfgg', 'admin/product/2019120210494991792.jpg', 'admin/product/2019120210494984436.jpg', 'admin/product/2019120210494971542.jpg', 'dfgdfg', 'dfg', 'dcgbsdfg', NULL, NULL);
INSERT INTO `product` (`id`, `productName`, `productDescription`, `productSummary`, `departmentId`, `categoryId`, `subcategoryId`, `price`, `discount`, `discountPrice`, `status`, `comment`, `image1`, `image2`, `image3`, `metaTitle`, `metaKey`, `metaDes`, `created_at`, `updated_at`) VALUES
(19, 'স্টিচড জর্জেট সালোয়ার কামিজ', '<div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\">Fabric: kameez- Cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">salwar-cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">orna- Georgette</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Colour: Blue</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Size: 36, 38, 40, 42, 44</span></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 1, 4, 2, '400', NULL, NULL, 'online', 'sadfwaef', 'admin/product/2019120210504649393.jpg', 'admin/product/2019120210504681283.jpg', 'admin/product/2019120210504649877.jpg', 'gytyjn bnm', 'wetert', 'sadgvargf', NULL, NULL),
(20, 'স্টিচড জর্জেট সালোয়ার কামিজ', '<div class=\"product-detail-header\" style=\"width: 855.359px; float: left; background-color: rgb(233, 234, 235); padding: 8px; margin-bottom: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"header-text-new\" style=\"float: left; width: 419.672px; font-size: 18px; color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">প্রোডাক্টের বিবরণ</div><div class=\"header-code-text\" style=\"float: left; width: 419.672px; text-align: right; font-family: SolaimanLipi, Helvetica, Verdana; color: rgb(93, 90, 90);\">ডিল কোড:&nbsp;<span id=\"DealCodeLabel\">১০১৬৪১১</span></div></div><div class=\"product-details-text-wrapper\" style=\"width: 855.359px; float: left; margin-bottom: 20px; margin-top: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Fabric: kameez- Cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">salwar-cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">orna- Georgette</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Colour: Blue</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Size: 36, 38, 40, 42, 44</span></div></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 1, 4, 3, '400', NULL, NULL, 'online', 'adsfgfdj', 'admin/product/2019120210514388107.jpg', 'admin/product/2019120210514345914.jpg', 'admin/product/2019120210514367021.jpg', 'fghrfgth', 'hfghfrgh', 'fvgbhgt', NULL, NULL),
(21, 'স্টিচড জর্জেট সালোয়ার কামিজ', '<div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\">Fabric: kameez- Cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">salwar-cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">orna- Georgette</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Colour: Blue</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Size: 36, 38, 40, 42, 44</span></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 1, 4, 1, '500', NULL, NULL, 'online', 'SZDgh', 'admin/product/2019120210523938095.jpg', 'admin/product/2019120210523985806.jpg', 'admin/product/2019120210523936464.jpg', 'fmghjuy', 'gfhfh', 'dsfhfrtgjh', NULL, NULL),
(22, 'স্টিচড জর্জেট সালোয়ার কামিজ', '<div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">Fabric: kameez- Cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">salwar-cotton</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">orna- Georgette</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Colour: Blue</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">&nbsp;Size: 36, 38, 40, 42, 44</span></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 1, 4, 2, '400', NULL, NULL, 'online', 'sadfsdg', 'admin/product/2019120210534124070.jpg', 'admin/product/2019120210534152020.jpg', 'admin/product/2019120210534158460.jpg', 'nbjyhur', 'ghdfh', 'dfgsfd', NULL, NULL),
(23, 'Holica অ্যালোভেরা সুদিং জেল', '<div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\">মাল্টি কেয়ার সুদিং জেল যা স্কিনকে করে স্মুথ ও রিফ্রেশিং</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">পরিমানঃ ২৫০ মিলি</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">অ্যালোভেরার নির্যাস থেকে প্রস্তুত</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">ম্যানুফ্যাকচারারঃ Encos</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">কান্ট্রি অফ অরিজিনঃ রিপাবলিক অফ কোরিয়া</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">নন স্টিকি অ্যালোভেরা জেল যা চুলকে করবে ঘন ও প্রাণবন্ত</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">কোন পার্শ্বপ্রতিক্রিয়া নেই</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">ব্র্যান্ড নিউ প্রোডাক্ট</span></div>', '<p><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।</span><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><br style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\"><span style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span><br></p>', 1, 4, 2, '500', NULL, NULL, 'online', 'sdffg', 'admin/product/2019120210555446133.jpg', 'admin/product/2019120210555493123.jpg', 'admin/product/2019120210555448816.jpg', 'sdsdg', 'gsdfg', 'sdfgasd', NULL, NULL),
(24, 'Holica অ্যালোভেরা সুদিং জেল', '<div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\">মাল্টি কেয়ার সুদিং জেল যা স্কিনকে করে স্মুথ ও রিফ্রেশিং</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">পরিমানঃ ২৫০ মিলি</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">অ্যালোভেরার নির্যাস থেকে প্রস্তুত</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">ম্যানুফ্যাকচারারঃ Encos</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">কান্ট্রি অফ অরিজিনঃ রিপাবলিক অফ কোরিয়া</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">নন স্টিকি অ্যালোভেরা জেল যা চুলকে করবে ঘন ও প্রাণবন্ত</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">কোন পার্শ্বপ্রতিক্রিয়া নেই</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">ব্র্যান্ড নিউ প্রোডাক্ট</span></div>', '<div class=\"product-detail-header\" style=\"width: 855.359px; float: left; background-color: rgb(233, 234, 235); padding: 8px; margin-bottom: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"header-text-new\" style=\"float: left; width: 419.672px; font-size: 18px; color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">দ্রষ্টব্য</div></div><div class=\"product-details-stock-condition-new-data-inner\" style=\"float: left; margin-left: 2px; width: 855.359px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><span class=\"product-details-stock-condition-text\" style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।<br><br>২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।<br><br>৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span></div>', 3, 5, 3, '400', '10', '360', 'online', 'dfgsdfg', 'admin/product/2019120210570131831.jpg', 'admin/product/2019120210570189916.jpg', 'admin/product/2019120210570146418.jpg', 'dfghsdfg', 'gvbdsfg', 'dfgsrtywfgvb', NULL, NULL),
(25, 'Holica অ্যালোভেরা সুদিং জেল', '<div class=\"product-detail-header\" style=\"width: 855.359px; float: left; background-color: rgb(233, 234, 235); padding: 8px; margin-bottom: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"header-text-new\" style=\"float: left; width: 419.672px; font-size: 18px; color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">প্রোডাক্টের বিবরণ</div><div class=\"header-code-text\" style=\"float: left; width: 419.672px; text-align: right; font-family: SolaimanLipi, Helvetica, Verdana; color: rgb(93, 90, 90);\">ডিল কোড: <span id=\"DealCodeLabel\">৪০৩৪০৬</span></div></div><div class=\"product-details-text-wrapper\" style=\"width: 855.359px; float: left; margin-bottom: 20px; margin-top: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">মাল্টি কেয়ার সুদিং জেল যা স্কিনকে করে স্মুথ ও রিফ্রেশিং</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">পরিমানঃ ২৫০ মিলি</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">অ্যালোভেরার নির্যাস থেকে প্রস্তুত</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">ম্যানুফ্যাকচারারঃ Encos</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">কান্ট্রি অফ অরিজিনঃ রিপাবলিক অফ কোরিয়া</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">নন স্টিকি অ্যালোভেরা জেল যা চুলকে করবে ঘন ও প্রাণবন্ত</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">কোন পার্শ্বপ্রতিক্রিয়া নেই</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">ব্র্যান্ড নিউ প্রোডাক্ট</span></div></div>', '<div class=\"product-detail-header\" style=\"width: 855.359px; float: left; background-color: rgb(233, 234, 235); padding: 8px; margin-bottom: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"header-text-new\" style=\"float: left; width: 419.672px; font-size: 18px; color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">দ্রষ্টব্য</div></div><div class=\"product-details-stock-condition-new-data-inner\" style=\"float: left; margin-left: 2px; width: 855.359px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><span class=\"product-details-stock-condition-text\" style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।<br><br>২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।<br><br>৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span></div>', 3, 5, 4, '500', NULL, NULL, 'online', 'asdfgbfg', 'admin/product/2019120210575629494.jpg', 'admin/product/2019120210575617891.jpg', 'admin/product/2019120210575638528.jpg', 'rftgfdg', 'dfgdfg', 'cvhbfgh', NULL, NULL),
(26, 'Holica অ্যালোভেরা সুদিং জেল', '<div class=\"product-detail-header\" style=\"width: 855.359px; float: left; background-color: rgb(233, 234, 235); padding: 8px; margin-bottom: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"header-text-new\" style=\"float: left; width: 419.672px; font-size: 18px; color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">প্রোডাক্টের বিবরণ</div><div class=\"header-code-text\" style=\"float: left; width: 419.672px; text-align: right; font-family: SolaimanLipi, Helvetica, Verdana; color: rgb(93, 90, 90);\">ডিল কোড: <span id=\"DealCodeLabel\">৪০৩৪০৬</span></div></div><div class=\"product-details-text-wrapper\" style=\"width: 855.359px; float: left; margin-bottom: 20px; margin-top: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">মাল্টি কেয়ার সুদিং জেল যা স্কিনকে করে স্মুথ ও রিফ্রেশিং</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">পরিমানঃ ২৫০ মিলি</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">অ্যালোভেরার নির্যাস থেকে প্রস্তুত</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">ম্যানুফ্যাকচারারঃ Encos</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">কান্ট্রি অফ অরিজিনঃ রিপাবলিক অফ কোরিয়া</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">নন স্টিকি অ্যালোভেরা জেল যা চুলকে করবে ঘন ও প্রাণবন্ত</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">কোন পার্শ্বপ্রতিক্রিয়া নেই</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">ব্র্যান্ড নিউ প্রোডাক্ট</span></div></div>', '<div class=\"product-detail-header\" style=\"width: 855.359px; float: left; background-color: rgb(233, 234, 235); padding: 8px; margin-bottom: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"header-text-new\" style=\"float: left; width: 419.672px; font-size: 18px; color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">দ্রষ্টব্য</div></div><div class=\"product-details-stock-condition-new-data-inner\" style=\"float: left; margin-left: 2px; width: 855.359px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><span class=\"product-details-stock-condition-text\" style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।<br><br>২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।<br><br>৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span></div>', 3, 5, 3, '400', NULL, NULL, 'online', 'cxvgbh', 'admin/product/2019120210584911480.jpg', 'admin/product/2019120210584978850.jpg', 'admin/product/2019120210584989380.jpg', 'dfgdfg', 'fgdfg', 'dfg', NULL, NULL),
(27, 'Holica অ্যালোভেরা সুদিং জেল', '<div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">মাল্টি কেয়ার সুদিং জেল যা স্কিনকে করে স্মুথ ও রিফ্রেশিং</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">পরিমানঃ ২৫০ মিলি</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">অ্যালোভেরার নির্যাস থেকে প্রস্তুত</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">ম্যানুফ্যাকচারারঃ Encos</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">কান্ট্রি অফ অরিজিনঃ রিপাবলিক অফ কোরিয়া</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">নন স্টিকি অ্যালোভেরা জেল যা চুলকে করবে ঘন ও প্রাণবন্ত</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">কোন পার্শ্বপ্রতিক্রিয়া নেই</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90); font-family: SolaimanLipi, helvetica, verdana;\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">ব্র্যান্ড নিউ প্রোডাক্ট</span></div>', '<div class=\"product-detail-header\" style=\"width: 855.359px; float: left; background-color: rgb(233, 234, 235); padding: 8px; margin-bottom: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"header-text-new\" style=\"float: left; width: 419.672px; font-size: 18px; color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">দ্রষ্টব্য</div></div><div class=\"product-details-stock-condition-new-data-inner\" style=\"float: left; margin-left: 2px; width: 855.359px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><span class=\"product-details-stock-condition-text\" style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।<br><br>২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।<br><br>৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span></div>', 3, 5, 2, '500', NULL, NULL, 'online', 'dsafvdfg', 'admin/product/2019120210595283145.jpg', 'admin/product/2019120210595260787.jpg', 'admin/product/2019120210595293584.jpg', 'dfgedtgergbv', 'dcgdfg', 'dfgdfgh', NULL, NULL),
(28, 'Holica অ্যালোভেরা সুদিং জেল', '<div class=\"product-detail-header\" style=\"width: 855.359px; float: left; background-color: rgb(233, 234, 235); padding: 8px; margin-bottom: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"header-text-new\" style=\"float: left; width: 419.672px; font-size: 18px; color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">প্রোডাক্টের বিবরণ</div><div class=\"header-code-text\" style=\"float: left; width: 419.672px; text-align: right; font-family: SolaimanLipi, Helvetica, Verdana; color: rgb(93, 90, 90);\">ডিল কোড: <span id=\"DealCodeLabel\">৪০৩৪০৬</span></div></div><div class=\"product-details-text-wrapper\" style=\"width: 855.359px; float: left; margin-bottom: 20px; margin-top: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">মাল্টি কেয়ার সুদিং জেল যা স্কিনকে করে স্মুথ ও রিফ্রেশিং</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">পরিমানঃ ২৫০ মিলি</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">অ্যালোভেরার নির্যাস থেকে প্রস্তুত</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">ম্যানুফ্যাকচারারঃ Encos</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">কান্ট্রি অফ অরিজিনঃ রিপাবলিক অফ কোরিয়া</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">নন স্টিকি অ্যালোভেরা জেল যা চুলকে করবে ঘন ও প্রাণবন্ত</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">কোন পার্শ্বপ্রতিক্রিয়া নেই</span></div><div class=\"product-details-text\" style=\"width: 855.359px; float: left; font-size: 15px; line-height: 27px; color: rgb(93, 90, 90);\"><span style=\"margin-right: 10px;\"><img src=\"https://static.ajkerdeal.com/images/dealdetailsnew/dealdetails_arrow.svg\" style=\"border: 0px; margin-top: -5px !important;\"></span><span style=\"margin-right: 10px;\">ব্র্যান্ড নিউ প্রোডাক্ট</span></div></div>', '<div class=\"product-detail-header\" style=\"width: 855.359px; float: left; background-color: rgb(233, 234, 235); padding: 8px; margin-bottom: 10px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><div class=\"header-text-new\" style=\"float: left; width: 419.672px; font-size: 18px; color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">দ্রষ্টব্য</div></div><div class=\"product-details-stock-condition-new-data-inner\" style=\"float: left; margin-left: 2px; width: 855.359px; color: rgb(51, 51, 51); font-family: SolaimanLipi, helvetica, verdana;\"><span class=\"product-details-stock-condition-text\" style=\"color: rgb(93, 90, 90); font-family: SolaimanLipi, Helvetica, Verdana;\">১। প্রোডাক্টের অর্ডার স্টক থাকা সাপেক্ষে ডেলিভারি করা হবে। অনিবার্য কারনে পন্যের ডেলিভারিতে বিক্রেতা প্রতিশ্রুত ডেলিভারী সময়ের বেশী সময় লাগতে পারে।<br><br>২। অর্ডার কনফার্মেশনের পরেও অনিবার্য কারনবশত যেকোনো সময়ে আজকেরডিল আপনার অর্ডার বাতিল করার ক্ষমতা রাখে। এক্ষেত্রে অগ্রিম মুল্য প্রদান করা হলে রিফান্ডের প্রয়োজনীয় তথ্য (বিকাশ নং/রকেট নং/কার্ড নং ও অন্যান্য) এবং প্রোডাক্ট ডেলিভারির জন্য কুরিয়ার দেয়ার পর আপনি গ্রহণ না করলে উক্ত কুরিয়ার থেকে প্রোডাক্টটি আজকেরডিলে ফেরত আসার পর সর্বোচ্চ ১০ কার্যদিবসের মধ্যে টাকা ফেরত দেয়া হবে।<br><br>৩। যে সকল প্রোডাক্টের গায়ে মূল্য লেখা থাকে এবং কোনো কারণে আজকেরডিলের মূল্য তার থেকে যদি বেশি থাকে, সেক্ষেত্রে অতিরিক্ত মূল্যের ক্ষেত্রে আপনাকে অতিসত্তর ৪৮ ঘন্টার মধ্যে complain@ajkerdeal.com এ মেইল করে কমপ্লেইন রেজিস্টার করতে হবে। আপনার কমপ্লেইনটি ঠিক হলে আপনার প্রদানকৃত অতিরিক্ত মূল্য ১০ কার্যদিবসের মধ্যে বিকাশের মাধ্যমে ফেরত দেয়া হবে।</span></div>', 3, 5, 4, '500', NULL, NULL, 'online', 'sadfgfjjk', 'admin/product/2019120211010612659.jpg', 'admin/product/2019120211010614736.jpg', 'admin/product/2019120211010658101.jpg', 'dftg', 'dsadfcv', 'njkldfg', NULL, NULL);
INSERT INTO `product` (`id`, `productName`, `productDescription`, `productSummary`, `departmentId`, `categoryId`, `subcategoryId`, `price`, `discount`, `discountPrice`, `status`, `comment`, `image1`, `image2`, `image3`, `metaTitle`, `metaKey`, `metaDes`, `created_at`, `updated_at`) VALUES
(29, 'Gray Linen Kurti for Women - (Tailor Made)', '<h2 class=\"pdp-mod-section-title outer-title\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px 24px; font-family: Roboto-Medium; font-size: 16px; line-height: 52px; color: rgb(33, 33, 33); overflow: hidden; text-overflow: ellipsis; white-space: nowrap; height: 52px; background: rgb(250, 250, 250);\">Product details of Gray Linen Kurti for Women - (Tailor Made)</h2><div class=\"pdp-product-detail\" data-spm=\"product_detail\" style=\"margin: 0px; padding: 0px; position: relative; font-family: Roboto, -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", Helvetica, sans-serif; font-size: 12px;\"><div class=\"pdp-product-desc \" style=\"margin: 0px; padding: 5px 14px 5px 24px; height: auto; overflow-y: hidden;\"><div class=\"html-content pdp-product-highlights\" style=\"margin: 0px; padding: 11px 0px 16px; word-break: break-word; border-bottom: 1px solid rgb(239, 240, 245); overflow: hidden;\"><ul class=\"\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; list-style: none; overflow: hidden; columns: auto 2; column-gap: 32px;\"><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Tailor Made Product</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Material: Linen</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Stitched Kurti</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Gender: Women</li><li class=\"\" data-spm-anchor-id=\"a2a0e.pdp.product_detail.i0.202c248aPuXJjU\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Product\'s Color May Vary a Bit Due to Photography</li></ul></div></div></div>', '<p style=\"margin-right: 0px; margin-left: 0px; padding: 0px; font-family: Roboto, -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", Helvetica, sans-serif;\">Bend the Trend is one of the popular platform of quality products at reasonable price. They provide all types of electronics items, men\'s and women\'s dresses, shoes, sports items and other accessories frequently. Shop your choice from this seller.</p><div><br></div>', 3, 4, 2, '500', NULL, NULL, 'online', 'sadfasdfg', 'admin/product/2019120211073979328.jpg', 'admin/product/2019120211073957356.jpg', 'admin/product/2019120211073941793.jpg', 'xfgdeg', 'dafghrfg', 'xcgvdfsg', NULL, NULL),
(30, 'Gray Linen Kurti for Women - (Tailor Made)', '<h2 class=\"pdp-mod-section-title outer-title\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px 24px; font-family: Roboto-Medium; font-size: 16px; line-height: 52px; color: rgb(33, 33, 33); overflow: hidden; text-overflow: ellipsis; white-space: nowrap; height: 52px; background: rgb(250, 250, 250);\">Product details of Gray Linen Kurti for Women - (Tailor Made)</h2><div class=\"pdp-product-detail\" data-spm=\"product_detail\" style=\"margin: 0px; padding: 0px; position: relative; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-size: 12px;\"><div class=\"pdp-product-desc \" style=\"margin: 0px; padding: 5px 14px 5px 24px; height: auto; overflow-y: hidden;\"><div class=\"html-content pdp-product-highlights\" style=\"margin: 0px; padding: 11px 0px 16px; word-break: break-word; border-bottom: 1px solid rgb(239, 240, 245); overflow: hidden;\"><ul class=\"\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; list-style: none; overflow: hidden; columns: auto 2; column-gap: 32px;\"><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Tailor Made Product</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Material: Linen</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Stitched Kurti</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Gender: Women</li><li class=\"\" data-spm-anchor-id=\"a2a0e.pdp.product_detail.i0.202c248aPuXJjU\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Product\'s Color May Vary a Bit Due to Photography</li></ul></div></div></div>', '<p style=\"margin-right: 0px; margin-left: 0px; padding: 0px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;\">Bend the Trend is one of the popular platform of quality products at reasonable price. They provide all types of electronics items, men\'s and women\'s dresses, shoes, sports items and other accessories frequently. Shop your choice from this seller.</p><div><br></div>', 1, 4, 1, '400', NULL, NULL, 'online', 'XSdfsafg', 'admin/product/2019120211083225196.jpg', 'admin/product/2019120211083265523.jpg', 'admin/product/2019120211083296951.jpg', 'gdsfgasdfg', 'dfgdf', 'xcgvdfg', NULL, NULL),
(31, 'Gray Linen Kurti for Women - (Tailor Made)', '<h2 class=\"pdp-mod-section-title outer-title\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px 24px; font-family: Roboto-Medium; font-size: 16px; line-height: 52px; color: rgb(33, 33, 33); overflow: hidden; text-overflow: ellipsis; white-space: nowrap; height: 52px; background: rgb(250, 250, 250);\">Product details of Gray Linen Kurti for Women - (Tailor Made)</h2><div class=\"pdp-product-detail\" data-spm=\"product_detail\" style=\"margin: 0px; padding: 0px; position: relative; font-family: Roboto, -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", Helvetica, sans-serif; font-size: 12px;\"><div class=\"pdp-product-desc \" style=\"margin: 0px; padding: 5px 14px 5px 24px; height: auto; overflow-y: hidden;\"><div class=\"html-content pdp-product-highlights\" style=\"margin: 0px; padding: 11px 0px 16px; word-break: break-word; border-bottom: 1px solid rgb(239, 240, 245); overflow: hidden;\"><ul class=\"\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; list-style: none; overflow: hidden; columns: auto 2; column-gap: 32px;\"><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Tailor Made Product</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Material: Linen</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Stitched Kurti</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Gender: Women</li><li class=\"\" data-spm-anchor-id=\"a2a0e.pdp.product_detail.i0.202c248aPuXJjU\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Product\'s Color May Vary a Bit Due to Photography</li></ul></div></div></div>', '<h2 style=\"margin-top: 0px; margin-bottom: 17px; padding: 0px; font-size: 16px; font-weight: 700; line-height: 29px; color: rgb(0, 0, 0); font-family: Roboto, -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", Helvetica, sans-serif;\"><span style=\"margin: 0px; padding: 0px; font-weight: bolder;\">Bend the Trend</span></h2><p style=\"margin-right: 0px; margin-left: 0px; padding: 0px; font-family: Roboto, -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", Helvetica, sans-serif;\"><br style=\"margin: 0px; padding: 0px;\">Bend the Trend is one of the popular platform of quality products at reasonable price. They provide all types of electronics items, men\'s and women\'s dresses, shoes, sports items and other accessories frequently. Shop your choice from this seller.</p><div><br></div>', 3, 5, 2, '500', NULL, NULL, 'online', 'dfsgvdf', 'admin/product/2019120211093063827.jpg', 'admin/product/2019120211093083973.jpg', 'admin/product/2019120211093083039.jpg', 'dfhsdh', 'fvghdsfgh', 'cdgbvsdfgh', NULL, NULL),
(32, 'Gray Linen Kurti for Women - (Tailor Made)', '<h2 class=\"pdp-mod-section-title outer-title\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px 24px; font-family: Roboto-Medium; font-size: 16px; line-height: 52px; color: rgb(33, 33, 33); overflow: hidden; text-overflow: ellipsis; white-space: nowrap; height: 52px; background: rgb(250, 250, 250);\">Product details of Gray Linen Kurti for Women - (Tailor Made)</h2><div class=\"pdp-product-detail\" data-spm=\"product_detail\" style=\"margin: 0px; padding: 0px; position: relative; font-family: Roboto, -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", Helvetica, sans-serif; font-size: 12px;\"><div class=\"pdp-product-desc \" style=\"margin: 0px; padding: 5px 14px 5px 24px; height: auto; overflow-y: hidden;\"><div class=\"html-content pdp-product-highlights\" style=\"margin: 0px; padding: 11px 0px 16px; word-break: break-word; border-bottom: 1px solid rgb(239, 240, 245); overflow: hidden;\"><ul class=\"\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; list-style: none; overflow: hidden; columns: auto 2; column-gap: 32px;\"><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Tailor Made Product</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Material: Linen</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Stitched Kurti</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Gender: Women</li><li class=\"\" data-spm-anchor-id=\"a2a0e.pdp.product_detail.i0.202c248aPuXJjU\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Product\'s Color May Vary a Bit Due to Photography</li></ul></div></div></div>', '<h2 style=\"margin-top: 0px; margin-bottom: 17px; padding: 0px; font-size: 16px; font-weight: 700; line-height: 29px; color: rgb(0, 0, 0); font-family: Roboto, -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", Helvetica, sans-serif;\"><span style=\"margin: 0px; padding: 0px; font-weight: bolder;\">Bend the Trend</span></h2><p style=\"margin-right: 0px; margin-left: 0px; padding: 0px; font-family: Roboto, -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", Helvetica, sans-serif;\"><br style=\"margin: 0px; padding: 0px;\">Bend the Trend is one of the popular platform of quality products at reasonable price. They provide all types of electronics items, men\'s and women\'s dresses, shoes, sports items and other accessories frequently. Shop your choice from this seller.</p><div><br></div>', 3, 5, 2, '500', NULL, NULL, 'online', 'sdgffdsg', 'admin/product/2019120211101988200.jpg', 'admin/product/2019120211101997789.jpg', 'admin/product/2019120211101942980.jpg', 'fhgdfgdsfg', 'dfghs', 'dfshgsdfg', NULL, NULL),
(33, 'Gray Linen Kurti for Women - (Tailor Made)', '<h2 class=\"pdp-mod-section-title outer-title\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px 24px; font-family: Roboto-Medium; font-size: 16px; line-height: 52px; color: rgb(33, 33, 33); overflow: hidden; text-overflow: ellipsis; white-space: nowrap; height: 52px; background: rgb(250, 250, 250);\">Product details of Gray Linen Kurti for Women - (Tailor Made)</h2><div class=\"pdp-product-detail\" data-spm=\"product_detail\" style=\"margin: 0px; padding: 0px; position: relative; font-family: Roboto, -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", Helvetica, sans-serif; font-size: 12px;\"><div class=\"pdp-product-desc \" style=\"margin: 0px; padding: 5px 14px 5px 24px; height: auto; overflow-y: hidden;\"><div class=\"html-content pdp-product-highlights\" style=\"margin: 0px; padding: 11px 0px 16px; word-break: break-word; border-bottom: 1px solid rgb(239, 240, 245); overflow: hidden;\"><ul class=\"\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; list-style: none; overflow: hidden; columns: auto 2; column-gap: 32px;\"><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Tailor Made Product</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Material: Linen</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Stitched Kurti</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Gender: Women</li><li class=\"\" data-spm-anchor-id=\"a2a0e.pdp.product_detail.i0.202c248aPuXJjU\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Product\'s Color May Vary a Bit Due to Photography</li></ul></div></div></div>', '<h2 style=\"margin-top: 0px; margin-bottom: 17px; padding: 0px; font-size: 16px; font-weight: 700; line-height: 29px; color: rgb(0, 0, 0); font-family: Roboto, -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", Helvetica, sans-serif;\"><span style=\"margin: 0px; padding: 0px; font-weight: bolder;\">Bend the Trend</span></h2><p style=\"margin-right: 0px; margin-left: 0px; padding: 0px; font-family: Roboto, -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", Helvetica, sans-serif;\"><br style=\"margin: 0px; padding: 0px;\">Bend the Trend is one of the popular platform of quality products at reasonable price. They provide all types of electronics items, men\'s and women\'s dresses, shoes, sports items and other accessories frequently. Shop your choice from this seller.</p><div><br></div>', 3, 5, 2, '400', NULL, NULL, 'online', 'xfsadf', 'admin/product/2019120211110798388.jpg', 'admin/product/2019120211110768545.jpg', 'admin/product/2019120211110735107.jpg', 'gsdgdsfhyrtyhfd', 'gsdfgsad', 'cgfvsd', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_additional_photo_price`
--

CREATE TABLE `shipping_additional_photo_price` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `photoQuantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photoPrice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_additional_photo_price`
--

INSERT INTO `shipping_additional_photo_price` (`id`, `photoQuantity`, `photoPrice`, `created_at`, `updated_at`) VALUES
(1, '5', '10', NULL, NULL),
(2, '10', '15', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_box_category`
--

CREATE TABLE `shipping_box_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `categoryName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_box_category`
--

INSERT INTO `shipping_box_category` (`id`, `categoryName`, `created_at`, `updated_at`) VALUES
(1, 'Large', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_box_sub_category`
--

CREATE TABLE `shipping_box_sub_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `categoryId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subCategoryName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_box_sub_category`
--

INSERT INTO `shipping_box_sub_category` (`id`, `categoryId`, `subCategoryName`, `price`, `created_at`, `updated_at`) VALUES
(1, '1', 'simple packing', '129', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_order`
--

CREATE TABLE `shipping_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `swiftNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userEmail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoiceNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `packageId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trackingNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sentTo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useSticker` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setStickerPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useUrgent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setUrgentPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingAdditionalPicture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boxCondition` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `personalUse` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setAdditionalPhotoPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setBoxPrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customerSetTotalAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingBoxCategory` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingBoxSubCategory` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingShippingCategory` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingShippingSubCategory` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_countryCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminShippingCharge` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminServiceCharge` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fragileStickerAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insuranceAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additionalCost` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminFinalAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finalAmountStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deliveryDate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminShipmentCharge` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminDiscountAmount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminInvoiceImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adminImage10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createTime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoiceImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `packageName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `packagePrice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_order`
--

INSERT INTO `shipping_order` (`id`, `swiftNumber`, `userId`, `userEmail`, `productType`, `invoiceNumber`, `productDescription`, `packageId`, `from`, `trackingNumber`, `weight`, `sentTo`, `status`, `useSticker`, `setStickerPrice`, `useUrgent`, `setUrgentPrice`, `shippingAdditionalPicture`, `boxCondition`, `personalUse`, `shippingType`, `setAdditionalPhotoPrice`, `setBoxPrice`, `customerSetTotalAmount`, `shippingBoxCategory`, `shippingBoxSubCategory`, `shippingShippingCategory`, `shippingShippingSubCategory`, `bill_name`, `bill_address1`, `bill_address2`, `bill_country`, `bill_city`, `bill_state`, `bill_zip`, `bill_countryCode`, `bill_phone`, `adminShippingCharge`, `adminServiceCharge`, `fragileStickerAmount`, `insuranceAmount`, `additionalCost`, `adminFinalAmount`, `finalAmountStatus`, `deliveryDate`, `orderNumber`, `adminShipmentCharge`, `adminDiscountAmount`, `adminInvoiceImage`, `adminImage1`, `adminImage2`, `adminImage3`, `adminImage4`, `adminImage5`, `adminImage6`, `adminImage7`, `adminImage8`, `adminImage9`, `adminImage10`, `createTime`, `quantity`, `productImage`, `invoiceImage`, `packageName`, `packagePrice`, `created_at`, `updated_at`) VALUES
(7, '447895', '11', 'zabbirhossain729@gmail.com', 'Pant', '2019123018332053', 'aita pant', '#1421532', 'Amazon', '201912301833201442', '15.4', 'fayez', 'complete', '0', NULL, '0', NULL, '5', 'orginalBox', '1', NULL, '10', NULL, '10', NULL, NULL, NULL, 'Daraz', 'zabbir', 'Shantinagar Bazar Road, Dhaka, Bangladesh', 'Shirdi, Maharashtra, India', 'Afghanistan', 'Dhakaa', 'Dhaka Division', '1205', '44', '4551265685', '3', '3', NULL, '3', NULL, '3', '1', '2019-12-31', '2019123018470997', '120', '3', 'user/orderImage/2019123018500044682.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019/12/30', '4', 'user/orderImage/2019123018385461327.jpeg', 'user/orderImage/2019123018385471921.jpg', 'Simple', '120', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_setting`
--

CREATE TABLE `shipping_setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `howShipping` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `shippingAddress` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `insideShipping` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `outsideShipping` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_setting`
--

INSERT INTO `shipping_setting` (`id`, `howShipping`, `shippingAddress`, `insideShipping`, `outsideShipping`, `status`, `created_at`, `updated_at`) VALUES
(1, '<h2 style=\"text-align: center; margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\"><b>How To Re-Shipping</b></h2><ul><li style=\"margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Firstly Register Our Site. You get a swift address when you register our website.</li><li style=\"margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Using this swift number any market place shipping address.</li><li style=\"margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">When this product arrived our address we inform you.</li><li style=\"margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Then all the procedures we maintain.&nbsp;</li></ul>', '<h2 style=\"margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">What is Lorem Ipsum?</h2><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '<h2 style=\"text-align: center; margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\"><b>Inside Shipping Instruction</b></h2><ul><li style=\"text-align: left; margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">You use our swift address of any marketplace.</li><li style=\"text-align: left; margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Order from Bangladesh or any other country.</li><li style=\"text-align: left; margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">This shipping only for Bangladesh\'s place.&nbsp;</li></ul>', '<h2 style=\"margin-top: 0px; font-family: DauphinPlain; line-height: 24px; color: rgb(0, 0, 0); font-size: 24px; text-align: center; padding: 0px;\"><span style=\"font-weight: bolder;\">Outside Shipping Instruction</span></h2><h2 style=\"margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\"><ul style=\"font-family: Roboto, sans-serif; font-size: 14px;\"><li style=\"text-align: left; margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px;\">You use our swift address of any marketplace.</li><li style=\"text-align: left; margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px;\">Order from any other country. Only for Bangladeshi customers.&nbsp;</li><li style=\"text-align: left; margin-top: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px;\">This shipping only for Bangladesh\'s place.&nbsp;</li></ul></h2>', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_shipping_category`
--

CREATE TABLE `shipping_shipping_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `categoryName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_shipping_category`
--

INSERT INTO `shipping_shipping_category` (`id`, `categoryName`, `created_at`, `updated_at`) VALUES
(1, 'Local', NULL, NULL),
(2, 'International', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_shipping_sub_category`
--

CREATE TABLE `shipping_shipping_sub_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subCategoryName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoryId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_shipping_sub_category`
--

INSERT INTO `shipping_shipping_sub_category` (`id`, `subCategoryName`, `categoryId`, `created_at`, `updated_at`) VALUES
(1, 'Daraz', '1', NULL, NULL),
(2, 'Alibaba', '2', NULL, NULL),
(3, 'RMX', '2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscribe_user`
--

CREATE TABLE `subscribe_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subscribeUserEmail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createTime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribe_user`
--

INSERT INTO `subscribe_user` (`id`, `subscribeUserEmail`, `createTime`, `created_at`, `updated_at`) VALUES
(1, 'zabbir@gmail.com', '2019/10/31', NULL, NULL),
(4, 'zabbirhossain727@gmail.com', '2019/10/31', NULL, NULL),
(5, 'joy@gmail.com', '2019/10/31', NULL, NULL),
(6, 'farazshaon0@gmail.com', '2019/11/03', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subCategory_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `department_id`, `category_id`, `subCategory_type`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 'pant', NULL, NULL),
(2, 1, 4, 'tshirt', NULL, NULL),
(3, 1, 4, 'Hijab', NULL, NULL),
(4, 2, 6, 'pant', NULL, NULL),
(5, 2, 4, 'pant', NULL, NULL),
(6, 3, 5, 'pant', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sweet_address`
--

CREATE TABLE `sweet_address` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sweetAddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sweetCountry` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sweetCity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sweetState` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sweetHouse` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sweetRoad` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sweetPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sweet_address`
--

INSERT INTO `sweet_address` (`id`, `sweetAddress`, `sweetCountry`, `sweetCity`, `sweetState`, `sweetHouse`, `sweetRoad`, `status`, `sweetPhone`, `created_at`, `updated_at`) VALUES
(4, 'Shantinagar Bazar Road, Dhaka, Bangladesh', 'Armenia', 'Dhakaa', 'Dhaka Division', '1206', '12/4 dillie', '0', '01933722566', NULL, NULL),
(5, 'dhanmondi, dhaka', 'Bangladesh', 'Dhakaa', 'Dhaka Division', '1207', 'House num: 12/7 Road Num: 13/8', '1', '01933722564', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'zabbir', 'za@gmail.com', '2019-12-07 18:00:00', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_inside_shipping_address`
--

CREATE TABLE `user_inside_shipping_address` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_inside_shipping_address`
--

INSERT INTO `user_inside_shipping_address` (`id`, `userId`, `country`, `address1`, `address2`, `city`, `company`, `street`, `town`, `district`, `phone`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'Bangladesh', 'asd', 'asd', 'Dhaka', 'tomattos', 'dhaka', 'dhaka', NULL, '01933722564', 'zabbirhossain727@gmail.com', '0', NULL, NULL),
(2, '1', 'Bangladesh', 'asd2', 'asd2', 'Dhaka', 'tomattos', 'dhaka2', 'dhaka2', NULL, '01933722563', 'zabbirhossain729@gmail.com', '0', NULL, NULL),
(3, '2', 'Bangladesh', 'Dhanmondi Lake Road, Dhaka, Bangladesh', 'Dhanmondi, Dhaka, Bangladesh', 'Dhaka', 'tomattos', 'Dhaka Division', '1205', NULL, '01933722564', 'zabbirhossain727@gmail.com', '0', NULL, NULL),
(4, '4', 'India', 'Deodand, Uttar Pradesh, India', 'Dhanbad, Jharkhand, India', 'Deodand', 'Pipilika', 'UP', '272175', NULL, '01933722564', 'zabbirhossain727@gmail.com', '1', NULL, NULL),
(5, '4', 'Bangladesh', 'Dhanmondi Lake Road, Dhaka, Bangladesh', 'Indiranagar, Bengaluru, Karnataka, India', 'Dhaka', 'tomattos', 'Dhaka Division', '1205', NULL, '01933722563', 'zabbirhossain729@gmail.com', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_message`
--

CREATE TABLE `user_message` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userEmail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mailSubject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `createTime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_message`
--

INSERT INTO `user_message` (`id`, `userName`, `userEmail`, `mailSubject`, `message`, `createTime`, `created_at`, `updated_at`) VALUES
(1, 'zabbir', 'zabbirhossain729@gmail.com', 'test purpose', 'sadfasd', '2019/12/19', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_outside_shipping_address`
--

CREATE TABLE `user_outside_shipping_address` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_register`
--

CREATE TABLE `user_register` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userEmail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userPhone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userPassword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createTime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_register`
--

INSERT INTO `user_register` (`id`, `userName`, `userEmail`, `userPhone`, `userPassword`, `createTime`, `status`, `created_at`, `updated_at`) VALUES
(2, 'zabbir', 'zabbirhossain727@gmail.com', '01933722564', '25d55ad283aa400af464c76d713c07ad', '2019/10/30', '1', NULL, NULL),
(3, 'shauon', 'farazshaon0@gmail.com', '01774507042', '25d55ad283aa400af464c76d713c07ad', '2019/11/03', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_register_order`
--

CREATE TABLE `user_register_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userEmail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userPhone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userPassword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verify` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `createTime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_register_order`
--

INSERT INTO `user_register_order` (`id`, `userName`, `userEmail`, `userPhone`, `userPassword`, `hash`, `verify`, `createTime`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Zabbir', 'zabbirhossain729@gmail.com', '01933722564', '25d55ad283aa400af464c76d713c07ad', '2019111810185396193', '1', '2019/11/18', '1', NULL, NULL),
(2, 'Web Design', 'zabbirhossain727@gmail.com', '01933722563', '25d55ad283aa400af464c76d713c07ad', '2019120411494119524', '1', '2019/12/04', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_register_reshipping`
--

CREATE TABLE `user_register_reshipping` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userEmail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userPhone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userPassword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verify` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createTime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_register_reshipping`
--

INSERT INTO `user_register_reshipping` (`id`, `userName`, `userEmail`, `userPhone`, `userPassword`, `hash`, `verify`, `createTime`, `status`, `created_at`, `updated_at`) VALUES
(4, 'Zabbir', 'zabbirhossain729@gmail.com', '01933722564', '25d55ad283aa400af464c76d713c07ad', '2019112110293217738', '1', '2019/11/21', '1', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_inside_custom_order`
--
ALTER TABLE `add_inside_custom_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_send_message`
--
ALTER TABLE `admin_send_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_type` (`category_type`),
  ADD KEY `category_type_2` (`category_type`);

--
-- Indexes for table `company_setting`
--
ALTER TABLE `company_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_order`
--
ALTER TABLE `custom_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_order_outside`
--
ALTER TABLE `custom_order_outside`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecommerce_order`
--
ALTER TABLE `ecommerce_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecommerce_shipping_cost`
--
ALTER TABLE `ecommerce_shipping_cost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forget_password`
--
ALTER TABLE `forget_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inside_box_price`
--
ALTER TABLE `inside_box_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inside_order_box_price`
--
ALTER TABLE `inside_order_box_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inside_order_payment_setting`
--
ALTER TABLE `inside_order_payment_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inside_shipping_setting`
--
ALTER TABLE `inside_shipping_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inside_user_reshipping_order`
--
ALTER TABLE `inside_user_reshipping_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ltm_translations`
--
ALTER TABLE `ltm_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_additional_photo_price`
--
ALTER TABLE `order_additional_photo_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_basic_setting`
--
ALTER TABLE `order_basic_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_box_category`
--
ALTER TABLE `order_box_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_box_sub_category`
--
ALTER TABLE `order_box_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_product_type`
--
ALTER TABLE `order_product_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_shipping_category`
--
ALTER TABLE `order_shipping_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_shipping_sub_category`
--
ALTER TABLE `order_shipping_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outside_box_price`
--
ALTER TABLE `outside_box_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outside_order_box_price`
--
ALTER TABLE `outside_order_box_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outside_order_payment_setting`
--
ALTER TABLE `outside_order_payment_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outside_shipping_setting`
--
ALTER TABLE `outside_shipping_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `predefined_marketplace`
--
ALTER TABLE `predefined_marketplace`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_additional_photo_price`
--
ALTER TABLE `shipping_additional_photo_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_box_category`
--
ALTER TABLE `shipping_box_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_box_sub_category`
--
ALTER TABLE `shipping_box_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_order`
--
ALTER TABLE `shipping_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_setting`
--
ALTER TABLE `shipping_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_shipping_category`
--
ALTER TABLE `shipping_shipping_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_shipping_sub_category`
--
ALTER TABLE `shipping_shipping_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribe_user`
--
ALTER TABLE `subscribe_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sweet_address`
--
ALTER TABLE `sweet_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_inside_shipping_address`
--
ALTER TABLE `user_inside_shipping_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_message`
--
ALTER TABLE `user_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_outside_shipping_address`
--
ALTER TABLE `user_outside_shipping_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_register`
--
ALTER TABLE `user_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_register_order`
--
ALTER TABLE `user_register_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_register_reshipping`
--
ALTER TABLE `user_register_reshipping`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add_inside_custom_order`
--
ALTER TABLE `add_inside_custom_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_send_message`
--
ALTER TABLE `admin_send_message`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `company_setting`
--
ALTER TABLE `company_setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `custom_order`
--
ALTER TABLE `custom_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `custom_order_outside`
--
ALTER TABLE `custom_order_outside`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ecommerce_order`
--
ALTER TABLE `ecommerce_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ecommerce_shipping_cost`
--
ALTER TABLE `ecommerce_shipping_cost`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forget_password`
--
ALTER TABLE `forget_password`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `inside_box_price`
--
ALTER TABLE `inside_box_price`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inside_order_box_price`
--
ALTER TABLE `inside_order_box_price`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inside_order_payment_setting`
--
ALTER TABLE `inside_order_payment_setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `inside_shipping_setting`
--
ALTER TABLE `inside_shipping_setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `inside_user_reshipping_order`
--
ALTER TABLE `inside_user_reshipping_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ltm_translations`
--
ALTER TABLE `ltm_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `order_additional_photo_price`
--
ALTER TABLE `order_additional_photo_price`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `order_basic_setting`
--
ALTER TABLE `order_basic_setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_box_category`
--
ALTER TABLE `order_box_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `order_box_sub_category`
--
ALTER TABLE `order_box_sub_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_product_type`
--
ALTER TABLE `order_product_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `order_shipping_category`
--
ALTER TABLE `order_shipping_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `order_shipping_sub_category`
--
ALTER TABLE `order_shipping_sub_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `outside_box_price`
--
ALTER TABLE `outside_box_price`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `outside_order_box_price`
--
ALTER TABLE `outside_order_box_price`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `outside_order_payment_setting`
--
ALTER TABLE `outside_order_payment_setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `outside_shipping_setting`
--
ALTER TABLE `outside_shipping_setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `predefined_marketplace`
--
ALTER TABLE `predefined_marketplace`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `shipping_additional_photo_price`
--
ALTER TABLE `shipping_additional_photo_price`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `shipping_box_category`
--
ALTER TABLE `shipping_box_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shipping_box_sub_category`
--
ALTER TABLE `shipping_box_sub_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shipping_order`
--
ALTER TABLE `shipping_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `shipping_setting`
--
ALTER TABLE `shipping_setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shipping_shipping_category`
--
ALTER TABLE `shipping_shipping_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `shipping_shipping_sub_category`
--
ALTER TABLE `shipping_shipping_sub_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `subscribe_user`
--
ALTER TABLE `subscribe_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sweet_address`
--
ALTER TABLE `sweet_address`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_inside_shipping_address`
--
ALTER TABLE `user_inside_shipping_address`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_message`
--
ALTER TABLE `user_message`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_outside_shipping_address`
--
ALTER TABLE `user_outside_shipping_address`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_register`
--
ALTER TABLE `user_register`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_register_order`
--
ALTER TABLE `user_register_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_register_reshipping`
--
ALTER TABLE `user_register_reshipping`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
