<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Mail;
use Session;
use Validator,Redirect,Response,File;
if (!isset($_SESSION)) {
    session_start();
}

class OrderController extends Controller
{
    public function __construct()
    {
        // $this->middleware('checkReshippingLogin');
    }
    public function showOrderPage(){
      
      $userId=Session::get('userId');
    	$settingInfo=DB::table('order_basic_setting')
						->where('order_basic_setting.status',1)
						->first();
      $countries=$this->countryName();
      $departmentInfo=DB::table('department')
                        ->get();
      $categoryInfo=DB::table('category')
                        ->get();
      $productType=DB::table('order_product_type')
                        ->get();
      $userInfo=DB::table('customer')
                      ->where('customer.id',$userId)
                      ->first();
      $insideInitialPayment=DB::table('inside_order_payment_setting')
                                ->where('status',1)
                                ->first();
      $outsideInitialPayment=DB::table('outside_order_payment_setting')
                                ->where('status',1)
                                ->first();
      $marketPlaceInfo=DB::table('predefined_marketplace')
                            ->get();
		  return view('order.showOrderPage',compact('settingInfo','departmentInfo','categoryInfo','productType','insideInitialPayment','outsideInitialPayment','marketPlaceInfo','userInfo','countries'));
    }

    public function showInsideOrderPage(){
      
      $userId=Session::get('userId');
      if ($userId) {
      $settingInfo=DB::table('order_basic_setting')
            ->where('order_basic_setting.status',1)
            ->first();
      $countries=$this->countryName();
      $departmentInfo=DB::table('department')
                        ->get();
      $categoryInfo=DB::table('category')
                        ->get();
      $productType=DB::table('order_product_type')
                        ->get();
      $userInfo=DB::table('customer')
                      ->where('customer.id',$userId)
                      ->first();
      $insideInitialPayment=DB::table('inside_order_payment_setting')
                                ->where('status',1)
                                ->first();
      $outsideInitialPayment=DB::table('outside_order_payment_setting')
                                ->where('status',1)
                                ->first();
      $marketPlaceInfo=DB::table('predefined_marketplace')
                            ->get();
      return view('order.showInsideOrderPage',compact('settingInfo','departmentInfo','categoryInfo','productType','insideInitialPayment','outsideInitialPayment','marketPlaceInfo','userInfo','countries'));
      }else{
        return redirect()->back();
      }
    }

    public function showOutsideOrderPage(){
      
      $userId=Session::get('userId');
      if ($userId) {
      $settingInfo=DB::table('order_basic_setting')
            ->where('order_basic_setting.status',1)
            ->first();
      $countries=$this->countryName();
      $departmentInfo=DB::table('department')
                        ->get();
      $categoryInfo=DB::table('category')
                        ->get();
      $productType=DB::table('order_product_type')
                        ->get();
      $userInfo=DB::table('customer')
                      ->where('customer.id',$userId)
                      ->first();
      $insideInitialPayment=DB::table('inside_order_payment_setting')
                                ->where('status',1)
                                ->first();
      $outsideInitialPayment=DB::table('outside_order_payment_setting')
                                ->where('status',1)
                                ->first();
      $marketPlaceInfo=DB::table('predefined_marketplace')
                            ->get();
      return view('order.showOutsideOrderPage',compact('settingInfo','departmentInfo','categoryInfo','productType','insideInitialPayment','outsideInitialPayment','marketPlaceInfo','userInfo','countries'));
      }else{
        return redirect()->back();
      }
    }

    public function showOrderLogin(){
    
    $departmentInfo=DB::table('department')
                        ->get();
      $categoryInfo=DB::table('category')
                        ->get();
		return view('order.showOrderLogin',compact('departmentInfo','categoryInfo'));
    }

    public function orderUserLogin(Request $request){

    $this->validate($request, [
      'emailOrPhone' => 'required',
      'password' => 'required',
      ]);

      $userEmailOrPhone=$request->emailOrPhone;
      $password=md5($request->password);
      $userInformation=DB::table('user_register_order')
                          ->where('userEmail', $userEmailOrPhone)
                          ->orWhere('userPhone',$userEmailOrPhone)
                          ->where('userPassword', $password)
                          ->first();
      // echo "<pre/>";
      // print_r($userInformation);
      // exit();
      
      if ($userInformation) {

          if ($userInformation->status!='1') {
            Session::put('message','Account DeActive By Admin');
            return redirect()->route('showOrderLogin');
          }elseif ($userInformation->verify!='1') {
            Session::put('message','Please Verify Your Account By Email');
            return redirect()->route('showOrderLogin');
          }
          Session::put('userId',$userInformation->id);
          return redirect()->back();
        }else{
          Session::put('message','Email/Phone or Password Invalid');
          return redirect()->route('showOrderLogin');
        }
    }

    public function orderUserRegister(Request $request){
    	
    $this->validate($request, [
    'name' => 'required|min:3|max:50',
    'email' => 'email',
    'phone' => 'max:13',
    'password' => 'min:6|required_with:rePassword|same:rePassword',
    'rePassword' => 'min:6'
    ]);

    $data=array();
    $data['userName']=$request->name;
    $data['userEmail']=$request->email;
    $data['userPhone']=$request->phone;
    $data['userPassword']=md5($request->password);
    $data['createTime']=date("Y/m/d");
    $hash=date("YmdHis").rand(11111,99999);
    $data['hash']=$hash;
    $data['verify']=0;

    //email and phone number check start here.
    $checkEmailAndPhone=DB::table('user_register_order')
                     ->get();
    foreach($checkEmailAndPhone as $checkEP){
        //check phone number..
        if ($checkEP->userPhone == $request->phone) {
            Session::put('messageWarning','This Phone Number Allready Used!!');
            return redirect()->back();

        }
        //check email
        if ($checkEP->userEmail ==$request->email) {
            Session::put('messageWarning','This Email Allready Used!!');
            return redirect()->back();
        }
    }
    //email and phone number check end here.

    $customerEmail=$request->email;
    $customerPassword=$request->password;
    $messageBody="Successfully Registration ushopnship.com. "."Click This link for verification  "."http://127.0.0.1:8000/order-user-profile-verify/".$customerEmail."/".$hash." "; 
    Mail::send([], [],function ($message) use ($customerEmail,$messageBody) {

       $message->from('services@bee-next.com.bd', 'Shop And Ship');
       $message->subject("Registration Information");
       $message->setBody($messageBody);

        $message->to($customerEmail);
    });
    
    $userRegister=DB::table('user_register_order')
                      ->insert($data);
    Session::put('messagere','Registration Successfully Done!!! Please confirm your Registration by Mail');
    return redirect()->back();
    // echo "<pre/>";
    // print_r($data);
    // exit();
    }

    public function orderUserProfileVerify($email,$hash){

    $email=$email;
    $hash=$hash;

    $verifyCheck=DB::table('user_register_order')
                      ->where('userEmail',$email)
                      ->where('hash',$hash)
                      ->first();
    if ($verifyCheck) {
      
      if ($verifyCheck->verify=='1') {
      
          Session::put('messageVerifyR','Your Account Allready Verify. Please login !!');
          return redirect()->route('showOrder Login');
      }elseif ($verifyCheck->verify=='0') {
        
        $data=array();
        $data['verify']=1;
        $updateVerify=DB::table('user_register_order')
                            ->update($data);
        if ($updateVerify) {
            
            Session::put('messageVerifyS','Your Account Verify Successfully Done. Please login !!');
            return redirect()->route('showOrderLogin');
        }
      }

    }else{

      Session::put('messageVerifyU','Invalid link !!');
      return redirect()->route('showUserLogin');

    }
    
   }
/* Country name Start Here */

    private function countryName(){

        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"); 

    return $countries;

    }
/* Country name end here */

}
