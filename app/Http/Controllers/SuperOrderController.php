<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Mail;
use PDF;
use Session;
use Validator,Redirect,Response,File;
if (!isset($_SESSION)) {
    session_start();
}

class SuperOrderController extends Controller
{
    
    public function __construct(){

        set_time_limit(8000000);
    }


     public function superOrderDashboard(){
    	return view('order.dashboard');
    }

     public function orderUserLogout(){

        Session::put('orderUserId','');
        Session::put('orderUserName','');
        return redirect()->route('showOrderPage');
    }

    public function addInsideOrder(){

    	$marketPlaceInfo=DB::table('predefined_marketplace')
    							->get();
    	$insideOrderCost=DB::table('inside_order_payment_setting')
    							->where('inside_order_payment_setting.status',1)
    							->first();
    	return view('order.addInsideOrder',compact('marketPlaceInfo','insideOrderCost'));
    }

    public function addInsideCustomOrder(Request $request){

    	$insideOrderCost=DB::table('inside_order_payment_setting')
							->where('inside_order_payment_setting.status',1)
							->first();
		$totalAmount=$insideOrderCost->serviceCharge+$insideOrderCost->serviceCharge+$insideOrderCost->serviceCharge;
    	$userId=Session::get('orderUserId');
    	$data=array();
    	$data['address']=$request->address;
    	$data['country']=$request->country;
    	$data['state']=$request->state;
    	$data['city']=$request->city;
    	$data['zip']=$request->zip;
    	$data['house']=$request->house;
    	$data['road']=$request->road;
    	$data['product_link']=$request->product_link;
        $data['productName']=$request->productName;
    	$data['price']=$request->price;
    	$data['quantity']=$request->quantity;
    	$data['totalAmount']=$totalAmount;
    	$data['userId']=$userId;
        $data['createAt']=date("Y/m/d");
        $data['status']='pending';
        $data['orderNumber']=date("YmdHis").rand(11111,99999);
        $insertInsideCustomOrder=DB::table('add_inside_custom_order')
                                    ->insert($data);


        if ($insertInsideCustomOrder) {        
            Session::put('message','Inside Order Created Successfully !!!');
            return redirect()->back();
        }else{

            Session::put('messageWarning','Inside Order Created Failed !!!');
            return redirect()->back();
        }                   
        
        // echo "<pre/>";
        // print_r($data);
        // exit();

	    //Feedback mail to client
	    // $pdf = PDF::loadHTML('<h1>Test</h1>');
        
        // $pdf = PDF::loadView('order.insideOrderLoadInvoice', $data)->setPaper('a4');
        // return $pdf->download('pdfview.pdf');
        // exit();
        // Mail::send([], [],function ($message) use ($data,$pdf) {
        //     $message->from('services@bee-next.com.bd', 'Shop And Ship');
        //     $message->to('zabbirhossain729@gmail.com');
        //     $message->subject('Thank you For Ordering');
        //     //Attach PDF doc
        //     $message->attachData($pdf->output(),'invoice.pdf');
        // });

      


    }

    public function showUserInsidePendingOrder(){

        $userId=Session::get('orderUserId');
        $pendingOrderInfo=DB::table('add_inside_custom_order as ad')
                                ->join('user_register_order as us','us.id','=','ad.userId')
                                ->where('ad.status','pending')
                                ->where('ad.userId',$userId)
                                ->select('ad.*','us.userName')
                                ->get();
        return view('order.showUserInsidePendingOrder',compact('pendingOrderInfo'));
    }

    public function viewUserInsidePendingOrder($id){

        $insideOrderInformation=DB::table('add_inside_custom_order as ad')
                                ->join('user_register_order as us','us.id','=','ad.userId')
                                ->where('ad.id',$id)
                                ->select('ad.*','us.userName','us.userEmail','us.userPhone','us.createTime')
                                ->first();
        return view('order.viewUserInsidePendingOrder',compact('insideOrderInformation'));
    }


    public function showUserInsideProcessingOrder(){
        
        $userId=Session::get('orderUserId');
        $pendingOrderInfo=DB::table('add_inside_custom_order as ad')
                                ->join('user_register_order as us','us.id','=','ad.userId')
                                ->where('ad.status','processing')
                                ->where('ad.userId',$userId)
                                ->select('ad.*','us.userName')
                                ->get();
        return view('order.showUserInsideProcessingOrder',compact('pendingOrderInfo'));
    }

    public function showUserInsideActiveOrder(){
        
        $userId=Session::get('orderUserId');
        $pendingOrderInfo=DB::table('add_inside_custom_order as ad')
                                ->join('user_register_order as us','us.id','=','ad.userId')
                                ->where('ad.status','active')
                                ->where('ad.userId',$userId)
                                ->select('ad.*','us.userName')
                                ->get();
        return view('order.showUserInsideActiveOrder',compact('pendingOrderInfo'));
    }

    public function showUserInsideDeliveryOrder(){
        
        $userId=Session::get('orderUserId');
        $pendingOrderInfo=DB::table('add_inside_custom_order as ad')
                                ->join('user_register_order as us','us.id','=','ad.userId')
                                ->where('ad.status','delivery')
                                ->where('ad.userId',$userId)
                                ->select('ad.*','us.userName')
                                ->get();
        return view('order.showUserInsideDeliveryOrder',compact('pendingOrderInfo'));
    }

    public function showUserInsideCancelOrder(){
        
        $userId=Session::get('orderUserId');
        $pendingOrderInfo=DB::table('add_inside_custom_order as ad')
                                ->join('user_register_order as us','us.id','=','ad.userId')
                                ->where('ad.status','cancel')
                                ->where('ad.userId',$userId)
                                ->select('ad.*','us.userName')
                                ->get();
        return view('order.showUserInsideCancelOrder',compact('pendingOrderInfo'));
    }

    public function insideOrderLoadInvoice(){

    	return view('order.insideOrderLoadInvoice');
    }
}
