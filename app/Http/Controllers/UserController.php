<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Mail;
use Session;
use Cart;
use Validator,Redirect,Response,File;

class UserController extends Controller
{
   
   public function welcome(){
	  $departmentInfo=DB::table('department')
						->get();
    $categoryInfo=DB::table('category')
						->get();
    $onsaleProduct=DB::table('product as pro')
                    ->join('department as dep','dep.id','=','pro.departmentId')
                    ->join('category as cat','cat.id','=','pro.categoryId')
                    ->join('sub_category as subc','subc.id','=','pro.subcategoryId')
                    ->select('pro.*','dep.department_type','cat.category_type','subc.subCategory_type','dep.currency_prefix')
                    ->limit(12)
                    ->get();
    // echo "<pre/>";
    // print_r($onsaleProduct);
    // exit();
	return view('welcome',compact('departmentInfo','categoryInfo','onsaleProduct'));
   }

   public function languageChangeEn(Request $request){

      Session::put('language','');
     return redirect()->back();
   }

   public function languageChangeBn(Request $request){
    
     Session::put('language',1);
     return redirect()->back();
   }

   public function shop(){

    $departmentInfo=DB::table('department')
            ->get();
    $categoryInfo=DB::table('category')
            ->get();
    $onsaleProduct=DB::table('product as pro')
                    ->join('department as dep','dep.id','=','pro.departmentId')
                    ->join('category as cat','cat.id','=','pro.categoryId')
                    ->join('sub_category as subc','subc.id','=','pro.subcategoryId')
                    ->select('pro.*','dep.department_type','cat.category_type','subc.subCategory_type','dep.currency_prefix')
                    ->get();
    $latestArrival=DB::table('product as pro')
                    ->join('department as dep','dep.id','=','pro.departmentId')
                    ->join('category as cat','cat.id','=','pro.categoryId')
                    ->join('sub_category as subc','subc.id','=','pro.subcategoryId')
                    ->select('pro.*','dep.department_type','cat.category_type','subc.subCategory_type','dep.currency_prefix')
                    ->orderby('pro.id','DESC')
                    ->limit(6)
                    ->get();
    $leftGarments=DB::table('product as pro')
                    ->join('department as dep','dep.id','=','pro.departmentId')
                    ->join('category as cat','cat.id','=','pro.categoryId')
                    ->join('sub_category as subc','subc.id','=','pro.subcategoryId')
                    ->where('dep.department_type','Garments')
                    ->select('pro.*','dep.department_type','cat.category_type','subc.subCategory_type','dep.currency_prefix')
                    ->limit(6)
                    ->get();
    $rightGarments=DB::table('product as pro')
                      ->join('department as dep','dep.id','=','pro.departmentId')
                      ->join('category as cat','cat.id','=','pro.categoryId')
                      ->join('sub_category as subc','subc.id','=','pro.subcategoryId')
                      ->where('dep.department_type','Garments')
                      ->select('pro.*','dep.department_type','cat.category_type','subc.subCategory_type','dep.currency_prefix')
                      ->orderby('pro.id','DESC')
                      ->limit(6)
                      ->get();
  return view('shop',compact('departmentInfo','categoryInfo','onsaleProduct','latestArrival','leftGarments','rightGarments'));

   }

  public function showCategory(){
  $cate=DB::table('sub_category as subc')
              ->join('category as cat','subc.category_id','=','cat.id')
              ->join('department as dep','subc.department_id','=','dep.id')
              ->get();
  // echo "<pre/>";
  // print_r($cate);
  // exit();
  $departmentInfo=DB::table('department')
            ->get();
    $categoryInfo=DB::table('category')
            ->get();
  return view('user.showCategory',compact('departmentInfo','categoryInfo'));
   }

   public function sendMessage(Request $request){
   	$data=array();
   	$data['userName']=$request->userName;
   	$data['userEmail']=$request->userEmail;
   	$data['mailSubject']=$request->mailSubject;
   	$data['message']=$request->message;
   	$data['createTime']=date("Y/m/d");
   	// echo "<pre/>";
    // print_r($data);
    // exit();
   	$sendUserMessage=DB::table('user_message')
   							->insert($data);
   	Session::put('message','Message Send Successfully !!!');
    return redirect()->back();

   }

   public function showUserLogin(){
   	$departmentInfo=$this->departmentInfo();
    $categoryInfo=DB::table('category')
            ->get();
	return view('user.showUserLogin',compact('departmentInfo','categoryInfo'));
   } 

   public function showUserRegister(){
   	$departmentInfo=$this->departmentInfo();
   	return view('user.showUserRegister',compact('departmentInfo'));
   }

   public function showStore(){
   	$departmentInfo=$this->departmentInfo();
    $categoryInfo=DB::table('category')
                        ->get();
    
    $productInfo=DB::table('product as pro')
                    ->join('department as dep','dep.id','=','pro.departmentId')
                    ->join('category as cat','cat.id','=','pro.categoryId')
                    ->join('sub_category as subc','subc.id','=','pro.subcategoryId')
                    ->select('pro.*','dep.department_type','cat.category_type','subc.subCategory_type','dep.currency_prefix')
                    ->get();
    $issetShowStore=1;
    // echo "<pre/>";
    // print_r($productInfo);
    // exit();
   	return view('user.showStore',compact('departmentInfo','categoryInfo','productInfo','issetShowStore'));
   }

   public function showDepartmentStore($id){

    $departmentInfo=$this->departmentInfo();
    $issetDepartment=$id;
    $categoryInfo=DB::table('category')
                        ->get();
    
    $productInfo=DB::table('product as pro')
                    ->join('department as dep','dep.id','=','pro.departmentId')
                    ->join('category as cat','cat.id','=','pro.categoryId')
                    ->join('sub_category as subc','subc.id','=','pro.subcategoryId')
                    ->where('pro.departmentId',$id)
                    ->select('pro.*','dep.department_type','cat.category_type','subc.subCategory_type','dep.currency_prefix')
                    ->get();
    $departmentBread=DB::table('department')
                          ->where('id',$id)
                          ->first();
    // echo "<pre/>";
    // print_r($issetDepartment);
    // exit();
    return view('user.showStore',compact('departmentInfo','categoryInfo','productInfo','issetDepartment','departmentBread'));

   }

  public function showCategoryStore($id){
    
    $departmentInfo=$this->departmentInfo();
    $issetCategory=$id;
    $categoryInfo=DB::table('category')
                        ->get();
    $categoryBread=DB::table('category')
                          ->where('id',$id)
                          ->first();
    
    $productInfo=DB::table('product as pro')
                    ->join('department as dep','dep.id','=','pro.departmentId')
                    ->join('category as cat','cat.id','=','pro.categoryId')
                    ->join('sub_category as subc','subc.id','=','pro.subcategoryId')
                    ->where('pro.categoryId',$id)
                    ->select('pro.*','dep.department_type','cat.category_type','subc.subCategory_type','dep.currency_prefix')
                    ->get();
    // echo "<pre/>";
    // print_r($issetDepartment);
    // exit();
    return view('user.showStore',compact('departmentInfo','categoryInfo','productInfo','issetCategory','categoryBread'));
   }


  public function showContactUs(){
   	$departmentInfo=$this->departmentInfo();
    $categoryInfo=DB::table('category')
            ->get();
   	return view('user.showContactUs',compact('departmentInfo','categoryInfo'));
   }

   public function showAboutUs(){
   	$departmentInfo=$this->departmentInfo();
    $categoryInfo=DB::table('category')
            ->get();
   	return view('user.showAboutUs',compact('departmentInfo','categoryInfo'));
   }
   private function departmentInfo(){
   	$departmentInfo=DB::table('department')
						->get();
	return $departmentInfo;
   }

  public function userProfileVerify($email,$hash){

    $email=$email;
    $hash=$hash;

    $verifyCheck=DB::table('customer')
                      ->where('email',$email)
                      ->where('hash',$hash)
                      ->first();
    if ($verifyCheck) {
      
      if ($verifyCheck->verify=='1') {
      
          Session::put('messageVerifyR','Your Account Allready Verify. Please login !!');
          return redirect()->route('showUserLogin');
      }elseif ($verifyCheck->verify=='0') {
        
        $data=array();
        $data['verify']=1;
        $updateVerify=DB::table('customer')
                            ->update($data);
        if ($updateVerify) {
            
            Session::put('messageVerifyS','Your Account Verify Successfully Done. Please login !!');
            return redirect()->route('showUserLogin');
        }
      }

    }else{

      Session::put('messageVerifyU','Invalid link !!');
      return redirect()->route('showUserLogin');

    }
    
   }

   public function saveRegister(Request $request){
    $this->validate($request, [
    'userName' => 'required|min:3|max:50',
    'userEmail' => 'email',
    'userPhone' => 'max:13',
    'userPassword' => 'min:6|required_with:userRePassword|same:userRePassword',
    'userRePassword' => 'min:6'
    ]);
    
    

    $data=array();
    $data['firstName']=$request->userName;
    $data['email']=$request->userEmail;
    $data['phone']=$request->userPhone;
    $data['password']=md5($request->userPassword);
    $data['created_at']=date("Y/m/d");
    $hash=date("YmdHis").rand(11111,99999);
    $data['hash']=$hash;
    $digits = 2;
    $firstRandom=str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
    $secondRandom=str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
    $thirdRandom=str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
    $swiftNumber=$firstRandom.$secondRandom.$thirdRandom;

    $data['swiftNumber']=$swiftNumber;
    $name=$request->userName;

    $swiftAddress=DB::table('sweet_address')
                        ->where('sweet_address.status',1)
                        ->first();
    //email and phone number check start here.
    $checkEmailAndPhone=DB::table('customer')
                     ->get();
    foreach($checkEmailAndPhone as $checkEP){
        //check phone number..
        if ($checkEP->phone == $request->userPhone) {
            Session::put('messageWarning','This Phone Number Allready Used!!');
            return redirect()->back();

        }
        //check email
        if ($checkEP->email ==$request->userEmail) {
            Session::put('messageWarning','This Email Allready Used!!');
            return redirect()->back();
        }
    }
    //email and phone number check end here.

    $customerEmail=$request->userEmail;
    $customerPassword=$request->userPassword;
    $link="http://127.0.0.1:8000/user/user-profile-verify/".$customerEmail."/".$hash.""; 
    

      $messageBody = '<html><body>';
     $messageBody .="<h1>Hi , ".$request->userName." Successfully Registration for ShopnShip.com. "."Click This link for verification  "."<a href=".$link.">Please Click Here</a></h1>";
      $messageBody .="<br>";
      $messageBody .= '<h3>Your Suite Number And Suite Address</h3>';
      $messageBody .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
      $messageBody .= "<tr style='background: #eee;'><td><strong>Name:</strong> </td><td>" . $name . "</td></tr>";
      $messageBody .= "<tr><td><strong>Address1:</strong> </td><td>" . $swiftAddress->sweetAddress . "</td></tr>";
      $messageBody .= "<tr><td><strong>Address2:</strong> </td><td>" .'Suite ' .$swiftNumber . "</td></tr>";
      $messageBody .= "<tr><td><strong>Country:</strong> </td><td>" . $swiftAddress->sweetCountry . "</td></tr>";
      $messageBody .= "<tr><td><strong>City:</strong> </td><td>" . $swiftAddress->sweetCity . "</td></tr>";
      $messageBody .= "<tr><td><strong>State:</strong> </td><td>" . $swiftAddress->sweetState . "</td></tr>";
      $messageBody .= "<tr><td><strong>Zip Code:</strong> </td><td>" . $swiftAddress->sweetHouse . "</td></tr>";
      $messageBody .= "<tr><td><strong>House && Road No:</strong> </td><td>" . $swiftAddress->sweetRoad . "</td></tr>";
      $messageBody .= "<tr><td><strong>Phone Number:</strong> </td><td>" . $swiftAddress->sweetPhone . "</td></tr>";
      $messageBody .= "</table>";
      $messageBody .= "</body></html>";


    Mail::send([], [],function ($message) use ($customerEmail,$messageBody) {

       $message->from('services@bee-next.com.bd', 'Shop And Ship');
       $message->subject("Registration Information");
       $message->setBody($messageBody, 'text/html');
        $message->to($customerEmail);
    });
    
    $userRegister=DB::table('customer')
                      ->insert($data);
    Session::put('message','Registration Successfully Done!!! Please confirm your Registration by Mail');
    return redirect()->back();
    // echo "<pre/>";
    // print_r($data);
    // exit();

   }

   public function loginUser(Request $request){

      $this->validate($request, [
      'emailOrPhone' => 'required',
      'password' => 'required',
      ]);

      $userEmailOrPhone=$request->emailOrPhone;
      $password=md5($request->password);
      $userInformation=DB::table('customer')
                          ->where('email', $userEmailOrPhone)
                          // ->orWhere('phone',$userEmailOrPhone)
                          ->where('password', $password)
                          ->first();
      // echo "<pre/>";
      // print_r($userInformation);
      // exit();
      
      if ($userInformation) {
          if ($userInformation->status!='1') {
            Session::put('message','Account DeActive By Admin');
            return redirect()->route('showUserLogin');
          }elseif ($userInformation->verify!='1') {
           
            Session::put('message','Please Verify Your Account By Email');
            return redirect()->route('showUserLogin');
          }
          Session::put('userId',$userInformation->id);
          return redirect()->route('shop');
        }else{
          Session::put('message','Email or Password Invalid');
          return redirect()->route('showUserLogin');
        }

   }

   //Register Phone and Email Check Start Here
   public function checkUserEmail(Request $request){

    $userEmail=$request->userEmail;
    $userEmailCheck=DB::table('customer')
                          ->where('email',$userEmail)
                          ->select('customer.email')
                          ->first();
    if(isset($userEmailCheck->email)){
             
            echo json_encode("Found");
    }else{
        echo json_encode("Not Found");
    }


   }

   public function checkUserPhone(Request $request){

    $userPhone=$request->userPhone;
    $userPhoneCheck=DB::table('customer')
                            ->where('phone',$userPhone)
                            ->select('customer.phone')
                            ->first();
    if(isset($userPhoneCheck->phone)){
             
            echo json_encode("Found");
    }else{
        echo json_encode("Not Found");
    }


   }
   //Register phone and Email Check End Here

   public function searchProductSidebar($id,$departmentId,$categoryId){
    
    $departmentInfo=$this->departmentInfo();
    $categoryInfo=DB::table('category')
                        ->get();
    
    $issetSearchSidebar=1;
    $productInfo=DB::table('product as pro')
                    ->join('department as dep','dep.id','=','pro.departmentId')
                    ->join('category as cat','cat.id','=','pro.categoryId')
                    ->join('sub_category as subc','subc.id','=','pro.subcategoryId')
                    ->where('pro.subcategoryId',$id)
                    ->where('pro.departmentId',$departmentId)
                    ->where('pro.categoryId',$categoryId)
                    ->select('pro.*','dep.department_type','cat.category_type','subc.subCategory_type','dep.currency_prefix')
                    ->get();
    $bradeInfo=DB::table('product as pro')
                    ->join('department as dep','dep.id','=','pro.departmentId')
                    ->join('category as cat','cat.id','=','pro.categoryId')
                    ->join('sub_category as subc','subc.id','=','pro.subcategoryId')
                    ->where('pro.subcategoryId',$id)
                    ->where('pro.departmentId',$departmentId)
                    ->where('pro.categoryId',$categoryId)
                    ->select('dep.department_type','cat.category_type','subc.subCategory_type')
                    ->first();
    return view('user.showStore',compact('departmentInfo','categoryInfo','productInfo','issetSearchSidebar','bradeInfo'));
   }

   public function searchProductByProductName(Request $request){

    $departmentId=$request->departmentId;
    $productName=$request->productName;
    $departmentInfo=$this->departmentInfo();
    $categoryInfo=DB::table('category')
                        ->get();
    $issetSearchName=1;
    
    $breadeInfo=DB::table('department')
                    ->where('id',$departmentId)
                    ->first();
    $productInfo=DB::table('product as pro')
                    ->join('department as dep','dep.id','=','pro.departmentId')
                    ->join('category as cat','cat.id','=','pro.categoryId')
                    ->join('sub_category as subc','subc.id','=','pro.subcategoryId')
                    ->where('pro.departmentId',$departmentId)
                    ->Where('pro.productName', 'like', '%' .$productName. '%')
                    ->select('pro.*','dep.department_type','cat.category_type','subc.subCategory_type','dep.currency_prefix')
                    ->get();
    // echo "<pre/>";
    // print_r($productInfo);
    // exit();
    return view('user.showStore',compact('departmentInfo','categoryInfo','productInfo','issetSearchName','breadeInfo','productName'));
   }

   public function addSubscribeUser(Request $request){
    $subscribeUserEmail=$request->subscribeUserEmail;
    $subscribeUserEmailCheck=DB::table('subscribe_user')
                                ->where('subscribeUserEmail',$subscribeUserEmail)
                                ->select('subscribe_user.subscribeUserEmail')
                                ->first();
    if(isset($subscribeUserEmailCheck->subscribeUserEmail)){
             
            echo json_encode("Found");
    }else{

        $data=array();
        $data['subscribeUserEmail']=$request->subscribeUserEmail;
        $data['createTime']=date("Y/m/d");
        $insertSubscribeUser=DB::table('subscribe_user')
                                ->insert($data);
        if ($insertSubscribeUser) {
          echo json_encode("Success");
        }
        
    }

   }

    public function userLogout(){

        Session::put('userId','');
        return redirect()->route('shop');
    }

    public function userProfile(){
      if (Session::get('userId')) {
        $countries=$this->countryName();
        $userId=Session::get('userId');

        $departmentInfo=DB::table('department')
                            ->get();
        $categoryInfo=DB::table('category')
                          ->get();

        $userInfo=DB::table('customer')
                      ->where('customer.id',$userId)
                      ->first();
        $orderInfo=DB::table('ecommerce_order')
                      ->where('ecommerce_order.userId',$userId)
                      ->get();
        
        return view('user.showUserProfile',compact('categoryInfo','departmentInfo','userInfo','countries','orderInfo'));

      }else{

        return redirect()->route('welcome');

      }
    }

    public function userOrder(){
      if (Session::get('userId')) {
        $countries=$this->countryName();
        $userId=Session::get('userId');

        $departmentInfo=DB::table('department')
                            ->get();
        $categoryInfo=DB::table('category')
                          ->get();

        $userInfo=DB::table('customer')
                      ->where('customer.id',$userId)
                      ->first();
        $orderInfo=DB::table('ecommerce_order')
                      ->where('ecommerce_order.userId',$userId)
                      ->get();
        $customOrderInfo=DB::table('custom_order')
                      ->where('custom_order.userId',$userId)
                      ->get();
        $reshippingOrderInfo=DB::table('shipping_order')
                                  ->where('shipping_order.userId',$userId)
                                  ->get();
        return view('user.showUserOrder',compact('categoryInfo','departmentInfo','userInfo','countries','orderInfo','customOrderInfo','reshippingOrderInfo'));

      }else{

        return redirect()->route('welcome');

      }
    }

    public function userReshippingOrder(){
      if (Session::get('userId')) {
        $countries=$this->countryName();
        $userId=Session::get('userId');

        $departmentInfo=DB::table('department')
                            ->get();
        $categoryInfo=DB::table('category')
                          ->get();
        $reshippingOrderInfo=DB::table('shipping_order')
                                  ->where('shipping_order.userId',$userId)
                                  ->orderby('id','DESC')
                                  ->get();
        
        return view('user.showUserReshippingOrder',compact('categoryInfo','departmentInfo','countries','reshippingOrderInfo'));

      }else{

        return redirect()->route('welcome');

      }
    }

    public function userCustomOrder(){
      if (Session::get('userId')) {
        $countries=$this->countryName();
        $userId=Session::get('userId');

        $departmentInfo=DB::table('department')
                            ->get();
        $categoryInfo=DB::table('category')
                          ->get();
        $customOrderInfo=DB::table('custom_order')
                              ->where('custom_order.userId',$userId)
                              ->orderby('id','DESC')
                              ->get();
        $customOrderOutsideInfo=DB::table('custom_order_outside')
                                    ->where('custom_order_outside.userId',$userId)
                                    ->orderby('id','DESC')
                                    ->get();
        // echo "<pre/>";
        // print_r($customOrderOutsideInfo);
        // exit();
        return view('user.showUserCustomOrder',compact('categoryInfo','departmentInfo','countries','customOrderInfo','customOrderOutsideInfo'));

      }else{

        return redirect()->route('welcome');

      }
    }

    public function userBillingAddress(){
      if (Session::get('userId')) {
        $countries=$this->countryName();
        $userId=Session::get('userId');

        $departmentInfo=DB::table('department')
                            ->get();
        $categoryInfo=DB::table('category')
                          ->get();

        $userInfo=DB::table('customer')
                      ->where('customer.id',$userId)
                      ->first();
        $orderInfo=DB::table('ecommerce_order')
                      ->where('ecommerce_order.userId',$userId)
                      ->get();
        return view('user.showUserBillingAddress',compact('categoryInfo','departmentInfo','userInfo','countries','orderInfo'));

      }else{

        return redirect()->route('welcome');

      }
    }


    public function userShippingAddress(){
      if (Session::get('userId')) {
        $countries=$this->countryName();
        $userId=Session::get('userId');

        $departmentInfo=DB::table('department')
                            ->get();
        $categoryInfo=DB::table('category')
                          ->get();

        $userInfo=DB::table('customer')
                      ->where('customer.id',$userId)
                      ->first();
        $orderInfo=DB::table('ecommerce_order')
                      ->where('ecommerce_order.userId',$userId)
                      ->get();
        return view('user.showUserShippingAddress',compact('categoryInfo','departmentInfo','userInfo','countries','orderInfo'));

      }else{

        return redirect()->route('welcome');

      }
    }


    public function userSuiteAddress(){
      if (Session::get('userId')) {
        $countries=$this->countryName();
        $userId=Session::get('userId');

        $departmentInfo=DB::table('department')
                            ->get();
        $categoryInfo=DB::table('category')
                          ->get();

        $suiteAddress=DB::table('sweet_address')
                            ->where('status',1)
                            ->first();

        $userInfo=DB::table('customer')
                      ->where('customer.id',$userId)
                      ->first();
        $orderInfo=DB::table('ecommerce_order')
                      ->where('ecommerce_order.userId',$userId)
                      ->get();
        return view('user.showUserSuiteAddress',compact('categoryInfo','departmentInfo','userInfo','countries','orderInfo','suiteAddress'));

      }else{

        return redirect()->route('welcome');

      }
    }

    public function updateCustomerPersonalProfile(Request $request){
    $data=array();
    $userId=Session::get('userId');
    $userInfo=DB::table('customer')
                  ->where('customer.id',$userId)
                  ->first();

    if ($request->oldPassword !='' && $request->newPassword !='') {
      
      if ($request->oldPassword != $userInfo->password) {

        Session::put('messageWarningPer','Old Password Can not match !!!');
        return redirect()->back();

      }else{

        $data['password']=md5($request->newPassword);
      }


    }
    $data['firstName']=$request->firstName;
    $data['lastName']=$request->lastName;
    $data['countryCode']=$request->countryCode;
    $data['gender']=$request->gender;
    $data['dof']=$request->dof;
    $data['phone']=$request->phone;
    $data['password']=$userInfo->password;

    // echo "<pre/>";
    // print_r($data);
    // exit();

    $updatePersonalProfile=DB::table('customer')
                                ->where('customer.id',$userId)
                                ->update($data);
    
      Session::put('messagePer','Personal Information Save Successfully !!!');
      return redirect()->back();
   

  }

  public function updateCustomerAddress(Request $request){

      $userId=Session::get('userId');
      $data=array();
      $data['address1']=$request->address1;
      $data['address2']=$request->address2;
      $data['country']=$request->country;
      $data['state']=$request->state;
      $data['zip']=$request->zip;
      $data['city']=$request->city;
      $data['firstName']=$request->firstName;

      // echo "<pre/>";
      // print_r($data);
      // exit();

      $updateAddress=DB::table('customer')
                          ->where('id',$userId)
                          ->update($data);

      Session::put('messagePer','Personal Address Update Successfully !!!');
      return redirect()->back();


    }


    public function updateCustomerShippingAddress(Request $request){

        $userId=Session::get('userId');
        $same=$request->same;
        if ($same=='') {
          
          $data=array();
          $data['bill_name']=$request->bill_name;
          $data['bill_address1']=$request->bill_address1;
          $data['bill_address2']=$request->bill_address2;
          $data['bill_country']=$request->bill_country;
          $data['bill_city']=$request->bill_city;
          $data['bill_state']=$request->bill_state;
          $data['bill_zip']=$request->bill_zip;
          $data['bill_phone']=$request->bill_phone;
          $data['bill_countryCode']=$request->bill_address1;

        }else{

            $userInfo=DB::table('customer')
                            ->where('id',$userId)
                            ->first();
            $data=array();
            $data['bill_name']=$userInfo->firstName;
            $data['bill_address1']=$userInfo->address1;
            $data['bill_address2']=$userInfo->address2;
            $data['bill_country']=$userInfo->country;
            $data['bill_city']=$userInfo->city;
            $data['bill_state']=$userInfo->state;
            $data['bill_zip']=$userInfo->zip;
            $data['bill_phone']=$userInfo->phone;
            $data['bill_countryCode']=$userInfo->countryCode;
        }
        
       

        $updateAddress=DB::table('customer')
                            ->where('id',$userId)
                            ->update($data);

        Session::put('messagePer','Shipping Address Update Successfully !!!');
        return redirect()->back();

    }

    public function singleProduct($id){

        $departmentInfo=DB::table('department')
            ->get();
        $categoryInfo=DB::table('category')
            ->get();
      $singleProductInfo=DB::table('product as pro')
                          ->join('department as dep','dep.id','=','pro.departmentId')
                          ->join('category as cat','cat.id','=','pro.categoryId')
                          ->join('sub_category as subc','subc.id','=','pro.subcategoryId')
                          ->select('pro.*','dep.department_type','cat.category_type','subc.subCategory_type','dep.currency_prefix')
                          ->where('pro.id',$id)
                          ->first();

      $relatedProduct=DB::table('product as pro')
                    ->join('department as dep','dep.id','=','pro.departmentId')
                    ->join('category as cat','cat.id','=','pro.categoryId')
                    ->join('sub_category as subc','subc.id','=','pro.subcategoryId')
                    ->where('pro.departmentId','=',$singleProductInfo->departmentId)
                    ->where('pro.categoryId','=',$singleProductInfo->categoryId)
                    ->where('pro.subcategoryId','=',$singleProductInfo->subcategoryId)
                    ->select('pro.*','dep.department_type','cat.category_type','subc.subCategory_type','dep.currency_prefix')
                    ->limit(6)
                    ->get();
      $companyIn=DB::table('company_setting')
                        ->where('status',1)
                        ->first();
      // echo "<pre/>";
      // print_r($relatedProduct);
      // exit();

      return view('user.singleProduct',compact('singleProductInfo','relatedProduct','departmentInfo','categoryInfo','companyIn'));

    }

    public function addToCart(Request $request){

      $id=intval($request->id);
      $product=DB::table('product as pro')
                    ->where('pro.id',$id)
                    ->first();
      $discountCheck=$product->discount;
      if ($discountCheck=='') {
        
        $data=array();
        $data['id']=$product->id;
        $data['name']=$product->productName;
        $data['qty']=1;
        $data['price']=$product->price;
        $data['weight']=1;
        $data['options']['image']=$product->image1;
        
      }else{

        $data=array();
        $data['id']=$product->id;
        $data['name']=$product->productName;
        $data['qty']=1;
        $data['price']=$product->discountPrice;
        $data['weight']=1;
        $data['options']['image']=$product->image1;
        
      }

      $cartInsert=Cart::add($data);
      echo json_encode($cartInsert);
      

      
    }

  public function addToWishlist(Request $request){

      $id=intval($request->id);
      $product=DB::table('product as pro')
                    ->where('pro.id',$id)
                    ->first();

      $discountCheck=$product->discount;
      if ($discountCheck=='') {
        
        $data=array();
        $data['id']=$product->id;
        $data['name']=$product->productName;
        $data['qty']=1;
        $data['price']=$product->price;
        $data['weight']=1;
        $data['options']['image']=$product->image1;
        
      }else{

        $data=array();
        $data['id']=$product->id;
        $data['name']=$product->productName;
        $data['qty']=1;
        $data['price']=$product->discountPrice;
        $data['weight']=1;
        $data['options']['image']=$product->image1;
        
      }
      $cartInsert=Cart::instance('wishlist')->add($data);
      echo json_encode($cartInsert);
      

      
    }

  public function showCartValue(){
    // $rowId='51cffac1b173384bc7235ddb3575f42f';
    // Cart::update($rowId,0);
    // exit();
    // $content=Cart::content();
    // return response()->json($content);

    $content=Cart::instance('wishlist')->content();
    return response()->json($content);
  }

  public function showCartPage(){
    
    $departmentInfo=$this->departmentInfo();
    $categoryInfo=DB::table('category')
                        ->get();
    return view('user.showCartPage',compact('departmentInfo','categoryInfo'));
  }

  public function updateCartValue(Request $request){

    $rowId=$request->rowId;
    $qty=$request->qty;
    Cart::update($rowId, $qty);
    return redirect()->back();
  }

  public function deleteCartValue($rowId){
    Cart::update($rowId,0);
    return redirect()->back();
  }


  /* Country name Start Here */

    private function countryName(){

        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"); 

    return $countries;

    }
/* Country name end here */


/*checkout*/

public function showEcommerceOrderCheckout(){
  
  if (Session::get('userId')) {
  $userId=Session::get('userId');
  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                      ->get();
  $shippingCost=DB::table('ecommerce_shipping_cost')
                      ->first();
  $userInfo=DB::table('customer')
                ->where('id',$userId)
                ->first();
  return view('user.showCheckout',compact('departmentInfo','categoryInfo','shippingCost','userInfo'));
  }else{  
      return redirect()->route('showUserLogin');
    }
}


//Order POrtion

public function saveEcommerceOrder(Request $request){
  // Cart::destroy();
  $data=array();
  $data['productName']=implode(', ', $request->productName);
  $data['productPrice']=implode(', ', $request->productPrice);
  $data['productQun']=implode(', ', $request->productQun);
  $data['totalAmount']=$request->totalAmount;
  $data['shippingAmount']=$request->shippingAmount;
  $data['paidStatus']=0;
  $data['firstName']=$request->firstName;
  $data['lastName']=$request->lastName;
  $data['phone']=$request->phone;
  $data['email']=$request->email;
  $data['address']=$request->address;
  $data['country']=$request->country;
  $data['city']=$request->city;
  $data['state']=$request->state;
  $data['zip']=$request->zip;
  $data['shippingAddress']=$request->shippingAddress;
  $data['billingAddress']=$request->billingAddress;
  $data['orderStatus']='pending';
  $data['userId']=Session::get('userId');
  $data['orderDate']=date("Y/m/d");
  $data['OrderNumber']=date("YmdHis").rand(11111,99999);
  $insertEcommerceOrder=DB::table('ecommerce_order')
                            ->insert($data);
  if ($insertEcommerceOrder) {
   Cart::destroy();
   Session::put('messageOrder','Order Submit Successfully Done.Please pay your order amount !!!');
   return redirect()->route('userProfile');
  }

}

public function viewEcommerceOrder($id){


 if (Session::get('userId')) {
  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                      ->get();
  $userId=Session::get('userId');
  $orderInfo=DB::table('ecommerce_order')
                ->where('ecommerce_order.userId',$userId)
                ->where('ecommerce_order.id',$id)
                ->first();
  $userInfo=DB::table('customer as cus')
                ->join('ecommerce_order as e','cus.id','=','e.userId')
                ->where('e.id',$id)
                ->select('cus.*')
                ->first();

  $name=array();
  $productName=  explode(',', $orderInfo->productName);
      foreach($productName as $arType){
          $name[]=$arType;      

      }

  $qun=array();
  $productQuantity=  explode(',', $orderInfo->productQun);
      foreach($productQuantity as $arType){
          $qun[]=$arType;      

      }

  $price=array();
  $productPrice=  explode(',', $orderInfo->productPrice);
      foreach($productPrice as $arType){
          $price[]=$arType;      

      }
  return view('user.viewEcommerceOrder',compact('orderInfo','departmentInfo','categoryInfo','name','qun','price','userInfo'));

  }else{  
      return redirect()->route('welcome');
    }


}

//Custom Order Start Here
public function saveCustomOrder(Request $request){

  $userId=Session::get('userId');
  $emailInfo=DB::table('customer')
                  ->where('id',$userId)
                  ->first();

  $customerEmail=$emailInfo->email;
  $data=array();
  $shippingType=$request->bill_country;
  if ($shippingType=='Bangladesh') {
    $data['shippingType']='Local Shipping';
  }else{
    $data['shippingType']='International Shipping';
  }
  $data['userId']=$userId;
  $data['orderType']=$request->orderType;
  $data['productType']=$request->productType;
  $data['productLink']=$request->productLink;
  $data['productName']=$request->productName;
  $data['productDescription']=$request->productDescription;
  $data['productEstimatePrice']=$request->productEstimatePrice;
  $data['productQuantity']=$request->productQuantity;
  $data['bill_name']=$request->bill_name;
  $data['bill_address1']=$request->bill_address1;
  $data['bill_address2']=$request->bill_address2;
  $data['bill_country']=$request->bill_country;
  $data['bill_city']=$request->bill_city;
  $data['bill_state']=$request->bill_state;
  $data['bill_zip']=$request->bill_zip;
  $data['bill_countryCode']=$request->bill_countryCode;
  $data['bill_phone']=$request->bill_phone;
  $data['initialAmount']=$request->initialAmount;
  $data['initialAmountStatus']=0;
  $generateNumber=date("YmdHis").rand(11111,99999);
  $data['orderNumber']=$generateNumber;
  $data['orderStatus']='pending';
  $data['marketPlaceLink']=$request->marketPlaceLink;
  $data['orderDate']=date("Y-m-d");
 // echo "<pre/>";
 // print_r($data);
 // exit();
  $image1=$request->file('userImage1');
    if ($image1) {
        $image_name=date("YmdHis").rand(11111,99999);
        $ext=strtolower($image1->getClientOriginalExtension());
        $image_full_name=$image_name.'.'.$ext;
        $upload_path='user/orderImage/';
        $image_url=$upload_path.$image_full_name;
        $success=$image1->move($upload_path,$image_full_name); 
        $data['userImage1']=$image_url; 
    }
  $image2=$request->file('userImage2');
    if ($image2) {
        $image_name=date("YmdHis").rand(11111,99999);
        $ext=strtolower($image2->getClientOriginalExtension());
        $image_full_name=$image_name.'.'.$ext;
        $upload_path='user/orderImage/';
        $image_url=$upload_path.$image_full_name;
        $success=$image2->move($upload_path,$image_full_name); 
        $data['userImage2']=$image_url; 
    }

  $image3=$request->file('userImage3');
    if ($image3) {
        $image_name=date("YmdHis").rand(11111,99999);
        $ext=strtolower($image3->getClientOriginalExtension());
        $image_full_name=$image_name.'.'.$ext;
        $upload_path='user/orderImage/';
        $image_url=$upload_path.$image_full_name;
        $success=$image3->move($upload_path,$image_full_name); 
        $data['userImage3']=$image_url; 
    }

  $image4=$request->file('userImage4');
    if ($image4) {
        $image_name=date("YmdHis").rand(11111,99999);
        $ext=strtolower($image4->getClientOriginalExtension());
        $image_full_name=$image_name.'.'.$ext;
        $upload_path='user/orderImage/';
        $image_url=$upload_path.$image_full_name;
        $success=$image4->move($upload_path,$image_full_name); 
        $data['userImage4']=$image_url; 
    }

  $image5=$request->file('userImage5');
    if ($image5) {
        $image_name=date("YmdHis").rand(11111,99999);
        $ext=strtolower($image5->getClientOriginalExtension());
        $image_full_name=$image_name.'.'.$ext;
        $upload_path='user/orderImage/';
        $image_url=$upload_path.$image_full_name;
        $success=$image5->move($upload_path,$image_full_name); 
        $data['userImage5']=$image_url; 
    }
  // echo "<pre/>";
  // print_r($data);
  // exit();

  $messageBody = '<html><body>';
  $messageBody .="Your Order Successfully Send To Ushopnship Admin. Less Then Two Days You Get Information. ";
  $messageBody .="<br>";
  $messageBody .= '<h1> Order Information </h1>';
  $messageBody .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
  $messageBody .= "<tr style='background: #eee;'><td><strong>Order Number :</strong> </td><td>" . $generateNumber . "</td></tr>";
  $messageBody .= "<tr><td><strong>Product Type:</strong> </td><td>" . $request->productType . "</td></tr>";
  $messageBody .= "<tr><td><strong>Product Link:</strong> </td><td>" . $request->productLink . "</td></tr>";
  $messageBody .= "<tr><td><strong>Product Name:</strong> </td><td>" . $request->productName . "</td></tr>";
  $messageBody .= "<tr><td><strong>Product Description:</strong> </td><td>" . $request->productDescription . "</td></tr>";
  $messageBody .= "<tr><td><strong>Product Estimate Price:</strong> </td><td>" . $request->productEstimatePrice . "</td></tr>";
  $messageBody .= "<tr><td><strong>Product Quantity:</strong> </td><td>" . $request->productQuantity . "</td></tr>";
  $messageBody .= "</table></br>";
  $messageBody .= '<h1> Shipping Address Information </h1>';
  $messageBody .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
  $messageBody .= "<tr style='background: #eee;'><td><strong>Name :</strong> </td><td>" . $request->bill_name . "</td></tr>";
  $messageBody .= "<tr><td><strong>Phone :</strong> </td><td>" . $request->bill_countryCode.$request->bill_phone . "</td></tr>";
  $messageBody .= "<tr><td><strong>Address 1:</strong> </td><td>" .$request->bill_address1 . "</td></tr>";
  $messageBody .= "<tr><td><strong>Address 2:</strong> </td><td>" . $request->bill_address2 . "</td></tr>";
  $messageBody .= "<tr><td><strong>Country :</strong> </td><td>" . $request->bill_country . "</td></tr>";
  $messageBody .= "<tr><td><strong>City :</strong> </td><td>" . $request->bill_city . "</td></tr>";
  $messageBody .= "<tr><td><strong>State :</strong> </td><td>" . $request->bill_state . "</td></tr>";
  $messageBody .= "<tr><td><strong>Zip Code :</strong> </td><td>" . $request->bill_zip . "</td></tr>";
  $messageBody .= "</table>";
  $messageBody .= "</body></html>";


Mail::send([], [],function ($message) use ($customerEmail,$messageBody) {

   $message->from('services@bee-next.com.bd', 'Shop And Ship');
   $message->subject("Custom Order Information");
   $message->setBody($messageBody, 'text/html');
    $message->to($customerEmail);
});

$insertOrder=DB::table('custom_order')
                  ->insert($data);

if ($insertOrder) {
  Session::put('message','Custom Order Add Successfully Done.Please Check your Mail for more information.!!!');
  return redirect()->back();
}



}

public function saveCustomOrderOutside(Request $request){

$userId=Session::get('userId');
$emailInfo=DB::table('customer')
                  ->where('id',$userId)
                  ->first();

  $customerEmail=$emailInfo->email;
$data=array();
$generateNumber=date("YmdHis").rand(11111,99999);
$data['orderNumber']=$generateNumber;
$data['userId']=$userId;
$data['status']='pending';
$data['productName']=$request->productName;
$data['productDescription']=$request->productDescription;
$data['quantity']=$request->quantity;
$data['productLink']=$request->productLink;
$data['initialPayment']=$request->initialAmount;
$data['initialPaymentStatus']=0;
$data['orderDate']=date("Y-m-d");

$data['bill_name']=$request->bill_name;
$data['bill_address1']=$request->bill_address1;
$data['bill_address2']=$request->bill_address2;
$data['bill_country']=$request->bill_country;
$data['bill_city']=$request->bill_city;
$data['bill_state']=$request->bill_state;
$data['bill_zip']=$request->bill_zip;
$data['bill_countryCode']=$request->bill_countryCode;
$data['bill_phone']=$request->bill_phone;

$image1=$request->file('userImage1');
    if ($image1) {
        $image_name=date("YmdHis").rand(11111,99999);
        $ext=strtolower($image1->getClientOriginalExtension());
        $image_full_name=$image_name.'.'.$ext;
        $upload_path='user/orderImage/';
        $image_url=$upload_path.$image_full_name;
        $success=$image1->move($upload_path,$image_full_name); 
        $data['userImage1']=$image_url; 
    }
  $image2=$request->file('userImage2');
    if ($image2) {
        $image_name=date("YmdHis").rand(11111,99999);
        $ext=strtolower($image2->getClientOriginalExtension());
        $image_full_name=$image_name.'.'.$ext;
        $upload_path='user/orderImage/';
        $image_url=$upload_path.$image_full_name;
        $success=$image2->move($upload_path,$image_full_name); 
        $data['userImage2']=$image_url; 
    }

  $image3=$request->file('userImage3');
    if ($image3) {
        $image_name=date("YmdHis").rand(11111,99999);
        $ext=strtolower($image3->getClientOriginalExtension());
        $image_full_name=$image_name.'.'.$ext;
        $upload_path='user/orderImage/';
        $image_url=$upload_path.$image_full_name;
        $success=$image3->move($upload_path,$image_full_name); 
        $data['userImage3']=$image_url; 
    }

  $image4=$request->file('userImage4');
    if ($image4) {
        $image_name=date("YmdHis").rand(11111,99999);
        $ext=strtolower($image4->getClientOriginalExtension());
        $image_full_name=$image_name.'.'.$ext;
        $upload_path='user/orderImage/';
        $image_url=$upload_path.$image_full_name;
        $success=$image4->move($upload_path,$image_full_name); 
        $data['userImage4']=$image_url; 
    }

  $image5=$request->file('userImage5');
    if ($image5) {
        $image_name=date("YmdHis").rand(11111,99999);
        $ext=strtolower($image5->getClientOriginalExtension());
        $image_full_name=$image_name.'.'.$ext;
        $upload_path='user/orderImage/';
        $image_url=$upload_path.$image_full_name;
        $success=$image5->move($upload_path,$image_full_name); 
        $data['userImage5']=$image_url; 
    }

    $messageBody = '<html><body>';
  $messageBody .="Your Order Successfully Send To Ushopnship Admin.";
  $messageBody .="<br>";
  $messageBody .= '<h1> Order Information </h1>';
  $messageBody .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
  $messageBody .= "<tr style='background: #eee;'><td><strong>Order Number :</strong> </td><td>" . $generateNumber . "</td></tr>";
  $messageBody .= "<tr><td><strong>Product Link:</strong> </td><td>" . $request->productLink . "</td></tr>";
  $messageBody .= "<tr><td><strong>Product Name:</strong> </td><td>" . $request->productName . "</td></tr>";
  $messageBody .= "<tr><td><strong>Product Description:</strong> </td><td>" . $request->productDescription . "</td></tr>";
  $messageBody .= "</table></br>";
  $messageBody .= '<h1> Shipping Address Information </h1>';
  $messageBody .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
  $messageBody .= "<tr style='background: #eee;'><td><strong>Name :</strong> </td><td>" . $request->bill_name . "</td></tr>";
  $messageBody .= "<tr><td><strong>Phone :</strong> </td><td>" . $request->bill_countryCode.$request->bill_phone . "</td></tr>";
  $messageBody .= "<tr><td><strong>Address 1:</strong> </td><td>" .$request->bill_address1 . "</td></tr>";
  $messageBody .= "<tr><td><strong>Address 2:</strong> </td><td>" . $request->bill_address2 . "</td></tr>";
  $messageBody .= "<tr><td><strong>Country :</strong> </td><td>" . $request->bill_country . "</td></tr>";
  $messageBody .= "<tr><td><strong>City :</strong> </td><td>" . $request->bill_city . "</td></tr>";
  $messageBody .= "<tr><td><strong>State :</strong> </td><td>" . $request->bill_state . "</td></tr>";
  $messageBody .= "<tr><td><strong>Zip Code :</strong> </td><td>" . $request->bill_zip . "</td></tr>";
  $messageBody .= "</table>";
  $messageBody .= "</body></html>";


Mail::send([], [],function ($message) use ($customerEmail,$messageBody) {

   $message->from('services@bee-next.com.bd', 'Shop And Ship');
   $message->subject("Custom Order Information");
   $message->setBody($messageBody, 'text/html');
    $message->to($customerEmail);
});

    $insertOrder=DB::table('custom_order_outside')
                  ->insert($data);

if ($insertOrder) {
  Session::put('message','Custom Order Add Successfully Done.Please Check your Mail for more information.!!!');
  return redirect()->back();
}

}

public function showUserCustomOrderImage($id){

$userId=Session::get('userId');
$departmentInfo=$this->departmentInfo();
$categoryInfo=DB::table('category')
                  ->get();
if ($userId) {
  
  $orderImage=DB::table('custom_order')
                    ->where('id',$id)
                    ->first();
  return view('userCustomOrder.showUserCustomOrderImage',compact('orderImage','departmentInfo','categoryInfo'));

}else{

  return redirect()->back();
}

}

public function showUserCustomOrderImageOutside($id){

$userId=Session::get('userId');
$departmentInfo=$this->departmentInfo();
$categoryInfo=DB::table('category')
                  ->get();
if ($userId) {
  
  $orderImage=DB::table('custom_order_outside')
                    ->where('id',$id)
                    ->first();
  // echo "<pre/>";
  // print_r($orderImage);
  // exit();
  return view('userCustomOrderOutside.showUserCustomOrderImageOutside',compact('orderImage','departmentInfo','categoryInfo'));

}else{

  return redirect()->back();
}

}


public function viewCustomerPendingCustomOrder($id){

$departmentInfo=$this->departmentInfo();
$categoryInfo=DB::table('category')
                  ->get();
$userId=Session::get('userId');
$pendingOrder=DB::table('custom_order as co')
                  ->join('customer as cus','cus.id','=','co.userId')
                  ->where('co.orderStatus','pending')
                  ->where('co.id',$id)
                  ->where('co.userId',$userId)
                  ->select('co.*','cus.firstName','cus.email','cus.countryCode','cus.phone','cus.address1')
                  ->first();
if ($pendingOrder) {
  return view('userCustomOrder.viewCustomerPendingCustomOrder',compact('pendingOrder','departmentInfo','categoryInfo'));
}else{
  return redirect()->back();
}

}


public function editCustomerPreProcessingCustomOrder($id){

$departmentInfo=$this->departmentInfo();
$categoryInfo=DB::table('category')
                  ->get();
$countries=$this->countryName();

  $userId=Session::get('userId');
  $orderInfo=DB::table('custom_order')
                  ->where('orderStatus','userprocessing')
                  ->where('userId',$userId)
                  ->first();
  $userInfo=DB::table('custom_order')
                  ->where('id',$id)
                  ->first();
  $additionalPhotoPrice=DB::table('order_additional_photo_price')
                            ->get();
  $shippingBoxCategory=DB::table('shipping_box_category')
                        ->get();
  $shippingShippingCategoryLocal=DB::table('order_shipping_sub_category')
                                      ->where('categoryId',3)
                                      ->get();
  $shippingShippingCategoryInternational=DB::table('order_shipping_sub_category')
                                            ->where('categoryId',2)
                                            ->get();
  $packagePrice=DB::table('outside_box_price')
                      ->get();
  $paymentInformation=DB::table('inside_shipping_setting')
                                  ->where('status',1)
                                  ->first();

if ($orderInfo) {
  return view('userCustomOrder.editCustomerPreProcessingCustomOrder',compact('orderInfo','departmentInfo','countries','categoryInfo','additionalPhotoPrice','shippingBoxCategory','shippingShippingCategoryLocal','shippingShippingCategoryInternational','id','userInfo','paymentInformation','packagePrice'));
}else{
  return redirect()->back();
}

}

public function paySecondPayment(Request $request){

  $id=$request->id;
  $data=array();
  $data['adminProductFinalAmountStatus']=1;
  $data['orderStatus']='adminprocessing';
  $updatePayment=DB::table('custom_order')
                      ->where('id',$id)
                      ->update($data);
  return redirect()->back();


}

public function cancelCustomOrder(Request $request){

  $id=$request->id;
  $data=array();
  $data['orderStatus']='cancel';
  $updatePayment=DB::table('custom_order')
                      ->where('id',$id)
                      ->update($data);
  return redirect()->back();
}

public function cancelReshippingOrder(Request $request){

  $id=$request->id;
  $data=array();
  $data['status']='cancel';
  $updatePayment=DB::table('shipping_order')
                      ->where('id',$id)
                      ->update($data);
  return redirect()->back();

}

public function paySecondPaymentOutside(Request $request){

  $id=$request->id;
  $data=array();
  $data['finalAmountStatus']=1;
  $data['status']='active';
  $updatePayment=DB::table('custom_order_outside')
                      ->where('id',$id)
                      ->update($data);
  return redirect()->back();


}

public function payThirdPaymentOutside(Request $request){

  $id=$request->id;
  $data=array();
  $data['shippingPaymentPriceStatus']=1;
  $data['status']='complete';
  $updatePayment=DB::table('custom_order_outside')
                      ->where('id',$id)
                      ->update($data);
  return redirect()->back();


}

public function updateCustomerPreProcessingCustomOrder(Request $request){

  $id=$request->id;
  $insuranceAmount=$request->insuranceAmount;
  $insuranceRange=$request->insuranceRange;
  $totalAmount=$request->customerSetTotalAmount;



  $pictureAmount=$request->shippingAdditionalPicture;
  $getPictureQuantity=DB::table('order_additional_photo_price')
                          ->where('photoPrice',$pictureAmount)
                          ->first();
  $packageAmount=$request->shippingPackagePrice;
  $getPackageName=DB::table('outside_box_price')
                          ->where('boxPrice',$packageAmount)
                          ->first();
 
  $data=array();
  if ($totalAmount>$insuranceRange) {
    $set=($totalAmount/$insuranceRange)*$insuranceAmount;
    $data['finalInsuranceAmount']=$set;
  }else{

    $data['finalInsuranceAmount']=$insuranceAmount;
  }
  $data['additionalPicture']=$getPictureQuantity->photoQuantity;
  // $data['boxCondition']=$request->boxCondition;
  $personalUse=$request->personalUse;
  if ($personalUse=='') {
    $data['personalUse']=0;
  }else{
    $data['personalUse']=$request->personalUse;
  }
 
  $fragileStickerPrice=$request->fragileStickerPrice;
  $urgentShipPrice=$request->urgentShipPrice;
  if ($fragileStickerPrice=='') {
    $data['useSticker']=0;
  }else{
    $data['useSticker']=1;
    $data['fragileStickerAmount']=$fragileStickerPrice;
  }

  if ($urgentShipPrice=='') {
    $data['useUrgent']=0;
  }else{
    $data['useUrgent']=1;
    $data['setUrgentPrice']=$urgentShipPrice;
  }
  $data['boxCondition']=$request->boxCondition;
  $data['finalTotalAmount']=$request->customerSetTotalAmount;
  // $data['shippingShippingCategory']=$shippingCategoryName->categoryName;
  $data['shippingSubCategory']=$request->shippingShippingCategory;
  $data['bill_name']=$request->bill_name;
  $data['bill_address1']=$request->bill_address1;
  $data['bill_address2']=$request->bill_address2;
  $data['bill_country']=$request->bill_country;
  $data['bill_city']=$request->bill_city;
  $data['bill_state']=$request->bill_state;
  $data['bill_zip']=$request->bill_zip;
  $data['bill_countryCode']=$request->bill_countryCode;
  $data['bill_phone']=$request->bill_phone;
  $data['packageSize']=$getPackageName->boxSize;
  $data['packagePrice']=$packageAmount;
  $data['orderStatus']='processing';
  $data['submitDate']=date("Y/m/d");

  // echo "<pre/>";
  // print_r($data);
  // exit();
  $updateProcessingOrder=DB::table('custom_order')
                            ->where('id',$id)
                            ->update($data);
  if ($updateProcessingOrder) {
   
    return redirect()->route('userCustomOrder');

  }

}

public function editCustomerWaitingPaymentCustomOrder($id){

$departmentInfo=$this->departmentInfo();
$categoryInfo=DB::table('category')
                  ->get();
$userId=Session::get('userId');
// echo $userId;
// exit();
$pendingOrder=DB::table('custom_order as co')
                  ->join('customer as cus','cus.id','=','co.userId')
                  ->where('co.orderStatus','waitingpayment')
                  ->where('co.id',$id)
                  ->where('co.userId',$userId)
                  ->select('co.*','cus.firstName','cus.email','cus.countryCode','cus.phone','cus.address1')
                  ->first();


if ($pendingOrder) {
  return view('userCustomOrder.editCustomerWaitingPaymentCustomOrder',compact('pendingOrder','departmentInfo','categoryInfo','id'));
}else{
  return redirect()->back();
}

}

public function updateCustomerWaitingPaymentCustomOrder(Request $request){

  $id=$request->id;
  // echo $id;
  // exit();
  $data=array();
  $data['orderStatus']='active';
  $update=DB::table('custom_order')
                ->where('id',$id)
                ->update($data);
                
  return redirect()->route('userCustomOrder');
}

public function viewCustomerActiveCustomOrder($id){

$departmentInfo=$this->departmentInfo();
$categoryInfo=DB::table('category')
                  ->get();
$userId=Session::get('userId');
// echo $userId;
// exit();
$pendingOrder=DB::table('custom_order as co')
                  ->join('customer as cus','cus.id','=','co.userId')
                  ->where('co.orderStatus','active')
                  ->where('co.id',$id)
                  ->where('co.userId',$userId)
                  ->select('co.*','cus.firstName','cus.email','cus.countryCode','cus.phone','cus.address1')
                  ->first();


if ($pendingOrder) {
  return view('userCustomOrder.viewCustomerActiveCustomOrder',compact('pendingOrder','departmentInfo','categoryInfo','id'));
}else{
  return redirect()->back();
}

}
public function viewCustomerCompleteCustomOrder($id){

$departmentInfo=$this->departmentInfo();
$categoryInfo=DB::table('category')
                  ->get();
$userId=Session::get('userId');
// echo $userId;
// exit();
$pendingOrder=DB::table('custom_order as co')
                  ->join('customer as cus','cus.id','=','co.userId')
                  ->where('co.orderStatus','complete')
                  ->where('co.id',$id)
                  ->where('co.userId',$userId)
                  ->select('co.*','cus.firstName','cus.email','cus.countryCode','cus.phone','cus.address1')
                  ->first();


if ($pendingOrder) {
  return view('userCustomOrder.viewCustomerCompleteCustomOrder',compact('pendingOrder','departmentInfo','categoryInfo','id'));
}else{
  return redirect()->back();
}

}
public function getBoxSubCategory(Request $request){

        $id=intval($request->id);

       $Info=DB::table('order_box_sub_category')
                            ->where('categoryId',$id)
                            ->get();
        echo json_encode($Info);
    }

public function getShippingSubCategory(Request $request){

        $id=intval($request->id);

       $Info=DB::table('order_shipping_sub_category')
                            ->where('categoryId',$id)
                            ->get();
        echo json_encode($Info);
    }
//Custom Order End Here


//Shipping Order Portion Start Here

public function editCustomerPendingShippingOrder($id){
  $departmentInfo=$this->departmentInfo();
  $countries=$this->countryName();
  $categoryInfo=DB::table('category')
                  ->get();
  $userId=Session::get('userId');
  $orderInfo=DB::table('shipping_order')
                  ->where('status','pending')
                  ->where('userId',$userId)
                  ->first();
  $userInfo=DB::table('customer')
                  ->where('id',$userId)
                  ->first();
  $additionalPhotoPrice=DB::table('shipping_additional_photo_price')
                            ->get();
  $shippingBoxCategory=DB::table('shipping_box_category')
                        ->get();
  $shippingShippingCategoryLocal=DB::table('shipping_shipping_sub_category')
                                      ->where('categoryId',1)
                                      ->get();
  $shippingShippingCategoryInternational=DB::table('shipping_shipping_sub_category')
                                            ->where('categoryId',2)
                                            ->get();
  $packagePrice=DB::table('inside_box_price')
                      ->get();
  $paymentInformation=DB::table('inside_shipping_setting')
                                  ->where('status',1)
                                  ->first();
  if ($orderInfo) {
    return view('userShippingOrder.editCustomerPendingShippingOrder',compact('orderInfo','departmentInfo','countries','categoryInfo','additionalPhotoPrice','shippingBoxCategory','shippingShippingCategoryLocal','shippingShippingCategoryInternational','id','userInfo','paymentInformation','packagePrice'));
  }
}

public function updateCustomerProcessingShippingOrder(Request $request){

  $id=$request->id;
  $pictureAmount=$request->shippingAdditionalPicture;
  $getPictureQuantity=DB::table('shipping_additional_photo_price')
                          ->where('photoPrice',$pictureAmount)
                          ->first();
  $packageAmount=$request->shippingPackagePrice;
  $getPackageName=DB::table('inside_box_price')
                          ->where('boxPrice',$packageAmount)
                          ->first();
 
  $data=array();
  $data['shippingAdditionalPicture']=$getPictureQuantity->photoQuantity;
  $data['boxCondition']=$request->boxCondition;
  $personalUse=$request->personalUse;
  if ($personalUse=='') {
    $data['personalUse']=0;
  }else{
    $data['personalUse']=$request->personalUse;
  }
 
  $fragileStickerPrice=$request->fragileStickerPrice;
  $urgentShipPrice=$request->urgentShipPrice;
  if ($fragileStickerPrice=='') {
    $data['useSticker']=0;
  }else{
    $data['useSticker']=1;
    $data['setStickerPrice']=$fragileStickerPrice;
  }

  if ($urgentShipPrice=='') {
    $data['useUrgent']=0;
  }else{
    $data['useUrgent']=1;
    $data['setUrgentPrice']=$urgentShipPrice;
  }
  $data['setAdditionalPhotoPrice']=$request->setAdditionalPhotoPrice;
  $data['customerSetTotalAmount']=$request->customerSetTotalAmount;
  // $data['shippingShippingCategory']=$shippingCategoryName->categoryName;
  $data['shippingShippingSubCategory']=$request->shippingShippingCategory;
  $data['bill_name']=$request->bill_name;
  $data['bill_address1']=$request->bill_address1;
  $data['bill_address2']=$request->bill_address2;
  $data['bill_country']=$request->bill_country;
  $data['bill_city']=$request->bill_city;
  $data['bill_state']=$request->bill_state;
  $data['bill_zip']=$request->bill_zip;
  $data['bill_countryCode']=$request->bill_countryCode;
  $data['bill_phone']=$request->bill_phone;
  $data['packageName']=$getPackageName->boxSize;
  $data['packagePrice']=$packageAmount;
  $data['status']='processing';

  // echo "<pre/>";
  // print_r($data);
  // exit();
  $updateProcessingOrder=DB::table('shipping_order')
                            ->where('id',$id)
                            ->orderby('id','DESC')
                            ->update($data);
  if ($updateProcessingOrder) {
   
    return redirect()->route('userReshippingOrder');

  }
  
}
public function viewCustomerProcessingShippingOrder(Request $request){
  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                  ->get();
  $userId=Session::get('userId');
  $orderInfo=DB::table('shipping_order')
                  ->where('status','processing')
                  ->where('userId',$userId)
                  ->first();
  return view('userShippingOrder.viewCustomerProcessingShippingOrder',compact('orderInfo','departmentInfo','categoryInfo'));
}

public function editCustomerWaitingPaymentShippingOrder($id){
  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                  ->get();
  $userId=Session::get('userId');
  $orderInfo=DB::table('shipping_order')
                  ->where('status','waitingpayment')
                  ->where('userId',$userId)
                  ->where('id',$id)
                  ->first();
  return view('userShippingOrder.editCustomerWaitingPaymentShippingOrder',compact('orderInfo','departmentInfo','categoryInfo'));
}

public function viewCustomerActiveShippingOrder($id){
  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                  ->get();
  $userId=Session::get('userId');
  $orderInfo=DB::table('shipping_order')
                  ->where('status','active')
                  ->where('userId',$userId)
                  ->where('id',$id)
                  ->first();
  return view('userShippingOrder.viewCustomerActiveShippingOrder',compact('orderInfo','departmentInfo','categoryInfo'));
}

public function viewCustomerCompleteShippingOrder($id){
  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                  ->get();
  $userId=Session::get('userId');
  $orderInfo=DB::table('shipping_order')
                  ->where('status','complete')
                  ->where('userId',$userId)
                  ->where('id',$id)
                  ->first();
  return view('userShippingOrder.viewCustomerCompleteShippingOrder',compact('orderInfo','departmentInfo','categoryInfo'));
}

public function updateCustomerWaitingPaymentShippingOrder(Request $request){

    $id=$request->id;
    $data=array();
    $data['status']='active';
    $data['finalAmountStatus']=1;
    $update=DB::table('shipping_order')
                  ->where('id',$id)
                  ->update($data);
    $customerEmail=$request->userEmail;
    $address='First Address: '.$request->bill_address1.', Second Address: '.$request->bill_address2.', Country: '.$request->bill_country.', State: '.$request->bill_state.', City: '.$request->bill_city.', Zip: '.$request->bill_zip;
    
    $messageBody = '<html><body>';
    $messageBody .="<br>";
    $messageBody .= '<h1>Final Shipping Invoice</h1>';
    $messageBody .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
    $messageBody .= "<tr style='background: #eee;'><td><strong>Shipment Weight:</strong> </td><td>" . $request->weight . "</td></tr>";
    $messageBody .= "<tr><td><strong>Ship To Address:</strong> </td><td>" . $address . "</td></tr>";
    $messageBody .= "<tr><td><strong>Shipping Cost:</strong> </td><td>" .$request->shippingCost . "</td></tr>";
    $messageBody .= "<tr><td><strong>Service Cost:</strong> </td><td>" . $request->serviceCost . "</td></tr>";
    $messageBody .= "<tr><td><strong>Total Amount:</strong> </td><td>" . $request->totalAmount . "</td></tr>";
    $messageBody .= "</table>";
    $messageBody .= "</body></html>";


    Mail::send([], [],function ($message) use ($customerEmail,$messageBody) {

       $message->from('services@bee-next.com.bd', 'Shop And Ship');
       $message->subject("Reshipping Information");
       $message->setBody($messageBody, 'text/html');
        $message->to($customerEmail);
    });

    return redirect()->route('userOrder');
  }
public function getShippingBoxSubCategory(Request $request){

        $id=intval($request->id);

       $Info=DB::table('shipping_box_sub_category')
                            ->where('categoryId',$id)
                            ->get();
        echo json_encode($Info);
    }

public function getShippingShippingSubCategory(Request $request){

        $id=intval($request->id);

       $Info=DB::table('shipping_shipping_sub_category')
                            ->where('categoryId',$id)
                            ->get();
        echo json_encode($Info);
    }
//Shipping Order Portion End Here


//Forget Password Start Here
public function showForgetPassword(){

  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
          ->get();
  Session::put('first','1');
  Session::put('second','0');
  Session::put('third','0');
  $first=1;
  $second=0;
  $third=0;
  return view('user.showForgetPassword',compact('departmentInfo','categoryInfo','first','second','third'));
}

public function sendForgetVerifyCode(Request $request){

  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                      ->get();
  $userEmail=$request->userEmail;
  DB::table('forget_password')
        ->where('email', $userEmail)
        ->delete();

  $userCheck=DB::table('customer')
                  ->where('email',$userEmail)
                  ->first();

  $code=rand (10000 , 99999);
  $data=array();
  $data['email']=$userEmail;
  $data['verifyCode']=$code;

  $messageBody ="Successfully Reset Password shopnship.com. "."Your Verify Code is : ".$code."";
      


    Mail::send([], [],function ($message) use ($userEmail,$messageBody) {

       $message->from('services@bee-next.com.bd', 'Shop And Ship');
       $message->subject("Password Reset Information");
       $message->setBody($messageBody);
       $message->to($userEmail);
    });
  if ($userCheck) {
   
   $first=0;
   $second=1;
   $third=0;
   $email=$userEmail;
   
   $insertForget=DB::table('forget_password')
                    ->insert($data);
    Session::put('emailSucMess','Verify Code Send Your Email.Please Check Your Email.');
   return view('user.showForgetPassword',compact('departmentInfo','categoryInfo','first','second','third','email'));

  }else{
    $first=1;
    $second=0;
    $third=0;
    Session::put('emailMatchError','Can not find any user for this email.Please insert correct email.');
    return view('user.showForgetPassword',compact('departmentInfo','categoryInfo','first','second','third'));
  }

}


public function checkVerify(Request $request){

  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                      ->get();
  $userEmail=$request->userEmail;
  $verifyCode=$request->verifyCode;
  $checkData=DB::table('forget_password')
                  ->where('email',$userEmail)
                  ->where('verifyCode',$verifyCode)
                  ->first();
  if ($checkData) {
   $email=$userEmail;
   $first=0;
   $second=0;
   $third=1;
   return view('user.showForgetPassword',compact('departmentInfo','categoryInfo','first','second','third','email'));
  }else{
   $email=$userEmail;
   $first=0;
   $second=1;
   $third=0;
   Session::put('errorMes','Verify Code Does Not Match.');
   return view('user.showForgetPassword',compact('departmentInfo','categoryInfo','first','second','third','email'));
  }
}

public function savePassword(Request $request){
  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                      ->get();
  $userEmail=$request->userEmail;
  $password=md5($request->password);
  $conPassword=md5($request->conPassword);
  if ($password==$conPassword) {
    $data=array();
    $data['password']=$password;
    $savePassword=DB::table('customer')
                      ->where('email',$userEmail)
                      ->update($data);
    Session::put('message','Password Change Successfully Done');
    return view('user.showUserLogin',compact('departmentInfo','categoryInfo'));

  }else{
    $email=$userEmail;
    $first=0;
    $second=0;
    $third=1;
    Session::put('errorPass','Password and Confirm Password Does not match.');
    return view('user.showForgetPassword',compact('departmentInfo','categoryInfo','first','second','third','email'));
  }
}
//Forget password end Here



//Show All Page
public function showFaqPage(){

  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                      ->get();
  return view('user.showFaqPage',compact('departmentInfo','categoryInfo'));
}

public function showSupportPage(){

  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                      ->get();
  return view('user.showSupportPage',compact('departmentInfo','categoryInfo'));
}

public function showReportPage(){

  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                      ->get();
  return view('user.showReportPage',compact('departmentInfo','categoryInfo'));
}
public function showStoreLocationPage(){

  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                      ->get();
  return view('user.showStoreLocationPage',compact('departmentInfo','categoryInfo'));
}
public function showPrivacyPage(){

  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                      ->get();
  return view('user.showPrivacyPage',compact('departmentInfo','categoryInfo'));
}
public function showTermsPage(){

  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                      ->get();
  return view('user.showTermsPage',compact('departmentInfo','categoryInfo'));
}
public function showWarrantyPage(){

  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                      ->get();
  return view('user.showWarrantyPage',compact('departmentInfo','categoryInfo'));
}
public function showRefundPage(){

  $departmentInfo=$this->departmentInfo();
  $categoryInfo=DB::table('category')
                      ->get();
  return view('user.showRefundPage',compact('departmentInfo','categoryInfo'));
}

}


