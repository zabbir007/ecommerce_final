<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Mail;
use Session;
use Validator,Redirect,Response,File;
if (!isset($_SESSION)) {
    session_start();
}

class ReshippingController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('checkReshippingLogin');
    }
    public function showReshippingPage(){
      $departmentInfo=DB::table('department')
            ->get();
     $categoryInfo=DB::table('category')
            ->get();
    	$settingInfo=DB::table('shipping_setting')
						->where('shipping_setting.status',1)
						->first();
		return view('reshipping.showReshippingPage',compact('settingInfo','departmentInfo','categoryInfo'));
    }

    public function showShippingLogin(){

		return view('reshipping.showShippingLogin');
    }

    public function reshippingUserProfileVerify($email,$hash){

    $email=$email;
    $hash=$hash;

    $verifyCheck=DB::table('user_register_reshipping')
                      ->where('userEmail',$email)
                      ->where('hash',$hash)
                      ->first();
    if ($verifyCheck) {
      
      if ($verifyCheck->verify=='1') {
      
          Session::put('messageVerifyR','Your Account Allready Verify. Please login !!');
          return redirect()->route('showShippingLogin');
      }elseif ($verifyCheck->verify=='0') {
        
        $data=array();
        $data['verify']=1;
        $updateVerify=DB::table('user_register_reshipping')
                            ->update($data);
        if ($updateVerify) {
            
            Session::put('messageVerifyS','Your Account Verify Successfully Done. Please login !!');
            return redirect()->route('showShippingLogin');
        }
      }

    }else{

      Session::put('messageVerifyU','Invalid link !!');
      return redirect()->route('showUserLogin');

    }
    
   }

    public function reshippingUserLogin(Request $request){

    $this->validate($request, [
      'emailOrPhone' => 'required',
      'password' => 'required',
      ]);

      $userEmailOrPhone=$request->emailOrPhone;
      $password=md5($request->password);
      $userInformation=DB::table('user_register_reshipping')
                          ->where('userEmail', $userEmailOrPhone)
                          ->orWhere('userPhone',$userEmailOrPhone)
                          ->where('userPassword', $password)
                          ->first();
      // echo "<pre/>";
      // print_r($userInformation);
      // exit();
      
      if ($userInformation) {

          if ($userInformation->status!='1') {
            Session::put('message','Account DeActive By Admin');
            return redirect()->route('showShippingLogin');
          }elseif ($userInformation->verify!='1') {
            Session::put('message','Please Verify Your Account By Email');
            return redirect()->route('showShippingLogin');
          }
          Session::put('reshippingUserId',$userInformation->id);
          Session::put('reshippingUserName',$userInformation->userName);
          return redirect()->route('superReshippingDashboard');
        }else{
          Session::put('message','Email/Phone or Password Invalid');
          return redirect()->route('showShippingLogin');
        }
    }

    public function reshippingUserRegister(Request $request){
    	
    $this->validate($request, [
    'name' => 'required|min:3|max:50',
    'email' => 'email',
    'phone' => 'max:13',
    'password' => 'min:6|required_with:rePassword|same:rePassword',
    'rePassword' => 'min:6'
    ]);

    $data=array();
    $data['userName']=$request->name;
    $data['userEmail']=$request->email;
    $data['userPhone']=$request->phone;
    $data['userPassword']=md5($request->password);
    $data['createTime']=date("Y/m/d");
    $hash=date("YmdHis").rand(11111,99999);
    $data['hash']=$hash;
    $data['verify']=0;

    //email and phone number check start here.
    $checkEmailAndPhone=DB::table('user_register_reshipping')
                     ->get();
    foreach($checkEmailAndPhone as $checkEP){
        //check phone number..
        if ($checkEP->userPhone == $request->phone) {
            Session::put('messageWarning','This Phone Number Allready Used!!');
            return redirect()->back();

        }
        //check email
        if ($checkEP->userEmail ==$request->email) {
            Session::put('messageWarning','This Email Allready Used!!');
            return redirect()->back();
        }
    }
    //email and phone number check end here.

    $customerEmail=$request->email;
    $customerPassword=$request->password;
    $messageBody="Successfully Registration Shopandship1.com. "."Click This link for verification  "."http://127.0.0.1:8000/reshipping-user-profile-verify/".$customerEmail."/".$hash." "; 
    Mail::send([], [],function ($message) use ($customerEmail,$messageBody) {

       $message->from('services@bee-next.com.bd', 'Shop And Ship');
       $message->subject("Registration Information");
       $message->setBody($messageBody);

        $message->to($customerEmail);
    });
    
    $userRegister=DB::table('user_register_reshipping')
                      ->insert($data);
    Session::put('messagere','Registration Successfully Done!!! Please confirm your Registration by Mail');
    return redirect()->back();
    // echo "<pre/>";
    // print_r($data);
    // exit();
    }
}
