<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Mail;
use Session;
use Validator,Redirect,Response,File;
if (!isset($_SESSION)) {
    session_start();
}
class SuperReshippingController extends Controller
{
    public function __construct()
    {    
     $this->middleware('checkReshipping');
    }
 
    public function superReshippingDashboard(){
    	return view('reshipping.dashboard');
    }

     public function reshippingUserLogout(){

        Session::put('reshippingUserId','');
        Session::put('reshippingUserName','');
        return redirect()->route('showShippingLogin');
    }

    public function addInsideShippingAddress(){
        $countries=$this->countryName();
        return view('reshipping.addInsideShippingAddress',compact('countries'));
    }

    public function saveInsideShippingAddress(Request $request){

        $this->validate($request, [
          'country' => 'required',
          'address1' => 'required',
          'city' => 'required',
          'street' => 'required',
          'town' => 'required',
          'phone' => 'required',
          'email' => 'required',
        ]);
        $data=array();
        $updateInsideShippingDeactive=$this->updateInsideShippingDeactive();
        $data['userId']=Session::get('reshippingUserId');
        $data['country']=$request->country;
        $data['address1']=$request->address1;
        $data['address2']=$request->address2;
        $data['city']=$request->city;
        $data['company']=$request->company;
        $data['street']=$request->street;
        $data['town']=$request->town;
        $data['district']=$request->district;
        $data['phone']=$request->phone;
        $data['email']=$request->email;
        $data['status']=1;

        $insert=DB::table('user_inside_shipping_address')
                    ->insert($data);
        Session::put('message','Address Save Successfully Done!!!');
        return redirect()->back();
    }

    public function updateInsideShippingAddress(Request $request){

        $this->validate($request, [
          'country' => 'required',
          'address1' => 'required',
          'city' => 'required',
          'street' => 'required',
          'town' => 'required',
          'phone' => 'required',
          'email' => 'required',
        ]);
        $data=array();
        $id=$request->id;
        $data['userId']=Session::get('reshippingUserId');
        $data['country']=$request->country;
        $data['address1']=$request->address1;
        $data['address2']=$request->address2;
        $data['city']=$request->city;
        $data['company']=$request->company;
        $data['street']=$request->street;
        $data['town']=$request->town;
        $data['district']=$request->district;
        $data['phone']=$request->phone;
        $data['email']=$request->email;
        $statusCheck=$request->status;
        if ($statusCheck=='1') {
            $updateInsideShippingDeactive=$this->updateInsideShippingDeactive();
            $data['status']=1;
        }else{

            $data['status']=0; 
        }

        $insert=DB::table('user_inside_shipping_address')
                    ->where('user_inside_shipping_address.id',$id)
                    ->update($data);
        Session::put('message','Address Update Successfully Done!!!');
        return redirect()->back();
    }
    private function updateInsideShippingDeactive(){
        $data=array();
        $data['status']=0;
        $updateDeactive=DB::table('user_inside_shipping_address')
                                ->update($data); 
    }

    public function showInsideShippingAddress(){

        $userId=Session::get('reshippingUserId');
        $addressInfo=DB::table('user_inside_shipping_address as in')
                            ->where('in.userId',$userId)
                            ->get();
        // echo "<pre/>";
        // print_r($userId);
        // exit();
        return view('reshipping.showInsideShippingAddress',compact('addressInfo'));
    }

    public function editInsideShippingAddress($id){

        $userId=Session::get('reshippingUserId');
        $singleAddressInfo=DB::table('user_inside_shipping_address as in')
                                ->where('in.id',$id)
                                ->where('in.userId',$userId)
                                ->first();
        if ($singleAddressInfo) {
           
           return view('reshipping.editInsideShippingAddress',compact('singleAddressInfo','id'));
        }else{
            return redirect()->back();
        }
    }

    /* Country name Start Here */

    private function countryName(){

        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"); 

    return $countries;

    }
/* Country name end here */

    public function addUserInsideShipping(){
        $userId=Session::get('reshippingUserId');
        $insideShippingPrice=DB::table('inside_shipping_setting')
                                ->where('inside_shipping_setting.status',1)
                                ->first();
        $insideActiveAddress=DB::table('user_inside_shipping_address')
                                    ->where('user_inside_shipping_address.status','1')
                                    ->where('user_inside_shipping_address.userId',$userId)
                                    ->first();
        $sweetInfo=DB::table('sweet_address')
                            ->where('status',1)
                            ->get();
        if ($insideActiveAddress) {

            $insideBoxPrice=DB::table('inside_box_price')
                            ->get();
            return view('reshipping.addUserInsideShipping',compact('insideShippingPrice','insideActiveAddress','insideBoxPrice','sweetInfo'));
        }else{

            Session::put('messageWarningAdd','Firstly insert your address !!!');
           
            return view('reshipping.addInsideShippingAddress');
        }

        
    }

    public function showUserInsideShipping(){

        $userId=Session::get('reshippingUserId');
        $reshippingInfo=DB::table('inside_user_reshipping_order')
                            ->where('userId',$userId)
                            ->get();
        return view('reshipping.showUserInsideShipping',compact('reshippingInfo'));
    }

    public function viewUserInsideShipping($id){

        $userId=Session::get('reshippingUserId');
        $reshippingOrderInfo=DB::table('inside_user_reshipping_order')
                                    ->where('id',$id)
                                    ->where('userId',$userId)
                                    ->first();
        $amount=array();
        $amountApplication=  explode(',', $reshippingOrderInfo->boxId);

        foreach($amountApplication as $amountApp){
            $amount[]=$amountApp;
        }

        if ($reshippingOrderInfo) {
           
           return view('reshipping.viewUserInsideShipping',compact('reshippingOrderInfo','amount'));

        }else{

            return redirect()->back();
        }
    }

    public function getInsideBoxPrice(Request $request){

        $id=intval($request->id);
        $result=DB::table('inside_box_price')
              ->where('id', $id)
              ->first();
           if ($result) {
                        echo json_encode($result);
                       
                    }
    }

     public function saveUserInsideShipping(Request $request){

        $userId=Session::get('reshippingUserId');
        $deliveryTypeFind=DB::table('inside_shipping_setting')
                              ->where('status', 1)
                              ->first();
        if($deliveryTypeFind->normalDeliveryPrice==$request->deliveryPriceTotal){
                $finalDeliveryType=1;
                $deliveryDate=$deliveryTypeFind->normalDeliveryTime;
            }else if($deliveryTypeFind->fastDeliveryPrice==$request->deliveryPriceTotal){
            $finalDeliveryType=2;
            $deliveryDate=$deliveryTypeFind->fastDeliveryTime;
        }
        $insideActiveAddress=DB::table('user_inside_shipping_address')
                                    ->where('user_inside_shipping_address.status','1')
                                    ->where('user_inside_shipping_address.userId',$userId)
                                    ->first();
        $orderNumberGenerate= mt_rand(10000000, 99999999);  
        $data=array();
        $data['country']=$insideActiveAddress->country;
        $data['address1']=$insideActiveAddress->address1;
        $data['city']=$insideActiveAddress->city;
        $data['company']=$insideActiveAddress->company;
        $data['street']=$insideActiveAddress->street;
        $data['zipcode']=$insideActiveAddress->town;
        $data['phone']=$insideActiveAddress->phone;
        $data['email']=$insideActiveAddress->email;

        $data['totalAmount']=$request->totalAmount;
        $data['boxprice']=$request->boxprice;
        $data['boxId']=implode(', ', $request->box);
        $data['deliveryPriceTotal']=$request->deliveryPriceTotal;
        $data['deliveryType']=$finalDeliveryType;
        $data['serviceCharge']=$request->serviceCharge;
        $data['shippingCharge']=$request->shippingCharge;
        $data['orderNumber']=$request->orderNumber;
        $data['itemName']=$request->itemName;
        $data['itemUrl']=$request->itemUrl;
        $data['size']=$request->size;
        $data['color']=$request->color;
        $data['price']=$request->price;
        $data['anyOther']=$request->anyOther;
        $data['instruction']=$request->instruction;
        $data['quantity']=$request->quantity;
        $data['insideShippingOrderNumber']=$orderNumberGenerate;
        $data['userId']=$userId;
        $data['createTime']=date("Y/m/d");
        $NewDate=Date('Y/m/d', strtotime("+".$deliveryDate." days"));
        $data['deliveryDate']=$NewDate;
        $data['status']="pending";
        // echo "<pre/>";
        // print_r($data);
        // exit();

        $insertOrder=DB::table('inside_user_reshipping_order')
                            ->insert($data);

        if ($insertOrder) {
            
            Session::put('message','Inside Reshipping Order Create Successfully Done !!!');
            return redirect()->back();
        }
     }
}
