<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use Mail;
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 

class AdminController extends Controller
{

   public function __construct()
    {
        $this->middleware('checkAdminLogin');
    }

   public function login(){
   	return view('admin.login');
   }


   public function adminLogin(Request $request){
	   	$email=$request->adminEmail;
		$password=md5($request->adminPassword);
		


		$result = DB::table('admin')
					->where('email', $email)
					->where('password', $password)
					->first();
        
        // echo "<pre/>";
        // print_r($result);
        // exit();

					if ($result) {
						Session::put('adminId',$result->id);
						return redirect()->route('superAdminDashboard');
					}else{
						Session::put('message','Email or Password Invalid');
						return redirect()->route('login');
					}
	    }


}
