<?php

namespace App\Http\Middleware;

use Closure;
use Session;
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
class checkReshipping
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $reshippingId=Session::get('reshippingUserId');
        if(!$reshippingId){
            
            return redirect()->route('reshippingUserLogin');
            
        }

        return $next($request);
    }
}
