<?php

namespace App\Http\Middleware;

use Closure;
use Session;
if(!isset($_SESSION)) 
{ 
    session_start(); 
}

class checkReshippingLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $reshippingId=Session::get('reshippingUserId');
        if($reshippingId){
            
            return redirect()->route('superReshippingDashboard');
            
        }
        return $next($request);
    }
}
