<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// SSLCOMMERZ Start
Route::get('/example1', 'SslCommerzPaymentController@exampleEasyCheckout');
Route::get('/example2', 'SslCommerzPaymentController@exampleHostedCheckout');

Route::post('/pay', 'SslCommerzPaymentController@index');
Route::post('/pay-via-ajax', 'SslCommerzPaymentController@payViaAjax');

Route::post('/success', 'SslCommerzPaymentController@success');
Route::post('/fail', 'SslCommerzPaymentController@fail');
Route::post('/cancel', 'SslCommerzPaymentController@cancel');

Route::post('/ipn', 'SslCommerzPaymentController@ipn');
//SSLCOMMERZ END

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'UserController@welcome')->name('welcome');
Route::get('shop', 'UserController@shop')->name('shop');
/* Admin Panel Start Here */
Route::prefix('admin')->group(function () {

	Route::get('login', 'AdminController@login')->name('login');
    Route::post('admin-login','AdminController@adminLogin')->name('adminLogin');
	Route::get('super-admin-dashboard', 'SuperAdminController@superAdminDashboard')->name('superAdminDashboard');
	Route::get('admin-logout', 'SuperAdminController@adminLogout')->name('adminLogout');
	//add department start here
	Route::get('show-department','SuperAdminController@showDepartment')->name('showDepartment');
	Route::get('add-department','SuperAdminController@addDepartment')->name('addDepartment');
	Route::post('save-department','SuperAdminController@saveDepartment')->name('saveDepartment');
    Route::get('edit-department/{id}', 'SuperAdminController@editDepartment')->name('editDepartment');
    Route::post('update-department','SuperAdminController@updateDepartment')->name('updateDepartment');
    Route::post('delete-department', 'SuperAdminController@deleteDepartment')->name('deleteDepartment');
	//add department end here

	//add category start here
	Route::get('show-category','SuperAdminController@showCategory')->name('showCategory');
	Route::get('add-category','SuperAdminController@addCategory')->name('addCategory');
	Route::post('save-category','SuperAdminController@saveCategory')->name('saveCategory');
    Route::get('edit-category/{id}', 'SuperAdminController@editCategory')->name('editCategory');
    Route::post('update-category','SuperAdminController@updateCategory')->name('updateCategory');
    Route::post('delete-category', 'SuperAdminController@deleteCategory')->name('deleteCategory');
	//add category end here

	//add Subcategory start here
	Route::get('show-sub-category','SuperAdminController@showsubCategory')->name('showsubCategory');
	Route::get('add-sub-category','SuperAdminController@addsubCategory')->name('addsubCategory');
	Route::post('save-sub-category','SuperAdminController@savesubCategory')->name('savesubCategory');
    Route::get('edit-sub-category/{id}', 'SuperAdminController@editsubCategory')->name('editsubCategory');
    Route::post('update-sub-category','SuperAdminController@updatesubCategory')->name('updatesubCategory');
    Route::post('delete-sub-category', 'SuperAdminController@deletesubCategory')->name('deletesubCategory');
	//add Subcategory end here


    //Coupon Start Here
    Route::get('show-coupon','SuperAdminController@showCoupon')->name('showCoupon');
    Route::get('add-coupon','SuperAdminController@addCoupon')->name('addCoupon');
	Route::post('save-coupon','SuperAdminController@saveCoupon')->name('saveCoupon');
    Route::get('edit-coupon/{id}', 'SuperAdminController@editCoupon')->name('editCoupon');
    Route::post('update-coupon','SuperAdminController@updateCoupon')->name('updateCoupon');
    Route::post('delete-coupon', 'SuperAdminController@deleteCoupon')->name('deleteCoupon');
    //Coupon End Here

    //product information start here
    Route::get('add-product','SuperAdminController@addProduct')->name('addProduct');
    Route::post('get-sub-category', 'SuperAdminController@getsubCategory')->name('getsubCategory');
    Route::post('save-product', 'SuperAdminController@saveProduct')->name('saveProduct');
    Route::get('show-product','SuperAdminController@showProduct')->name('showProduct');
    Route::get('edit-product/{id}', 'SuperAdminController@editProduct')->name('editProduct');
    Route::post('update-product','SuperAdminController@updateProduct')->name('updateProduct');
    Route::post('delete-product', 'SuperAdminController@deleteProduct')->name('deleteProduct');
    //product information end here

    //Customer information start here
    Route::get('show-customer','SuperAdminController@showCustomer')->name('showCustomer');
    Route::get('add-customer','SuperAdminController@addCustomer')->name('addCustomer');
    Route::post('save-customer','SuperAdminController@saveCustomer')->name('saveCustomer');
    Route::post('admin-check-customer-email','SuperAdminController@adminCheckCustomerEmail')->name('adminCheckCustomerEmail');
    Route::post('admin-check-customer-phone','SuperAdminController@adminCheckCustomerPhone')->name('adminCheckCustomerPhone');
    Route::post('admin-check-customer-email-edit','SuperAdminController@adminCheckCustomerEmailEdit')->name('adminCheckCustomerEmailEdit');
    Route::post('admin-check-customer-phone-edit','SuperAdminController@adminCheckCustomerPhoneEdit')->name('adminCheckCustomerPhoneEdit');
    Route::get('admin-edit-customer/{id}', 'SuperAdminController@adminEditCustomer')->name('adminEditCustomer');
    Route::post('admin-update-customer','SuperAdminController@adminUpdateCustomer')->name('adminUpdateCustomer');
    Route::post('admin-delete-customer','SuperAdminController@adminDeleteCustomer')->name('adminDeleteCustomer');
    //Customer information end here

    //Shipping Module Start Here
    Route::get('show-shipping-setting','SuperAdminController@showShippingSetting')->name('showShippingSetting');
    Route::get('add-shipping-setting','SuperAdminController@addShippingSetting')->name('addShippingSetting');
    Route::post('save-shipping-setting','SuperAdminController@saveShippingSetting')->name('saveShippingSetting');
    Route::get('edit-shipping-setting/{id}','SuperAdminController@editShippingSetting')->name('editShippingSetting');
    Route::post('update-shipping-setting','SuperAdminController@updateShippingSetting')->name('updateShippingSetting');
    Route::post('delete-shipping-setting/{id}','SuperAdminController@deleteShippingSetting')->name('deleteShippingSetting');
    Route::get('show-shipping-order','SuperAdminController@showShippingOrder')->name('showShippingOrder');
    //Shipping Module End Here

    //Inside Shipping Module Start Here
    Route::get('show-inside-charge-setting','SuperAdminController@showInsideChargeSetting')->name('showInsideChargeSetting');
    Route::get('add-inside-charge-setting','SuperAdminController@addInsideChargeSetting')->name('addInsideChargeSetting');
    Route::post('save-inside-charge-setting','SuperAdminController@saveInsideChargeSetting')->name('saveInsideChargeSetting');
    Route::get('edit-inside-charge-setting/{id}', 'SuperAdminController@editInsideChargeSetting')->name('editInsideChargeSetting');
    Route::post('update-inside-charge-setting','SuperAdminController@updateInsideChargeSetting')->name('updateInsideChargeSetting');
    Route::post('delete-inside-charge-setting', 'SuperAdminController@deleteInsideChargeSetting')->name('deleteInsideChargeSetting');

    //Box price inside
    Route::get('show-inside-box-price','SuperAdminController@showInsideBoxPrice')->name('showInsideBoxPrice');
    Route::get('add-inside-box-price','SuperAdminController@addInsideBoxPrice')->name('addInsideBoxPrice');
    Route::post('save-inside-box-price','SuperAdminController@saveInsideBoxPrice')->name('saveInsideBoxPrice');
    Route::get('edit-inside-box-price/{id}', 'SuperAdminController@editInsideBoxPrice')->name('editInsideBoxPrice');
    Route::post('update-inside-box-price','SuperAdminController@updateInsideBoxPrice')->name('updateInsideBoxPrice');
    Route::post('delete-inside-box-price', 'SuperAdminController@deleteInsideBoxPrice')->name('deleteInsideBoxPrice');

    //Inside Shipping Module End Here



    //Outside Shipping Module Start Here
    Route::get('show-outside-charge-setting','SuperAdminController@showOutsideChargeSetting')->name('showOutsideChargeSetting');
    Route::get('add-outside-charge-setting','SuperAdminController@addOutsideChargeSetting')->name('addOutsideChargeSetting');
    Route::post('save-outside-charge-setting','SuperAdminController@saveOutsideChargeSetting')->name('saveOutsideChargeSetting');
    Route::get('edit-outside-charge-setting/{id}', 'SuperAdminController@editOutsideChargeSetting')->name('editOutsideChargeSetting');
    Route::post('update-outside-charge-setting','SuperAdminController@updateOutsideChargeSetting')->name('updateOutsideChargeSetting');
    Route::post('delete-outside-charge-setting', 'SuperAdminController@deleteOutsideChargeSetting')->name('deleteOutsideChargeSetting');

     Route::get('show-outside-box-price','SuperAdminController@showOutsideBoxPrice')->name('showOutsideBoxPrice');
    Route::get('add-outside-box-price','SuperAdminController@addOutsideBoxPrice')->name('addOutsideBoxPrice');
    Route::post('save-outside-box-price','SuperAdminController@saveOutsideBoxPrice')->name('saveOutsideBoxPrice');
    Route::get('edit-outside-box-price/{id}', 'SuperAdminController@editOutsideBoxPrice')->name('editOutsideBoxPrice');
    Route::post('update-outside-box-price','SuperAdminController@updateOutsideBoxPrice')->name('updateOutsideBoxPrice');
    Route::post('delete-outside-box-price', 'SuperAdminController@deleteOutsideBoxPrice')->name('deleteOutsideBoxPrice');
    //Outside Shipping Module End Here


    //Order Portion Start Here


    //inside order procedude
    Route::get('edit-inside-order-pending-order/{id}','SuperAdminController@editInsideOrderPendingOrder')->name('editInsideOrderPendingOrder');

    Route::get('show-inside-pending-order','SuperAdminController@showInsideOrderPendingOrder')->name('showInsideOrderPendingOrder');
    Route::get('show-inside-processing-order','SuperAdminController@showInsideOrderProcessingOrder')->name('showInsideOrderProcessingOrder');
    Route::get('view-inside-processing-order/{id}','SuperAdminController@viewInsideOrderProcessingOrder')->name('viewInsideOrderProcessingOrder');
    Route::get('show-inside-active-order','SuperAdminController@showInsideOrderActiveOrder')->name('showInsideOrderActiveOrder');
    Route::get('show-inside-delivery-order','SuperAdminController@showInsideOrderDeliveryOrder')->name('showInsideOrderDeliveryOrder');
    Route::post('get-inside-order-box-price','SuperAdminController@getInsideOrderBoxPrice')->name('getInsideOrderBoxPrice');
    Route::post('update-inside-order-pending-order','SuperAdminController@updateInsideOrderPendingOrder')->name('updateInsideOrderPendingOrder');


    //Basic Setting
    Route::get('show-order-basic-setting','SuperAdminController@showOrderBasicSetting')->name('showOrderBasicSetting');
    Route::get('add-order-basic-setting','SuperAdminController@addOrderBasicSetting')->name('addOrderBasicSetting');
    Route::post('save-order-basic-setting','SuperAdminController@saveOrderBasicSetting')->name('saveOrderBasicSetting');
    Route::get('edit-order-basic-setting/{id}', 'SuperAdminController@editOrderBasicSetting')->name('editOrderBasicSetting');
    Route::post('update-order-basic-setting','SuperAdminController@updateOrderBasicSetting')->name('updateOrderBasicSetting');
    Route::post('delete-order-basic-setting', 'SuperAdminController@deleteOrderBasicSetting')->name('deleteOrderBasicSetting');

    //Inside Payment Setting
    Route::get('show-inside-order-payment-setting','SuperAdminController@showInsideOrderPaymentSetting')->name('showInsideOrderPaymentSetting');
    Route::get('add-inside-order-payment-setting','SuperAdminController@addInsideOrderPaymentSetting')->name('addInsideOrderPaymentSetting');
    Route::post('save-inside-order-payment-setting','SuperAdminController@saveInsideOrderPaymentSetting')->name('saveInsideOrderPaymentSetting');
    Route::get('edit-inside-order-payment-setting/{id}', 'SuperAdminController@editInsideOrderPaymentSetting')->name('editInsideOrderPaymentSetting');
    Route::post('update-inside-order-payment-setting','SuperAdminController@updateInsideOrderPaymentSetting')->name('updateInsideOrderPaymentSetting');
    Route::post('delete-inside-order-payment-setting', 'SuperAdminController@deleteInsideOrderPaymentSetting')->name('deleteInsideOrderPaymentSetting');

    Route::get('show-inside-order-box-price','SuperAdminController@showInsideOrderBoxPrice')->name('showInsideOrderBoxPrice');
    Route::get('add-inside-order-box-price','SuperAdminController@addInsideOrderBoxPrice')->name('addInsideOrderBoxPrice');
    Route::post('save-inside-order-box-price','SuperAdminController@saveInsideOrderBoxPrice')->name('saveInsideOrderBoxPrice');
    Route::get('edit-inside-order-box-price/{id}', 'SuperAdminController@editInsideOrderBoxPrice')->name('editInsideOrderBoxPrice');
    Route::post('update-inside-order-box-price','SuperAdminController@updateInsideOrderBoxPrice')->name('updateInsideOrderBoxPrice');
    Route::post('delete-inside-order-box-price', 'SuperAdminController@deleteInsideOrderBoxPrice')->name('deleteInsideOrderBoxPrice');


    //Outside Payment Setting
    Route::get('show-outside-order-payment-setting','SuperAdminController@showOutsideOrderPaymentSetting')->name('showOutsideOrderPaymentSetting');
    Route::get('add-outside-order-payment-setting','SuperAdminController@addOutsideOrderPaymentSetting')->name('addOutsideOrderPaymentSetting');
    Route::post('save-outside-order-payment-setting','SuperAdminController@saveOutsideOrderPaymentSetting')->name('saveOutsideOrderPaymentSetting');
    Route::get('edit-outside-order-payment-setting/{id}', 'SuperAdminController@editOutsideOrderPaymentSetting')->name('editOutsideOrderPaymentSetting');
    Route::post('update-outside-order-payment-setting','SuperAdminController@updateOutsideOrderPaymentSetting')->name('updateOutsideOrderPaymentSetting');
    Route::post('delete-outside-order-payment-setting', 'SuperAdminController@deleteOutsideOrderPaymentSetting')->name('deleteOutsideOrderPaymentSetting');

    //Order Portion End Here

    //Company Setting Start Here
    Route::get('show-company-setting','SuperAdminController@showCompanySetting')->name('showCompanySetting');
    Route::get('add-company-setting','SuperAdminController@addCompanySetting')->name('addCompanySetting');
    Route::post('save-company-setting','SuperAdminController@saveCompanySetting')->name('saveCompanySetting');
    Route::get('edit-company-setting/{id}', 'SuperAdminController@editCompanySetting')->name('editCompanySetting');
    Route::post('update-company-setting','SuperAdminController@updateCompanySetting')->name('updateCompanySetting');
    Route::post('delete-company-setting', 'SuperAdminController@deleteCompanySetting')->name('deleteCompanySetting');

    Route::get('show-customer-message','SuperAdminController@showCustomerMessage')->name('showCustomerMessage');
    Route::get('view-customer-message/{id}','SuperAdminController@viewCustomerMessage')->name('viewCustomerMessage');
    Route::post('delete-customer-message','SuperAdminController@deleteCustomerMessage')->name('deleteCustomerMessage');
    //Company Setting End Here

    //Send Message For User Start Here
    Route::get('show-send-message','SuperAdminController@showSendMessage')->name('showSendMessage');
    Route::get('add-user-message','SuperAdminController@addUserMessage')->name('addUserMessage');
    Route::post('save-user-message','SuperAdminController@saveUserMessage')->name('saveUserMessage');
    Route::post('delete-user-message','SuperAdminController@deleteUserMessage')->name('deleteUserMessage');
    //Send Message For User End Here

    //predefined marketplace start here
    Route::get('show-predefined-marketplace','SuperAdminController@showPredefinedMarketplace')->name('showPredefinedMarketplace');
    Route::get('add-predefined-marketplace','SuperAdminController@addPredefinedMarketplace')->name('addPredefinedMarketplace');
    Route::post('save-predefined-marketplace','SuperAdminController@savePredefinedMarketplace')->name('savePredefinedMarketplace');
    Route::get('edit-predefined-marketplace/{id}', 'SuperAdminController@editPredefinedMarketplace')->name('editPredefinedMarketplace');
    Route::post('update-predefined-marketplace','SuperAdminController@updatePredefinedMarketplace')->name('updatePredefinedMarketplace');
    Route::post('delete-predefined-marketplace', 'SuperAdminController@deletePredefinedMarketplace')->name('deletePredefinedMarketplace');

    //predefined marketplace end here


    //Sweet Address Field Start Here

    Route::get('show-sweet-address','SuperAdminController@showSweetAddress')->name('showSweetAddress');
    Route::get('add-sweet-address','SuperAdminController@addSweetAddress')->name('addSweetAddress');
    Route::post('save-sweet-address','SuperAdminController@saveSweetAddress')->name('saveSweetAddress');
    Route::get('edit-sweet-address/{id}','SuperAdminController@editSweetAddress')->name('editSweetAddress');
    Route::post('update-sweet-address','SuperAdminController@updateSweetAddress')->name('updateSweetAddress');
    Route::post('delete-sweet-address','SuperAdminController@deleteSweetAddress')->name('deleteSweetAddress');

    //Sweet Address Field End Here


    //shipping cost
    Route::get('add-shipping-cost','SuperAdminController@addShippingCost')->name('addShippingCost');
    Route::post('update-shipping-cost','SuperAdminController@updateShippingCost')->name('updateShippingCost');
    

    //Ecommerce Order 
    Route::get('ecommerce-all-order','SuperAdminController@ecommerceAllOrder')->name('ecommerceAllOrder');
    Route::get('view-ecommerce-all-order/{id}','SuperAdminController@viewEcommerceAllOrder')->name('viewEcommerceAllOrder');
    Route::get('ecommerce-pending-order','SuperAdminController@ecommercePendingOrder')->name('ecommercePendingOrder');
    Route::get('ecommerce-edit-pending-order/{id}','SuperAdminController@ecommerceEditPendingOrder')->name('ecommerceEditPendingOrder');
    Route::post('ecommerce-update-pending-order','SuperAdminController@ecommerceUpdatePendingOrder')->name('ecommerceUpdatePendingOrder');
    Route::get('ecommerce-active-order','SuperAdminController@ecommerceActiveOrder')->name('ecommerceActiveOrder');
    Route::get('ecommerce-edit-active-order/{id}','SuperAdminController@ecommerceEditActiveOrder')->name('ecommerceEditActiveOrder');
    Route::post('ecommerce-update-active-order','SuperAdminController@ecommerceUpdateActiveOrder')->name('ecommerceUpdateActiveOrder');
    Route::get('ecommerce-complete-order','SuperAdminController@ecommerceCompleteOrder')->name('ecommerceCompleteOrder');
    Route::get('view-ecommerce-complete-order/{id}','SuperAdminController@viewEcommerceCompleteOrder')->name('viewEcommerceCompleteOrder');
    Route::get('ecommerce-cancel-order','SuperAdminController@ecommerceCancelOrder')->name('ecommerceCancelOrder');
    Route::get('view-ecommerce-cancel-order/{id}','SuperAdminController@viewEcommerceCancelOrder')->name('viewEcommerceCancelOrder');


    //custom box price start here//

      //order box category
    Route::get('show-order-box-category','SuperAdminController@showOrderBoxCategory')->name('showOrderBoxCategory');
    Route::get('add-order-box-category','SuperAdminController@addOrderBoxCategory')->name('addOrderBoxCategory');
    Route::post('save-order-box-category','SuperAdminController@saveOrderBoxCategory')->name('saveOrderBoxCategory');
    Route::get('edit-order-box-category/{id}','SuperAdminController@editOrderBoxCategory')->name('editOrderBoxCategory');
    Route::post('update-order-box-category','SuperAdminController@updateOrderBoxCategory')->name('updateOrderBoxCategory');
    Route::post('delete-order-box-category','SuperAdminController@deleteOrderBoxCategory')->name('deleteOrderBoxCategory');
      //order box sub category
    Route::get('show-order-box-subCategory','SuperAdminController@showOrderBoxSubCategory')->name('showOrderBoxSubCategory');
    Route::get('add-order-box-subCategory','SuperAdminController@addOrderBoxSubCategory')->name('addOrderBoxSubCategory');
    Route::post('save-order-box-subCategory','SuperAdminController@saveOrderBoxSubCategory')->name('saveOrderBoxSubCategory');
    Route::get('edit-order-box-subCategory/{id}','SuperAdminController@editOrderBoxSubCategory')->name('editOrderBoxSubCategory');
    Route::post('update-order-box-subCategory','SuperAdminController@updateOrderBoxSubCategory')->name('updateOrderBoxSubCategory');
    Route::post('delete-order-box-subCategory','SuperAdminController@deleteOrderBoxSubCategory')->name('deleteOrderBoxSubCategory');

    //custom box price end here//



    //custom Shipping Method start here//

      //order Shipping category
    Route::get('show-order-shipping-category','SuperAdminController@showOrderShippingCategory')->name('showOrderShippingCategory');
    Route::get('add-order-shipping-category','SuperAdminController@addOrderShippingCategory')->name('addOrderShippingCategory');
    Route::post('save-order-shipping-category','SuperAdminController@saveOrderShippingCategory')->name('saveOrderShippingCategory');
    Route::get('edit-order-shipping-category/{id}','SuperAdminController@editOrderShippingCategory')->name('editOrderShippingCategory');
    Route::post('update-order-shipping-category','SuperAdminController@updateOrderShippingCategory')->name('updateOrderShippingCategory');
    Route::post('delete-order-shipping-category','SuperAdminController@deleteOrderShippingCategory')->name('deleteOrderShippingCategory');
      //order shipping sub category
    Route::get('show-order-shipping-subCategory','SuperAdminController@showOrderShippingSubCategory')->name('showOrderShippingSubCategory');
    Route::get('add-order-shipping-subCategory','SuperAdminController@addOrderShippingSubCategory')->name('addOrderShippingSubCategory');
    Route::post('save-order-shipping-subCategory','SuperAdminController@saveOrderShippingSubCategory')->name('saveOrderShippingSubCategory');
    Route::get('edit-order-shipping-subCategory/{id}','SuperAdminController@editOrderShippingSubCategory')->name('editOrderShippingSubCategory');
    Route::post('update-order-shipping-subCategory','SuperAdminController@updateOrderShippingSubCategory')->name('updateOrderShippingSubCategory');
    Route::post('delete-order-shipping-subCategory','SuperAdminController@deleteOrderShippingSubCategory')->name('deleteOrderShippingSubCategory');  

    //custom Shipping Method end here//


    //Additional Photo Price Start Here
    
    Route::get('show-order-additional-photo-price','SuperAdminController@showOrderAdditionalPhotoPrice')->name('showOrderAdditionalPhotoPrice');
    Route::get('add-order-additional-photo-price','SuperAdminController@addOrderAdditionalPhotoPrice')->name('addOrderAdditionalPhotoPrice');
    Route::post('save-order-additional-photo-price','SuperAdminController@saveOrderAdditionalPhotoPrice')->name('saveOrderAdditionalPhotoPrice');
    Route::get('edit-order-additional-photo-price/{id}','SuperAdminController@editOrderAdditionalPhotoPrice')->name('editOrderAdditionalPhotoPrice');
    Route::post('update-order-additional-photo-price','SuperAdminController@updateOrderAdditionalPhotoPrice')->name('updateOrderAdditionalPhotoPrice');
    Route::post('delete-order-additional-photo-price','SuperAdminController@deleteOrderAdditionalPhotoPrice')->name('deleteOrderAdditionalPhotoPrice');

    //Additional Photo Price End Here

    //Order Product Type Start Here
    Route::get('show-order-product-type','SuperAdminController@showOrderProductType')->name('showOrderProductType');
    Route::get('add-order-product-type','SuperAdminController@addOrderProductType')->name('addOrderProductType');
    Route::post('save-order-product-type','SuperAdminController@saveOrderProductType')->name('saveOrderProductType');
    Route::get('edit-order-product-type/{id}','SuperAdminController@editOrderProductType')->name('editOrderProductType');
    Route::post('update-order-product-type','SuperAdminController@updateOrderProductType')->name('updateOrderProductType');
    Route::post('delete-order-product-type','SuperAdminController@deleteOrderProductType')->name('deleteOrderProductType');
    //Order Product Type End Here

    //Custom Order Start Here
    Route::get('show-admin-pending-custom-order','SuperAdminController@showAdminPendingCustomOrder')->name('showAdminPendingCustomOrder');
    Route::get('show-admin-admin-processing-custom-order','SuperAdminController@showAdminAdminProcessingCustomOrder')->name('showAdminAdminProcessingCustomOrder');
    Route::get('edit-admin-admin-processing-custom-order/{id}','SuperAdminController@editAdminAdminProcessingCustomOrder')->name('editAdminAdminProcessingCustomOrder');
    Route::post('update-admin-admin-processing-custom-order','SuperAdminController@updateAdminAdminProcessingCustomOrder')->name('updateAdminAdminProcessingCustomOrder');
    Route::get('edit-admin-pending-custom-order/{id}','SuperAdminController@editAdminPendingCustomOrder')->name('editAdminPendingCustomOrder');
    Route::get('edit-admin-pending-custom-order-outside/{id}','SuperAdminController@editAdminPendingCustomOrderOutside')->name('editAdminPendingCustomOrderOutside');
    Route::post('update-admin-pending-custom-order','SuperAdminController@updateAdminPendingCustomOrder')->name('updateAdminPendingCustomOrder');
    Route::post('update-admin-pending-custom-order-outside','SuperAdminController@updateAdminPendingCustomOrderOutside')->name('updateAdminPendingCustomOrderOutside');

    Route::get('show-admin-processing-custom-order','SuperAdminController@showAdminProcessingCustomOrder')->name('showAdminProcessingCustomOrder');
    Route::get('edit-admin-processing-custom-order/{id}','SuperAdminController@editAdminProcessingCustomOrder')->name('editAdminProcessingCustomOrder');
    Route::post('update-admin-processing-custom-order','SuperAdminController@updateAdminProcessingCustomOrder')->name('updateAdminProcessingCustomOrder');


    Route::get('show-admin-waiting-payment-custom-order','SuperAdminController@showAdminWaitingPaymentCustomOrder')->name('showAdminWaitingPaymentCustomOrder');
    Route::get('view-admin-waiting-payment-custom-order/{id}','SuperAdminController@viewAdminWaitingPaymentCustomOrder')->name('viewAdminWaitingPaymentCustomOrder');
    Route::get('show-admin-active-custom-order','SuperAdminController@showAdminActiveCustomOrder')->name('showAdminActiveCustomOrder');
    Route::get('edit-admin-active-custom-order/{id}','SuperAdminController@editAdminActiveCustomOrder')->name('editAdminActiveCustomOrder');
    Route::post('update-admin-active-custom-order','SuperAdminController@updateAdminActiveCustomOrder')->name('updateAdminActiveCustomOrder');
    Route::get('show-admin-complete-custom-order','SuperAdminController@showAdminCompleteCustomOrder')->name('showAdminCompleteCustomOrder');
    Route::get('view-admin-complete-custom-order/{id}','SuperAdminController@viewAdminCompleteCustomOrder')->name('viewAdminCompleteCustomOrder');




    Route::get('show-admin-pending-custom-order-outside','SuperAdminController@showAdminPendingCustomOrderOutside')->name('showAdminPendingCustomOrderOutside');
    Route::get('show-admin-active-custom-order-outside','SuperAdminController@showAdminActiveCustomOrderOutside')->name('showAdminActiveCustomOrderOutside');
    Route::get('edit-admin-active-custom-order-outside/{id}','SuperAdminController@editAdminActiveCustomOrderOutside')->name('editAdminActiveCustomOrderOutside');
    Route::post('update-admin-active-custom-order-outside','SuperAdminController@updateAdminActiveCustomOrderOutside')->name('updateAdminActiveCustomOrderOutside');
    Route::get('show-admin-complete-custom-order-outside','SuperAdminController@showAdminCompleteCustomOrderOutside')->name('showAdminCompleteCustomOrderOutside');
    Route::get('view-admin-complete-custom-order-outside/{id}','SuperAdminController@viewAdminCompleteCustomOrderOutside')->name('viewAdminCompleteCustomOrderOutside');
    
    //Custom Order End Here






    //Shipping box price start here//

      //shipping box category
    Route::get('show-shipping-box-category','SuperAdminController@showShippingBoxCategory')->name('showShippingBoxCategory');
    Route::get('add-shipping-box-category','SuperAdminController@addShippingBoxCategory')->name('addShippingBoxCategory');
    Route::post('save-shipping-box-category','SuperAdminController@saveShippingBoxCategory')->name('saveShippingBoxCategory');
    Route::get('edit-shipping-box-category/{id}','SuperAdminController@editShippingBoxCategory')->name('editShippingBoxCategory');
    Route::post('update-shipping-box-category','SuperAdminController@updateShippingBoxCategory')->name('updateShippingBoxCategory');
    Route::post('delete-shipping-box-category','SuperAdminController@deleteShippingBoxCategory')->name('deleteShippingBoxCategory');
      //shipping box sub category
    Route::get('show-shipping-box-subCategory','SuperAdminController@showShippingBoxSubCategory')->name('showShippingBoxSubCategory');
    Route::get('add-shipping-box-subCategory','SuperAdminController@addShippingBoxSubCategory')->name('addShippingBoxSubCategory');
    Route::post('save-shipping-box-subCategory','SuperAdminController@saveShippingBoxSubCategory')->name('saveShippingBoxSubCategory');
    Route::get('edit-shipping-box-subCategory/{id}','SuperAdminController@editShippingBoxSubCategory')->name('editShippingBoxSubCategory');
    Route::post('update-shipping-box-subCategory','SuperAdminController@updateShippingBoxSubCategory')->name('updateShippingBoxSubCategory');
    Route::post('delete-shipping-box-subCategory','SuperAdminController@deleteShippingBoxSubCategory')->name('deleteShippingBoxSubCategory');

    //shipping box price end here//



    //shipping Shipping Method start here//

      //shipping Shipping category
    Route::get('show-shipping-shipping-category','SuperAdminController@showShippingShippingCategory')->name('showShippingShippingCategory');
    Route::get('add-shipping-shipping-category','SuperAdminController@addShippingShippingCategory')->name('addShippingShippingCategory');
    Route::post('save-shipping-shipping-category','SuperAdminController@saveShippingShippingCategory')->name('saveShippingShippingCategory');
    Route::get('edit-shipping-shipping-category/{id}','SuperAdminController@editShippingShippingCategory')->name('editShippingShippingCategory');
    Route::post('update-shipping-shipping-category','SuperAdminController@updateShippingShippingCategory')->name('updateShippingShippingCategory');
    Route::post('delete-shipping-shipping-category','SuperAdminController@deleteShippingShippingCategory')->name('deleteShippingShippingCategory');
      //order shipping sub category
    Route::get('show-shipping-shipping-subCategory','SuperAdminController@showShippingShippingSubCategory')->name('showShippingShippingSubCategory');
    Route::get('add-shipping-shipping-subCategory','SuperAdminController@addShippingShippingSubCategory')->name('addShippingShippingSubCategory');
    Route::post('save-shipping-shipping-subCategory','SuperAdminController@saveShippingShippingSubCategory')->name('saveShippingShippingSubCategory');
    Route::get('edit-shipping-shipping-subCategory/{id}','SuperAdminController@editShippingShippingSubCategory')->name('editShippingShippingSubCategory');
    Route::post('update-shipping-shipping-subCategory','SuperAdminController@updateShippingShippingSubCategory')->name('updateShippingShippingSubCategory');
    Route::post('delete-shipping-shipping-subCategory','SuperAdminController@deleteShippingShippingSubCategory')->name('deleteShippingShippingSubCategory');  

    //shipping Shipping Method end here//


    //Additional Photo Price Start Here
    
    Route::get('show-shipping-additional-photo-price','SuperAdminController@showShippingAdditionalPhotoPrice')->name('showShippingAdditionalPhotoPrice');
    Route::get('add-shipping-additional-photo-price','SuperAdminController@addShippingAdditionalPhotoPrice')->name('addShippingAdditionalPhotoPrice');
    Route::post('save-shipping-additional-photo-price','SuperAdminController@saveShippingAdditionalPhotoPrice')->name('saveShippingAdditionalPhotoPrice');
    Route::get('edit-shipping-additional-photo-price/{id}','SuperAdminController@editShippingAdditionalPhotoPrice')->name('editShippingAdditionalPhotoPrice');
    Route::post('update-shipping-additional-photo-price','SuperAdminController@updateShippingAdditionalPhotoPrice')->name('updateShippingAdditionalPhotoPrice');
    Route::post('delete-shipping-additional-photo-price','SuperAdminController@deleteShippingAdditionalPhotoPrice')->name('deleteShippingAdditionalPhotoPrice');

    //Additional Photo Price End Here


    //Reshipping Order Start Here
    Route::get('admin-add-shipping-order','SuperAdminController@adminAddShippingOrder')->name('adminAddShippingOrder');
    Route::post('admin-save-shipping-order','SuperAdminController@adminSaveShippingOrder')->name('adminSaveShippingOrder');


    Route::get('show-admin-shipping-pending-order','SuperAdminController@showAdminShippingPendingOrder')->name('showAdminShippingPendingOrder');
    Route::get('show-admin-shipping-processing-order','SuperAdminController@showAdminShippingProcessingOrder')->name('showAdminShippingProcessingOrder');
    Route::get('edit-admin-shipping-processing-order/{id}','SuperAdminController@editAdminShippingProcessingOrder')->name('editAdminShippingProcessingOrder');
    Route::post('update-admin-shipping-processing-order','SuperAdminController@updateAdminShippingProcessingOrder')->name('updateAdminShippingProcessingOrder');
    Route::get('show-admin-shipping-waitingPayment-order','SuperAdminController@showAdminShippingWaitingPaymentOrder')->name('showAdminShippingWaitingPaymentOrder');
    Route::get('view-admin-shipping-waitingPayment-order/{id}','SuperAdminController@viewAdminShippingWaitingPaymentOrder')->name('viewAdminShippingWaitingPaymentOrder');
    Route::get('show-admin-shipping-active-order','SuperAdminController@showAdminShippingActiveOrder')->name('showAdminShippingActiveOrder');
    Route::get('view-admin-shipping-active-order/{id}','SuperAdminController@viewAdminShippingActiveOrder')->name('viewAdminShippingActiveOrder');
    Route::post('update-admin-shipping-active-order','SuperAdminController@updateAdminShippingActiveOrder')->name('updateAdminShippingActiveOrder');
    Route::get('show-admin-shipping-complete-order','SuperAdminController@showAdminShippingCompleteOrder')->name('showAdminShippingCompleteOrder');
    Route::get('view-admin-shipping-complete-order/{id}','SuperAdminController@viewAdminShippingCompleteOrder')->name('viewAdminShippingCompleteOrder');

    //Reshipping Order End Here


});
/* Admin Panel End Here */

/* User Panel Start Here */
Route::prefix('user')->group(function () {

Route::post('language-change-en','UserController@languageChangeEn')->name('languageChangeEn');
Route::post('language-change-bn','UserController@languageChangeBn')->name('languageChangeBn');


Route::get('user-login','UserController@showUserLogin')->name('showUserLogin');
Route::get('user-register','UserController@showUserRegister')->name('showUserRegister');
Route::get('forget-password','UserController@showForgetPassword')->name('showForgetPassword');
Route::post('send-forget-verify-code','UserController@sendForgetVerifyCode')->name('sendForgetVerifyCode');
Route::post('check-verify-code','UserController@checkVerify')->name('checkVerify');
Route::post('save-password','UserController@savePassword')->name('savePassword');
Route::get('store','UserController@showStore')->name('showStore');
Route::get('search-by-department/{id}','UserController@showDepartmentStore')->name('showDepartmentStore');
Route::get('search-by-category/{id}','UserController@showCategoryStore')->name('showCategoryStore');
Route::get('contact-us','UserController@showContactUs')->name('showContactUs');
Route::get('about-us','UserController@showAboutUs')->name('showAboutUs');
Route::post('send-message','UserController@sendMessage')->name('sendMessage');  
Route::post('check-user-email','UserController@checkUserEmail')->name('checkUserEmail');
Route::post('check-user-phone','UserController@checkUserPhone')->name('checkUserPhone');
Route::post('save-register','UserController@saveRegister')->name('saveRegister');
Route::post('login-user','UserController@loginUser')->name('loginUser');
Route::get('search-product-category/{id}/{departmentId}/{categoryId}','UserController@searchProductSidebar')->name('searchProductSidebar');
Route::post('search-product-by-productName','UserController@searchProductByProductName')->name('searchProductByProductName');
Route::get('user-logout','UserController@userLogout')->name('userLogout');
Route::get('user-profile','UserController@userProfile')->name('userProfile');
Route::get('user-order','UserController@userOrder')->name('userOrder');
Route::get('user-reshipping-order','UserController@userReshippingOrder')->name('userReshippingOrder');
Route::get('user-custom-order','UserController@userCustomOrder')->name('userCustomOrder');
Route::get('order-image/{id}','UserController@showUserCustomOrderImage')->name('showUserCustomOrderImage');
Route::get('order-image-outside/{id}','UserController@showUserCustomOrderImageOutside')->name('showUserCustomOrderImageOutside');
Route::get('user-billing-address','UserController@userBillingAddress')->name('userBillingAddress');
Route::get('user-shipping-address','UserController@userShippingAddress')->name('userShippingAddress');
Route::get('user-suite-address','UserController@userSuiteAddress')->name('userSuiteAddress');
Route::post('add-subscribe-user','UserController@addSubscribeUser')->name('addSubscribeUser');
Route::post('update-customer-personal-profile','UserController@updateCustomerPersonalProfile')->name('updateCustomerPersonalProfile');
Route::post('update-customer-address','UserController@updateCustomerAddress')->name('updateCustomerAddress');
Route::post('update-customer-shipping-address','UserController@updateCustomerShippingAddress')->name('updateCustomerShippingAddress');

Route::get('single-product/{id}','UserController@singleProduct')->name('singleProduct');


//product add to cart 
Route::post('add-to-cart','UserController@addToCart')->name('addToCart');
Route::post('add-to-wishlist','UserController@addToWishlist')->name('addToWishlist');
Route::get('show-cart-value','UserController@showCartValue')->name('showCartValue');
Route::post('update-cart-value','UserController@updateCartValue')->name('updateCartValue');
Route::get('delete-cart-value/{rowId}','UserController@deleteCartValue')->name('deleteCartValue');

Route::get('show-cart-page','UserController@showCartPage')->name('showCartPage');

Route::get('user-profile-verify/{email}/{hash}','UserController@userProfileVerify')->name('userProfileVerify');


//checkout page
Route::get('show-ecommerce-order-checkout','UserController@showEcommerceOrderCheckout')->name('showEcommerceOrderCheckout');

//Order Portion
Route::post('save-ecommerce-order','UserController@saveEcommerceOrder')->name('saveEcommerceOrder');
Route::get('view-ecommerce-order/{id}','UserController@viewEcommerceOrder')->name('viewEcommerceOrder');


//Custom Order Portion Start Here
Route::get('view-customer-pending-custom-order/{id}','UserController@viewCustomerPendingCustomOrder')->name('viewCustomerPendingCustomOrder');
Route::get('edit-customer-preprocessing-custom-order/{id}','UserController@editCustomerPreProcessingCustomOrder')->name('editCustomerPreProcessingCustomOrder');
Route::post('update-customer-preprocessing-custom-order','UserController@updateCustomerPreProcessingCustomOrder')->name('updateCustomerPreProcessingCustomOrder');

Route::get('edit-customer-waiting-payment-custom-order/{id}','UserController@editCustomerWaitingPaymentCustomOrder')->name('editCustomerWaitingPaymentCustomOrder');
Route::post('update-customer-waiting-payment-custom-order','UserController@updateCustomerWaitingPaymentCustomOrder')->name('updateCustomerWaitingPaymentCustomOrder');


Route::get('view-customer-active-custom-order/{id}','UserController@viewCustomerActiveCustomOrder')->name('viewCustomerActiveCustomOrder');

Route::get('view-customer-complete-custom-order/{id}','UserController@viewCustomerCompleteCustomOrder')->name('viewCustomerCompleteCustomOrder');
Route::post('pay-second-payment','UserController@paySecondPayment')->name('paySecondPayment');
Route::post('cancel-custom-order','UserController@cancelCustomOrder')->name('cancelCustomOrder');
Route::post('cancel-reshipping-order','UserController@cancelReshippingOrder')->name('cancelReshippingOrder');
Route::post('pay-second-payment-outside','UserController@paySecondPaymentOutside')->name('paySecondPaymentOutside');
Route::post('pay-third-payment-outside','UserController@payThirdPaymentOutside')->name('payThirdPaymentOutside');

Route::post('get-box-subcategory','UserController@getBoxSubCategory')->name('getBoxSubCategory');
Route::post('get-shipping-subcategory','UserController@getShippingSubCategory')->name('getShippingSubCategory');


//Shipping Order Portion Start Here
Route::get('edit-customer-pending-shipping-order/{id}','UserController@editCustomerPendingShippingOrder')->name('editCustomerPendingShippingOrder');
Route::get('edit-customer-waiting-payment-shipping-order/{id}','UserController@editCustomerWaitingPaymentShippingOrder')->name('editCustomerWaitingPaymentShippingOrder');
Route::post('update-customer-waiting-payment-shipping-order','UserController@updateCustomerWaitingPaymentShippingOrder')->name('updateCustomerWaitingPaymentShippingOrder');
Route::post('update-customer-processing-shipping-order','UserController@updateCustomerProcessingShippingOrder')->name('updateCustomerProcessingShippingOrder');
Route::get('view-customer-processing-shipping-order/{id}','UserController@viewCustomerProcessingShippingOrder')->name('viewCustomerProcessingShippingOrder');
Route::get('view-customer-active-shipping-order/{id}','UserController@viewCustomerActiveShippingOrder')->name('viewCustomerActiveShippingOrder');
Route::get('view-customer-complete-shipping-order/{id}','UserController@viewCustomerCompleteShippingOrder')->name('viewCustomerCompleteShippingOrder');

Route::post('get-shipping-box-subcategory','UserController@getShippingBoxSubCategory')->name('getShippingBoxSubCategory');
Route::post('get-shipping-shipping-subcategory','UserController@getShippingShippingSubCategory')->name('getShippingShippingSubCategory');


//All page link up
Route::get('faq','UserController@showFaqPage')->name('showFaqPage');
Route::get('support&services','UserController@showSupportPage')->name('showSupportPage');
Route::get('report','UserController@showReportPage')->name('showReportPage');
Route::get('store-location','UserController@showStoreLocationPage')->name('showStoreLocationPage');
Route::get('privacy-policy','UserController@showPrivacyPage')->name('showPrivacyPage');
Route::get('terms&condition','UserController@showTermsPage')->name('showTermsPage');
Route::get('warranty-policy','UserController@showWarrantyPage')->name('showWarrantyPage');
Route::get('refund&return-policy','UserController@showRefundPage')->name('showRefundPage');


});
/* User Panel End Here */


/*Reshipping Panel Start Here*/
Route::get('re-shipping','ReshippingController@showReshippingPage')->name('showReshippingPage');
Route::get('show-shipping-login-page','ReshippingController@showShippingLogin')->name('showShippingLogin');
Route::post('reshipping-user-login','ReshippingController@reshippingUserLogin')->name('reshippingUserLogin');
Route::post('reshipping-user-register','ReshippingController@reshippingUserRegister')->name('reshippingUserRegister');
Route::get('reshipping-user-logout','SuperReshippingController@reshippingUserLogout')->name('reshippingUserLogout');
Route::get('dashboard','SuperReshippingController@superReshippingDashboard')->name('superReshippingDashboard');
Route::get('add-inside-shipping-address','SuperReshippingController@addInsideShippingAddress')->name('addInsideShippingAddress');
Route::post('save-inside-shipping-address','SuperReshippingController@saveInsideShippingAddress')->name('saveInsideShippingAddress');
Route::get('show-inside-shipping-address','SuperReshippingController@showInsideShippingAddress')->name('showInsideShippingAddress');
Route::get('edit-inside-shipping-address/{id}','SuperReshippingController@editInsideShippingAddress')->name('editInsideShippingAddress');
Route::post('update-inside-shipping-address','SuperReshippingController@updateInsideShippingAddress')->name('updateInsideShippingAddress');
Route::get('add-user-inside-shipping','SuperReshippingController@addUserInsideShipping')->name('addUserInsideShipping');
Route::post('save-user-inside-shipping','SuperReshippingController@saveUserInsideShipping')->name('saveUserInsideShipping');
Route::get('add-outside-shipping-address','SuperReshippingController@addOutsideShippingAddress')->name('addOutsideShippingAddress');
Route::get('show-user-inside-shipping','SuperReshippingController@showUserInsideShipping')->name('showUserInsideShipping');
Route::get('view-user-inside-shipping/{id}','SuperReshippingController@viewUserInsideShipping')->name('viewUserInsideShipping');
Route::post('get-inside-box-price','SuperReshippingController@getInsideBoxPrice')->name('getInsideBoxPrice');

Route::get('reshipping-user-profile-verify/{email}/{hash}','ReshippingController@reshippingUserProfileVerify')->name('reshippingUserProfileVerify');

/*Reshipping Panel End Here*/


/*Order Panel Start Here*/



Route::get('custom-order','OrderController@showOrderPage')->name('showOrderPage');
Route::get('inside-order','OrderController@showInsideOrderPage')->name('showInsideOrderPage');
Route::get('outside-order','OrderController@showOutsideOrderPage')->name('showOutsideOrderPage');
Route::get('show-order-login-page','OrderController@showOrderLogin')->name('showOrderLogin');
Route::post('order-user-login','OrderController@orderUserLogin')->name('orderUserLogin');
Route::post('order-user-register','OrderController@orderUserRegister')->name('orderUserRegister');
Route::get('order-user-logout','SuperOrderController@orderUserLogout')->name('orderUserLogout');
Route::get('order-user-dashboard','SuperOrderController@superOrderDashboard')->name('superOrderDashboard');
Route::get('order-user-profile-verify/{email}/{hash}','OrderController@orderUserProfileVerify')->name('orderUserProfileVerify');

//Inside Order
Route::get('add-inside-order','SuperOrderController@addInsideOrder')->name('addInsideOrder');
Route::get('inside-order-load-invoice','SuperOrderController@insideOrderLoadInvoice')->name('insideOrderLoadInvoice');
Route::post('add-inside-custom-order','SuperOrderController@addInsideCustomOrder')->name('addInsideCustomOrder');
Route::get('show-user-inside-pending-order','SuperOrderController@showUserInsidePendingOrder')->name('showUserInsidePendingOrder');
Route::get('show-user-inside-processing-order','SuperOrderController@showUserInsideProcessingOrder')->name('showUserInsideProcessingOrder');
Route::get('show-user-inside-active-order','SuperOrderController@showUserInsideActiveOrder')->name('showUserInsideActiveOrder');
Route::get('show-user-inside-delivery-order','SuperOrderController@showUserInsideDeliveryOrder')->name('showUserInsideDeliveryOrder');
Route::get('show-user-inside-cancel-order','SuperOrderController@showUserInsideCancelOrder')->name('showUserInsideCancelOrder');
Route::get('view-user-inside-pending-order/{id}','SuperOrderController@viewUserInsidePendingOrder')->name('viewUserInsidePendingOrder');

//order insert start here
Route::post('save-custom-order','UserController@saveCustomOrder')->name('saveCustomOrder');
Route::post('save-custom-order-outside','UserController@saveCustomOrderOutside')->name('saveCustomOrderOutside');
//order insert end Here


/*Order Panel End Here*/

