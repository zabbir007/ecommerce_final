<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title> Reshipping </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="_base_url" content="{{ url('/') }}">
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- Plugins css -->
        <link href="{{asset('reshipping/assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- App css -->
        <link href="{{asset('reshipping/assets/css/main.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('reshipping/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('reshipping/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('reshipping/assets/css/app.min.css')}}" rel="stylesheet" type="text/css"/>

        <link href="{{asset('reshipping/assets/libs/jquery-nice-select/nice-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('reshipping/assets/libs/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('reshipping/assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('reshipping/assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('reshipping/assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('reshipping/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css')}}" rel="stylesheet" type="text/css" />

        <!-- third party css -->
        <link href="{{asset('reshipping/assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('reshipping/assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('reshipping/assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('reshipping/assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <!-- Begin page -->
        <div id="wrapper">
            
            <!-- Topbar Start -->

            
            @include('includes.reshipping.headerNavigation')
            
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->

             @include('includes.reshipping.headerSidebar')
            
           
            
            <!-- Left Sidebar End -->

            <div class="content-page">

              <div class="content">
                            @yield('content')
                  </div> <!-- container -->
                
              </div> <!-- content -->

                <!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                2015 - 2018 &copy; UBold theme by <a href="">Coderthemes</a>
                            </div>
                            <div class="col-md-6">
                                <div class="text-md-right footer-links d-none d-sm-block">
                                    <a href="javascript:void(0);">About Us</a>
                                    <a href="javascript:void(0);">Help</a>
                                    <a href="javascript:void(0);">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

            </div>
            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->
        </div>
        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{asset('reshipping/assets/js/vendor.min.js')}}"></script>

        <!-- Plugins js-->
        <script src="{{asset('reshipping/assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/jquery-knob/jquery.knob.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/flot-charts/jquery.flot.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/flot-charts/jquery.flot.time.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/flot-charts/jquery.flot.tooltip.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/flot-charts/jquery.flot.selection.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/flot-charts/jquery.flot.crosshair.js')}}"></script>

        <script src="{{asset('reshipping/assets/libs/jquery-nice-select/jquery.nice-select.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/switchery/switchery.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/multiselect/jquery.multi-select.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/select2/select2.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/jquery-mockjax/jquery.mockjax.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/autocomplete/jquery.autocomplete.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

        <!-- third party js -->
        <script src="{{asset('reshipping/assets/libs/datatables/jquery.dataTables.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/datatables/buttons.flash.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/datatables/buttons.print.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/datatables/dataTables.select.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <script src="{{asset('reshipping/assets/libs/pdfmake/vfs_fonts.js')}}"></script>
        <!-- third party js ends -->


        <!-- Custom Js -->

        <script src="{{asset('customJs/reshipping/addInsideReshipping.js')}}"></script>
        

        <!-- Datatables init -->
        <script src="{{asset('reshipping/assets/js/pages/datatables.init.js')}}"></script>

        <!-- Dashboar 1 init js-->
        <script src="{{asset('reshipping/assets/js/pages/dashboard-1.init.js')}}"></script>

        <!-- App js-->
        <script src="{{asset('reshipping/assets/js/app.min.js')}}"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCirSuiG-0hyQm_U1A_3ksf1iYHRSqCoDk&libraries=places&callback=initAutocomplete" async defer></script>

    </body>
</html>
