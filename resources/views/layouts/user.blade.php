<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="_base_url" content="{{ url('/') }}">
        <title>@yield('title','UshopnShip')</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('user/images/fav.png')}}" />
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('user/css/xzoom.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/jquery.fancybox.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/magnific-popup.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link rel="stylesheet" href="{{asset('user/css/normalize.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/main.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/responsive.css')}}">
		<link rel="stylesheet" href="{{asset('user/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/slicknav.min.css')}}" />
        <link rel="stylesheet" href="{{asset('user/css/pricerange.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/lightbox.min.css')}}">
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script> -->
    </head>
    <body>
        @include('includes.user.headerNavigation')
          <div class="body-start">
            @yield('content')
          </div>
        @include('includes.user.footerNavigation')
        <script src="{{asset('user/js/vendor/modernizr-3.5.0.min.js')}}"></script>
        <script src="{{asset('user/js/main.js')}}"></script>
        
        <script src="{{asset('user/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('user/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('user/js/jquery.slicknav.min.js')}}"></script>
        <script src="{{asset('user/js/xzoom.min.js')}}"></script>
        <script src="{{asset('user/js/jquery.hammer.min.js')}}"></script>
        <script src="{{asset('user/js/jquery.fancybox.js')}}"></script>
        <script src="{{asset('user/js/magnific-popup.js')}}"></script>
        <script src="{{asset('user/js/foundation.min.js')}}"></script>
        <script src="{{asset('user/js/setup.js')}}"></script>
        <script src="{{asset('user/js/clientslider.js')}}"></script>
        <script src="{{asset('user/js/lightbox-plus-jquery.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        
        <!-- Custom Js -->
        <script src="{{asset('customJs/userjs/checkEmailPhone.js')}}"></script>
        <script src="{{asset('customJs/userjs/addSubscribeUser.js')}}"></script>
        <script src="{{asset('customJs/userjs/addToCart.js')}}"></script>
        <script src="{{asset('customJs/userjs/addToWishlist.js')}}"></script>
        <script src="{{asset('customJs/userjs/sameShipping.js')}}"></script>
        
        <!--Start of Tawk.to Script-->
          <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
             (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5e0b3cfe7e39ea1242a27b29/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
          </script>
        <!--End of Tawk.to Script-->
        
        <!-- Custom Js -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCirSuiG-0hyQm_U1A_3ksf1iYHRSqCoDk&libraries=places&callback=initAutocomplete" async defer></script>
    </body>
</html>
