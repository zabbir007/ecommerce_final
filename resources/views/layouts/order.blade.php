<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title> Order panel</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Tomattos" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- Plugins css -->
        <link href="{{asset('order/assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- App css -->
        <link href="{{asset('order/assets/css/main.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('order/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('order/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('order/assets/css/app.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('order/assets/css/custom.css')}}" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <!-- Begin page -->
        <div id="wrapper">
            
            <!-- Topbar Start -->

            
            @include('includes.order.headerNavigation')
            
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->

             @include('includes.order.headerSidebar')
            
           
            
            <!-- Left Sidebar End -->

            <div class="content-page">

              <div class="content">
                            @yield('content')
                  </div> <!-- container -->
                
              </div> <!-- content -->

                <!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                2019 &copy; Develop by <a href="">Tomattos</a>
                            </div>
                            <div class="col-md-6">
                                <div class="text-md-right footer-links d-none d-sm-block">
                                    <a href="javascript:void(0);">About Us</a>
                                    <a href="javascript:void(0);">Help</a>
                                    <a href="javascript:void(0);">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

            </div>
            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->
        </div>
        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{asset('order/assets/js/vendor.min.js')}}"></script>

        <!-- Plugins js-->
        <script src="{{asset('order/assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{asset('order/assets/libs/jquery-knob/jquery.knob.min.js')}}"></script>
        <script src="{{asset('order/assets/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('order/assets/libs/flot-charts/jquery.flot.js')}}"></script>
        <script src="{{asset('order/assets/libs/flot-charts/jquery.flot.time.js')}}"></script>
        <script src="{{asset('order/assets/libs/flot-charts/jquery.flot.tooltip.min.js')}}"></script>
        <script src="{{asset('order/assets/libs/flot-charts/jquery.flot.selection.js')}}"></script>
        <script src="{{asset('order/assets/libs/flot-charts/jquery.flot.crosshair.js')}}"></script>

        <!-- Dashboar 1 init js-->
        <script src="{{asset('order/assets/js/pages/dashboard-1.init.js')}}"></script>

        <!-- App js-->
        <script src="{{asset('order/assets/js/app.min.js')}}"></script>

    </body>
</html>
