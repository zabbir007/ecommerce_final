<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="_base_url" content="{{ url('/') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="manifest" href="site.webmanifest">
        <link rel="apple-touch-icon" href="icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="{{asset('user/css/normalize.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/main.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/responsive.css')}}">
		    <link rel="stylesheet" href="{{asset('user/css/bootstrap.min.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('user/css/slicknav.min.css')}}" />
    </head>
<body>
<!--top-header------------------------------------------------------------------->
<section class="top-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <p class="ml-5">Shop and Ship anywhere in the world! <span>Wellcome to Ushopnship!</span>    Need Help? Call Us:<span>  01841-167177,01841-167178</span> </p>
      </div>
      <div class="col-md-6">
        <ul class="right-nav mr-5">
          <li> <a href="#"> <i class="fab fa-facebook-f"></i> </a> </li>
          <li> <a href="#"> <i class="fab fa-twitter"></i> </a> </li>
          <li> <a href="#"> <i class="fab fa-google-plus-g"></i> </a> </li>
          <li> <a href="#"> <i class="fab fa-youtube"></i> </a> </li>
        </ul>
      </div>
    </div>
  </div>
</section>
  <!--main nav------------------------------------------------------------------->
<section class="main-nav">
  <nav class="navbar navbar-expand-lg navbar-light">
    <?php 
      $companyInfo=DB::table('company_setting')
                        ->where('status',1)
                        ->first();
    ?>
    <a class="navbar-brand allpadding" href="{{route('welcome')}}"> <img src="{{$companyInfo->companyLogo}}"class="img-fluid" alt="logo"> </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav  font-weight-bold">
          <li class="nav-item active">
            <a class="nav-link" href="{{route('welcome')}}">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{route('showStore')}}">Store</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{route('showAboutUs')}}">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{route('showContactUs')}}">Contact Us</a>
          </li>
        </ul>
        <ul class="main-nav-right ml-auto">
           <?php 
            if (Session::get('userId')) {
          ?>
          <li>
            <div class="btn-group dropleft">
              <a class="btn dropdown-toggle" href="profile.html" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i> Profile
              </a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="{{route('userProfile')}}">Profile</a>
                <a class="dropdown-item" href="{{route('userLogout')}}">Logout</a>
              </div>
            </div>
          </li>
          <?php
            }else{
          ?>
            <li> <a href="{{route('showUserLogin')}}"> Login or  </a> </li>
            <li> <a href="{{route('showUserLogin')}}"> Register </a> </li>
          <?php 
            }
          ?>
        </ul>
      </div>
  </nav>

  <div class="nav-2 allpadding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <div class="accordion-menu">
              <div id="accordion">
                <div class="card card-menu">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                    </h5>
                  </div>
                  <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body card-body-menu-popup">
                      <div class='cssmenuvv'>
                      <ul>
                         <li class='active'> <a href="{{route('welcome')}}"> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <div class="manu">
              <ul>
                <li>
                  <a href="{{route('userOrder')}}"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
            </div>
          </div>
        </div>
        </div>
      </div>
</section>
<!--box------------------------------------------------------------------->

<section class="wrapper mt-5 allpadding">
  <div class="container-fluid">
    <h2 class="text-center">How We Can Amaze You <span>To Day ?</span> </h2>
    <div class="row mt-5">
     <div class="col-md-3 col-sm-6 col-6">
       <div class="box1">
          <a href="{{route('shop')}}">
            <img src="{{asset('user/images/Shop.png')}}" class="img-fluid" alt="">
          </a>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-6">
       <div class="box1 box2">
          <a href="{{route('showOrderPage')}}">
            <img src="{{asset('user/images/Shop-From.png')}}" class="img-fluid" alt="">
          </a>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-6">
       <div class="box1 box3">
          <a href="{{route('showReshippingPage')}}">
            <img src="{{asset('user/images/ship-localy.png')}}"class="img-fluid" alt="">
          </a>
       </div>
       </div>
       <div class="col-md-3 col-sm-6 col-6">
         <div class="box1 box4">
            <a href="{{route('shop')}}">
              <img src="{{asset('user/images/Shipping-Globally.png')}}"class="img-fluid" alt="">
            </a>
         </div>
       </div>
   </div>
  </div>
</section>
<!--Banner--------------------------------------------------->
<section class="banner allpadding">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <div class="banner-image">
          <a href="#"><img src="{{asset('user/images/banner15.jpg')}}" class="img-fluid" alt=""></a>
        </div>
      </div>
      <div class="col-md-6">
        <div class="banner-image">
          <a href="#"><img src="{{asset('user/images/download.jpg')}}" class="img-fluid" alt=""></a>
        </div>
      </div>
  </div>
</section>
<!--PRODUCT------------------------------------------------------------------->
<section class="product mt-3 allpadding">
  <div class="container-fluid">
    <div class="title-box">
      <h4>LATEST ARRIVALS</h4>
    </div>
    <div class="row">
      <div class="col-md-2">
        <div class="row">
        @foreach($latestArrival as $latest)
        <div class="col-md-12 col-6">
          <div class="media media-product">
            <a href="{{route('singleProduct',[$latest->id])}}"><img class="mr-3" src="{{asset( $latest->image1 )}}" alt="Generic placeholder image"></a>
            <div class="media-body product-body">
              <div class="product-info">
                <a href="{{route('singleProduct',[$latest->id])}}">{{$latest->productName}}</a>
              </div>
             <!--  <div class="rateit-range">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star-half-alt"></i>
              </div> -->
              <!-- <div class="product-price">
                <span class="price"> $450.99 </span>
              </div> -->
              <?php
                $checkDiscount=$latest->discount;
                if ($checkDiscount=='') {
              ?>
                <span class="price">{{$latest->currency_prefix.' '.$latest->price}}</span><br>
              <?php
                }else{
              ?>
                <span class="price">{{$latest->currency_prefix.' '.$latest->discountPrice}}</span><br>
                <span><strike>{{$latest->currency_prefix.' '.$latest->price}}</strike> -{{$latest->discount}}%</span>
              <?php
                }
              ?>
            </div>
          </div>
          <hr>
        </div>
        @endforeach
        


        </div>
      </div>
      <!--Right side product---------------------------->
      <div class="col-md-10">
        <div class="row mt-2">  

          @foreach($onsaleProduct as $onsale)
           <div class="col-md-2 col-sm-6 col-6">
             <div class="p-grid product-hidden">
                 <div class="product-image">
                     <a href="{{route('singleProduct',[$onsale->id])}}"class="text-decoration-none">
                         <img class="pic-1" src="{{asset( $onsale->image1 )}}">
                         <img class="pic-2" src="{{asset( $onsale->image2 )}}">
                     </a>
                      <?php
                        $checkDiscount=$onsale->discount;
                        if ($checkDiscount!='') {
                      ?>
                     <span class="product-discount-label">-{{$onsale->discount}}%</span>
                     <?php
                      }else{
                     ?>
                     <span class="product-discount-label">-0%</span>
                     <?php } ?>
                     <ul class="social">
                       <li>
                         <button type="button"class="add-to-Wishlist" name="button"data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></button>
                       </li>
                       <li>
                       <button type="button" class="add-to-Wishlist addcart" data-id="{{$onsale->id}}" name="button" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></button>
                       </li>
                     </ul>
                 </div>
                 <div class="product-content">
                     <h3 class="title">
                         <a href="{{route('singleProduct',[$onsale->id])}}"class="text-decoration-none">{{$onsale->productName}}</a>
                     </h3>
                     <span class="product-category">
                         <a href="{{route('singleProduct',[$onsale->id])}}"class="text-decoration-none">{{$onsale->department_type.' - '.$onsale->category_type}}</a>
                     </span>
                     <?php
                        $checkDiscount=$onsale->discount;
                        if ($checkDiscount=='') {
                      ?>
                     <div class="price ">{{$onsale->currency_prefix.' '.$onsale->price}}</div>
                      <?php }else{ ?>
                     <div class="price">{{$onsale->currency_prefix.' '.$onsale->discountPrice}}
                          <span>{{$onsale->currency_prefix.' '.$onsale->price}}</span>
                      </div>
                      <?php } ?>
                     <button type="button" class="add-to-cart addcart" data-id="{{$onsale->id}}" name="button">ADD TO CART</button>
                 </div>
             </div>
           </div>
           @endforeach

       <div id="loadMore" class="mt-4">
         <button type="button" class="btn btn-danger">Load More</button>
       </div>
     </div><!--row End------->
      </div>

    </div><!--end row------------->
  </div>
</section>
<!--Banner2--------------------------------------->
<section class="banner-2 mt-5">
  <div class="banner-overley">

  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
      <div class="banner-contain text-center ">
        <h2 >2019</h2>
        <h3 >fashion trends</h3>
        <h4 >special offer</h4>
      </div>
      </div>
      <div class="col-md-6">
        <div class="banner-contain text-center ">
          <h2 >ciber sale</h2>
          <h3 >on summer collections</h3>
          <h4 >50% or more off</h4>
        </div>
      </div>
    </div>
  </div>
</section>
<!--SOUDI PRODUCT------------------------------------------------------>
<section class="product-2 mt-5">
  <div class="container-fluid">
    <div class="row">
      <!--Left Site---------------------------------------------------------->
      <div class="col-md-4">
        <div class="row">
          
          @foreach($leftGarments as $left)
          <div class="col-md-4 col-sm-6 col-6">
             <div class="product-grid">
                <div class="product-image">
                    <a href="{{route('singleProduct',[$left->id])}}">
                        <img class="pic-1" src="{{asset( $left->image1 )}}">
                        <img class="pic-2" src="{{asset( $left->image2 )}}">
                    </a>
                    <span class="product-trend-label">Garments</span>
                     <?php
                        $checkDiscount=$left->discount;
                        if ($checkDiscount!='') {
                      ?>
                     <span class="product-discount-label">-{{$left->discount}}%</span>
                     <?php
                      }else{
                     ?>
                     <span class="product-discount-label">-0%</span>
                     <?php } ?>
                    <ul class="social">
                      <li>
                      <button type="button"class="add-to-Wishlist" name="button"data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></button>
                      </li>
                      <li>
                      <button type="button"class="add-to-Wishlist addcart" data-id="{{$left->id}}" name="button" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></button>
                      </li>
                    </ul>
                </div>
                <div class="product-content">
                    <h3 class="title"><a href="{{route('singleProduct',[$left->id])}}"class="text-decoration-none">{{$left->productName}}</a></h3>
                    <?php
                        $checkDiscount=$left->discount;
                        if ($checkDiscount=='') {
                      ?>
                     <div class="price discount">{{$left->currency_prefix.' '.$left->price}}</div>
                      <?php }else{ ?>
                     <div class="price">{{$left->currency_prefix.' '.$left->discountPrice}}
                          <span>{{$left->currency_prefix.' '.$left->price}}</span>
                      </div>
                      <?php } ?>
                    
                </div>
            </div>
          </div>
          @endforeach

        </div>
      </div>
      <!--MIDDEL SITE---------------------------------------------------->
      <div class="col-md-4">
        <div class="pp">
          <img src="{{asset('user/images/ship-nd-shop.jpg')}}"class="img-fluid" alt="">
        </div>

      </div>

      <!--RIGHT SIDE---------------------------------------------->
      <div class="col-md-4 ">
        <div class="row">
          
          @foreach($rightGarments as $right)
          <div class="col-md-4 col-sm-6 col-6">
             <div class="product-grid">
                <div class="product-image">
                    <a href="{{route('singleProduct',[$right->id])}}">
                        <img class="pic-1" src="{{asset( $right->image1 )}}">
                        <img class="pic-2" src="{{asset( $right->image2 )}}">
                    </a>
                    <span class="product-trend-label">Garments</span>
                     <?php
                        $checkDiscount=$right->discount;
                        if ($checkDiscount!='') {
                      ?>
                     <span class="product-discount-label">-{{$right->discount}}%</span>
                     <?php
                      }else{
                     ?>
                     <span class="product-discount-label">-0%</span>
                     <?php } ?>
                    <ul class="social">
                      <li>
                      <button type="button"class="add-to-Wishlist" name="button"data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></button>
                      </li>
                      <li>
                      <button type="button"class="add-to-Wishlist addcart" data-id="{{$right->id}}" name="button" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></button>
                      </li>
                    </ul>
                </div>
                <div class="product-content">
                    <h3 class="title"><a href="{{route('singleProduct',[$right->id])}}"class="text-decoration-none">{{$right->productName}}</a></h3>
                    <?php
                        $checkDiscount=$right->discount;
                        if ($checkDiscount=='') {
                      ?>
                     <div class="price discount">{{$right->currency_prefix.' '.$right->price}}</div>
                      <?php }else{ ?>
                     <div class="price">{{$right->currency_prefix.' '.$right->discountPrice}}
                          <span>{{$right->currency_prefix.' '.$right->price}}</span>
                      </div>
                      <?php } ?>
                    
                </div>
            </div>
          </div>
          @endforeach


        </div>
      </div>
    </div>
  </div>
</section>


<section class="footer-1">
  <div class="container">
    <div class="footer-header">
        <h4>Need help? Call our award-winning support team 24/7 at +880 2-9511236</h4>
    </div>
  </div>
</section>
<!--FOOTER------------------------------------------------------------------->
<footer class="site-footer">
   <div class="container-fluid">
     <div class="row">
       <div class="col-md-9">
         <div class="row">
           <div class="col-md-3 col-12">
            <?php 
              $companyInfo=DB::table('company_setting')
                                ->where('status',1)
                                ->first();
            ?>
             <h6 class=" uShopnShip"> {{$companyInfo->companyName}}</h6>
             <ul class="footer-links ">
               <li> <strong>Visit Us</strong> </li>
               <li>{{$companyInfo->companyAddress}}</li>
               <li> <strong>Call Us 24/7</strong>  </li>
               <li> {{$companyInfo->companyPhone}}</li>
               <li> <strong>Drop us a line</strong> </li>
               <li>{{$companyInfo->companyEmail}}</li>
               
               <li class="mb-2"> Copyright 2019 | {{$companyInfo->companyName}} </li>
             </ul>

           </div>
           <div class="col-md-3 col-6 help">
             <h6>Help & Support</h6>
             <ul class="footer-links">
               <li> <a href="{{route('showFaqPage')}}">FAQ’s</a> </li>
               <li> <a href="{{route('showForgetPassword')}}">Lost password?</a> </li>
               <li> <a href="{{route('showSupportPage')}}">Support & Service</a> </li>
               <li> <a href="{{route('showReportPage')}}">Report An Issue</a> </li>
             </ul>
           </div>
           <div class="col-md-3 col-6 information">
             <h6>Information</h6>
             <ul class="footer-links">
               <li> <a href="{{route('showAboutUs')}}">About Us</a> </li>
               <li> <a href="{{route('showContactUs')}}">Contact Us</a> </li>
               <li> <a href="{{route('showStoreLocationPage')}}">Store Locator</a> </li>
             </ul>
           </div>

           <div class="col-md-3 col-6 legal">
             <h6>Legal</h6>
             <ul class="footer-links">
               <li> <a href="{{route('showPrivacyPage')}}">Privacy Policy</a> </li>
               <li> <a href="{{route('showTermsPage')}}">Terms Of Service</a> </li>
               <li> <a href="{{route('showWarrantyPage')}}">Warranty Policy</a> </li>
               <li> <a href="{{route('showRefundPage')}}">Refund & Return Policy</a> </li>
             </ul>
           </div>
         </div>
         <ul class="social-icons ">
           <li> <a class="facebook" href="#"> <img src="{{asset('user/images/1.png')}}" alt="footer-img"> </a> </li>
           <li> <a class="twitter" href="#"> <img src="{{asset('user/images/2.png')}}" alt="footer-img"> </a> </li>
           <li> <a class="dribbble" href="#"> <img src="{{asset('user/images/3.png')}}" alt="footer-img"> </a> </li>
           <li> <a class="linkedin" href="#"> <img src="{{asset('user/images/4.png')}}" alt="footer-img"> </a> </li>
         </ul>
       </div><!---end col-8-------------->

       <div class="col-md-3 subscribe">
         <div class="left-content">
          <h3 class="title">Subscribe Our Newsletter</h3>
          <div class="description">Sign up to our newsletter to get updates &amp; offers along with product support and new inventory.</div>
            <p>Email Address:</p>
        </div>
        <form method="post">
          <div class="form-row">
            <input type="text" class="form-control subscribe" name="subscribeUserEmail" placeholder="Your Email Address" required>
            <div class="subscribe-btn ml-2">
              <button class="btn btn-primary insertSubscribeUser" type="button">Subscribe</button>
          </div>
        </form>
       </div>

       <div class="rounded-social-buttons">
          <a class="social-button facebook" href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a>
          <a class="social-button twitter" href="https://www.twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a>
          <a class="social-button linkedin" href="https://www.linkedin.com/" target="_blank"><i class="fab fa-linkedin"></i></a>
          <a class="social-button youtube" href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube"></i></a>
          <a class="social-button instagram" href="https://www.instagram.com/" target="_blank"><i class="fab fa-instagram"></i></a>
       </div>
       </div>
     <hr>
   </div>
   <div class="container-fluid">
     <div class="row">
       <div class="col-md-8 col-sm-6 col-xs-12">
         <div class="store-img">
           <!-- <a href="#"> <img src="images/apple.jpg" class="img-fluid" alt="footer-img"> </a>
           <a href="#"> <img src="images/google.jpg" class="img-fluid"> </a> -->
         </div>
       </div>

       <div class="col-md-4 col-sm-6 col-xs-12">
       </div>
     </div>
   </div>
</footer>
<script src="{{asset('admin/assets/libs/sweetalert2/sweetalert.min.js')}}"></script>
<script src="{{asset('user/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('customJs/userjs/addToCart.js')}}"></script>
<script src="{{asset('customJs/userjs/addSubscribeUser.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="{{asset('user/js/vendor/modernizr-3.5.0.min.js')}}"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
<script src="{{asset('user/js/main.js')}}"></script>

<script src="{{asset('user/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('user/js/bootstrap.min.js')}}"></script>
<script src="{{asset('user/js/jquery.slicknav.min.js')}}"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> -->

<script>
	$(function(){
		$('.cssmenuvv').slicknav();
	});
</script>

<!--Load More Button----------------------------------->
<script type="text/javascript">
$( document ).ready(function () {
$(".product-hidden").slice(0, 12).show();

  $("#loadMore").on('click', function (e) {
    e.preventDefault();
    $(".product-hidden:hidden").slice(0, 6).slideDown();
    if ($(".product-hidden:hidden").length == 0) {
      $("#loadMore").fadeOut('slow');
    }
  });
});
</script>

<script type="text/javascript">

    $(".addcart").click(function(){ 
        var counter = parseInt($("#hiddenVal").val());
        counter++;
        $("#hiddenVal").val(counter);
        $("#theCount").text(counter);
    });

</script>
<!--Start of Tawk.to Script-->
  <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
     (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5e0b3cfe7e39ea1242a27b29/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
  </script>
 <!--End of Tawk.to Script-->
</body>
</html>
