@extends('layouts.admin')

@section('title') Basic Order @endsection

@section('content')

<form class="needs-validation" method="post" action="{{route('saveOrderBasicSetting')}}" novalidate>
    @csrf
        <?php 
        $message=Session::get('message');
        if($message){
        ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php
                    echo $message;
                    Session::put('message','');
                ?>
            </div>
        <?php
        }
        ?>

        <?php 
        $message=Session::get('messageWarning');
        if($message){
        ?>
            <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                <?php
                    echo $message;
                    Session::put('messageWarning','');
                ?>
            </div>
        <?php   
        }
        ?>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="product-description">Inside Order Payment Information<span class="text-danger">*</span></label>
                    <textarea class="form-control" id="product-description" rows="5" placeholder="Please Describe How To Shipping This Website" name="howOrder"></textarea>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="product-description1">Outside Order Payment Information<span class="text-danger">*</span></label>
                    <textarea class="form-control" id="product-description1" rows="5" placeholder="Which Type of Product Import" name="importProduct"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="product-description">How to Order Information<span class="text-danger">*</span></label>
                    <textarea class="form-control" id="product-description2" rows="5" placeholder="Which Type of Product Export" name="exportProduct"></textarea>
                </div>
                <button class="btn btn-primary" type="submit">Add Basic Setting</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection