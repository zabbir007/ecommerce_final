@extends('layouts.admin')

@section('title') Order Basic Setting @endsection

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show Order Basic Setting</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addOrderBasicSetting')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add New</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">First Payment Information</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($orderBasicSettingInfo as $orderBasic)
                        <tr>
                            <td class="text-center" width="60%"><?php echo $orderBasic->howOrder ?></td>
                            <td class="text-center" width="20%"><?php if($orderBasic->status=='1'){echo "Active";}else if($orderBasic->status=='0'){echo "De-Active";} ?></td>
                            <td class="text-center" width="20%">
                                <a href="{{route('editOrderBasicSetting',[$orderBasic->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnOrderBasicSettingDelete" id="{{$orderBasic->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection