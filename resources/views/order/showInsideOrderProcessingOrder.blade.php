@extends('layouts.admin')

@section('title') Inside Processing Order @endsection

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Inside Order Processing Order</h4>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">User Name</th>
                            <th class="text-center">Address</th>
                            <th class="text-center">Order Date</th>
                            <th class="text-center">Order Number</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($pendingOrderInfo as $pending)
                        <tr>
                            <td class="text-center">{{$pending->userName}}</td>
                            <td class="text-center">{{$pending->address}}</td>
                            <td class="text-center">{{$pending->createAt}}</td>
                            <td class="text-center">{{$pending->orderNumber}}</td>
                            <td class="text-center"><?php if($pending->status=='processing'){echo "Processing";}?></td>
                            <td class="text-center">
                                <a href="{{route('viewInsideOrderProcessingOrder',[$pending->id])}}" target="_blank" class="action-icon"> <i class="mdi mdi-eye"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection