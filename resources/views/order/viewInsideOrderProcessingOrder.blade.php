@extends('layouts.admin')

@section('title') Inside Processing Order @endsection

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-60"><font color="red">User Information</font></h4>
		                    <p class="mb-1"><b>Client Name:</b> {{$insideOrderInformation->userName}}</p>
		                    <p class="mb-0"><b>Client Email:</b> {{$insideOrderInformation->userEmail}}</p>
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Client Phone:</b> {{$insideOrderInformation->userPhone}}</p>
		            <p class="mb-0"><b>Register Date:</b> {{$insideOrderInformation->createTime}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                    <h4 class="mt-0 mb-2 font-30"><font color="red">Price Information</font></h4>
		                    <p class="mb-1">
		                    	<label for="heard">Box Type</label><br>
		                         <?php
				                    $pkgCount=count($amount);
				                    for($i=0;$i<$pkgCount;$i++){
				                       
				                        $testAmountInfo=DB::table('inside_order_box_price')
										                  ->where('id',$amount[$i])
										                  ->get();
											foreach($testAmountInfo as $te){
									           echo $te->boxSize.' , ';
									        }
									         
				                    }
			                  
				            	?>
		                    </p>
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Product Price:</b> {{$insideOrderInformation->price}}</p>
                    <p class="mb-0"><b>Quantity:</b> {{$insideOrderInformation->quantity}}</p>
		        </div>

		        <div class="col-sm-6">
		            <p class="mb-1"><b>Admin Note:</b></p>
                    <p class="mb-0">
                    	{{$insideOrderInformation->adminNote}}
                    </p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>

	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-30"><font color="red">Inside Order Information</font></h4>
		                    <p class="mb-1"><b>Address:</b> {{$insideOrderInformation->address}}</p>
		                    <p class="mb-1"><b>Country:</b> {{$insideOrderInformation->country}}</p>
		                    <p class="mb-1"><b>State:</b> {{$insideOrderInformation->state}}</p>
		                    <p class="mb-1"><b>City:</b> {{$insideOrderInformation->city}}</p>
		                    <p class="mb-1"><b>Zip:</b> {{$insideOrderInformation->zip}}</p>
		                    <p class="mb-1"><b>House:</b> {{$insideOrderInformation->house}}</p>
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Road:</b> {{$insideOrderInformation->road}}</p>
                    <p class="mb-1"><b>Product Link:</b><a href="{{$insideOrderInformation->product_link}}" target="_blank">Click Here</a></p>
                    <p class="mb-1"><b>Product Name:</b> {{$insideOrderInformation->productName}}</p>
                    <p class="mb-1"><b>Price:</b> {{$insideOrderInformation->price}}</p>
                    <p class="mb-1"><b>Quantity:</b> {{$insideOrderInformation->quantity}}</p>
                    <p class="mb-1"><b>Total Amount:</b> {{$insideOrderInformation->totalAmount}}</p>
                    <p class="mb-1"><b>Order Date:</b> {{$insideOrderInformation->createAt}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-12">
		            <div class="card-body">
	                  <div class="info-box text-center">
	                    <h3>Total Price</h3>
	                    <div class="table-responsive">
	                      <table class="table mb-0">
	                          <thead>
	                          <tr>
	                            <th>Product</th>
	                            <th>Price</th>
	                          </tr>
	                          </thead>
	                          <tbody>
	                          <tr>
	                            <th scope="row">Services Price</th>
	                            <td>{{$insideOrderInformation->totalAmount}}</td>
	                          </tr>
	                          <tr>
	                            <th scope="row">Product Price</th>
	                            <td>
	                            	{{$insideOrderInformation->price}}
	                            </td>
	                            
	                          </tr>
	                          <tr>
	                            <th scope="row">Box Price</th>
	                            <td>
	                              {{$insideOrderInformation->boxPrice}}
	                            </td>
	                          </tr>
	                          <tr>
	                            <th scope="row">Discount %</th>
	                            <td>
	                              {{$insideOrderInformation->discount.' %'}}
	                            </td>
	                          </tr>
	                          <tr>
	                            <th scope="row">Total</th>
	                            <td>
									{{$insideOrderInformation->secondFinalAmount}}
	                            </td>
	                          </tr>
	                          </tbody>
	                      </table>
	                    </div>
	                  </div>
	                </div>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>

</div>
@endsection