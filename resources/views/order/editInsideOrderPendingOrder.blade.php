@extends('layouts.admin')

@section('title') Inside Pending Order @endsection

@section('content')
<form method="post" action="{{route('updateInsideOrderPendingOrder')}}">
	@csrf
	<?php 
    $message=Session::get('message');
    if($message){
    ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php
                echo $message;
                Session::put('message','');
            ?>
        </div>
    <?php
    }
    ?>
<div class="row">
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-60"><font color="red">User Information</font></h4>
		                    <p class="mb-1"><b>Client Name:</b> {{$insideOrderInformation->userName}}</p>
		                    <p class="mb-0"><b>Client Email:</b> {{$insideOrderInformation->userEmail}}</p>
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Client Phone:</b> {{$insideOrderInformation->userPhone}}</p>
		            <p class="mb-0"><b>Register Date:</b> {{$insideOrderInformation->createTime}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                    <h4 class="mt-0 mb-2 font-30"><font color="red">Price SetUp</font></h4>
		                    <p class="mb-1">
		                    	<label for="heard">Select Box</label><br>
		                         @foreach($insideBoxPrice as $inside)
		                          <input class="my-activity" type="checkbox" name="boxId[]" value="{{$inside->id}}"> {{$inside->boxSize.' Price'.'= '.$inside->boxPrice}}<br/>
		                         @endforeach
		                    </p>
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Product Price:</b> <input type="text" class="form-control" value="{{$insideOrderInformation->price}}" id="getProductPrice" name="updateProductPrice" required placeholder="Product Price"/></p>
                    <p class="mb-0"><b>Quantity:</b> <input type="text" class="form-control" value="{{$insideOrderInformation->quantity}}" id="getNumber" name="updateQuantity" required placeholder="Quantity"/></p>
		        </div>

		        <div class="col-sm-6">
		            <p class="mb-1"><b>Admin Note:</b></p>
                    <p class="mb-0">
                    	<textarea required class="form-control" name="adminNote"></textarea>
                    </p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>

	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-30"><font color="red">Inside Order Information</font></h4>
		                    <p class="mb-1"><b>Address:</b> {{$insideOrderInformation->address}}</p>
		                    <p class="mb-1"><b>Country:</b> {{$insideOrderInformation->country}}</p>
		                    <p class="mb-1"><b>State:</b> {{$insideOrderInformation->state}}</p>
		                    <p class="mb-1"><b>City:</b> {{$insideOrderInformation->city}}</p>
		                    <p class="mb-1"><b>Zip:</b> {{$insideOrderInformation->zip}}</p>
		                    <p class="mb-1"><b>House:</b> {{$insideOrderInformation->house}}</p>
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Road:</b> {{$insideOrderInformation->road}}</p>
                    <p class="mb-1"><b>Product Link:</b><a href="{{$insideOrderInformation->product_link}}" target="_blank">Click Here</a></p>
                    <p class="mb-1"><b>Product Name:</b> {{$insideOrderInformation->productName}}</p>
                    <p class="mb-1"><b>Price:</b> {{$insideOrderInformation->price}}</p>
                    <p class="mb-1"><b>Quantity:</b> {{$insideOrderInformation->quantity}}</p>
                    <p class="mb-1"><b>Total Amount:</b> {{$insideOrderInformation->totalAmount}}</p>
                    <p class="mb-1"><b>Order Date:</b> {{$insideOrderInformation->createAt}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-12">
		            <div class="card-body">
	                  <div class="info-box text-center">
	                    <h3>Total Price</h3>
	                    <div class="table-responsive">
	                      <table class="table mb-0">
	                          <thead>
	                          <tr>
	                            <th>Product</th>
	                            <th>Price</th>
	                          </tr>
	                          </thead>
	                          <tbody>
	                          <tr>
	                            <th scope="row">Services Price</th>
	                            <td>{{$insideOrderInformation->totalAmount}}</td>
	                            <input type="hidden" name="getServicePrice" id="getServicePrice" value="{{$insideOrderInformation->totalAmount}}">
	                          </tr>
	                          <tr>
	                            <th scope="row">Product Price</th>
	                            <td id="productPrice">
	                            	
	                            </td>
	                            
	                          </tr>
	                          <tr>
	                            <th scope="row">Box Price</th>
	                            <td id="boxPrice">
	                              
	                            </td>
	                          </tr>
	                          <tr>
	                            <th scope="row">Discount %</th>
	                            <td>
	                              <input data-parsley-type="alphanum" name="discount" type="text"class="form-control" id="discount" required placeholder="% Amount"/>
	                            </td>
	                          </tr>

	                          <tr>
	                            <th scope="row">Additional Price %</th>
	                            <td>
	                              <input data-parsley-type="alphanum" name="additionalPricePer" type="text"class="form-control" id="additionalPricePer" required placeholder="% Additional Price"/>
	                            </td>
	                          </tr>
                             
	                          <tr>
	                            <th scope="row">Additional Price</th>
	                            <td>
	                              <input data-parsley-type="alphanum" name="additionalPrice" type="text"class="form-control" id="additionalPrice" required placeholder="Additional Price"/>
	                            </td>
	                          </tr>

	                          <tr>
	                            <th scope="row">Total</th>
	                            <td id="showTotal">

	                            </td>
	                            
	                            <div id="showTotalAmount">
	                    
	                            </div>
	                          </tr>
	                          </tbody>
	                      </table>
	                    </div>
	                  </div>
	                </div>
		        </div>
		        <input type="hidden" name="id" value="{{$insideOrderInformation->id}}">
		        
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
		<button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">Update Order</button>
	</div>

</div>
</form>

@endsection