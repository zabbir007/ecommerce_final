<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Custom Order</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('user/css/normalize.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/main.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/responsive.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/bootstrap.min.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    </head>
    <body>
      <section class="top-header">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6">
              <p class="ml-5">Shop and Ship anywhere in the world! <span>Wellcome to Ushopnship!</span>    Need Help? Call Us:<span>  01841-167177,01841-167178</span> </p>
            </div>
            <div class="col-md-6">
              <ul class="right-nav mr-5">
                <li> <a href="#"> <i class="fab fa-facebook-f"></i> </a> </li>
                <li> <a href="#"> <i class="fab fa-twitter"></i> </a> </li>
                <li> <a href="#"> <i class="fab fa-google-plus-g"></i> </a> </li>
                <li> <a href="#"> <i class="fab fa-youtube"></i> </a> </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
<section class="main-nav">
  <nav class="navbar navbar-expand-lg navbar-light">
    <?php 
      $companyInfo=DB::table('company_setting')
                        ->where('status',1)
                        ->first();
    ?>
    <a class="navbar-brand allpadding" href="{{route('welcome')}}"> <img src="{{$companyInfo->companyLogo}}"class="img-fluid" alt="logo"> </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav  font-weight-bold">
          <li class="nav-item active">
            <a class="nav-link" href="{{route('welcome')}}">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{route('showStore')}}">Store</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{route('showAboutUs')}}">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{route('showContactUs')}}">Contact Us</a>
          </li>
        </ul>
        <ul class="main-nav-right ml-auto">
           <?php 
            if (Session::get('userId')) {
          ?>
          <li>
            <div class="btn-group dropleft">
              <a class="btn dropdown-toggle" href="profile.html" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i> Profile
              </a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="{{route('userProfile')}}">Profile</a>
                <a class="dropdown-item" href="{{route('userLogout')}}">Logout</a>
              </div>
            </div>
          </li>
          <?php
            }else{
          ?>
          <li> <a href="{{route('showUserLogin')}}"> Login or  </a> </li>
              <li> <a href="{{route('showUserLogin')}}"> Register </a> </li>
          <?php 
            }
          ?>
        </ul>
      </div>
  </nav>

  <div class="nav-2 allpadding">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2">
          <div class="accordion-menu">
            <div id="accordion1">
              <div class="card card-menu">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                      <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                  </h5>
                </div>
                <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion1">
                  <div class="card-body card-body-menu-popup">
                    <div class='cssmenuvv'>
                    <ul>
                         <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
        </div>
        <div class="col-md-4">
          <div class="manu">
            <ul>
                <li>
                  <a href="#"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                <!-- <li>
                   <a href="#"> <i class="far fa-heart"></i> <span class="count"><?php //echo Cart::instance('wishlist')->count(); ?></span> </a>
                </li> -->
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
          </div>
        </div>
      </div>
      </div>
    </div>
</section>

<!--Banner---------------------------->
<section class="shipping-banner mt-3">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="banner-text">
          <h2>Custom Ordering System</h2>
          <p>You Can Order any product from Bangladesh or any other country and will shippling both local and International.</p>
        </div>
      </div>
      <!-- <div class="col-md-6">

      </div> -->
    </div>
  </div>
</section>
<?php 
  if (Session::get('userId')) {
?>

<section class="shipping-area">
  <div class="container">
    <?php 
      $message=Session::get('message');
      if($message){
      ?>
          <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <?php
                  echo $message;
                  Session::put('message','');
              ?>
          </div>
      <?php
      }
      ?>
    
      <div class="row mt-5">
        
        <div class="col-md-12">
          <div class="card">
              
              
                <div class="card-body">
                  <div class="card-body">
                    <form class="needs-validation" novalidate method="post" action="{{route('saveCustomOrderOutside')}}" enctype="multipart/form-data">
                      @csrf
                      <div class="form-row check-form">

                        <div class="col-md-6 mb-3">
                          <label for="validationCustom01" >Product Link <span>*</span></label>
                          <input type="text" class="form-control" id="validationCustom01" name="productLink" required="" placeholder="Product Link">
                          <div class="invalid-feedback">
                            Please provide a valid Product Link.
                          </div>
                        </div>
                        <div class="col-md-6 mb-3">
                          <label for="validationCustom01" >Product Name</label>
                          <input type="text" class="form-control" id="validationCustom01" name="productName" placeholder="Product Name">
                          <div class="invalid-feedback">
                            Please provide a valid Product Name.
                          </div>
                        </div>
                        <div class="col-md-6 mb-3">
                          <label for="validationCustom02">Product Description <span>*</span></label>
                          <!-- <input type="text" class="form-control" id="validationCustom02" placeholder="Last name"  required> -->
                          <textarea rows="2" cols="42"class="form-control" placeholder="Color,Size,Buy From,Advise,Notes" name="productDescription" id="validationCustom02" required></textarea>
                          <div class="valid-feedback" >
                            Looks good!
                          </div>
                        </div>
                        <div class="col-md-6 mb-3">
                          <label for="validationCustom04">Product Quantity <span>*</span></label>
                          <input type="text" class="form-control" name="quantity" id="validationCustom04" required="" placeholder="Product Quantity">
                          <div class="invalid-feedback">
                            Please provide a valid Product Quantity.
                          </div>
                        </div>

                        <div class="col-md-12 mb-3">
                          <h4>You send any picture? If yes,then send your product picture.</h4>
                          <div class="row">
                            <div class="col md-4">
                             
                              <div class="form-group">
                                <label for="exampleFormControlFile1">Products Images</label>
                                <input type="file" class="form-control-file" name="userImage1" id="exampleFormControlFile1">
                              </div>
                        
                            </div>
                            <div class="col md-4">
                            
                              <div class="form-group">
                                <label for="exampleFormControlFile1">Products Images</label>
                                <input type="file" class="form-control-file" name="userImage2" id="exampleFormControlFile1">
                              </div>
                     
                            </div>
                            <div class="col md-3">
                              
                              <div class="form-group">
                                <label for="exampleFormControlFile1">Products Images</label>
                                <input type="file" class="form-control-file" name="userImage3" id="exampleFormControlFile1">
                              </div>
                        
                            </div>
                          </div>
                          <div class="row">
                            <div class="col md-4">
                            
                              <div class="form-group">
                                <label for="exampleFormControlFile1">Products Images</label>
                                <input type="file" class="form-control-file" name="userImage4" id="exampleFormControlFile1">
                              </div>
                        
                            </div>
                            <div class="col md-4">
                           
                              <div class="form-group">
                                <label for="exampleFormControlFile1">Products Images</label>
                                <input type="file" class="form-control-file" name="userImage5" id="exampleFormControlFile1">
                              </div>
                       
                            </div>
                            <div class="col md-4">

                            </div>
                          </div>
                        </div>
                        <div class="col-md-12 mb-3">
                          <h5>Shipping Information</h5>
                          <div class="row">

                            <div class="col-md-6 mb-3">
                                <label for="validationCustom01" >Name </label>
                                <input type="text" name="bill_name" value="{{$userInfo->bill_name}}" class="form-control" id="validationCustom01" placeholder="Name" required="">
                                <div class="valid-feedback">
                                  Looks good!
                                </div>
                              </div>
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom01" >Address 1 </label>
                                <input type="text" name="bill_address1" class="form-control" id="validationCustom02" value="{{$userInfo->bill_address1}}" placeholder="Street,district,Area" required="">
                                <div class="valid-feedback">
                                  Looks good!
                                </div>
                              </div>
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom01" >Address 2 </label>
                                <input type="text" name="bill_address2" class="form-control" id="validationCustom02" value="{{$userInfo->bill_address2}}" placeholder="Suit, building, flat No." required="">
                                <div class="valid-feedback">
                                  Looks good!
                                </div>
                              </div>
                              <div class="col-md-6 mb-3">
                                <div class="form-group">
                                  <label for="validationCustom05">Country</label>
                                  <select class="form-control field-validate" name="bill_country" required="">
                                     <?php
                                       $length = count($countries);
                                          for ($i = 0; $i < $length; $i++) {
                                     ?>
                                     <option value="<?php echo $countries[$i]; ?>" <?php if($countries[$i]==$userInfo->bill_country) ?> ><?php echo $countries[$i]; ?></option>
                                     <?php
                                       }
                                     ?>
                                  </select>
                                </div>
                              </div>


                              <div class="col-md-6 mb-3">
                                <label for="validationCustom01" >City </label>
                                <input type="text" class="form-control" id="validationCustom04" name="bill_city" placeholder="City" value="{{$userInfo->bill_city}}" required="">
                                <div class="valid-feedback">
                                  Looks good!
                                </div>
                              </div> 

                              <div class="col-md-6 mb-3">
                                  <label for="validationCustom03">State </label>
                                  <input type="text" class="form-control " name="bill_state" value="{{$userInfo->bill_state}}" id="validationCustom05" placeholder="State" required="">
                                  <div class="invalid-feedback">
                                    Please provide a valid city.
                                  </div>

                              </div>
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom04">Zip/Postal Code</label>
                                <input type="text" class="form-control" name="bill_zip" value="{{$userInfo->bill_zip}}" id="validationCustom06" placeholder="New Password" required="">

                              </div>

                              <!--p------------------------------------------>

                              <div class="col-md-12 mb-3">
                                <div class="row">
                                  <label  class="col-sm-3 col-form-label">Phone Number</label>
                                    <div class="col-sm-4">
                                    <select name="bill_countryCode" id="" class="form-control" required="">
                                        <option data-bill_countryCode="GB" value="44" <?php if($userInfo->bill_countryCode=='44'){echo "selected";} ?> >UK (+44)</option>
                                        <option data-bill_countryCode="US" value="1" <?php if($userInfo->bill_countryCode=='1'){echo "selected";} ?> >USA (+1)</option>
                                        <option data-bill_countryCode="DZ" value="213" <?php if($userInfo->bill_countryCode=='213'){echo "selected";} ?> >Algeria (+213)</option>
                                        <option data-bill_countryCode="AD" value="376" <?php if($userInfo->bill_countryCode=='376'){echo "selected";} ?> >Andorra (+376)</option>
                                        <option data-bill_countryCode="AO" value="244" <?php if($userInfo->bill_countryCode=='244'){echo "selected";} ?> >Angola (+244)</option>
                                        <option data-bill_countryCode="AI" value="1264" <?php if($userInfo->bill_countryCode=='1264'){echo "selected";} ?> >Anguilla (+1264)</option>
                                        <option data-bill_countryCode="AG" value="1268" <?php if($userInfo->bill_countryCode=='1268'){echo "selected";} ?> >Antigua &amp; Barbuda (+1268)</option>
                                        <option data-bill_countryCode="AR" value="54" <?php if($userInfo->bill_countryCode=='54'){echo "selected";} ?> >Argentina (+54)</option>
                                        <option data-bill_countryCode="AM" value="374" <?php if($userInfo->bill_countryCode=='374'){echo "selected";} ?> >Armenia (+374)</option>
                                        <option data-bill_countryCode="AW" value="297" <?php if($userInfo->bill_countryCode=='297'){echo "selected";} ?> >Aruba (+297)</option>
                                        <option data-bill_countryCode="AU" value="61" <?php if($userInfo->bill_countryCode=='61'){echo "selected";} ?> >Australia (+61)</option>
                                        <option data-bill_countryCode="AT" value="43" <?php if($userInfo->bill_countryCode=='43'){echo "selected";} ?> >Austria (+43)</option>
                                        <option data-bill_countryCode="AZ" value="994" <?php if($userInfo->bill_countryCode=='994'){echo "selected";} ?> >Azerbaijan (+994)</option>
                                        <option data-bill_countryCode="BS" value="1242" <?php if($userInfo->bill_countryCode=='1242'){echo "selected";} ?> >Bahamas (+1242)</option>
                                        <option data-bill_countryCode="BH" value="973" <?php if($userInfo->bill_countryCode=='973'){echo "selected";} ?> >Bahrain (+973)</option>
                                        <option data-bill_countryCode="BD" value="880" <?php if($userInfo->bill_countryCode=='880'){echo "selected";} ?> >Bangladesh (+880)</option>
                                        <option data-bill_countryCode="BB" value="1246" <?php if($userInfo->bill_countryCode=='1246'){echo "selected";} ?> >Barbados (+1246)</option>
                                        <option data-bill_countryCode="BY" value="375" <?php if($userInfo->bill_countryCode=='375'){echo "selected";} ?> >Belarus (+375)</option>
                                        <option data-bill_countryCode="BE" value="32" <?php if($userInfo->bill_countryCode=='32'){echo "selected";} ?> >Belgium (+32)</option>
                                        <option data-bill_countryCode="BZ" value="501" <?php if($userInfo->bill_countryCode=='501'){echo "selected";} ?> >Belize (+501)</option>
                                        <option data-bill_countryCode="BJ" value="229" <?php if($userInfo->bill_countryCode=='229'){echo "selected";} ?> >Benin (+229)</option>
                                        <option data-bill_countryCode="BM" value="1441" <?php if($userInfo->bill_countryCode=='1441'){echo "selected";} ?> >Bermuda (+1441)</option>
                                        <option data-bill_countryCode="BT" value="975" <?php if($userInfo->bill_countryCode=='975'){echo "selected";} ?> >Bhutan (+975)</option>
                                        <option data-bill_countryCode="BO" value="591" <?php if($userInfo->bill_countryCode=='591'){echo "selected";} ?> >Bolivia (+591)</option>
                                        <option data-bill_countryCode="BA" value="387" <?php if($userInfo->bill_countryCode=='387'){echo "selected";} ?> >Bosnia Herzegovina (+387)</option>
                                        <option data-bill_countryCode="BW" value="267" <?php if($userInfo->bill_countryCode=='267'){echo "selected";} ?> >Botswana (+267)</option>
                                        <option data-bill_countryCode="BR" value="55" <?php if($userInfo->bill_countryCode=='55'){echo "selected";} ?> >Brazil (+55)</option>
                                        <option data-bill_countryCode="BN" value="673" <?php if($userInfo->bill_countryCode=='673'){echo "selected";} ?> >Brunei (+673)</option>
                                        <option data-bill_countryCode="BG" value="359" <?php if($userInfo->bill_countryCode=='359'){echo "selected";} ?> >Bulgaria (+359)</option>
                                        <option data-bill_countryCode="BF" value="226" <?php if($userInfo->bill_countryCode=='226'){echo "selected";} ?> >Burkina Faso (+226)</option>
                                        <option data-bill_countryCode="BI" value="257" <?php if($userInfo->bill_countryCode=='257'){echo "selected";} ?> >Burundi (+257)</option>
                                        <option data-bill_countryCode="KH" value="855" <?php if($userInfo->bill_countryCode=='855'){echo "selected";} ?> >Cambodia (+855)</option>
                                        <option data-bill_countryCode="CM" value="237" <?php if($userInfo->bill_countryCode=='237'){echo "selected";} ?> >Cameroon (+237)</option>
                                        <option data-bill_countryCode="CA" value="1" <?php if($userInfo->bill_countryCode=='1'){echo "selected";} ?> >Canada (+1)</option>
                                        <option data-bill_countryCode="CV" value="238" <?php if($userInfo->bill_countryCode=='238'){echo "selected";} ?> >Cape Verde Islands (+238)</option>
                                        <option data-bill_countryCode="KY" value="1345" <?php if($userInfo->bill_countryCode=='1345'){echo "selected";} ?> >Cayman Islands (+1345)</option>
                                        <option data-bill_countryCode="CF" value="236" <?php if($userInfo->bill_countryCode=='236'){echo "selected";} ?> >Central African Republic (+236)</option>
                                        <option data-bill_countryCode="CL" value="56" <?php if($userInfo->bill_countryCode=='56'){echo "selected";} ?> >Chile (+56)</option>
                                        <option data-bill_countryCode="CN" value="86" <?php if($userInfo->bill_countryCode=='86'){echo "selected";} ?> >China (+86)</option>
                                        <option data-bill_countryCode="CO" value="57" <?php if($userInfo->bill_countryCode=='57'){echo "selected";} ?> >Colombia (+57)</option>
                                        <option data-bill_countryCode="KM" value="269" <?php if($userInfo->bill_countryCode=='269'){echo "selected";} ?> >Comoros (+269)</option>
                                        <option data-bill_countryCode="CG" value="242" <?php if($userInfo->bill_countryCode=='242'){echo "selected";} ?> >Congo (+242)</option>
                                        <option data-bill_countryCode="CK" value="682" <?php if($userInfo->bill_countryCode=='682'){echo "selected";} ?> >Cook Islands (+682)</option>
                                        <option data-bill_countryCode="CR" value="506" <?php if($userInfo->bill_countryCode=='506'){echo "selected";} ?> >Costa Rica (+506)</option>
                                        <option data-bill_countryCode="HR" value="385" <?php if($userInfo->bill_countryCode=='385'){echo "selected";} ?> >Croatia (+385)</option>
                                        <option data-bill_countryCode="CU" value="53" <?php if($userInfo->bill_countryCode=='53'){echo "selected";} ?> >Cuba (+53)</option>
                                        <option data-bill_countryCode="CY" value="90392" <?php if($userInfo->bill_countryCode=='90392'){echo "selected";} ?> >Cyprus North (+90392)</option>
                                        <option data-bill_countryCode="CY" value="357" <?php if($userInfo->bill_countryCode=='357'){echo "selected";} ?> >Cyprus South (+357)</option>
                                        <option data-bill_countryCode="CZ" value="42" <?php if($userInfo->bill_countryCode=='42'){echo "selected";} ?> >Czech Republic (+42)</option>
                                        <option data-bill_countryCode="DK" value="45" <?php if($userInfo->bill_countryCode=='45'){echo "selected";} ?> >Denmark (+45)</option>
                                        <option data-bill_countryCode="DJ" value="253" <?php if($userInfo->bill_countryCode=='253'){echo "selected";} ?> >Djibouti (+253)</option>
                                        <option data-bill_countryCode="DM" value="1809" <?php if($userInfo->bill_countryCode=='1809'){echo "selected";} ?> >Dominica (+1809)</option>
                                        <option data-bill_countryCode="DO" value="1809" <?php if($userInfo->bill_countryCode=='1809'){echo "selected";} ?> >Dominican Republic (+1809)</option>
                                        <option data-bill_countryCode="EC" value="593" <?php if($userInfo->bill_countryCode=='593'){echo "selected";} ?> >Ecuador (+593)</option>
                                        <option data-bill_countryCode="EG" value="20" <?php if($userInfo->bill_countryCode=='20'){echo "selected";} ?> >Egypt (+20)</option>
                                        <option data-bill_countryCode="SV" value="503" <?php if($userInfo->bill_countryCode=='503'){echo "selected";} ?> >El Salvador (+503)</option>
                                        <option data-bill_countryCode="GQ" value="240" <?php if($userInfo->bill_countryCode=='240'){echo "selected";} ?> >Equatorial Guinea (+240)</option>
                                        <option data-bill_countryCode="ER" value="291" <?php if($userInfo->bill_countryCode=='291'){echo "selected";} ?> >Eritrea (+291)</option>
                                        <option data-bill_countryCode="EE" value="372" <?php if($userInfo->bill_countryCode=='372'){echo "selected";} ?> >Estonia (+372)</option>
                                        <option data-bill_countryCode="ET" value="251" <?php if($userInfo->bill_countryCode=='251'){echo "selected";} ?> >Ethiopia (+251)</option>
                                        <option data-bill_countryCode="FK" value="500" <?php if($userInfo->bill_countryCode=='500'){echo "selected";} ?> >Falkland Islands (+500)</option>
                                        <option data-bill_countryCode="FO" value="298" <?php if($userInfo->bill_countryCode=='298'){echo "selected";} ?> >Faroe Islands (+298)</option>
                                        <option data-bill_countryCode="FJ" value="679" <?php if($userInfo->bill_countryCode=='679'){echo "selected";} ?> >Fiji (+679)</option>
                                        <option data-bill_countryCode="FI" value="358" <?php if($userInfo->bill_countryCode=='358'){echo "selected";} ?> >Finland (+358)</option>
                                        <option data-bill_countryCode="FR" value="33" <?php if($userInfo->bill_countryCode=='33'){echo "selected";} ?> >France (+33)</option>
                                        <option data-bill_countryCode="GF" value="594" <?php if($userInfo->bill_countryCode=='594'){echo "selected";} ?> >French Guiana (+594)</option>
                                        <option data-bill_countryCode="PF" value="689" <?php if($userInfo->bill_countryCode=='689'){echo "selected";} ?> >French Polynesia (+689)</option>
                                        <option data-bill_countryCode="GA" value="241" <?php if($userInfo->bill_countryCode=='241'){echo "selected";} ?> >Gabon (+241)</option>
                                        <option data-bill_countryCode="GM" value="220" <?php if($userInfo->bill_countryCode=='220'){echo "selected";} ?> >Gambia (+220)</option>
                                        <option data-bill_countryCode="GE" value="7880" <?php if($userInfo->bill_countryCode=='7880'){echo "selected";} ?> >Georgia (+7880)</option>
                                        <option data-bill_countryCode="DE" value="49" <?php if($userInfo->bill_countryCode=='49'){echo "selected";} ?> >Germany (+49)</option>
                                        <option data-bill_countryCode="GH" value="233" <?php if($userInfo->bill_countryCode=='233'){echo "selected";} ?> >Ghana (+233)</option>
                                        <option data-bill_countryCode="GI" value="350" <?php if($userInfo->bill_countryCode=='350'){echo "selected";} ?> >Gibraltar (+350)</option>
                                        <option data-bill_countryCode="GR" value="30" <?php if($userInfo->bill_countryCode=='30'){echo "selected";} ?> >Greece (+30)</option>
                                        <option data-bill_countryCode="GL" value="299" <?php if($userInfo->bill_countryCode=='299'){echo "selected";} ?> >Greenland (+299)</option>
                                        <option data-bill_countryCode="GD" value="1473" <?php if($userInfo->bill_countryCode=='1473'){echo "selected";} ?> >Grenada (+1473)</option>
                                        <option data-bill_countryCode="GP" value="590" <?php if($userInfo->bill_countryCode=='590'){echo "selected";} ?> >Guadeloupe (+590)</option>
                                        <option data-bill_countryCode="GU" value="671" <?php if($userInfo->bill_countryCode=='671'){echo "selected";} ?> >Guam (+671)</option>
                                        <option data-bill_countryCode="GT" value="502" <?php if($userInfo->bill_countryCode=='502'){echo "selected";} ?> >Guatemala (+502)</option>
                                        <option data-bill_countryCode="GN" value="224" <?php if($userInfo->bill_countryCode=='224'){echo "selected";} ?> >Guinea (+224)</option>
                                        <option data-bill_countryCode="GW" value="245" <?php if($userInfo->bill_countryCode=='245'){echo "selected";} ?> >Guinea - Bissau (+245)</option>
                                        <option data-bill_countryCode="GY" value="592" <?php if($userInfo->bill_countryCode=='592'){echo "selected";} ?> >Guyana (+592)</option>
                                        <option data-bill_countryCode="HT" value="509" <?php if($userInfo->bill_countryCode=='509'){echo "selected";} ?> >Haiti (+509)</option>
                                        <option data-bill_countryCode="HN" value="504" <?php if($userInfo->bill_countryCode=='504'){echo "selected";} ?> >Honduras (+504)</option>
                                        <option data-bill_countryCode="HK" value="852" <?php if($userInfo->bill_countryCode=='852'){echo "selected";} ?> >Hong Kong (+852)</option>
                                        <option data-bill_countryCode="HU" value="36" <?php if($userInfo->bill_countryCode=='36'){echo "selected";} ?> >Hungary (+36)</option>
                                        <option data-bill_countryCode="IS" value="354" <?php if($userInfo->bill_countryCode=='354'){echo "selected";} ?> >Iceland (+354)</option>
                                        <option data-bill_countryCode="IN" value="91" <?php if($userInfo->bill_countryCode=='91'){echo "selected";} ?> >India (+91)</option>
                                        <option data-bill_countryCode="ID" value="62" <?php if($userInfo->bill_countryCode=='62'){echo "selected";} ?> >Indonesia (+62)</option>
                                        <option data-bill_countryCode="IR" value="98" <?php if($userInfo->bill_countryCode=='98'){echo "selected";} ?> >Iran (+98)</option>
                                        <option data-bill_countryCode="IQ" value="964" <?php if($userInfo->bill_countryCode=='964'){echo "selected";} ?> >Iraq (+964)</option>
                                        <option data-bill_countryCode="IE" value="353" <?php if($userInfo->bill_countryCode=='353'){echo "selected";} ?> >Ireland (+353)</option>
                                        <option data-bill_countryCode="IL" value="972" <?php if($userInfo->bill_countryCode=='972'){echo "selected";} ?> >Israel (+972)</option>
                                        <option data-bill_countryCode="IT" value="39" <?php if($userInfo->bill_countryCode=='39'){echo "selected";} ?> >Italy (+39)</option>
                                        <option data-bill_countryCode="JM" value="1876" <?php if($userInfo->bill_countryCode=='1876'){echo "selected";} ?> >Jamaica (+1876)</option>
                                        <option data-bill_countryCode="JP" value="81" <?php if($userInfo->bill_countryCode=='81'){echo "selected";} ?> >Japan (+81)</option>
                                        <option data-bill_countryCode="JO" value="962" <?php if($userInfo->bill_countryCode=='962'){echo "selected";} ?> >Jordan (+962)</option>
                                        <option data-bill_countryCode="KZ" value="7" <?php if($userInfo->bill_countryCode=='7'){echo "selected";} ?> >Kazakhstan (+7)</option>
                                        <option data-bill_countryCode="KE" value="254" <?php if($userInfo->bill_countryCode=='254'){echo "selected";} ?> >Kenya (+254)</option>
                                        <option data-bill_countryCode="KI" value="686" <?php if($userInfo->bill_countryCode=='686'){echo "selected";} ?> >Kiribati (+686)</option>
                                        <option data-bill_countryCode="KP" value="850" <?php if($userInfo->bill_countryCode=='850'){echo "selected";} ?> >Korea North (+850)</option>
                                        <option data-bill_countryCode="KR" value="82" <?php if($userInfo->bill_countryCode=='82'){echo "selected";} ?> >Korea South (+82)</option>
                                        <option data-bill_countryCode="KW" value="965" <?php if($userInfo->bill_countryCode=='965'){echo "selected";} ?> >Kuwait (+965)</option>
                                        <option data-bill_countryCode="KG" value="996" <?php if($userInfo->bill_countryCode=='996'){echo "selected";} ?> >Kyrgyzstan (+996)</option>
                                        <option data-bill_countryCode="LA" value="856" <?php if($userInfo->bill_countryCode=='856'){echo "selected";} ?> >Laos (+856)</option>
                                        <option data-bill_countryCode="LV" value="371" <?php if($userInfo->bill_countryCode=='371'){echo "selected";} ?> >Latvia (+371)</option>
                                        <option data-bill_countryCode="LB" value="961" <?php if($userInfo->bill_countryCode=='961'){echo "selected";} ?> >Lebanon (+961)</option>
                                        <option data-bill_countryCode="LS" value="266" <?php if($userInfo->bill_countryCode=='266'){echo "selected";} ?> >Lesotho (+266)</option>
                                        <option data-bill_countryCode="LR" value="231" <?php if($userInfo->bill_countryCode=='231'){echo "selected";} ?> >Liberia (+231)</option>
                                        <option data-bill_countryCode="LY" value="218" <?php if($userInfo->bill_countryCode=='218'){echo "selected";} ?> >Libya (+218)</option>
                                        <option data-bill_countryCode="LI" value="417" <?php if($userInfo->bill_countryCode=='417'){echo "selected";} ?> >Liechtenstein (+417)</option>
                                        <option data-bill_countryCode="LT" value="370" <?php if($userInfo->bill_countryCode=='370'){echo "selected";} ?> >Lithuania (+370)</option>
                                        <option data-bill_countryCode="LU" value="352" <?php if($userInfo->bill_countryCode=='352'){echo "selected";} ?> >Luxembourg (+352)</option>
                                        <option data-bill_countryCode="MO" value="853" <?php if($userInfo->bill_countryCode=='853'){echo "selected";} ?> >Macao (+853)</option>
                                        <option data-bill_countryCode="MK" value="389" <?php if($userInfo->bill_countryCode=='389'){echo "selected";} ?> >Macedonia (+389)</option>
                                        <option data-bill_countryCode="MG" value="261" <?php if($userInfo->bill_countryCode=='261'){echo "selected";} ?> >Madagascar (+261)</option>
                                        <option data-bill_countryCode="MW" value="265" <?php if($userInfo->bill_countryCode=='265'){echo "selected";} ?> >Malawi (+265)</option>
                                        <option data-bill_countryCode="MY" value="60" <?php if($userInfo->bill_countryCode=='60'){echo "selected";} ?> >Malaysia (+60)</option>
                                        <option data-bill_countryCode="MV" value="960" <?php if($userInfo->bill_countryCode=='960'){echo "selected";} ?> >Maldives (+960)</option>
                                        <option data-bill_countryCode="ML" value="223" <?php if($userInfo->bill_countryCode=='223'){echo "selected";} ?> >Mali (+223)</option>
                                        <option data-bill_countryCode="MT" value="356" <?php if($userInfo->bill_countryCode=='356'){echo "selected";} ?> >Malta (+356)</option>
                                        <option data-bill_countryCode="MH" value="692" <?php if($userInfo->bill_countryCode=='692'){echo "selected";} ?> >Marshall Islands (+692)</option>
                                        <option data-bill_countryCode="MQ" value="596" <?php if($userInfo->bill_countryCode=='596'){echo "selected";} ?> >Martinique (+596)</option>
                                        <option data-bill_countryCode="MR" value="222" <?php if($userInfo->bill_countryCode=='222'){echo "selected";} ?> >Mauritania (+222)</option>
                                        <option data-bill_countryCode="YT" value="269" <?php if($userInfo->bill_countryCode=='269'){echo "selected";} ?> >Mayotte (+269)</option>
                                        <option data-bill_countryCode="MX" value="52" <?php if($userInfo->bill_countryCode=='52'){echo "selected";} ?> >Mexico (+52)</option>
                                        <option data-bill_countryCode="FM" value="691" <?php if($userInfo->bill_countryCode=='691'){echo "selected";} ?> >Micronesia (+691)</option>
                                        <option data-bill_countryCode="MD" value="373" <?php if($userInfo->bill_countryCode=='373'){echo "selected";} ?> >Moldova (+373)</option>
                                        <option data-bill_countryCode="MC" value="377" <?php if($userInfo->bill_countryCode=='377'){echo "selected";} ?> >Monaco (+377)</option>
                                        <option data-bill_countryCode="MN" value="976" <?php if($userInfo->bill_countryCode=='976'){echo "selected";} ?> >Mongolia (+976)</option>
                                        <option data-bill_countryCode="MS" value="1664" <?php if($userInfo->bill_countryCode=='1664'){echo "selected";} ?> >Montserrat (+1664)</option>
                                        <option data-bill_countryCode="MA" value="212" <?php if($userInfo->bill_countryCode=='212'){echo "selected";} ?> >Morocco (+212)</option>
                                        <option data-bill_countryCode="MZ" value="258" <?php if($userInfo->bill_countryCode=='258'){echo "selected";} ?> >Mozambique (+258)</option>
                                        <option data-bill_countryCode="MN" value="95" <?php if($userInfo->bill_countryCode=='95'){echo "selected";} ?> >Myanmar (+95)</option>
                                        <option data-bill_countryCode="NA" value="264" <?php if($userInfo->bill_countryCode=='264'){echo "selected";} ?> >Namibia (+264)</option>
                                        <option data-bill_countryCode="NR" value="674" <?php if($userInfo->bill_countryCode=='674'){echo "selected";} ?> >Nauru (+674)</option>
                                        <option data-bill_countryCode="NP" value="977" <?php if($userInfo->bill_countryCode=='977'){echo "selected";} ?> >Nepal (+977)</option>
                                        <option data-bill_countryCode="NL" value="31" <?php if($userInfo->bill_countryCode=='31'){echo "selected";} ?> >Netherlands (+31)</option>
                                        <option data-bill_countryCode="NC" value="687" <?php if($userInfo->bill_countryCode=='687'){echo "selected";} ?> >New Caledonia (+687)</option>
                                        <option data-bill_countryCode="NZ" value="64" <?php if($userInfo->bill_countryCode=='64'){echo "selected";} ?> >New Zealand (+64)</option>
                                        <option data-bill_countryCode="NI" value="505" <?php if($userInfo->bill_countryCode=='505'){echo "selected";} ?> >Nicaragua (+505)</option>
                                        <option data-bill_countryCode="NE" value="227" <?php if($userInfo->bill_countryCode=='227'){echo "selected";} ?> >Niger (+227)</option>
                                        <option data-bill_countryCode="NG" value="234" <?php if($userInfo->bill_countryCode=='234'){echo "selected";} ?> >Nigeria (+234)</option>
                                        <option data-bill_countryCode="NU" value="683" <?php if($userInfo->bill_countryCode=='683'){echo "selected";} ?> >Niue (+683)</option>
                                        <option data-bill_countryCode="NF" value="672" <?php if($userInfo->bill_countryCode=='672'){echo "selected";} ?> >Norfolk Islands (+672)</option>
                                        <option data-bill_countryCode="NP" value="670" <?php if($userInfo->bill_countryCode=='670'){echo "selected";} ?> >Northern Marianas (+670)</option>
                                        <option data-bill_countryCode="NO" value="47" <?php if($userInfo->bill_countryCode=='47'){echo "selected";} ?> >Norway (+47)</option>
                                        <option data-bill_countryCode="OM" value="968" <?php if($userInfo->bill_countryCode=='968'){echo "selected";} ?> >Oman (+968)</option>
                                        <option data-bill_countryCode="PW" value="680" <?php if($userInfo->bill_countryCode=='680'){echo "selected";} ?> >Palau (+680)</option>
                                        <option data-bill_countryCode="PA" value="507" <?php if($userInfo->bill_countryCode=='507'){echo "selected";} ?> >Panama (+507)</option>
                                        <option data-bill_countryCode="PG" value="675" <?php if($userInfo->bill_countryCode=='675'){echo "selected";} ?> >Papua New Guinea (+675)</option>
                                        <option data-bill_countryCode="PY" value="595" <?php if($userInfo->bill_countryCode=='595'){echo "selected";} ?> >Paraguay (+595)</option>
                                        <option data-bill_countryCode="PE" value="51" <?php if($userInfo->bill_countryCode=='51'){echo "selected";} ?> >Peru (+51)</option>
                                        <option data-bill_countryCode="PH" value="63" <?php if($userInfo->bill_countryCode=='63'){echo "selected";} ?> >Philippines (+63)</option>
                                        <option data-bill_countryCode="PL" value="48" <?php if($userInfo->bill_countryCode=='48'){echo "selected";} ?> >Poland (+48)</option>
                                        <option data-bill_countryCode="PT" value="351" <?php if($userInfo->bill_countryCode=='351'){echo "selected";} ?> >Portugal (+351)</option>
                                        <option data-bill_countryCode="PR" value="1787" <?php if($userInfo->bill_countryCode=='1787'){echo "selected";} ?> >Puerto Rico (+1787)</option>
                                        <option data-bill_countryCode="QA" value="974" <?php if($userInfo->bill_countryCode=='974'){echo "selected";} ?> >Qatar (+974)</option>
                                        <option data-bill_countryCode="RE" value="262" <?php if($userInfo->bill_countryCode=='262'){echo "selected";} ?> >Reunion (+262)</option>
                                        <option data-bill_countryCode="RO" value="40" <?php if($userInfo->bill_countryCode=='40'){echo "selected";} ?> >Romania (+40)</option>
                                        <option data-bill_countryCode="RU" value="7" <?php if($userInfo->bill_countryCode=='7'){echo "selected";} ?> >Russia (+7)</option>
                                        <option data-bill_countryCode="RW" value="250" <?php if($userInfo->bill_countryCode=='250'){echo "selected";} ?> >Rwanda (+250)</option>
                                        <option data-bill_countryCode="SM" value="378" <?php if($userInfo->bill_countryCode=='378'){echo "selected";} ?> >San Marino (+378)</option>
                                        <option data-bill_countryCode="ST" value="239" <?php if($userInfo->bill_countryCode=='239'){echo "selected";} ?> >Sao Tome &amp; Principe (+239)</option>
                                        <option data-bill_countryCode="SA" value="966" <?php if($userInfo->bill_countryCode=='966'){echo "selected";} ?> >Saudi Arabia (+966)</option>
                                        <option data-bill_countryCode="SN" value="221" <?php if($userInfo->bill_countryCode=='221'){echo "selected";} ?> >Senegal (+221)</option>
                                        <option data-bill_countryCode="CS" value="381" <?php if($userInfo->bill_countryCode=='381'){echo "selected";} ?> >Serbia (+381)</option>
                                        <option data-bill_countryCode="SC" value="248" <?php if($userInfo->bill_countryCode=='248'){echo "selected";} ?> >Seychelles (+248)</option>
                                        <option data-bill_countryCode="SL" value="232" <?php if($userInfo->bill_countryCode=='232'){echo "selected";} ?> >Sierra Leone (+232)</option>
                                        <option data-bill_countryCode="SG" value="65" <?php if($userInfo->bill_countryCode=='65'){echo "selected";} ?> >Singapore (+65)</option>
                                        <option data-bill_countryCode="SK" value="421" <?php if($userInfo->bill_countryCode=='421'){echo "selected";} ?> >Slovak Republic (+421)</option>
                                        <option data-bill_countryCode="SI" value="386" <?php if($userInfo->bill_countryCode=='386'){echo "selected";} ?> >Slovenia (+386)</option>
                                        <option data-bill_countryCode="SB" value="677" <?php if($userInfo->bill_countryCode=='677'){echo "selected";} ?> >Solomon Islands (+677)</option>
                                        <option data-bill_countryCode="SO" value="252" <?php if($userInfo->bill_countryCode=='252'){echo "selected";} ?> >Somalia (+252)</option>
                                        <option data-bill_countryCode="ZA" value="27" <?php if($userInfo->bill_countryCode=='27'){echo "selected";} ?> >South Africa (+27)</option>
                                        <option data-bill_countryCode="ES" value="34" <?php if($userInfo->bill_countryCode=='34'){echo "selected";} ?> >Spain (+34)</option>
                                        <option data-bill_countryCode="LK" value="94" <?php if($userInfo->bill_countryCode=='94'){echo "selected";} ?> >Sri Lanka (+94)</option>
                                        <option data-bill_countryCode="SH" value="290" <?php if($userInfo->bill_countryCode=='290'){echo "selected";} ?> >St. Helena (+290)</option>
                                        <option data-bill_countryCode="KN" value="1869" <?php if($userInfo->bill_countryCode=='1869'){echo "selected";} ?> >St. Kitts (+1869)</option>
                                        <option data-bill_countryCode="SC" value="1758" <?php if($userInfo->bill_countryCode=='1758'){echo "selected";} ?> >St. Lucia (+1758)</option>
                                        <option data-bill_countryCode="SD" value="249" <?php if($userInfo->bill_countryCode=='249'){echo "selected";} ?> >Sudan (+249)</option>
                                        <option data-bill_countryCode="SR" value="597" <?php if($userInfo->bill_countryCode=='597'){echo "selected";} ?> >Suriname (+597)</option>
                                        <option data-bill_countryCode="SZ" value="268" <?php if($userInfo->bill_countryCode=='268'){echo "selected";} ?> >Swaziland (+268)</option>
                                        <option data-bill_countryCode="SE" value="46" <?php if($userInfo->bill_countryCode=='46'){echo "selected";} ?> >Sweden (+46)</option>
                                        <option data-bill_countryCode="CH" value="41" <?php if($userInfo->bill_countryCode=='41'){echo "selected";} ?> >Switzerland (+41)</option>
                                        <option data-bill_countryCode="SI" value="963" <?php if($userInfo->bill_countryCode=='963'){echo "selected";} ?> >Syria (+963)</option>
                                        <option data-bill_countryCode="TW" value="886" <?php if($userInfo->bill_countryCode=='886'){echo "selected";} ?> >Taiwan (+886)</option>
                                        <option data-bill_countryCode="TJ" value="7" <?php if($userInfo->bill_countryCode=='7'){echo "selected";} ?> >Tajikstan (+7)</option>
                                        <option data-bill_countryCode="TH" value="66" <?php if($userInfo->bill_countryCode=='66'){echo "selected";} ?> >Thailand (+66)</option>
                                        <option data-bill_countryCode="TG" value="228" <?php if($userInfo->bill_countryCode=='228'){echo "selected";} ?> >Togo (+228)</option>
                                        <option data-bill_countryCode="TO" value="676" <?php if($userInfo->bill_countryCode=='676'){echo "selected";} ?> >Tonga (+676)</option>
                                        <option data-bill_countryCode="TT" value="1868" <?php if($userInfo->bill_countryCode=='1868'){echo "selected";} ?> >Trinidad &amp; Tobago (+1868)</option>
                                        <option data-bill_countryCode="TN" value="216" <?php if($userInfo->bill_countryCode=='216'){echo "selected";} ?> >Tunisia (+216)</option>
                                        <option data-bill_countryCode="TR" value="90" <?php if($userInfo->bill_countryCode=='90'){echo "selected";} ?> >Turkey (+90)</option>
                                        <option data-bill_countryCode="TM" value="7" <?php if($userInfo->bill_countryCode=='7'){echo "selected";} ?> >Turkmenistan (+7)</option>
                                        <option data-bill_countryCode="TM" value="993" <?php if($userInfo->bill_countryCode=='993'){echo "selected";} ?> >Turkmenistan (+993)</option>
                                        <option data-bill_countryCode="TC" value="1649" <?php if($userInfo->bill_countryCode=='1649'){echo "selected";} ?> >Turks &amp; Caicos Islands (+1649)</option>
                                        <option data-bill_countryCode="TV" value="688" <?php if($userInfo->bill_countryCode=='688'){echo "selected";} ?> >Tuvalu (+688)</option>
                                        <option data-bill_countryCode="UG" value="256" <?php if($userInfo->bill_countryCode=='256'){echo "selected";} ?> >Uganda (+256)</option>
                                        <!-- <option data-bill_countryCode="GB" value="44">UK (+44)</option> -->
                                        <option data-bill_countryCode="UA" value="380" <?php if($userInfo->bill_countryCode=='380'){echo "selected";} ?> >Ukraine (+380)</option>
                                        <option data-bill_countryCode="AE" value="971" <?php if($userInfo->bill_countryCode=='971'){echo "selected";} ?> >United Arab Emirates (+971)</option>
                                        <option data-bill_countryCode="UY" value="598" <?php if($userInfo->bill_countryCode=='598'){echo "selected";} ?> >Uruguay (+598)</option>
                                        <!-- <option data-bill_countryCode="US" value="1">USA (+1)</option> -->
                                        <option data-bill_countryCode="UZ" value="7" <?php if($userInfo->bill_countryCode=='7'){echo "selected";} ?> >Uzbekistan (+7)</option>
                                        <option data-bill_countryCode="VU" value="678" <?php if($userInfo->bill_countryCode=='678'){echo "selected";} ?> >Vanuatu (+678)</option>
                                        <option data-bill_countryCode="VA" value="379" <?php if($userInfo->bill_countryCode=='379'){echo "selected";} ?> >Vatican City (+379)</option>
                                        <option data-bill_countryCode="VE" value="58" <?php if($userInfo->bill_countryCode=='58'){echo "selected";} ?> >Venezuela (+58)</option>
                                        <option data-bill_countryCode="VN" value="84" <?php if($userInfo->bill_countryCode=='84'){echo "selected";} ?> >Vietnam (+84)</option>
                                        <option data-bill_countryCode="VG" value="84" <?php if($userInfo->bill_countryCode=='84'){echo "selected";} ?> >Virgin Islands - British (+1284)</option>
                                        <option data-bill_countryCode="VI" value="84" <?php if($userInfo->bill_countryCode=='84'){echo "selected";} ?> >Virgin Islands - US (+1340)</option>
                                        <option data-bill_countryCode="WF" value="681" <?php if($userInfo->bill_countryCode=='681'){echo "selected";} ?> >Wallis &amp; Futuna (+681)</option>
                                        <option data-bill_countryCode="YE" value="969" <?php if($userInfo->bill_countryCode=='969'){echo "selected";} ?> >Yemen (North)(+969)</option>
                                        <option data-bill_countryCode="YE" value="967" <?php if($userInfo->bill_countryCode=='967'){echo "selected";} ?> >Yemen (South)(+967)</option>
                                        <option data-bill_countryCode="ZM" value="260" <?php if($userInfo->bill_countryCode=='260'){echo "selected";} ?> >Zambia (+260)</option>
                                        <option data-bill_countryCode="ZW" value="263" <?php if($userInfo->bill_countryCode=='263'){echo "selected";} ?> >Zimbabwe (+263)</option>
                                      </select>
                                    </div>
                                    <div class="col-sm-4">
                                      <input type="text" name="bill_phone" class="form-control number-validate" placeholder="Number" value="{{$userInfo->bill_phone}}">
                                    </div>
                                  </div>
                                </div>

                              <!--dddddddddddddddddddddddddddddddddddddddddddd-->

                          </div>

                          

                        </div>
                          <div class="col-md-12">
                            <p class="border dollar">Initial Service Cost {{$outsideInitialPayment->cancelAmount}} Dollar. If This Order Complete Then This Amount Add To Your Total Amount. If you Cancle This Order Can't Refund This Amount.</p>
                          </div>
                          <div class="col-md-12">
                            <p class="border dollar"> Initial Services Amount $ {{$outsideInitialPayment->cancelAmount}} .
                              <button type="button" class="btn btn-danger">Pay</button>
                              <input type="hidden" name="initialAmount" value="{{$outsideInitialPayment->cancelAmount}}">
                            </p>
                          </div>
                        <div class="checkout-btn mt-2 mb-2">
                          <button class="btn btn-primary" type="submit">Place order</button>
                        </div>
                      </div>
                    </form>
                    </div>
                </div>
              </div>
      </div>

      
<!--predefign your marketplase--------------------------------------->
        
      </div>
</section>
<?php } ?>
<!--box------------------------------------------------------------------->

<!--shipping-services------------------------------------->
<section class="shipping-services">
  <div class="container">
    <h5 class="text-left">Order Any Product From Bangladesh Or Any Other Country And Get Local and International Shipping.</h5>
      <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-4">
          <div class="shipping-services-item">
            <img src="{{asset('user/images/service-1.png')}}" alt="">
            <h4>Order Your Product Inside Only In Bangladesh</h4>
            <ul>
              <li><i class="fas fa-check-circle"></i> Bangladeshie Customer Can Order Any Product In Bangladesh.</li>
              <li><i class="fas fa-check-circle"></i> Any Other Country Customer Can Order Any Product In Bangladesh</li>
            </ul>
          </div>
        </div>
        <div class="col-md-4">
          <div class="shipping-services-item">
            <img src="{{asset('user/images/service-2.png')}}" alt="">
            <h4>Order Your Product From Any Country Only For Bangladeshie Customer.</h4>
            <ul>
              <li><i class="fas fa-check-circle"></i> This Order Only For Bangladeshie Customer</li>
            </ul>
          </div>
        </div>
        <div class="col-md-2">

        </div>
        <div class="col-md-4">
          <div class="shipping-services-item">
            <img src="{{asset('user/images/service-3.png')}}" alt="">
            <h4>Order Inside In Bangladesh</h4>
        
              <?php echo $settingInfo->howOrder; ?>
      
          </div>
        </div>
        <div class="col-md-4">
          <div class="shipping-services-item">
            <img src="{{asset('user/images/service-6.png')}}" alt="">
            <h4>Order Outside In Bangladesh</h4>
       
              <?php echo $settingInfo->importProduct; ?>
     
          </div>
        </div>
        <div class="col-md-4">
          <div class="shipping-services-item">
            <img src="{{asset('user/images/service-5.png')}}" alt="">
            <h4>How to Order</h4>
       
              <?php echo $settingInfo->exportProduct; ?>
            
          </div>
        </div>
      </div>
  </div>
</section>
<!--ready Shop-------------------------------------------------------->
<?php 
  if (Session::get('userId')) {
?>



<?php
}else{
?>

<section class="ready-shop  text-center">
  <div class="container">
    <h2>Ready to Ordering</h2>
    <p>All you need is a membership to instantly get your ushopnship address.</p>
    <a href="{{route('showUserLogin')}}"class="text-decoration-none">
    <button type="button" class="btn btn-danger">Sign In</button>
    </a>
  </div>
</section>

<?php } ?>



<section class="footer-1">
  <div class="container">
    <div class="footer-header">
        <h4>Need help? Call our award-winning support team 24/7 at +880 2-9511236</h4>
    </div>
  </div>
</section>
<!--FOOTER------------------------------------------------------------------->
<footer class="site-footer">
   <div class="container-fluid">
     <div class="row">
       <div class="col-md-9">
         <div class="row">
           <div class="col-md-3 col-12">
            <?php 
              $companyInfo=DB::table('company_setting')
                                ->where('status',1)
                                ->first();
            ?>
             <h6 class=" uShopnShip"> {{$companyInfo->companyName}}</h6>
             <ul class="footer-links ">
               <li> <strong>Visit Us</strong> </li>
               <li>{{$companyInfo->companyAddress}}</li>
               <li> <strong>Call Us 24/7</strong>  </li>
               <li> {{$companyInfo->companyPhone}}</li>
               <li> <strong>Drop us a line</strong> </li>
               <li>{{$companyInfo->companyEmail}}</li>
               
               <li class="mb-2"> Copyright 2019 | {{$companyInfo->companyName}} </li>
             </ul>

           </div>
           <div class="col-md-3 col-6 help">
             <h6>Help & Support</h6>
             <ul class="footer-links">
               <li> <a href="{{route('showFaqPage')}}">FAQ’s</a> </li>
               <li> <a href="{{route('showForgetPassword')}}">Lost password?</a> </li>
               <li> <a href="{{route('showSupportPage')}}">Support & Service</a> </li>
               <li> <a href="{{route('showReportPage')}}">Report An Issue</a> </li>
             </ul>
           </div>
           <div class="col-md-3 col-6 information">
             <h6>Information</h6>
             <ul class="footer-links">
               <li> <a href="{{route('showAboutUs')}}">About Us</a> </li>
               <li> <a href="{{route('showContactUs')}}">Contact Us</a> </li>
               <li> <a href="{{route('showStoreLocationPage')}}">Store Locator</a> </li>
             </ul>
           </div>

           <div class="col-md-3 col-6 legal">
             <h6>Legal</h6>
             <ul class="footer-links">
               <li> <a href="{{route('showPrivacyPage')}}">Privacy Policy</a> </li>
               <li> <a href="{{route('showTermsPage')}}">Terms Of Service</a> </li>
               <li> <a href="{{route('showWarrantyPage')}}">Warranty Policy</a> </li>
               <li> <a href="{{route('showRefundPage')}}">Refund & Return Policy</a> </li>
             </ul>
           </div>
         </div>
         <ul class="social-icons ">
           <li> <a class="facebook" href="#"> <img src="{{asset('user/images/1.png')}}" alt="footer-img"> </a> </li>
           <li> <a class="twitter" href="#"> <img src="{{asset('user/images/2.png')}}" alt="footer-img"> </a> </li>
           <li> <a class="dribbble" href="#"> <img src="{{asset('user/images/3.png')}}" alt="footer-img"> </a> </li>
           <li> <a class="linkedin" href="#"> <img src="{{asset('user/images/4.png')}}" alt="footer-img"> </a> </li>
         </ul>
       </div><!---end col-8-------------->

       <div class="col-md-3 subscribe">
         <div class="left-content">
          <h3 class="title">Subscribe Our Newsletter</h3>
          <div class="description">Sign up to our newsletter to get updates &amp; offers along with product support and new inventory.</div>
            <p>Email Address:</p>
        </div>
        <form method="post">
          <div class="form-row">
            <input type="text" class="form-control subscribe" name="subscribeUserEmail" placeholder="Your Email Address" required>
            <div class="subscribe-btn ml-2">
              <button class="btn btn-primary insertSubscribeUser" type="button">Subscribe</button>
          </div>
        </form>
       </div>

       <div class="rounded-social-buttons">
          <a class="social-button facebook" href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a>
          <a class="social-button twitter" href="https://www.twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a>
          <a class="social-button linkedin" href="https://www.linkedin.com/" target="_blank"><i class="fab fa-linkedin"></i></a>
          <a class="social-button youtube" href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube"></i></a>
          <a class="social-button instagram" href="https://www.instagram.com/" target="_blank"><i class="fab fa-instagram"></i></a>
       </div>
       </div>
     <hr>
   </div>
   <div class="container-fluid">
     <div class="row">
       <div class="col-md-8 col-sm-6 col-xs-12">
         <div class="store-img">
           <!-- <a href="#"> <img src="images/apple.jpg" class="img-fluid" alt="footer-img"> </a>
           <a href="#"> <img src="images/google.jpg" class="img-fluid"> </a> -->
         </div>
       </div>

       <div class="col-md-4 col-sm-6 col-xs-12">
       </div>
     </div>
   </div>
</footer>
  <script src="{{asset('admin/assets/libs/sweetalert2/sweetalert.min.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="{{asset('user/js/vendor/modernizr-3.5.0.min.js')}}"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
  <script src="{{asset('user/js/main.js')}}"></script>
  <script src="{{asset('customJs/userjs/addSubscribeUser.js')}}"></script>
  <script src="{{asset('user/js/jquery-3.4.1.min.js')}}"></script>
  <script src="{{asset('user/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('user/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('user/js/progressbar.js')}}"></script>
  <script src="{{asset('user/js/wow.min.js')}}"></script>



  <script>
  // Example starter JavaScript for disabling form submissions if there are invalid fields
  (function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
  </script>

  <script>
  new WOW().init();
  </script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>


<!-- If you want to use the popup integration, -->
<script>
    var obj = {};
    obj.cus_name = $('#customer_name').val();
    obj.cus_phone = $('#mobile').val();
    obj.cus_email = $('#email').val();
    obj.cus_addr1 = $('#address').val();
    obj.amount = $('#total_amount').val();

    $('#sslczPayBtn').prop('postdata', obj);

    (function (window, document) {
        var loader = function () {
            var script = document.createElement("script"), tag = document.getElementsByTagName("script")[0];
            // script.src = "https://seamless-epay.sslcommerz.com/embed.min.js?" + Math.random().toString(36).substring(7); // USE THIS FOR LIVE
            script.src = "https://sandbox.sslcommerz.com/embed.min.js?" + Math.random().toString(36).substring(7); // USE THIS FOR SANDBOX
            tag.parentNode.insertBefore(script, tag);
        };

        window.addEventListener ? window.addEventListener("load", loader, false) : window.attachEvent("onload", loader);
    })(window, document);
</script>

</body>
</html>
