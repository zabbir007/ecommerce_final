<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title> Order panel</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Tomattos" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('order/assets/images/favicon.ico')}}">

        <!-- App css -->
        <link href="{{asset('order/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('order/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('order/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('order/assets/css/custom.css')}}" rel="stylesheet" type="text/css" />

        <style>
            thead {color:green;}
            tbody {color:blue;}
            tfoot {color:red;}

            table, th, td {
              border: 1px solid black;
            }
        </style>
       
    </head>

    <body>
        <!-- Begin page -->
        <div id="wrapper">
            
            <!-- Topbar Start -->

            
            @include('includes.order.headerNavigation')
            
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->

             @include('includes.order.headerSidebar')
            
           
            
            <!-- Left Sidebar End -->

            <div class="content-page">

                <div class="content">
                
                    <h4 class="header-title mb-3 mt-4">Add Custom Order</h4>

                        <form method="post" action="{{route('addInsideCustomOrder')}}">
                            <?php 
                            $message=Session::get('message');
                            if($message){
                            ?>
                                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <?php
                                        echo $message;
                                        Session::put('message','');
                                    ?>
                                </div>
                            <?php
                            }
                            ?>

                            <?php 
                            $message=Session::get('messageWarning');
                            if($message){
                            ?>
                                <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    <?php
                                        echo $message;
                                        Session::put('messageWarning','');
                                    ?>
                                </div>
                            <?php   
                            }
                            ?>
                            @csrf
                            <div id="progressbarwizard">

                                <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-3">
                                    <li class="nav-item">
                                        <a href="#account-2" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-account-circle mr-1"></i>
                                            <span class="d-none d-sm-inline">First Step</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#profile-tab-2" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-face-profile mr-1"></i>
                                            <span class="d-none d-sm-inline">Second Step</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#finish-2" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2" id="finish">
                                            <i class="mdi mdi-checkbox-marked-circle-outline mr-1"></i>
                                            <span class="d-none d-sm-inline">Finish</span>
                                        </a>
                                    </li>
                                </ul>
                            
                                <div class="tab-content b-0 mb-0">
                            
                                    <div id="bar" class="progress mb-3" style="height: 7px;">
                                        <div class="bar progress-bar progress-bar-striped progress-bar-animated bg-success"></div>
                                    </div>
                            
                                    <div class="tab-pane" id="account-2">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="form-group row mb-2">
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control address" id="autocomplete" onFocus="geolocate()" name="address" required placeholder="Input Your Address">
                                                        <input class="form-control" id="street_number" disabled="true" type="hidden">
                                                        <input class="form-control" id="route" disabled="true" type="hidden">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-2">
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="country" name="country" required placeholder="Country">
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-2">

                                                    <div class="row">
                                                       <div class="col-md-4">
                                                            
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="administrative_area_level_1" name="state" required placeholder="State">
                                                            </div>   
                                                        </div> 
                                                       <div class="col-md-4">   
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="locality" name="city" required placeholder="City/District">
                                                            </div>   
                                                        </div>
                                                        <div class="col-md-4">
                                                            
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="postal_code" name="zip" required placeholder="Post Code">
                                                            </div>   
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                    
                                                    
                                                <div class="form-group row mb-2">
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="house" name="house" required placeholder="Flat,House etc">
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-2">
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="road" name="road" required placeholder="Road">
                                                    </div>
                                                </div>
                                            </div> <!-- end col -->
                                            
                                        </div> <!-- end row -->
                                    </div>

                                    <div class="tab-pane" id="profile-tab-2">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row mb-2">
                                                    <div class="col-md-3">
                                                        <label for="heard"> Select MarketPlace *:</label>
                                                        <select id="heard" class="form-control selectOption" required="" name="">
                                                            @foreach($marketPlaceInfo as $market)
                                                            <option value="{{$market->id}}">{{$market->marketPlaceName}}</option>
                                                            @endforeach
                                                            <option value="other">Other..</option>
                                                        </select>
                                                        <input type="text" style="display: none;" class="form-control placeName" id="placeName" name="userName3" placeholder="Market Place Name">
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-2">
                                                    <div class="col-md-3">
                                                        <label for="heard"> Product Link :</label>
                                                        <input type="text" class="form-control" id="product_link" name="product_link" required placeholder="#">
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-2">
                                                    <div class="col-md-3">
                                                        <label for="heard"> Product Name :</label>
                                                        <input type="text" class="form-control" id="productName" name="productName" placeholder="Product Name">
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-2">

                                                    <div class="row">
                                                       <div class="col-md-6">
                                                            
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="price" name="price" required placeholder="Price">
                                                            </div>   
                                                        </div> 
                                                       <div class="col-md-6">
                                                            
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="quantity" name="quantity" required placeholder="Quantity">
                                                            </div>   
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                            </div> <!-- end col -->
                                        </div> <!-- end row -->
                                    </div>

                                    <div class="tab-pane" id="finish-2">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <h4 class="header-title">To</h4>

                                                                <p class="sub-header" id="insertAddress">
                                                                    
                                                                </p>
                                                               
                                                                <p class="sub-header" id="insertCountry">
                                                                    
                                                                </p>
                                                                <p class="sub-header">
                                                                    <span id="insertCity">
                                                                        
                                                                    </span>
                                                                    <span id="insertState">
                                                                        
                                                                    </span>
                                                                    <span id="insertZip">
                                                                        
                                                                    </span>
                                                                    
                                                                </p>
                                                                
                                                                <p class="sub-header" id="insertHouse">
                                                                    
                                                                </p>
                                                                <p class="sub-header" id="insertRoad">
                                                                   
                                                                </p>
                                                            </div> <!-- end col -->
                    
                                                            <div class="col-md-6">
                                                                <h4 class="header-title mt-5 mt-sm-0">Amount Summary</h4>
                                                                <table style="height: 70%;width: 70%;">
                                                                  <thead>
                                                                    <tr>
                                                                      <th>Cost Type</th>
                                                                      <th>Amount</th>
                                                                    </tr>
                                                                  </thead>
                                                                  <tbody>
                                                                    <tr>
                                                                      <td>Service Charge</td>
                                                                      <td>{{$insideOrderCost->serviceCharge}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td>Shipping Charge</td>
                                                                      <td>{{$insideOrderCost->serviceCharge}}</td>
                                                                    </tr>

                                                                    <tr>
                                                                      <td>Bute Cost</td>
                                                                      <td>{{$insideOrderCost->serviceCharge}}</td>
                                                                    </tr>

                                                                  </tbody>
                                                                  <tfoot>
                                                                    <tr>
                                                                      <td>Total Amount</td>
                                                                      <td>{{$insideOrderCost->serviceCharge+$insideOrderCost->serviceCharge+$insideOrderCost->serviceCharge}}</td>
                                                                    </tr>
                                                                  </tfoot>
                                                                </table>

                                                                <p><b>Note:</b> If You cancel then {{$insideOrderCost->cancelAmount}} amount cut from your amount.</p>
                                                            </div> <!-- end col -->
                                                            <button type="submit" class="btn btn-success btn-rounded waves-effect waves-light" onClick="empty()">Add Order</button>
                                                        </div> <!-- end row -->

                                                    </div> <!-- end card-body-->
                                                </div> <!-- end card -->
                                            </div> <!-- end col -->
                                        </div>
                                        <!-- end row -->
                                    </div>

                                    <ul class="list-inline mb-0 wizard">
                                        <li class="previous list-inline-item">
                                            <a href="javascript: void(0);" class="btn btn-secondary">Previous</a>
                                        </li>
                                        <li class="next list-inline-item float-right">
                                            <a href="javascript: void(0);" class="btn btn-secondary" id="next">Next</a>
                                        </li>
                                    </ul>

                                </div> <!-- tab-content -->
                            </div> <!-- end #progressbarwizard-->
                        </form>  

                </div> <!-- container -->
                
              </div> <!-- content -->

                <!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                2019 &copy; Develop by <a href="">Tomattos</a>
                            </div>
                            <div class="col-md-6">
                                <div class="text-md-right footer-links d-none d-sm-block">
                                    <a href="javascript:void(0);">About Us</a>
                                    <a href="javascript:void(0);">Help</a>
                                    <a href="javascript:void(0);">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

            </div>
            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->
        </div>
         <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <!-- Vendor js -->
        <script src="{{asset('order/assets/js/vendor.min.js')}}"></script>

        <!-- Plugins js-->
        <script src="{{asset('order/assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>

        <!-- Init js-->
        <script src="{{asset('order/assets/js/pages/form-wizard.init.js')}}"></script>

        <script src="{{asset('customJs/order/orderData.js')}}"></script>

        <!-- App js -->
        <script src="{{asset('order/assets/js/app.min.js')}}"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCirSuiG-0hyQm_U1A_3ksf1iYHRSqCoDk&libraries=places&callback=initAutocomplete" async defer></script>

        <script type="text/javascript">
            function empty() {
                
                var address = document.getElementById("autocomplete").value;
                var country = document.getElementById("country").value;
                var state = document.getElementById("administrative_area_level_1").value;
                var city = document.getElementById("locality").value;
                var zip = document.getElementById("postal_code").value;
                var house = document.getElementById("house").value;
                var road = document.getElementById("road").value;
                var product_link = document.getElementById("product_link").value;
                var price = document.getElementById("price").value;
                var quantity = document.getElementById("quantity").value;
                if (address == "") {
                    swal("Ohh No!", "Please Enter Address!", "error");
                    return false;
                }else if (country == "") {
                    swal("Ohh No!", "Please Enter Country!", "error");
                    return false;
                }else if (state == "") {
                    swal("Ohh No!", "Please Enter state!", "error");
                    return false;
                }else if (city == "") {
                    swal("Ohh No!", "Please Enter city!", "error");
                    return false;
                }else if (zip == "") {
                    swal("Ohh No!", "Please Enter zip Code!", "error");
                    return false;
                }else if (house == "") {
                    swal("Ohh No!", "Please Enter House Name!", "error");
                    return false;
                }else if (road == "") {
                    swal("Ohh No!", "Please Enter road number!", "error");
                    return false;
                }else if (product_link == "") {
                    swal("Ohh No!", "Please Enter product link!", "error");
                    return false;
                }else if (price == "") {
                    swal("Ohh No!", "Please Enter product price!", "error");
                    return false;
                }else if (quantity == "") {
                    swal("Ohh No!", "Please Enter product quantity!", "error");
                    return false;
                }
            }
        </script>

        <script type="text/javascript">
          var placeSearch, autocomplete;
          var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
          };

          function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                {types: ['geocode']});

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);

            //place suggest second box

          }

          function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
              document.getElementById(component).value = '';
              document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
              var addressType = place.address_components[i].types[0];
              if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
              }
            }
          }

          // Bias the autocomplete object to the user's geographical location,
          // as supplied by the browser's 'navigator.geolocation' object.
          function geolocate() {
            if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                  center: geolocation,
                  radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
              });
            }
          }
    </script>

    </body>
</html>
