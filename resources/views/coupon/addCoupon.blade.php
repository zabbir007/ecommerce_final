
@extends('layouts.admin')

@section('title') Add Coupon @endsection

@section('content')


<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title mb-3">Add Coupon/Discount</h4>

                <form method="post" action="{{route('saveCoupon')}}" enctype="multipart/form-data" accept-charset="utf-8" data-parsley-validate="">
                @csrf
            	<?php 
			    $message=Session::get('message');
			    if($message){
			    ?>
			        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
			            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                <span aria-hidden="true">&times;</span>
			            </button>
			            <?php
			                echo $message;
			                Session::put('message','');
			            ?>
			        </div>
			    <?php
			    }
			    ?>


			    <?php 
			    $message=Session::get('messageWarning');
			    if($message){
			    ?>
			        <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
			                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                    <span aria-hidden="true">&times;</span>
			                </button>
			            <?php
			                echo $message;
			                Session::put('messageWarning','');
			            ?>
			        </div>
			    <?php   
			    }
			    ?>
                    <div id="progressbarwizard">
                        
                        <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-3">
                            <li class="nav-item">
                                <a href="#account-2" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                    <i class="mdi mdi-account-circle mr-1"></i>
                                    <span class="d-none d-sm-inline">First Step</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#profile-tab-2" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                    <i class="mdi mdi-face-profile mr-1"></i>
                                    <span class="d-none d-sm-inline">Second Step</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#finish-2" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                    <i class="mdi mdi-checkbox-marked-circle-outline mr-1"></i>
                                    <span class="d-none d-sm-inline">Finish</span>
                                </a>
                            </li>
                        </ul>
                    
                        <div class="tab-content b-0 mb-0">
                    
                            <div id="bar" class="progress mb-3" style="height: 7px;">
                                <div class="bar progress-bar progress-bar-striped progress-bar-animated bg-success"></div>
                            </div>
                    
                            <div class="tab-pane" id="account-2">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="userName1">Coupon Number</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="userName1" name="coupon_number" required="">
                                            </div>
                                            <div class="col-md-2">
                                            	<button type="button" class="btn btn-outline-secondary btn-rounded waves-effect" id="generat_random">Generat Coupon</button>	
                                            </div>
                                        </div>

                                        <div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="userName1">Discount Amount</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="userName1" name="discount_amount" required="">
                                            </div>
                                        </div>
                                        
                                    </div> <!-- end col -->
                                </div> <!-- end row -->
                            </div>

                            <div class="tab-pane" id="profile-tab-2">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="name1"> Expire Date </label>
                                            <div class="col-md-9">
                                            	<input type="text" id="disable-datepicker" class="form-control" placeholder="Expire Date" name="expire_date">
                                            </div>
                                        </div>
                                        <div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="surname1"> Department </label>
                                            <div class="col-md-9">
	                                            <select class="selectpicker" data-live-search="true"  data-style="btn-light" name="department_id" id="department_id" required="">
							                        <option value="">Select Department Name</option>
							                        @foreach($departmentInfo as $department)
							                        <option value="{{$department->id}}">{{$department->department_type}}</option>
							                        @endforeach
							                    </select>
							                </div>
                                        </div>
                                    </div> <!-- end col -->
                                </div> <!-- end row -->
                            </div>

                            <div class="tab-pane" id="finish-2">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="name1"> Use Per Coupon </label>
                                            <div class="col-md-9">
                                            	<input type="text" class="form-control" id="userName1" name="use_per_coupon" placeholder="How Many Time Use This Coupon" required="">
                                            </div>
                                        </div>
                                        <div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="name1"> Use Per User </label>
                                            <div class="col-md-9">
                                            	<input type="text" class="form-control" id="userName1" placeholder="How Many User Use This Coupon" name="use_per_user" required="">
                                            </div>
                                        </div>

                                        <div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="name1"> </label>
                                            <div class="col-md-3">
                                            	<button type="submit" class="btn btn-outline-success btn-rounded waves-effect waves-light">Save Coupon</button>
                                            </div>
                                        </div>
                                        
                                    </div> <!-- end col -->
                                </div> <!-- end row -->
                            </div>

                            <ul class="list-inline mb-0 wizard">
                                <li class="previous list-inline-item">
                                    <a href="javascript: void(0);" class="btn btn-secondary">Previous</a>
                                </li>
                                <li class="next list-inline-item float-right">
                                    <a href="javascript: void(0);" class="btn btn-secondary">Next</a>
                                </li>
                            </ul>

                        </div> <!-- tab-content -->
                    </div> <!-- end #progressbarwizard-->
                </form>

            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col -->
</div>

@endsection