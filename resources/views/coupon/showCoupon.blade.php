@extends('layouts.admin')

@section('title') Show Coupon @endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Coupon</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addCoupon')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add New Coupon</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Coupon Number</th>
                            <th class="text-center">Discount Amount</th>
                            <th class="text-center">Department</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($couponInfo as $coupon)
                        <tr>
                            <td class="text-center">{{$coupon->coupon_number}}</td>
                            <td class="text-center">{{$coupon->discount_amount}}</td>
                            <td class="text-center">{{$coupon->department_type}}</td>
                            <td class="text-center"><?php $currentDate=date("d-m-Y"); if($currentDate==$coupon->expire_date){echo "Today Expire";}else if($currentDate<$coupon->expire_date){echo "Active";}else if($currentDate>$coupon->expire_date){echo "Expired";} ?></td>
                            <td class="text-center">
                                <a href="{{route('editCoupon',[$coupon->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnCouponDelete" id="{{$coupon->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection