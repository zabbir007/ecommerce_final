@extends('layouts.admin')

@section('title') Order Shipping Sub Category @endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Order Shipping Sub Category</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addOrderShippingSubCategory')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add Shipping Sub Category</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Category Name</th>
                            <th class="text-center">Sub Category Name</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($shippingSubCategoryInfo as $shipping)
                        <tr>
                            <td class="text-center">{{$shipping->categoryName}}</td>
                            <td class="text-center">{{$shipping->subCategoryName}}</td>
                            <td class="text-center">
                                <a href="{{route('editOrderShippingSubCategory',[$shipping->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnShippingSubCategoryDelete" id="{{$shipping->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection