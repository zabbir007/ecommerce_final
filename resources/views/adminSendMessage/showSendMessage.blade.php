@extends('layouts.admin')

@section('title') Show Send Message @endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show Send Message</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addUserMessage')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Send Message</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Title</th>
                            <th class="text-center">User Type</th>
                            <th class="text-center">Message</th>
                            <th class="text-center">Send Date</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($messageInfo as $message)
                        <tr>
                            <td class="text-center">{{$message->messageTitle}}</td>
                            <td class="text-center"><?php if($message->userType=='1'){echo "Ecommerce User";}else if($message->userType=='2'){echo "Shipping User";}else if($message->userType=='3'){echo "Order User";} ?></td>
                            <td class="text-center">{{$message->message}}</td>
                            <td class="text-center">{{$message->sendDate}}</td>
                            <td class="text-center">
                                <a href="#" class="action-icon btnSendMessageDelete" id="{{$message->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>


@endsection