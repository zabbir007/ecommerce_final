@extends('layouts.admin')

@section('title') Add Message @endsection

@section('content')

<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <form class="needs-validation" method="post" action="{{route('saveUserMessage')}}" novalidate>
            @csrf
            	<?php 
                $message=Session::get('message');
                if($message){
                ?>
	                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                    <?php
	                        echo $message;
	                        Session::put('message','');
	                    ?>
	                </div>
                <?php
                }
                ?>

                <?php 
                $message=Session::get('messageWarning');
                if($message){
                ?>
                    <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        <?php
                            echo $message;
                            Session::put('messageWarning','');
                        ?>
                    </div>
                <?php   
                }
                ?>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Select User Type</label>
                    <select class="selectpicker" data-live-search="true"  data-style="btn-light" name="userType">
                        <option value="1">Ecommerce User</option>
                        <option value="2">Shipping User</option>
                        <option value="3">Order User</option>
                    </select>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Message Subject</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Please Enter Message Title" required name="messageTitle">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Message Title.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Message</label>
                    <textarea class="form-control" id="validationTooltip01" placeholder="Please Enter Message" required name="message"></textarea>
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Message.
                    </div>
                </div>

                <button class="btn btn-primary" type="submit">Send Message</button>
            </form>
        </div>
    </div>
</div>

@endsection