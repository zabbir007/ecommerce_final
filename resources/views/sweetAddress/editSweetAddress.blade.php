@extends('layouts.admin')

@section('title') Edit Swift Address @endsection

@section('content')

<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <form class="needs-validation" method="post" action="{{route('updateSweetAddress')}}" novalidate>
            @csrf
            	<?php 
                $message=Session::get('message');
                if($message){
                ?>
	                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                    <?php
	                        echo $message;
	                        Session::put('message','');
	                    ?>
	                </div>
                <?php
                }
                ?>

                <?php 
                $message=Session::get('messageWarning');
                if($message){
                ?>
                    <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        <?php
                            echo $message;
                            Session::put('messageWarning','');
                        ?>
                    </div>
                <?php   
                }
                ?>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Address</label>
                    <input type="text" class="form-control" id="autocomplete" onFocus="geolocate()" placeholder="" required name="sweetAddress" value="{{$sweetAddressInfo->sweetAddress}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Address.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label class="col-sm-4 col-form-label">Country</label>
                       <select class="form-control field-validate" name="sweetCountry">
                         <?php
                           $length = count($countries);
                              for ($i = 0; $i < $length; $i++) {
                         ?>
                         <option value="<?php echo $countries[$i]; ?>" <?php if($countries[$i]==$sweetAddressInfo->sweetCountry){echo "selected";} ?> ><?php echo $countries[$i]; ?></option>
                         <?php
                           }
                         ?>
                       </select>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">City</label>
                    <input type="text" class="form-control" id="locality" placeholder="" value="{{$sweetAddressInfo->sweetCity}}" required name="sweetCity">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter City Name.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">State</label>
                    <input type="text" class="form-control" value="{{$sweetAddressInfo->sweetState}}" id="administrative_area_level_1" placeholder="" required name="sweetState">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter State Name.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Zip Code</label>
                    <input type="text" class="form-control" id="validationTooltip01" value="{{$sweetAddressInfo->sweetHouse}}" placeholder="" required name="sweetHouse">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Zip Code.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">House && Road Number</label>
                    <input type="text" class="form-control" id="validationTooltip01" value="{{$sweetAddressInfo->sweetRoad}}" placeholder="" required name="sweetRoad">
                    <input type="hidden" name="id" value="{{$id}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Road Number.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Phone</label>
                    <input type="text" class="form-control" id="validationTooltip01" value="{{$sweetAddressInfo->sweetPhone}}" placeholder="" required name="sweetPhone">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Phone Number.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label class="col-sm-4 col-form-label">Status</label>
                       <select class="form-control field-validate" name="status">
                        
                         <option value="1" <?php if($sweetAddressInfo->status=='1'){echo "selected";} ?> >Active</option>
                         <option value="0" <?php if($sweetAddressInfo->status=='0'){echo "selected";} ?> >DeActive</option>
                        
                       </select>
                </div>
                <button class="btn btn-primary" type="submit">Update SwiftAddress</button>
            </form>
        </div>
    </div>
</div>

@endsection