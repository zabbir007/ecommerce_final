@extends('layouts.admin')

@section('title') Show Sweet Address @endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Swift Address</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addSweetAddress')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add SwiftAddress</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Address</th>
                            <th class="text-center">Country</th>
                            <th class="text-center">City</th>
                            <th class="text-center">State</th>
                            <th class="text-center">Zip Code</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($sweetAddressInfo as $sweet)
                        <tr>
                            <td class="text-center">{{$sweet->sweetAddress}}</td>
                            <td class="text-center">{{$sweet->sweetCountry}}</td>
                            <td class="text-center">{{$sweet->sweetCity}}</td>
                            <td class="text-center">{{$sweet->sweetState}}</td>
                            <td class="text-center">{{$sweet->sweetHouse}}</td>
                            <td class="text-center"><?php if($sweet->status=='1'){echo "Active";}else if($sweet->status=='0'){echo "De-Active";} ?></td>
                            <td class="text-center">
                                <a href="{{route('editSweetAddress',[$sweet->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnSweetAddressDelete" id="{{$sweet->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection