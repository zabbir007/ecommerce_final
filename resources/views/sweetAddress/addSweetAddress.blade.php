@extends('layouts.admin')

@section('title') Swift Address @endsection

@section('content')

<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <form class="needs-validation" method="post" action="{{route('saveSweetAddress')}}" novalidate>
            @csrf
            	<?php 
                $message=Session::get('message');
                if($message){
                ?>
	                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                    <?php
	                        echo $message;
	                        Session::put('message','');
	                    ?>
	                </div>
                <?php
                }
                ?>

                <?php 
                $message=Session::get('messageWarning');
                if($message){
                ?>
                    <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        <?php
                            echo $message;
                            Session::put('messageWarning','');
                        ?>
                    </div>
                <?php   
                }
                ?>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Address</label>
                    <input type="text" class="form-control" placeholder="" required name="sweetAddress">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Address.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label class="col-sm-4 col-form-label">Country</label>
                       <select class="form-control field-validate" name="sweetCountry">
                         <?php
                           $length = count($countries);
                              for ($i = 0; $i < $length; $i++) {
                         ?>
                         <option value="<?php echo $countries[$i]; ?>" ><?php echo $countries[$i]; ?></option>
                         <?php
                           }
                         ?>
                       </select>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">City</label>
                    <input type="text" class="form-control" id="locality" placeholder="" required name="sweetCity">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter City Name.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">State</label>
                    <input type="text" class="form-control" id="administrative_area_level_1" placeholder="" required name="sweetState">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter State Name.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Zip Code</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="" required name="sweetHouse">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Zip Code.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">House && Road Number</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="" required name="sweetRoad">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Road Number.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Phone</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="" required name="sweetPhone">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Phone Number.
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Add SwiftAddress</button>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
              (document.getElementById('autocomplete')),
            {types: ['geocode']});
        autocomplete.addListener('place_changed', fillInAddress);

        //place suggest second box
        new google.maps.places.Autocomplete(
          (document.getElementById('autocomplete2')),
          {types: ['geocode']}
        );

      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

       
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
</script>
@endsection