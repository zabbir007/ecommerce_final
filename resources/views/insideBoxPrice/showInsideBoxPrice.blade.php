@extends('layouts.admin')

@section('title') Show Package Price @endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Package Price</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addInsideBoxPrice')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add Package Price</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Package Name</th>
                            <th class="text-center">Package Price</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($insideBoxInfo as $insideBox)
                        <tr>
                            <td class="text-center">{{$insideBox->boxSize}}</td>
                            <td class="text-center">{{$insideBox->boxPrice}}</td>
                            <td class="text-center">
                                <a href="{{route('editInsideBoxPrice',[$insideBox->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnInsideBoxDelete" id="{{$insideBox->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection