@extends('layouts.admin')

@section('title') Department @endsection

@section('content')

<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <form class="needs-validation" method="post" action="{{route('saveDepartment')}}" novalidate>
            @csrf
            	<?php 
                $message=Session::get('message');
                if($message){
                ?>
	                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                    <?php
	                        echo $message;
	                        Session::put('message','');
	                    ?>
	                </div>
                <?php
                }
                ?>

                <?php 
                $message=Session::get('messageWarning');
                if($message){
                ?>
                    <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        <?php
                            echo $message;
                            Session::put('messageWarning','');
                        ?>
                    </div>
                <?php   
                }
                ?>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Department Name</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Department name" required name="departmentType">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Department Name.
                    </div>
                </div>

                <div class="form-group position-relative mb-3 categoryShow">
                    <label for="validationTooltip01">Currency Type</label>
                    <select class="selectpicker" data-live-search="true"  data-style="btn-light" name="currency_type" id="category_id">
                        <option value="">Select</option>
                        <option value="taka">Taka</option>
                        <option value="dollar">Dollar</option>
                    </select>
                </div>

                <div class="form-group position-relative mb-3 categoryShow">
                    <label for="validationTooltip01">Currency Prefix</label>
                    <select class="selectpicker" data-live-search="true"  data-style="btn-light" name="currency_prefix" id="category_id">
                        <option value="">Select</option>
                        <option value="৳">৳</option>
                        <option value="$">$</option>
                    </select>
                </div>

                <button class="btn btn-primary" type="submit">Add Department</button>
            </form>
        </div>
    </div>
</div>

@endsection