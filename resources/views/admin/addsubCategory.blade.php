@extends('layouts.admin')

@section('title') Sub Category @endsection

@section('content')

<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <form class="needs-validation" method="post" action="{{route('savesubCategory')}}" novalidate>
            @csrf
            	<?php 
                $message=Session::get('message');
                if($message){
                ?>
	                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                    <?php
	                        echo $message;
	                        Session::put('message','');
	                    ?>
	                </div>
                <?php
                }
                ?>

                <?php 
                $message=Session::get('messageWarning');
                if($message){
                ?>
                    <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        <?php
                            echo $message;
                            Session::put('messageWarning','');
                        ?>
                    </div>
                <?php   
                }
                ?>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Department Name</label>
                    <select class="selectpicker" data-live-search="true"  data-style="btn-light" name="department_id" id="department_id">
                        <option value="">Select Department Name</option>
                        @foreach($departmentInfo as $department)
                        <option value="{{$department->id}}">{{$department->department_type}}</option>
                        @endforeach
                    </select>

                </div>
                <div class="form-group position-relative mb-3 categoryShow" style="display: none;" id="categoryShow">
                    <label for="validationTooltip01">Category Name</label>
                    <select class="selectpicker" data-live-search="true"  data-style="btn-light" name="category_id" id="category_id">
                        <option value="">Select Category Name</option>
                        @foreach($categoryInfo as $category)
                        <option value="{{$category->id}}">{{$category->category_type}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group position-relative mb-3 subShow" style="display: none;" id="subShow">
                    <label for="validationTooltip01">Sub Category Name</label>
                    <input name="subcategoryType" type="text" class="form-control" id="validationTooltip01" placeholder="Sub Category name" required>
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Sub Category Name.
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Add Sub Category</button>
            </form>
        </div>
    </div>
</div>

@endsection