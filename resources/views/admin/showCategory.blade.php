@extends('layouts.admin')

@section('title') Dashboard @endsection

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Category</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addCategory')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add New Category</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Category Type</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($categoryInfo as $category)
                        <tr>
                            <td class="text-center">{{$category->category_type}}</td>
                            <td class="text-center">
                                <a href="{{route('editCategory',[$category->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnCategoryDelete" id="{{$category->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection