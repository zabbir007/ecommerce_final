@extends('layouts.admin')

@section('title') Dashboard @endsection

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Sub Category</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addsubCategory')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add Sub Category</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Department Type</th>
                            <th class="text-center">Category Type</th>
                            <th class="text-center">Sub Category Name</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($subcategoryInfo as $subcategory)
                        <tr>
                            <td class="text-center">{{$subcategory->department_type}}</td>
                            <td class="text-center">{{$subcategory->category_type}}</td>
                            <td class="text-center">{{$subcategory->subCategory_type}}</td>
                            <td class="text-center">
                                <a href="{{route('editsubCategory',[$subcategory->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnsubcategoryDelete" id="{{$subcategory->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection