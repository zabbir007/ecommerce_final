<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{asset('custom/login.css')}}">
</head>
<body>
	<header>
		
	</header>
	<div class="login-box">
		<h1>Admin Login
			<span class="span1"></span></h1>

		<form action="{{route('adminLogin')}}" method="post">
			@csrf
			<br/>
            <?php 
            $message=Session::get('message');
            if($message){
            ?>
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php
                        echo $message;
                        Session::put('message','');
                    ?>
                </div>
            <?php
            }
            ?>
			<div class="text-box">
				<i class="fa fa-user"></i>
				<input type="text" name="adminEmail" placeholder="Email" required="">
				<span class="span2"></span>
			</div>
			<div class="text-box">
				<i class="fa fa-lock"></i>
				<input type="password" name="adminPassword" placeholder="Password" required="">
				<span class="span3"></span>

			</div>
			<div class="btn-box">
				<input class="btn" type="submit" name="btn" value="Sign in">
				<span class="span_1"></span>
				<span class="span_2"></span>
				<span class="span_3"></span>
				<span class="span_4"></span>	
			</div>
		</form>

	</div>
</body>
</html>