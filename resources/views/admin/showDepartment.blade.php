@extends('layouts.admin')

@section('title') Dashboard @endsection

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Deparment</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addDepartment')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add New Department</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Department Type</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($departmentInfo as $department)
                        <tr>
                            <td class="text-center">{{$department->department_type}}</td>
                            <td class="text-center">
                                <a href="{{route('editDepartment',[$department->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnDepatmentDelete" id="{{$department->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection