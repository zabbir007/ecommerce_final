@extends('layouts.admin')

@section('title') Dashboard @endsection

@section('content')


<!-- First portion start -->


<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Admin Dashboard</h4>
        </div>
    </div>
</div>     
<!-- end page title --> 

<div class="row">
    <div class="col-md-6 col-xl-2">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                
                <div class="col-12">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$totalCustomer}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">Total Customer</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->

    <div class="col-md-6 col-xl-2">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                
                <div class="col-12">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$totalProduct}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">Total Product</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->

    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                
                <div class="col-12">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$ecommercePending}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">Ecommerce Pending Order</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->

    <div class="col-md-6 col-xl-2">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                
                <div class="col-12">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$customPending}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">Custom Pending Order</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                
                <div class="col-12">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$reshippingPending}}</span></h3>
                        <p class="mb-1">Re-Shipping Pending Order</p>
                    </div>
                </div>
            </div> <!-- end row-->
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->
</div>

<!-- First portion start -->



<!-- second portion Start-->
<div class="row">
    <div class="col-xl-6">
        <div class="card-box">
            <h4 class="header-title mb-3">Recent Register 5 Users</h4>

            <div class="table-responsive">
                <table class="table table-borderless table-hover table-centered m-0">

                    <thead class="thead-light">
                        <tr>
                            <th colspan="2">Profile</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Address</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($userInfo as $user)
                        <tr>
                            <td style="width: 36px;">
                                <img src="assets/images/users/user-2.jpg" alt="contact-img" title="contact-img" class="rounded-circle avatar-sm" />
                            </td>

                            <td>
                                <h5 class="m-0 font-weight-normal">{{$user->firstName}}</h5>
                                <p class="mb-0 text-muted"><small>Member Since {{$user->created_at}}</small></p>
                            </td>

                            <td>
                                 {{$user->email}}
                            </td>

                            <td>
                                {{$user->phone}}
                            </td>

                            <td>
                                {{$user->address1}}
                            </td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->

    <div class="col-xl-6">
        <div class="card-box">
            <h4 class="header-title mb-3">Recent 5 Ecommerce Pending Order</h4>

            <div class="table-responsive">
                <table class="table table-borderless table-hover table-centered m-0">

                    <thead class="thead-light">
                        <tr>
                            <th>Order Number</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Customer Address</th>
                            <th>Delivery Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ecommercePendingOrder as $ecommerce)
                        <tr>
                            <td>
                                <h5 class="m-0 font-weight-normal">{{$ecommerce->orderNumber}}</h5>
                            </td>

                            <td>
                                {{$ecommerce->orderDate}}
                            </td>

                            <td>
                                {{$ecommerce->totalAmount}}
                            </td>

                            <td>
                                <span class="badge bg-soft-warning text-warning">{{$ecommerce->shippingAddress}}</span>
                            </td>

                            <td>
                                <span class="badge bg-soft-success text-success">{{$ecommerce->billingAddress}}</span>
                            </td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div> <!-- end .table-responsive-->
        </div> <!-- end card-box-->
    </div> <!-- end col -->
</div>
<!-- second portion End-->


<!-- Third portion Start-->
<div class="row">
    <div class="col-xl-6">
        <div class="card-box">
            <h4 class="header-title mb-3">Recent 5 Custom Order Pending Order</h4>

            <div class="table-responsive">
                <table class="table table-borderless table-hover table-centered m-0">

                    <thead class="thead-light">
                        <tr>
                            <th>Order Number</th>
                            <th>Order Type</th>
                            <th>Order Date</th>
                            <th>Product Name</th>
                            <th>Delivery Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($customPendingOrder as $custom)
                        <tr>
                            <td>
                                <h5 class="m-0 font-weight-normal">{{$custom->orderNumber}}</h5>
                            </td>

                            <td>
                                <?php if($custom->orderType=='1'){echo "Inside Order";}else if($custom->orderType=='2'){echo "Outside Order";} ?>
                            </td>

                            <td>
                                {{$custom->orderDate}}
                            </td>

                            <td>
                                <span class="badge bg-soft-warning text-warning">{{$custom->productName}}</span>
                            </td>

                            <td>
                                <span class="badge bg-soft-success text-success">{{$custom->bill_address1}}</span>
                            </td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div> <!-- end .table-responsive-->
        </div> <!-- end card-box-->
    </div> <!-- end col -->

    <div class="col-xl-6">
        <div class="card-box">
            <h4 class="header-title mb-3">Recent 5 Reshipping Pending Order</h4>

            <div class="table-responsive">
                <table class="table table-borderless table-hover table-centered m-0">

                    <thead class="thead-light">
                        <tr>
                            <th>Swift Number</th>
                            <th>Tracking Number</th>
                            <th>Package Id</th>
                            <th>Product Type</th>
                            <th>Product Weight</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reshippingPendingOrder as $reshipping)
                        <tr>
                            <td>
                                <h5 class="m-0 font-weight-normal">{{$reshipping->swiftNumber}}</h5>
                            </td>

                            <td>
                                {{$reshipping->trackingNumber}}
                            </td>

                            <td>
                                {{$reshipping->packageId}}
                            </td>

                            <td>
                                <span class="badge bg-soft-warning text-warning">{{$reshipping->productType}}</span>
                            </td>

                            <td>
                                <span class="badge bg-soft-success text-success">{{$reshipping->weight}}</span>
                            </td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div> <!-- end .table-responsive-->
        </div> <!-- end card-box-->
    </div> <!-- end col -->
</div>
<!-- third portion End-->


@endsection