@extends('layouts.admin')

@section('title') Order Additional Photo Price @endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Additional Photo Price</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addOrderAdditionalPhotoPrice')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add Photo Price</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Photo Quantity</th>
                            <th class="text-center">Photo Price</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($photoPriceInfo as $photo)
                        <tr>
                            <td class="text-center">{{$photo->photoQuantity}}</td>
                            <td class="text-center">{{$photo->photoPrice}}</td>
                            <td class="text-center">
                                <a href="{{route('editOrderAdditionalPhotoPrice',[$photo->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnAdditionalPhotoDelete" id="{{$photo->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection