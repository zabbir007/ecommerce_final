@extends('layouts.admin')

@section('title') Reshipping Order @endsection

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-60"><font color="red">Initial Information</font></h4>
		                    <p class="mb-1"><b>Product Type:</b> {{$orderInfo->productType}}</p>
		                    <p class="mb-1"><b>Product Description:</b> {{$orderInfo->productDescription}}</p>
		                    <p class="mb-1"><b>Package Id:</b> {{$orderInfo->packageId}}</p>
		                    <p class="mb-0"><b>Invoice Number:</b> {{$orderInfo->invoiceNumber}}</p>
		                    <input type="hidden" name="customerEmail" value="{{$orderInfo->userEmail}}">
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Product From:</b> {{$orderInfo->from}}</p>
		            <p class="mb-1"><b>Product Weight:</b> {{$orderInfo->weight}}</p>
		            <p class="mb-1"><b>Product Sent To:</b> {{$orderInfo->sentTo}}</p>
		            <p class="mb-0"><b>Tracking Number:</b> {{$orderInfo->trackingNumber}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-60"><font color="red">Product Information By User</font></h4>
		                    <p class="mb-1"><b>Picture Quantity:</b> {{$orderInfo->shippingAdditionalPicture}}</p>
		                    <p class="mb-1"><b>Box Condition:</b> {{$orderInfo->boxCondition}}</p>
		                    <p class="mb-1"><b>Use For:</b> <?php if($orderInfo->personalUse=='1'){echo "Personal";}else{echo "Not Only Personal";} ?></p>
		                    <p class="mb-1"><b>Shipping Type:</b><?php if($orderInfo->shippingType=='1'){echo "Inside Shipping";}else{echo "Outside Shipping";} ?></p>
		                    <p class="mb-1"><b>Picture Price:</b> {{$orderInfo->setAdditionalPhotoPrice}}</p>
		                    <p class="mb-0"><b>Box Price:</b> {{$orderInfo->setBoxPrice}}</p>
		                  
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Total Amount:</b> {{$orderInfo->customerSetTotalAmount}}</p>
		            <p class="mb-1"><b>Box Category:</b> {{$orderInfo->shippingBoxCategory}}</p>
		            <p class="mb-1"><b>Box Sub-Category:</b> {{$orderInfo->shippingBoxSubCategory}}</p>
		            <p class="mb-1"><b>Shipping Category:</b> {{$orderInfo->shippingShippingCategory}}</p>
		            <p class="mb-0"><b>Shipping Sub-Category:</b> {{$orderInfo->shippingShippingSubCategory}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
    <div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-12">
		            <div class="card-body">
	                  <div class="info-box text-center">
	                    <h3>Total Price</h3>
	                    <div class="table-responsive">
	                      <table class="table mb-0">
	                          <thead>
	                          <tr>
	                            <th>Product</th>
	                            <th>Price</th>
	                          </tr>
	                          </thead>
	                          <tbody>
	                          
	                          <tr>
	                            <th scope="row">Box Price</th>
	                            <td id="">
	                            	<span>{{$orderInfo->setBoxPrice}}</span>
	                            </td>
	                          </tr>

	                          <tr>
	                            <th scope="row">Additional Picture Price</th>
	                            <td id="">
	                            	<span>{{$orderInfo->setAdditionalPhotoPrice}}</span>
	                            </td>
	                          </tr>
	                          
	                          <tr>
	                            <th scope="row">Service Price</th>
	                            <td id="">
	                            	<span>{{$orderInfo->adminServiceCharge}}</span>
	                            	
	                            </td>
	                          </tr>

	                          <tr>
	                            <th scope="row">Shipping Price</th>
	                            <td id="">
	                            	<span>{{$orderInfo->adminShippingCharge}}</span>
	                            	
	                            </td>
	                          </tr>

	                          <tr>
	                            <th scope="row">Fragile Sticker Price</th>
	                            <td id="">
	                            	<span>{{$orderInfo->fragileStickerAmount}}</span>
	                            	
	                            </td>
	                          </tr>


	                          <tr>
	                            <th scope="row">Insurance Amount</th>
	                           
	                            <td id="">
	                            	<span>{{$orderInfo->insuranceAmount}}</span>
	                            </td>
	                          </tr>
	                          <tr>
	                            <th scope="row">Additional Cost</th>
	                            <td id="">
	                            	<span>{{$orderInfo->additionalCost}}</span>
	                            </td>
	                          </tr>

	                           <tr>
	                            <th scope="row">Total Amount</th>
	                            
	                            <td id="">
	                            	<span>{{$orderInfo->adminFinalAmount}}</span>
	                            </td>
	                          </tr>

	                          </tbody>
	                      </table>
	                    </div>
	                  </div>
	                </div>
		        </div>
		        <input type="hidden" name="id" value="{{$orderInfo->id}}">
		        
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
		
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-30"><font color="red">Shipping Information</font></h4>
		                    <p class="mb-1"><b>Name:</b> {{$orderInfo->bill_name}}</p>
		                    <p class="mb-1"><b>Address1:</b> {{$orderInfo->bill_address1}}</p>
		                    <p class="mb-1"><b>Address2:</b> {{$orderInfo->bill_address2}}</p>
		                    <p class="mb-1"><b>Country:</b> {{$orderInfo->bill_country}}</p>
		                    
		                   
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
                    <p class="mb-1"><b>State:</b> {{$orderInfo->bill_state}}</p>
                    <p class="mb-1"><b>City:</b> {{$orderInfo->bill_city}}</p>
                    <p class="mb-1"><b>Zip:</b> {{$orderInfo->bill_zip}}</p>
                    <p class="mb-1"><b>Phone:</b> {{$orderInfo->bill_countryCode.'-'.$orderInfo->bill_phone}}</p>
                    <p class="mb-1"><b>Delivery Date:{{$orderInfo->deliveryDate}}</p>
                    
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>



	
</div>
</form>

@endsection