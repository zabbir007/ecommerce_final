@extends('layouts.admin')

@section('title') Pending Shipping Order @endsection

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Shipping Pending Order</h4>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Swift Number</th>
                            <th class="text-center">Product Type</th>
                            <th class="text-center">Invoice Number</th>
                            <th class="text-center">Product Description</th>
                            <th class="text-center">Package Id</th>
                            <th class="text-center">Tracking Number</th>
                            <th class="text-center">Product Send</th>
                            <th class="text-center">Weight</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($orderInfo as $order)
                        <tr>
                            <td class="text-center">{{$order->swiftNumber}}</td>
                            <td class="text-center">{{$order->productType}}</td>
                            <td class="text-center">{{$order->invoiceNumber}}</td>
                            <td class="text-center">{{$order->productDescription}}</td>
                            <td class="text-center">{{$order->packageId}}</td>
                            <td class="text-center">{{$order->trackingNumber}}</td>
                            <td class="text-center">{{$order->sentTo}}</td>
                            <td class="text-center">{{$order->weight}}</td>
                           
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection