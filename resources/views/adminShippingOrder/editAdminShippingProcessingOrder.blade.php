@extends('layouts.admin')

@section('title') Custom Processing Order @endsection

@section('content')
<form method="post" action="{{route('updateAdminShippingProcessingOrder')}}" enctype="multipart/form-data">
	@csrf
	<?php 
    $message=Session::get('message');
    if($message){
    ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php
                echo $message;
                Session::put('message','');
            ?>
        </div>
    <?php
    }
    ?>
<div class="row">
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-60"><font color="red">Initial Information</font></h4>
		                    <p class="mb-1"><b>Product Type:</b> {{$orderInfo->productType}}</p>
		                    <p class="mb-1"><b>Product Description:</b> {{$orderInfo->productDescription}}</p>
		                    <p class="mb-1"><b>Package Id:</b> {{$orderInfo->packageId}}</p>
		                    <p class="mb-0"><b>Invoice Number:</b> {{$orderInfo->invoiceNumber}}</p>
		                    <input type="hidden" name="customerEmail" value="{{$orderInfo->userEmail}}">
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Product From:</b> {{$orderInfo->from}}</p>
		            <p class="mb-1"><b>Product Weight:</b> {{$orderInfo->weight}}</p>
		            <p class="mb-1"><b>Product Sent To:</b> {{$orderInfo->sentTo}}</p>
		            <p class="mb-0"><b>Tracking Number:</b> {{$orderInfo->trackingNumber}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-60"><font color="red">Product Information By User</font></h4>
		                    <p class="mb-1"><b>Picture Quantity:</b> {{$orderInfo->shippingAdditionalPicture}}</p>
		                    <p class="mb-1"><b>Box Condition:</b> {{$orderInfo->boxCondition}}</p>
		                    <p class="mb-1"><b>Use For:</b> <?php if($orderInfo->personalUse=='1'){echo "Personal";}else{echo "Not Only Personal";} ?></p>
		                    <p class="mb-1"><b>Use Fragile Sticker:</b> <?php if($orderInfo->useSticker=='1'){echo "Yes";}else{echo "No";} ?></p>
		                    <p class="mb-1"><b>Take Urgent Ship:</b> <?php if($orderInfo->useUrgent=='1'){echo "Yes";}else{echo "No";} ?></p>
		            		<p class="mb-0"><b>Shipping:</b> {{$orderInfo->shippingShippingSubCategory}}</p>
		                    
		                    
		                  
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		        	<p class="mb-1"><b>Picture Price:</b> {{$orderInfo->setAdditionalPhotoPrice}}</p>
                    <p class="mb-1"><b>Sticker Price:</b> {{$orderInfo->setStickerPrice}}</p>
                    <p class="mb-1"><b>Urgent Price:</b> {{$orderInfo->setUrgentPrice}}</p>
		            <p class="mb-1"><b>Total Amount:</b> {{$orderInfo->customerSetTotalAmount}}</p>
		           
		            
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
    <div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-12">
		            <div class="card-body">
	                  <div class="info-box text-center">
	                    <h3>Total Price</h3>
	                    <div class="table-responsive">
	                      <table class="table mb-0">
	                          <thead>
	                          <tr>
	                            <th>Product</th>
	                            <th>Price</th>
	                          </tr>
	                          </thead>
	                          <tbody>
	                          
	                          <tr>
	                            <th scope="row">Order Number</th>
	                            <td id="">
	                            	<input type="text" name="orderNumber" value="<?php echo date("YmdHis").rand(11,99); ?>" class="form-control" id="validationCustom01" placeholder="Order Number" required>
	                            </td>
	                          </tr>

	                          <tr>
	                            <th scope="row">User Option Price</th>
	                            <td id="">
	                            	<input type="text" name="customerSetTotalAmount" value="{{$orderInfo->customerSetTotalAmount}}" class="form-control" id="validationCustom01" placeholder="" required>
	                            </td>
	                          </tr>
	                          
	                          <tr>
	                            <th scope="row">Shipment Price</th>
	                            <td id="">
	                            	<input type="text" name="adminShipmentCharge" value="{{$paymentInfo->serviceCharge}}" class="form-control" id="validationCustom01" placeholder="" required>
	                            </td>
	                          </tr>

	                          <tr>
	                            <th scope="row">Shipping Cost</th>
	                            <td id="adminShippingCost">
	                            	<span><input type="text" class="form-control" id="adminShippingCost" name="adminShippingCharge" required placeholder="Shipping Amount"/></span>
	                            </td>
	                          </tr>

	                          <tr>
	                            <th scope="row">Service Cost</th>
	                            <td id="adminServiceCost">
	                            	<span><input type="text" class="form-control" id="adminServiceCost" name="adminServiceCharge" required placeholder="Service Amount"/></span>
	                            </td>
	                          </tr>

	                          <tr>
	                            <th scope="row">Insurance Amount</th>
	                            <input type="hidden" name="getInsuranceAmount" id="getInsuranceAmount" value="{{$paymentInfo->insuranceAmount}}">
	                            <input type="hidden" name="getInsuranceRange" id="getInsuranceRange" value="{{$paymentInfo->insuranceRange}}">
	                            <td id="">

	                            <span><input type="text" class="form-control" id="" name="adminInsuranceAmount" required placeholder="Insurance Amount"/></span>
	                            <p>{{$paymentInfo->insuranceAmount}} Dollar Insurance Include for equal and less than total {{$paymentInfo->insuranceRange}} Dollar</p>
	                            	
	                            </td>
	                          </tr>
	                          <tr>
	                            <th scope="row">Discount</th>
	                            <td id="shippingAdditionalCost">
	                            	<span><input type="text" class="form-control" id="" name="adminDiscountAmount" required placeholder="Discount"/></span>
	                            </td>
	                          </tr>

	                          <tr>
	                            <th scope="row">Tracking Number</th>
	                            <td id="">
	                            	<span><input type="text" value="{{$orderInfo->trackingNumber}}" class="form-control" id="" name="trackingNumber" required placeholder="Tracking Number"/></span>
	                            </td>
	                          </tr>
	                          <tr>
	                            <th scope="row">Delivery Date</th>
	                            <td id="">
	                            	<span><input type="text" id="basic-datepicker" class="form-control flatpickr-input" placeholder="Delivery Date" readonly="readonly" name="deliveryDate"></span>
	                            </td>
	                          </tr>

	                           <tr>
	                            <th scope="row">Total Amount</th>

	                            <td id="">
	                            	<input type="text" class="form-control" id="adminAdditionalAmount" name="adminFinalAmount" required placeholder="Total Amount"/>
	                            </td>
	                          </tr>

	                          <tr>
	                            <th scope="row">Invoice Image</th>
	                            <td id="">
	                            	<input type="file" class="dropify" data-max-file-size="1M" name="adminInvoiceImage">
	                            </td>
	                          </tr>

	                          </tbody>
	                      </table>
	                    </div>
	                  </div>
	                </div>
		        </div>
		        <input type="hidden" name="id" value="{{$orderInfo->id}}">
		        
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
		
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-30"><font color="red">Shipping Information</font></h4>
		                    <p class="mb-1"><b>Name:</b> {{$orderInfo->bill_name}}</p>
		                    <p class="mb-1"><b>Address1:</b> {{$orderInfo->bill_address1}}</p>
		                    <p class="mb-1"><b>Address2:</b> {{$orderInfo->bill_address2}}</p>
		                    <p class="mb-1"><b>Country:</b> {{$orderInfo->bill_country}}</p>
		                    
		                   
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
                    <p class="mb-1"><b>State:</b> {{$orderInfo->bill_state}}</p>
                    <p class="mb-1"><b>City:</b> {{$orderInfo->bill_city}}</p>
                    <p class="mb-1"><b>Zip:</b> {{$orderInfo->bill_zip}}</p>
                    <p class="mb-1"><b>Phone:</b> {{$orderInfo->bill_countryCode.'-'.$orderInfo->bill_phone}}</p>
                    
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>

	

	<div class="col-md-12">
		<h4>Admin Send Image To Customer</h4>
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        
		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Product Image</label>
					    <input type="file" name="adminImage1" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>
		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Product Image</label>
					    <input type="file" name="adminImage2" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>
		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Product Image</label>
					    <input type="file" name="adminImage3" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>
		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Product Image</label>
					    <input type="file" name="adminImage4" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>
		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Product Image</label>
					    <input type="file" name="adminImage5" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>
		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Product Image</label>
					    <input type="file" name="adminImage6" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>
		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Product Image</label>
					    <input type="file" name="adminImage7" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>
		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Product Image</label>
					    <input type="file" name="adminImage8" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>
		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Product Image</label>
					    <input type="file" name="adminImage9" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>
		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Product Image</label>
					    <input type="file" name="adminImage10" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>
		        
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>

	<button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">Update Order</button>

</div>
</form>

@endsection