@extends('layouts.admin')

@section('title') Waiting Payment Shipping Order @endsection

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Shipping Waiting Payment Order</h4>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Swift Number</th>
                            <th class="text-center">Product Type</th>
                            <th class="text-center">Invoice Number</th>
                            <th class="text-center">Product Description</th>
                            <th class="text-center">Package Id</th>
                            <th class="text-center">Tracking Number</th>
                            <th class="text-center">Product Send</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($orderInfo as $order)
                        <tr>
                            <td class="text-center">{{$order->swiftNumber}}</td>
                            <td class="text-center">{{$order->productType}}</td>
                            <td class="text-center">{{$order->invoiceNumber}}</td>
                            <td class="text-center">{{$order->productDescription}}</td>
                            <td class="text-center">{{$order->packageId}}</td>
                            <td class="text-center">{{$order->trackingNumber}}</td>
                            <td class="text-center">{{$order->sentTo}}</td>
                            <td class="text-center">
                                <a href="{{route('viewAdminShippingWaitingPaymentOrder',[$order->id])}}" target="_blank" class="action-icon"> <i class="mdi mdi-eye"></i></a>
                            </td>
                           
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection