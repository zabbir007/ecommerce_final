@extends('layouts.admin')

@section('title') Re-Shipping Order @endsection

@section('content')

<form class="needs-validation" method="post" action="{{route('adminSaveShippingOrder')}}" novalidate enctype="multipart/form-data">
    @csrf
        <?php 
        $message=Session::get('message');
        if($message){
        ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php
                    echo $message;
                    Session::put('message','');
                ?>
            </div>
        <?php
        }
        ?>

        <?php 
        $message=Session::get('messageWarning');
        if($message){
        ?>
            <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                <?php
                    echo $message;
                    Session::put('messageWarning','');
                ?>
            </div>
        <?php   
        }
        ?>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Shipping Order From</h4>
                <p class="sub-header">Provide All Information for Re-Shipping Order</p>
                <div class="form-group mb-3">
                    <label for="validationCustom01">Swift Number</label>
                    <select class="selectpicker" data-live-search="true" data-style="btn-light" tabindex="-98" required="" name="swiftNumber">
                        @foreach($switeNumberInfo as $sweetNumber)
                        <option value="{{$sweetNumber->swiftNumber}}">{{$sweetNumber->swiftNumber}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group mb-3">
                    <label for="validationCustom01">Product Type</label>
                    <input type="text" name="productType" class="form-control" id="validationCustom01" placeholder="Product Type" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="form-group mb-3">
                    <label for="validationCustom01">Invoice Number</label>
                    <input type="text" name="invoiceNumber" value="<?php echo date("YmdHis").rand(11,99); ?>" class="form-control" id="validationCustom01" placeholder="Invoice Number" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>

                <div class="form-group mb-3">
                    <label for="validationCustom01">Product Description</label>
                    <input type="text" name="productDescription" class="form-control" id="validationCustom01" placeholder="Product Description" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>

                <div class="form-group mb-3">
                    <label for="validationCustom01">Package Id</label>
                    <input type="text" name="packageId" class="form-control" id="validationCustom01" placeholder="Package Id" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>

                 <div class="form-group mb-3">
                    <label for="validationCustom01">Invoice Image</label>
                    <input type="file" class="dropify" name="invoiceImage" data-max-file-size="1M" required="">
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group mb-3">
                    <label for="validationCustom01">Product From</label>
                    <input type="text" name="from" class="form-control" id="validationCustom01" placeholder="Product From" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>

                <div class="form-group mb-3">
                    <label for="validationCustom01">Tracking Number</label>
                    <input type="text" name="trackingNumber" value="<?php echo date("YmdHis").rand(1111,9999); ?>" class="form-control" id="validationCustom01" placeholder="Tracking Number" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                
                <div class="form-group mb-3">
                    <label for="validationCustom01">Product Weight</label>
                    <input type="text" name="weight" class="form-control" id="validationCustom01" placeholder="Product Weight" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>

                <div class="form-group mb-3">
                    <label for="validationCustom01">Product Sent To</label>
                    <input type="text" name="sentTo" class="form-control" id="validationCustom01" placeholder="Product Sent To" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>

                <div class="form-group mb-3">
                    <label for="validationCustom01">Product Quantity</label>
                    <input type="text" name="quantity" class="form-control" id="validationCustom01" placeholder="Product Quantity" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>

                <div class="form-group mb-3">
                    <label for="validationCustom01">Product Image</label>
                    <input type="file" class="dropify" name="productImage" data-max-file-size="1M" required="">
                </div>

               
                
                <button class="btn btn-primary" type="submit">Add Order</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection