@extends('layouts.admin')

@section('title') Ecommerce Order @endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card-box mb-2">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="media">
                        <div class="media-body">
                            <h4 class="mt-0 mb-2 font-60"><font color="red">Client Information</font></h4>
                            <p class="mb-1"><b>Client Name :</b> {{$orderInfo->firstName}}</p>
                            <p class="mb-0"><b>Client Email :</b> {{$orderInfo->email}}</p>
                            <p class="mb-1"><b>Country :</b> {{$orderInfo->country}}</p>
                            <p class="mb-1"><b>City :</b> {{$orderInfo->city}}</p>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <p class="mb-1"><b>Client Phone :</b> {{$orderInfo->phone}}</p>
                    <p class="mb-0"><b>Address :</b> {{$orderInfo->address}}</p>
                    <p class="mb-1"><b>State :</b> {{$orderInfo->state}}</p>
                    <p class="mb-1"><b>Zip :</b> {{$orderInfo->zip}}</p>

                </div>
            </div> <!-- end row -->
        </div> <!-- end card-box-->
    </div>
    <div class="col-md-6">
        <div class="card-box mb-2">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="media">
                        <div class="media-body">
                            <h4 class="mt-0 mb-2 font-30"><font color="red">Order Information</font></h4>
                            <p class="mb-1"><b>Order Number :</b> {{$orderInfo->orderNumber}}</p>
                            <p class="mb-1"><b>Order Date :</b> {{$orderInfo->orderDate}}</p>
                            <p class="mb-1"><b>Delivery Date :</b> {{$orderInfo->deliveryDate}}</p>
                            <p class="mb-1"><b>Shipping Address :</b> {{$orderInfo->shippingAddress}}</p>
                            <p class="mb-1"><b>Billing Address :</b> {{$orderInfo->billingAddress}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <p class="mb-1"><b>Order Status :</b> <?php if($orderInfo->orderStatus=='pending'){echo "Pending";}else if($orderInfo->orderStatus=='active'){echo "Active";}else if($orderInfo->orderStatus=='complete'){echo "Complete";}else if($orderInfo->orderStatus=='cancel'){echo "Cancel";} ?></p>
                    <p class="mb-1"><b>Paid Status :</b> <?php if($orderInfo->paidStatus=='0'){echo "Unpaid";}else if($orderInfo->paidStatus=='1'){echo "Paid";} ?></p>

                    <p class="mb-1"><b>Total Amount :</b> {{$orderInfo->totalAmount}}</p>
                    <p class="mb-1"><b>Shipping Amount :</b> {{$orderInfo->shippingAmount}}</p>
                    <p class="mb-1"><b>Paid Method :</b> {{$orderInfo->paidMethod}}</p>

                </div>
            </div> <!-- end row -->
        </div> <!-- end card-box-->
    </div>

    
    <div class="col-md-6">
        <div class="card-box mb-2">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="card-body">
                      <div class="info-box text-center">
                        <h3>Product Information</h3>
                        <div class="table-responsive">
                          <table class="table mb-0">
                              <thead>
                              <tr>
                                <th>Product Name</th>
                                <th>Product Quantity</th>
                                <th>Price</th>
                              </tr>
                              </thead>
                              <tbody>

                             <?php
                                $pkgCount=count($name);
                                for($i=0;$i<$pkgCount;$i++){
                             ?>

                                <tr>
                                    <td>
                                        <?php echo $name[$i]; ?>
                                    </td>
                                    <td>
                                       <?php echo $qun[$i]; ?>
                                    </td>
                                    <td>
                                        <?php echo $price[$i]; ?>
                                    </td>
                                    
                                </tr>

                            <?php } ?>
                              <tr>
                                <th scope="row">Shipping Amount</th>
                                <td></td>
                                <td>
                                  {{$orderInfo->shippingAmount}}
                                </td>
                              </tr>
                              
                              <tr>
                                <th scope="row">Total Amount</th>
                                <td></td>
                                <td>
                                    {{$orderInfo->totalAmount}}
                                </td>
                              
                              </tr>
                              </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                </div>
            </div> <!-- end row -->
        </div> <!-- end card-box-->
    </div>

</div>

@endsection