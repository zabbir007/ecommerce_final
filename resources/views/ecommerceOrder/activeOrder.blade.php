@extends('layouts.admin')

@section('title') Ecommerce Active Order @endsection

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show Active Ecommerce Order</h4>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Order Number</th>
                            <th class="text-center">Order Date</th>
                            <th class="text-center">Customer Name</th>
                            <th class="text-center">Customer Email</th>
                            <th class="text-center">Customer Phone</th>
                            <th class="text-center">Order Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($orderInfo as $order)
                        <tr>
                            <td class="text-center">{{$order->orderNumber}}</td>
                            <td class="text-center">{{$order->orderDate}}</td>
                            <td class="text-center">{{$order->firstName}}</td>
                            <td class="text-center">{{$order->email}}</td>
                            <td class="text-center">{{$order->phone}}</td>
                            <td class="text-center"><?php if($order->orderStatus=='pending'){echo "Pending";}else if($order->orderStatus=='active'){echo "Active";}else if($order->orderStatus=='complete'){echo "Complete";}else if($order->orderStatus=='cancel'){echo "Cancel";}?></td>
                            <td class="text-center">
                                <a href="{{route('ecommerceEditActiveOrder',[$order->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection