@extends('layouts.user')

@section('title') Order Information @endsection

@section('content')

<div class="nav-2 allpadding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <div class="accordion-menu">
              <div id="accordion">
                <div class="card card-menu">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                    </h5>
                  </div>
                  <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body card-body-menu-popup">
                      <div class='cssmenuvv'>
                      <ul>
                         <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <div class="manu">
              <ul>
                <li>
                  <a href="{{route('userOrder')}}"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
            </div>
          </div>
        </div>
        </div>
      </div>
</section>

<section class="mt-5">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="contact-left border text-center p-5">
          <h6>Shipping Address</h6>
            <table class="table">
			  <thead>
			    <tr>
			      <th>Name</th>
			      <th>{{$pendingOrder->bill_name}}</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td>First Address</td>
			      <td>{{$pendingOrder->bill_address1}}</td>
			    </tr>

			    <tr>
			      <td>Second Address</td>
			      <td>{{$pendingOrder->bill_address2}}</td>
			    </tr>

			    <tr>
			      <td>Country</td>
			      <td>{{$pendingOrder->bill_country}}</td>
			    </tr>

			    <tr>
			      <td>City</td>
			      <td>{{$pendingOrder->bill_city}}</td>
			    </tr>

			    <tr>
			      <td>State</td>
			      <td>{{$pendingOrder->bill_state}}</td>
			    </tr>

			    <tr>
			      <td>City</td>
			      <td>{{$pendingOrder->bill_zip}}</td>
			    </tr>
			    
			  </tbody>
			</table>

        </div>
      </div>
      <div class="col-md-4">
        <div class="contact-left border text-center p-5">
	          <h6>Product Information</h6>
	            <table class="table">
				  <thead>
				    <tr>
				      <th>Order Type</th>
				      <th><?php if($pendingOrder->orderType=='1'){echo "Inside Order";}else if($pendingOrder->orderType=='2'){echo "Outside Order";} ?></th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td>Product Type</td>
				      <td>{{$pendingOrder->productType}}</td>
				    </tr>

				    <tr>
				      <td>Product Name</td>
				      <td>{{$pendingOrder->productName}}</td>
				    </tr>

				    <tr>
				      <td>Product Link</td>
				      <td><a href="{{$pendingOrder->productLink}}">Click Here</a></td>
				    </tr>

				    <tr>
				      <td>Product Description</td>
				      <td>{{$pendingOrder->productDescription}}</td>
				    </tr>

				    <tr>
				      <td>Product Estimate Price</td>
				      <td>{{$pendingOrder->productEstimatePrice}}</td>
				    </tr>

				    <tr>
				      <td>Product Quantity</td>
				      <td>{{$pendingOrder->productQuantity}}</td>
				    </tr>
				    
				  </tbody>
				</table>
	        </div>
	      </div>

	      <div class="col-md-4">
        <div class="contact-left border text-center p-5">
	          <h6>Order Information</h6>
	            <table class="table">
				  <thead>
				    <tr>
				      <th>Order Number</th>
				      <th>{{$pendingOrder->orderNumber}}</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td>Order Date</td>
				      <td>{{$pendingOrder->orderDate}}</td>
				    </tr>

				    <tr>
				      <td>Order Status</td>
				      <td>{{$pendingOrder->orderStatus}}</td>
				    </tr>

				    <tr>
				      <td>Initial Amount</td>
				      <td>{{$pendingOrder->initialAmount}}</td>
				    </tr>

				    <tr>
				      <td>Paid Status</td>
				      <td><?php if($pendingOrder->initialAmountStatus=='1'){echo "Paid";}else if($pendingOrder->initialAmountStatus=='0'){echo "Un-Paid";} ?></td>
				    </tr>

				    
				    
				  </tbody>
				</table>
	        </div>
	      </div>
    </div>
</section>

<section id="gallary" class=" mt-5 pb-3 bg-light">
  <div class="container">
    <h2 class="text-center p-2">Product Images</h2>
    <div class="row">
      <div class="col-md-3 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->userImage1 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->userImage1 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-3 text-center text-md-left">
        <a  href="{{asset( $pendingOrder->userImage2 )}}"class="img-fluid" data-lightbox="example-set">
          <img  src="{{asset( $pendingOrder->userImage2 )}}"class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-3 text-center text-md-left">
        <a  href="{{asset( $pendingOrder->userImage3 )}}"class="img-fluid" data-lightbox="example-set">
          <img  src="{{asset( $pendingOrder->userImage3 )}}"class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-3 text-center text-md-left">
        <a  href="{{asset( $pendingOrder->userImage4 )}}"class="img-fluid" data-lightbox="example-set">
          <img  src="{{asset( $pendingOrder->userImage4 )}}"class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4">

      </div>
      <div class="col-md-4 text-center text-md-left mt-5">
        <a  href="{{asset( $pendingOrder->userImage5 )}}"class="img-fluid" data-lightbox="example-set">
          <img  src="{{asset( $pendingOrder->userImage5 )}}"class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4">

      </div>
    </div>
  </div>
</section>



@endsection