@extends('layouts.user')

@section('title') Order Information @endsection

@section('content')


<div class="nav-2 allpadding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <div class="accordion-menu">
              <div id="accordion">
                <div class="card card-menu">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                    </h5>
                  </div>
                  <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body card-body-menu-popup">
                      <div class='cssmenuvv'>
                      <ul>
                         <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <div class="manu">
              <ul>
                <li>
                  <a href="{{route('userOrder')}}"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
            </div>
          </div>
        </div>
        </div>
      </div>
</section>

<form method="post" action="{{route('updateCustomerWaitingPaymentCustomOrder')}}"> 
  @csrf
  
<section class="mt-5">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="contact-left border text-center p-5">
          <h6>Admin Resend Information</h6>
          
          <table class="table">
            <thead>
              <tr>
                <th>Product Price</th>
                <th>{{$pendingOrder->adminProductPrice}}</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Product Quantity</td>
                <td>{{$pendingOrder->adminProductQuantity}}</td>
              </tr>

              <tr>
                <td>Product Size</td>
                <td>{{$pendingOrder->adminProductSize}}</td>
              </tr>

              <tr>
                <td>Product Total Amount</td>
                <td>{{$pendingOrder->adminProductFinalAmount}}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div class="col-md-6">
        <div class="contact-left border text-center ">
          <h6>Total Amount</h6>
          <table class="table table-bordered">
`          <thead>
            <tr>
              <th scope="col">Product</th>
              <th scope="col">Price</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Product Price</td>
              <td>{{$pendingOrder->adminProductFinalAmount}}</td>
              <input type="hidden" name="productPrice" id="productPrice" value="{{$pendingOrder->adminProductFinalAmount}}">
              <input type="hidden" name="orderNumber" id="orderNumber" value="{{$pendingOrder->orderNumber}}">
              <input type="hidden" name="email" id="email" value="{{$pendingOrder->email}}">
            </tr>
            <tr>
              <td>Fragile Sticker Price</td>
              <td>{{$pendingOrder->fragileStickerAmount}}</td>
            </tr>
            <tr>
              <td>Image Price</td>
              <td>{{$pendingOrder->setAdditionalPhotoPrice}}</td>
              
            </tr>
            <tr>
              <td>Box Price</td>
             <td>{{$pendingOrder->setBoxPrice}}</td>
              
            </tr>

            <tr>
              <td>Insurance Amount</td>
              <td>{{$pendingOrder->finalInsuranceAmount}}</td>
              
            </tr>
            <tr>
              <td>Service Amount</td>
              <td>{{$pendingOrder->adminServicePrice}}</td>
              
            </tr>
            <tr>
              <td>Shipping Amount</td>
              <td>{{$pendingOrder->adminShippingPrice}}</td>
              
            </tr>
            <tr>
              <td>Bute Amount</td>
              <td>{{$pendingOrder->adminButePrice}}</td>
              
            </tr>
            <tr>
              <td>Additional Amount</td>
              <td>{{$pendingOrder->adminAdditionalAmount}}</td>
              
            </tr>
            <tr>
              <td>Final Amount</td>
              <td>{{$pendingOrder->adminSetProductFinalAmount}}</td>
              
            </tr>
            <input type="hidden" name="id" value="{{$id}}">

          </tbody>
        </table>`
        </div>
      </div>
      </div>
      <input type="submit" class="text-center btn-success mt-3" name="update" value="Pay">
    </div>
</section>





<section id="gallary" class=" mt-5 pb-3 bg-light">
  <div class="container">
    <h6 class="text-center p-2">Admin Resend Images</h6>
    <div class="row">
    <?php if($pendingOrder->additionalPicture=='1'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($pendingOrder->additionalPicture=='2'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($pendingOrder->additionalPicture=='3'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage6 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage6 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($pendingOrder->additionalPicture=='4'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage6 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage6 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage7 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage7 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($pendingOrder->additionalPicture=='5'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage6 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage6 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage7 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage7 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage8 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage8 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($pendingOrder->additionalPicture=='6'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage6 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage6 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage7 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage7 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage8 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage8 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage9 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage9 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($pendingOrder->additionalPicture=='7'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage6 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage6 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage7 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage7 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage8 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage8 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage9 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage9 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage10 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage10 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($pendingOrder->additionalPicture=='8'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage6 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage6 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage7 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage7 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage8 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage8 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage9 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage9 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage10 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage10 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage11 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage11 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($pendingOrder->additionalPicture=='9'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage6 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage6 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage7 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage7 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage8 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage8 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage9 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage9 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage10 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage10 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage11 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage11 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage12 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage12 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($pendingOrder->additionalPicture=='10'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage6 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage6 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage7 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage7 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage8 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage8 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage9 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage9 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage10 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage10 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage11 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage11 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage12 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage12 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $pendingOrder->adminImage13 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $pendingOrder->adminImage13 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
      
      <div class="col-md-4">

      </div>
    </div>
  </div>
</section>

</form>
@endsection