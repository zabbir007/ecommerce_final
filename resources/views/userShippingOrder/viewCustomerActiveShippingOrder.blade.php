@extends('layouts.user')

@section('title') Re-Shipping Information @endsection

@section('content')


<div class="nav-2 allpadding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <div class="accordion-menu">
              <div id="accordion">
                <div class="card card-menu">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                    </h5>
                  </div>
                  <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body card-body-menu-popup">
                      <div class='cssmenuvv'>
                      <ul>
                         <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <div class="manu">
              <ul>
                <li>
                  <a href="{{route('userOrder')}}"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
            </div>
          </div>
        </div>
        </div>
      </div>
</section>
<form method="post" action="{{route('updateCustomerWaitingPaymentShippingOrder')}}">
  @csrf
<section class="mt-5">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="contact-left border text-center p-5">
          <h6>Admin Send Information</h6>
          
          <table class="table">
            <thead>
              <tr>
                <th>Product Type</th>
                <th>{{$orderInfo->productType}}</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Product Description</td>
                <td>{{$orderInfo->productDescription}}</td>
              </tr>

              <tr>
                <td>Tracking Number</td>
                <td>{{$orderInfo->trackingNumber}}</td>
              </tr>

              <tr>
                <td>Invoice Number</td>
                <td>{{$orderInfo->invoiceNumber}}</td>
              </tr>

              <tr>
                <td>Package Id</td>
                <td>{{$orderInfo->packageId}}</td>
                <input type="hidden" name="id" value="{{$orderInfo->id}}">
              </tr>

              <tr>
                <td>Product From</td>
                <td>{{$orderInfo->from}}</td>
              </tr>

              <tr>
                <td>Product Weight</td>
                <td>{{$orderInfo->weight}}</td>
              </tr>

              <tr>
                <td>Product Sent To</td>
                <td>{{$orderInfo->sentTo}}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-4">
          <div class="contact-left border text-center p-5">
            <h6>Your Order Information</h6>
            
            <table class="table">
              <thead>
                <tr>
                  <th>Additional Image</th>
                  <th>{{$orderInfo->shippingAdditionalPicture}}</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Box Condition</td>
                  <td>{{$orderInfo->boxCondition}}</td>
                </tr>

                <tr>
                  <td>Personal Use</td>
                  <td><?php if($orderInfo->personalUse=='1'){echo "Personal Use Only";}else{echo "Not Only Personal Use";} ?></td>
                </tr>

                <tr>
                  <td>Shipping Type</td>
                  <td><?php if($orderInfo->shippingType=='1'){echo "Inside Shipping";}else{echo "Outside Shipping";} ?></td>
                </tr>

                <tr>
                  <td>Picture Price</td>
                  <td>{{$orderInfo->setAdditionalPhotoPrice}}</td>
                </tr>

                <tr>
                  <td>Box Price</td>
                  <td>{{$orderInfo->setBoxPrice}}</td>
                </tr>

                <tr>
                  <td>Box Category</td>
                  <td>{{$orderInfo->shippingBoxCategory}}</td>
                </tr>
                 <tr>
                  <td>Box Sub Category</td>
                  <td>{{$orderInfo->shippingBoxSubCategory}}</td>
                </tr>
                <tr>
                  <td>Shipping Category</td>
                  <td>{{$orderInfo->shippingBoxCategory}}</td>
                </tr>
                <tr>
                  <td>Shipping Sub Category</td>
                  <td>{{$orderInfo->shippingBoxSubCategory}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div class="col-md-4">
          <div class="contact-left border text-center p-5">
            <h6>Shipping Address</h6>
            
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>{{$orderInfo->bill_name}}</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Address 1</td>
                  <td>{{$orderInfo->bill_address1}}</td>
                </tr>

                <tr>
                  <td>Address 2</td>
                  <td>{{$orderInfo->bill_address2}}</td>
                </tr>

                <tr>
                  <td>Country</td>
                  <td>{{$orderInfo->bill_country}}</td>
                </tr>

                <tr>
                  <td>City</td>
                  <td>{{$orderInfo->bill_city}}</td>
                </tr>

                <tr>
                  <td>State</td>
                  <td>{{$orderInfo->bill_state}}</td>
                </tr>

                <tr>
                  <td>Zip</td>
                  <td>{{$orderInfo->bill_zip}}</td>
                </tr>

                <tr>
                  <td>Phone</td>
                  <td>{{$orderInfo->bill_countryCode .'-'.$orderInfo->bill_phone}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div class="col-md-4">
          <div class="contact-left border text-center p-5">
            <h6>Payment Information</h6>
            
            <table class="table">
              <thead>
                <tr>
                  <th>Shipping Charge</th>
                  <th>{{$orderInfo->adminShippingCharge}}</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Service Charge</td>
                  <td>{{$orderInfo->adminServiceCharge}}</td>
                </tr>
                <tr>
                  <td>Fragile Sticker Amount</td>
                  <td>{{$orderInfo->fragileStickerAmount}}</td>
                </tr>
                <tr>
                  <td>Insurance Amount</td>
                  <td>{{$orderInfo->insuranceAmount}}</td>
                </tr>

                <tr>
                  <td>Total Amount</td>
                  <td>{{$orderInfo->adminFinalAmount}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

      </div>
     
    </div>
</section>
</form>
<section id="gallary" class=" mt-5 pb-3 bg-light">
  <div class="container">
    <h6 class="text-center p-2">Admin Resend Images</h6>
    <div class="row">
    <?php if($orderInfo->shippingAdditionalPicture=='1'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage1 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($orderInfo->shippingAdditionalPicture=='2'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage2 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage3 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($orderInfo->shippingAdditionalPicture=='3'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage1 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage2 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage3 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($orderInfo->shippingAdditionalPicture=='4'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage1 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage2 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage3 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($orderInfo->shippingAdditionalPicture=='5'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage1 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage2 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage3 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($orderInfo->shippingAdditionalPicture=='6'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage1 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage2 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage3 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage6 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage6 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($orderInfo->shippingAdditionalPicture=='7'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage1 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage2 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage3 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage6 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage6 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage7 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage7 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($orderInfo->shippingAdditionalPicture=='8'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage1 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage2 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage3 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage6 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage6 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage7 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage7 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage8 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage8 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($orderInfo->shippingAdditionalPicture=='9'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage1 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage2 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage3 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage6 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage6 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage7 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage7 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage8 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage8 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage9 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage9 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
     <?php if($orderInfo->shippingAdditionalPicture=='10'){ ?>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage1 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage2 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage3 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage4 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage5 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage5 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage6 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage6 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage7 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage7 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage8 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage8 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage9 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage9 )}}" class="img-fluid" alt=""/></a>
      </div>
      <div class="col-md-4 text-center text-md-left">
      <a  href="{{asset( $orderInfo->adminImage10 )}}"class="img-fluid" data-lightbox="example-set">
        <img  src="{{asset( $orderInfo->adminImage10 )}}" class="img-fluid" alt=""/></a>
      </div>
    <?php } ?>
      
      <div class="col-md-4">

      </div>
    </div>
  </div>
</section>

@endsection