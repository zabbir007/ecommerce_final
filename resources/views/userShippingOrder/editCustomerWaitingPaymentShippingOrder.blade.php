@extends('layouts.user')

@section('title') Re-Shipping Information @endsection

@section('content')


<div class="nav-2 allpadding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <div class="accordion-menu">
              <div id="accordion">
                <div class="card card-menu">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                    </h5>
                  </div>
                  <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body card-body-menu-popup">
                      <div class='cssmenuvv'>
                      <ul>
                         <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <div class="manu">
              <ul>
                <li>
                  <a href="{{route('userOrder')}}"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
            </div>
          </div>
        </div>
        </div>
      </div>
</section>

  <section>
    <div class="container">
      <div class="gallery">
        <h2 class="text-center w-50 border border-danger mt-5 mb-5 py-3 text-dark mx-auto">Invoice Images</h2>
        <img src="{{asset($orderInfo->adminInvoiceImage)}}" alt="">
      </div>
    </div>
  </section>
<section class="img-gallery">
  <div class="container">
    <h2 class="text-center w-50 border border-danger mt-5 py-3 text-dark mx-auto">Admin Resend Images</h2>
    <div class="row">


      <?php if($orderInfo->shippingAdditionalPicture=='1'){ ?>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage1 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
      <?php } ?>
       <?php if($orderInfo->shippingAdditionalPicture=='2'){ ?>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage1 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage2 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        
      <?php } ?>
       <?php if($orderInfo->shippingAdditionalPicture=='3'){ ?>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage1 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage2 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage3 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        
      <?php } ?>
       <?php if($orderInfo->shippingAdditionalPicture=='4'){ ?>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage1 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage2 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage3 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
         <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage4 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
      <?php } ?>
       <?php if($orderInfo->shippingAdditionalPicture=='5'){ ?>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage1 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage2 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage3 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
         <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage4 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage5 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage5 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        
      <?php } ?>
       <?php if($orderInfo->shippingAdditionalPicture=='6'){ ?>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage1 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage2 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage3 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
         <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage4 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage5 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage5 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage6 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage6 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
      <?php } ?>
       <?php if($orderInfo->shippingAdditionalPicture=='7'){ ?>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage1 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage2 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage3 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
         <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage4 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage5 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage5 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage6 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage6 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage7 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage7 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
      <?php } ?>
       <?php if($orderInfo->shippingAdditionalPicture=='8'){ ?>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage1 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage2 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage3 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
         <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage4 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage5 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage5 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage6 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage6 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage7 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage7 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage8 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage8 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
      <?php } ?>
       <?php if($orderInfo->shippingAdditionalPicture=='9'){ ?>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage1 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage2 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage3 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
         <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage4 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage5 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage5 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage6 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage6 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage7 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage7 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage8 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage8 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage9 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage9 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
      <?php } ?>
       <?php if($orderInfo->shippingAdditionalPicture=='10'){ ?>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage1 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage1 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage2 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage2 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage3 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage3 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
         <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage4 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage4 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage5 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage5 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage6 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage6 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage7 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage7 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage8 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage8 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage9 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage9 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="grid-item grid-item--width2 transition">
            <a href="{{asset( $orderInfo->adminImage10 )}}" data-lightbox="example-set">
              <img  src="{{asset( $orderInfo->adminImage10 )}}" class="img-fluid" alt="">
              <div class="mymasonry-overlay">
                <i class="fas fa-expand"></i>
            </div>
            </a>
          </div>
        </div>
      <?php } ?>
      
      
    </div>
  </div>
</section>

@endsection