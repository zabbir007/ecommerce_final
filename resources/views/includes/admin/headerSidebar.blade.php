========== Left Sidebar Start ========== -->
            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                        <ul class="metismenu" id="side-menu">

                            <li class="menu-title">  </li>

                            <li>
                                <a href="{{route('superAdminDashboard')}}">
                                    <i class="fas fa-database"></i>
                                    <span> Dashboards </span>
                                </a>

                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-cart-plus"></i>
                                    <span> Order </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <span class="badge badge-success badge-pill float-right">
                                        <?php
                                            $total=DB::table('ecommerce_order')
                                                        ->count();
                                            echo $total;
                                        ?>
                                        </span>
                                        <a href="{{route('ecommerceAllOrder')}}">All Order</a>
                                    </li>
                                    <li>
                                        <span class="badge badge-success badge-pill float-right">
                                        <?php
                                            $total=DB::table('ecommerce_order')
                                                        ->where('orderStatus','pending')
                                                        ->count();
                                            echo $total;
                                        ?>
                                        </span>
                                        <a href="{{route('ecommercePendingOrder')}}">Pending Order</a>
                                    </li>
                                    <li>
                                        <span class="badge badge-success badge-pill float-right">
                                        <?php
                                            $total=DB::table('ecommerce_order')
                                                        ->where('orderStatus','active')
                                                        ->count();
                                            echo $total;
                                        ?>
                                         </span>
                                        <a href="{{route('ecommerceActiveOrder')}}">Active Order</a>
                                    </li>
                                    <li>
                                        <span class="badge badge-success badge-pill float-right">
                                        <?php
                                            $total=DB::table('ecommerce_order')
                                                        ->where('orderStatus','complete')
                                                        ->count();
                                            echo $total;
                                        ?>
                                        </span>
                                        <a href="{{route('ecommerceCompleteOrder')}}">Completed Order</a>
                                    </li>
                                    <li>
                                        <span class="badge badge-success badge-pill float-right">
                                        <?php
                                            $total=DB::table('ecommerce_order')
                                                        ->where('orderStatus','cancel')
                                                        ->count();
                                            echo $total;
                                        ?>
                                         </span>
                                        <a href="{{route('ecommerceCancelOrder')}}">Canceled Order</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-file-alt"></i>
                                    <span> Invoices </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="#">All Invoices</a>
                                    </li>
                                    <li>
                                        <a href="#">Unpaid Invoices</a>
                                    </li>
                                    <li>
                                        <a href="#">Paid Invoices</a>
                                    </li>
                                    <li>
                                        <a href="#">Canceled Invoices</a>
                                    </li>
                                    <li>
                                        <a href="#">Create Invoices</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-user"></i>
                                    <span> Client </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <span class="badge badge-success badge-pill float-right">
                                        <?php
                                            $totalCustomer=DB::table('customer')
                                                                ->count();
                                            echo $totalCustomer;
                                        ?>
                                        </span>
                                        <a href="{{route('showCustomer')}}">View Search Client</a>
                                    </li>
                                    <li>
                                        <a href="{{route('addCustomer')}}">Add A New Client</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript: void(0);">
                                  <i class="fas fa-clipboard"></i>
                                    <span> Report </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="#">Sales Report</a>
                                    </li>
                                    <li>
                                        <a href="#">Stock Report</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-mail"></i>
                                    <span> Utilities </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="#">Email A Customer</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showSendMessage')}}">Send Mass  Email</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-shopping-bag"></i>
                                    <span> Product </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <span class="badge badge-success badge-pill float-right">
                                            <?php
                                                $countProduct=DB::table('product')
                                                                    ->count();
                                                echo $countProduct;
                                            ?>
                                        </span>
                                        <a href="{{route('showProduct')}}">All Product</a>
                                    </li>
                                    <li>
                                        <a href="{{route('addProduct')}}">Add Product</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showDepartment')}}">Departments</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showCategory')}}">Categories</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showsubCategory')}}">Sub Categories</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showCoupon')}}">Coupon</a>
                                    </li>
                                    <li>
                                        <a href="{{route('addShippingCost')}}">Shipping Cost</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="far fa-clipboard"></i>
                                    <span> Custom Order </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                  <li>
                                      <a href="#" aria-expanded="false">Inside Order
                                          <span class="menu-arrow"></span>
                                      </a>
                                      <ul class="nav-third-level nav" aria-expanded="false">
                                          <li>
                                            <span class="badge badge-success badge-pill float-right">
                                                <?php
                                                $totalCustomOrder=DB::table('custom_order')
                                                                    ->where('orderStatus','pending')
                                                                    ->count();
                                                echo $totalCustomOrder;
                                                ?>
                                            </span>
                                            <a href="{{route('showAdminPendingCustomOrder')}}">Pending Order</a>
                                          </li>
                                          <li>
                                             <span class="badge badge-success badge-pill float-right">
                                                <?php
                                                $totalInsideCustomOrder=DB::table('custom_order')
                                                                            ->where('orderStatus','adminprocessing')
                                                                            ->count();
                                                echo $totalInsideCustomOrder;
                                                ?>
                                            </span>
                                            <a href="{{route('showAdminAdminProcessingCustomOrder')}}">Admin Processing</a>
                                          </li>
                                          <li>
                                             <span class="badge badge-success badge-pill float-right">
                                                <?php
                                                $totalInsideCustomOrder=DB::table('custom_order')
                                                                            ->where('orderStatus','processing')
                                                                            ->count();
                                                echo $totalInsideCustomOrder;
                                                ?>
                                            </span>
                                            <a href="{{route('showAdminProcessingCustomOrder')}}">Processing Order</a>
                                          </li>
                                          <li>
                                            <span class="badge badge-success badge-pill float-right">
                                                <?php
                                                $totalInsideCustomOrder=DB::table('custom_order')
                                                                            ->where('orderStatus','waitingpayment')
                                                                            ->count();
                                                echo $totalInsideCustomOrder;
                                                ?>
                                            </span>
                                              <a href="{{route('showAdminWaitingPaymentCustomOrder')}}">Waiting Payment</a>
                                          </li>
                                          <li>
                                            <span class="badge badge-success badge-pill float-right">
                                                <?php
                                                $totalInsideCustomOrder=DB::table('custom_order')
                                                                            ->where('orderStatus','active')
                                                                            ->count();
                                                echo $totalInsideCustomOrder;
                                                ?>
                                            </span>
                                            <a href="{{route('showAdminActiveCustomOrder')}}">Active Order</a>
                                          </li>
                                          <li>
                                            <span class="badge badge-success badge-pill float-right">
                                                <?php
                                                $totalInsideCustomOrder=DB::table('custom_order')
                                                                            ->where('orderStatus','complete')
                                                                            ->count();
                                                echo $totalInsideCustomOrder;
                                                ?>
                                            </span>
                                              <a href="{{route('showAdminCompleteCustomOrder')}}">Completed Order</a>
                                          </li>
                                      </ul>
                                  </li>
                                 
                                  <li>
                                      <a href="#" aria-expanded="false">Outside Order
                                          <span class="menu-arrow"></span>
                                      </a>
                                      <ul class="nav-third-level nav" aria-expanded="false">
                                          <li>
                                            <span class="badge badge-success badge-pill float-right">
                                                <?php
                                                $totalCustomOrder=DB::table('custom_order_outside')
                                                                    ->where('status','pending')
                                                                    ->count();
                                                echo $totalCustomOrder;
                                                ?>
                                            </span>
                                            <a href="{{route('showAdminPendingCustomOrderOutside')}}">Pending Order</a>
                                          </li>
                                          <li>
                                            <span class="badge badge-success badge-pill float-right">
                                                <?php
                                                $totalCustomOrder=DB::table('custom_order_outside')
                                                                    ->where('status','active')
                                                                    ->count();
                                                echo $totalCustomOrder;
                                                ?>
                                            </span>
                                            <a href="{{route('showAdminActiveCustomOrderOutside')}}">Active Order</a>
                                          </li>
                                          <li>
                                            <span class="badge badge-success badge-pill float-right">
                                                <?php
                                                $totalCustomOrder=DB::table('custom_order_outside')
                                                                    ->where('status','complete')
                                                                    ->count();
                                                echo $totalCustomOrder;
                                                ?>
                                            </span>
                                            <a href="{{route('showAdminCompleteCustomOrderOutside')}}">Complete Order</a>
                                          </li>
                                      </ul>
                                  </li>
                                    <li>
                                        <a href="{{route('showOrderProductType')}}" aria-expanded="false">Product Type
                                        </a>
                                    </li>
                                    <!-- <li>
                                        <a href="javascript: void(0);" aria-expanded="false">Box Price
                                            <span class="menu-arrow"></span>
                                        </a>
                                        <ul class="nav-third-level nav" aria-expanded="false">
                                            <li>
                                                <a href="{{route('showOrderBoxCategory')}}">Box Category</a>
                                            </li>
                                            <li>
                                                <a href="{{route('showOrderBoxSubCategory')}}">Box Sub Category</a>
                                            </li>
                                        </ul>
                                    </li> -->

                                    <li>
                                        <a href="javascript: void(0);" aria-expanded="false">Shipping Method
                                            <span class="menu-arrow"></span>
                                        </a>
                                        <ul class="nav-third-level nav" aria-expanded="false">
                                            <li>
                                                <a href="{{route('showOrderShippingCategory')}}">Shipping Category</a>
                                            </li>
                                            <li>
                                                <a href="{{route('showOrderShippingSubCategory')}}">Shipping Sub Category</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="javascript: void(0);" aria-expanded="false">Additional Photo Price
                                            <span class="menu-arrow"></span>
                                        </a>
                                        <ul class="nav-third-level nav" aria-expanded="false">
                                            <li>
                                                <a href="{{route('showOrderAdditionalPhotoPrice')}}">Show Price</a>
                                            </li>
                                           
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="{{route('showPredefinedMarketplace')}}">Predefined MarketPlace</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showOrderBasicSetting')}}">Basic Setting</a>
                                    </li>
                                    <li>
                                        <!-- inside box price -->
                                        <a href="{{route('showOutsideBoxPrice')}}">Package Price</a>
                                    </li>
                                    <li>
                                        <a href="javascript: void(0);" aria-expanded="false">Setting Local
                                            <span class="menu-arrow"></span>
                                        </a>
                                        <ul class="nav-third-level nav" aria-expanded="false">
                                            <li>
                                                <a href="{{route('showInsideOrderPaymentSetting')}}">Show Config</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript: void(0);" aria-expanded="false">Setting International
                                            <span class="menu-arrow"></span>
                                        </a>
                                        <ul class="nav-third-level nav" aria-expanded="false">
                                            <li>
                                                <a href="{{route('showOutsideOrderPaymentSetting')}}">Show Config</a>
                                            </li>
                                           
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-shipping-fast"></i>
                                    <span> Reshipping </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level nav" aria-expanded="false">

                                    <li>
                                        <a href="#" aria-expanded="false">All Order
                                            <span class="menu-arrow"></span>
                                        </a>
                                        <ul class="nav-third-level nav" aria-expanded="false">
                                            <li>
                                                <span class="badge badge-success badge-pill float-right">
                                                    <?php
                                                    $totalInsideCustomOrder=DB::table('shipping_order')
                                                                                ->where('status','pending')
                                                                                ->count();
                                                    echo $totalInsideCustomOrder;
                                                    ?>
                                                </span>
                                                <a href="{{route('showAdminShippingPendingOrder')}}">Pending Order</a>
                                            </li>
                                            <li>
                                                <span class="badge badge-success badge-pill float-right">
                                                <?php
                                                $totalInsideCustomOrder=DB::table('shipping_order')
                                                                            ->where('status','processing')
                                                                            ->count();
                                                echo $totalInsideCustomOrder;
                                                ?>
                                                </span>
                                                <a href="{{route('showAdminShippingProcessingOrder')}}">Processing Order</a>
                                            </li>
                                            <li>
                                                <span class="badge badge-success badge-pill float-right">
                                                <?php
                                                $totalInsideCustomOrder=DB::table('shipping_order')
                                                                            ->where('status','waitingpayment')
                                                                            ->count();
                                                echo $totalInsideCustomOrder;
                                                ?>
                                                </span>
                                                <a href="{{route('showAdminShippingWaitingPaymentOrder')}}">Waiting Payment Order</a>
                                            </li>
                                            <li>
                                                <span class="badge badge-success badge-pill float-right">
                                                <?php
                                                $totalInsideCustomOrder=DB::table('shipping_order')
                                                                            ->where('status','active')
                                                                            ->count();
                                                echo $totalInsideCustomOrder;
                                                ?>
                                                </span>
                                                <a href="{{route('showAdminShippingActiveOrder')}}">Active Order</a>
                                            </li>

                                            <li>
                                                <span class="badge badge-success badge-pill float-right">
                                                <?php
                                                $totalInsideCustomOrder=DB::table('shipping_order')
                                                                            ->where('status','complete')
                                                                            ->count();
                                                echo $totalInsideCustomOrder;
                                                ?>
                                                </span>
                                                <a href="{{route('showAdminShippingCompleteOrder')}}">Complete Order</a>
                                            </li>

                                          
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{route('adminAddShippingOrder')}}">Add New Order</a>
                                    </li>
                                    <!-- <li>
                                        <a href="javascript: void(0);" aria-expanded="false">Box Price
                                            <span class="menu-arrow"></span>
                                        </a>
                                        <ul class="nav-third-level nav" aria-expanded="false">
                                            <li>
                                                <a href="{{route('showShippingBoxCategory')}}">Box Category</a>
                                            </li>
                                            <li>
                                                <a href="{{route('showShippingBoxSubCategory')}}">Box Sub Category</a>
                                            </li>
                                        </ul>
                                    </li> -->

                                    <li>
                                        <a href="javascript: void(0);" aria-expanded="false">Shipping Method
                                            <span class="menu-arrow"></span>
                                        </a>
                                        <ul class="nav-third-level nav" aria-expanded="false">
                                            <li>
                                                <a href="{{route('showShippingShippingCategory')}}">Shipping Category</a>
                                            </li>
                                            <li>
                                                <a href="{{route('showShippingShippingSubCategory')}}">Shipping Sub Category</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="javascript: void(0);" aria-expanded="false">Additional Photo Price
                                            <span class="menu-arrow"></span>
                                        </a>
                                        <ul class="nav-third-level nav" aria-expanded="false">
                                            <li>
                                                <a href="{{route('showShippingAdditionalPhotoPrice')}}">Show Price</a>
                                            </li>
                                           
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{route('showShippingSetting')}}">Basic Setting</a>
                                    </li>

                                    <li>
                                        <a href="{{route('showInsideChargeSetting')}}">Payment Setting</a>
                                    </li>

                                    <li>
                                        <!-- inside box price -->
                                        <a href="{{route('showInsideBoxPrice')}}">Package Price</a>
                                    </li>

                                   <!--  <li>
                                        <a href="javascript: void(0);" aria-expanded="false">Setting Local
                                            <span class="menu-arrow"></span>
                                        </a>
                                        <ul class="nav-third-level nav" aria-expanded="false">
                                            <li>
                                                <a href="{{route('showInsideChargeSetting')}}">Show Setting</a>
                                            </li>
                                           
                                        </ul>
                                    </li> -->
                                   <!--  <li>
                                        <a href="javascript: void(0);" aria-expanded="false">Setting International
                                            <span class="menu-arrow"></span>
                                        </a>
                                        <ul class="nav-third-level nav" aria-expanded="false">
                                            <li>
                                                <a href="{{route('showOutsideChargeSetting')}}">Show Setting</a>
                                            </li>
                                           
                                        </ul>
                                    </li> -->
                                    <li>
                                        <a href="#" aria-expanded="false">Swift
                                            <span class="menu-arrow"></span>
                                        </a>
                                        <ul class="nav-third-level nav" aria-expanded="false">
                                            <li>
                                                <a href="{{route('showSweetAddress')}}">Warehouse Locations</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fas fa-cog"></i>
                                    <span> Setup </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('showCompanySetting')}}">General</a>
                                    </li>
                                    <li>
                                        <a href="#">Localization</a>
                                    </li>
                                    <li>
                                        <a href="#">Ordering</a>
                                    </li>
                                    <li>
                                        <a href="#">Mail And SMS</a>
                                    </li>
                                    <li>
                                        <a href="#">Invoices</a>
                                    </li>
                                    <li>
                                        <a href="#">Security</a>
                                    </li>
                                    <li>
                                        <a href="#">Others</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->