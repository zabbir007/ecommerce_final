  <section class="top-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <p class="ml-5">Shop and Ship anywhere in the world! <span>Wellcome to Ushopnship!</span>    Need Help? Call Us:<span>  01841-167177,01841-167178</span> </p>
        </div>
        <div class="col-md-6">
          <ul class="right-nav mr-5">
            <?php 

              $language=Session::get('language');
              if ($language=='1') {
             ?>
            <li> 
            
            <form method="post" action="{{route('languageChangeEn')}}">
              @csrf
              <button type="submit" class="btn btn-success language-btn">English</button>
            </form>

            </li>
          <?php }else{ ?>
            <li> 
            
            <form method="post" action="{{route('languageChangeBn')}}">
              @csrf
              <button type="submit" class="btn btn-success language-btn">Bangla</button>
            </form>

            </li>
          <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </section>
    <!--main nav------------------------------------------------------------------->
  <section class="main-nav">
    <nav class="navbar navbar-expand-lg navbar-light">
      <?php 
        $companyInfo=DB::table('company_setting')
                          ->where('status',1)
                          ->first();
        // echo $companyInfo->companyLogo;

      ?>
      <a class="navbar-brand allpadding" href="{{route('welcome')}}"> <img src="{{asset('admin/img/2019120213100756012.png')}}
" class="img-fluid" alt="logo"> </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav  font-weight-bold">
          <li class="nav-item active">
            <a class="nav-link" href="{{route('welcome')}}">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{route('showStore')}}">Store</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{route('showAboutUs')}}">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{route('showContactUs')}}">Contact Us</a>
          </li>
        </ul>
        <ul class="main-nav-right ml-auto">
           <?php 
            if (Session::get('userId')) {
          ?>
          <li>
            <div class="btn-group dropleft">
              <a class="btn dropdown-toggle" href="profile.html" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i> Profile
              </a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="{{route('userProfile')}}">Profile</a>
                <a class="dropdown-item" href="{{route('userLogout')}}">Logout</a>
              </div>
            </div>
          </li>
          <?php
            }else{
          ?>
          <li> <a href="{{route('showUserLogin')}}"> Login or  </a> </li>
          <li> <a href="{{route('showUserLogin')}}"> Register </a> </li>
          <?php 
            }
          ?>
        </ul>
      </div>
    </nav>