<!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                      <ul class="metismenu" id="side-menu">
                        <li>
                            <a href="javascript: void(0);">
                                <i class="fas fa-shipping-fast"></i>
                                <span> In-Side Order </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                               
                                <li>
                                    <a href="{{route('addInsideOrder')}}">Add Order</a>
                                </li>

                                <li>
                                  <span class="badge badge-success badge-pill float-right">
                                  <?php
                                    $userId=Session::get('orderUserId');
                                    $totalInsideCustomOrder=DB::table('add_inside_custom_order')
                                                                ->where('status','pending')
                                                                ->where('userId',$userId)
                                                                ->count();
                                    echo $totalInsideCustomOrder;
                                  ?>
                                  </span>
                                    <a href="{{route('showUserInsidePendingOrder')}}">Pending Order</a>
                                </li>
                                
                                <li>
                                  <span class="badge badge-success badge-pill float-right">
                                    <?php
                                      $userId=Session::get('orderUserId');
                                      $totalInsideCustomOrder=DB::table('add_inside_custom_order')
                                                                  ->where('status','processing')
                                                                  ->where('userId',$userId)
                                                                  ->count();
                                      echo $totalInsideCustomOrder;
                                    ?>
                                  </span>
                                  <a href="{{route('showUserInsideProcessingOrder')}}">Processing Order</a>
                                </li>

                                <li>
                                  <span class="badge badge-success badge-pill float-right">
                                  <?php
                                      $userId=Session::get('orderUserId');
                                      $totalInsideCustomOrder=DB::table('add_inside_custom_order')
                                                                  ->where('status','active')
                                                                  ->where('userId',$userId)
                                                                  ->count();
                                      echo $totalInsideCustomOrder;
                                    ?>
                                  </span>
                                    <a href="{{route('showUserInsideActiveOrder')}}">Active Order</a>
                                    
                                </li>

                                <li>
                                  <span class="badge badge-success badge-pill float-right">
                                  <?php
                                      $userId=Session::get('orderUserId');
                                      $totalInsideCustomOrder=DB::table('add_inside_custom_order')
                                                                  ->where('status','delivery')
                                                                  ->where('userId',$userId)
                                                                  ->count();
                                      echo $totalInsideCustomOrder;
                                    ?>
                                  </span>
                                    <a href="{{route('showUserInsideDeliveryOrder')}}">Delivery Order</a>
                                </li>

                                <li>
                                  <span class="badge badge-success badge-pill float-right">
                                  <?php
                                      $userId=Session::get('orderUserId');
                                      $totalInsideCustomOrder=DB::table('add_inside_custom_order')
                                                                  ->where('status','delivery')
                                                                  ->where('userId',$userId)
                                                                  ->count();
                                      echo $totalInsideCustomOrder;
                                    ?>
                                  </span>
                                    <a href="{{route('showUserInsideCancelOrder')}}">Cancel Order</a>
                                </li>
                            </ul>
                        </li>



                        <li>
                            <a href="javascript: void(0);">
                                <i class="fe-users"></i>
                                <span> Outside Order </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                              <li>
                                  <a href="outsite-shopping-address.html">Outside Shipping Address</a>
                              </li>
                              <li>
                                  <a href="outsideshow-address.html">Outside Show Address</a>
                              </li>
                              <li>
                                  <a href="outside-shopping-charge.html">Outsite Shipping Add</a>
                              </li>
                              <li>
                                  <a href="outside-show-shipping.html">Outside Show Shipping</a>
                              </li>
                            </ul>
                        </li>

                      </ul>
                    </div>
                    <!-- End Sidebar -->
                    <div class="clearfix"></div>
                </div>
                <!-- Sidebar -left -->
            </div>
            <!-- Left Sidebar End -->