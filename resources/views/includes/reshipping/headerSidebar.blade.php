<!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                      <ul class="metismenu" id="side-menu">
                        <li>
                            <a href="javascript: void(0);">
                                <i class="fas fa-shipping-fast"></i>
                                <span> In Side Shapping </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="{{route('addInsideShippingAddress')}}">Shipping Address</a>
                                </li>
                                <li>
                                    <a href="{{route('showInsideShippingAddress')}}">Show Address</a>
                                </li>
                                <li>
                                    <a href="{{route('addUserInsideShipping')}}">Add Shipping</a>
                                </li>

                                <li>
                                    <a href="{{route('showUserInsideShipping')}}">Show Shipping</a>
                                </li>
                                <!-- <li>
                                    <a href="apps-projects.html">Projects</a>
                                </li>
                                <li>
                                    <a href="apps-tickets.html">Tickets</a>
                                </li>
                                <li>
                                    <a href="apps-companies.html">Companies</a>
                                </li> -->
                            </ul>
                        </li>



                        <li>
                            <a href="javascript: void(0);">
                                <i class="fe-users"></i>
                                <span> Outside shipping </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                              <li>
                                  <a href="outsite-shopping-address.html">Outside Shipping Address</a>
                              </li>
                              <li>
                                  <a href="outsideshow-address.html">Outside Show Address</a>
                              </li>
                              <li>
                                  <a href="outside-shopping-charge.html">Outsite Shipping Add</a>
                              </li>
                              <li>
                                  <a href="outside-show-shipping.html">Outside Show Shipping</a>
                              </li>
                            </ul>
                        </li>

                      </ul>
                    </div>
                    <!-- End Sidebar -->
                    <div class="clearfix"></div>
                </div>
                <!-- Sidebar -left -->
            </div>
            <!-- Left Sidebar End -->