@extends('layouts.admin')

@section('title') Custom Pending Order @endsection

@section('content')
<form method="post" action="{{route('updateAdminPendingCustomOrderOutside')}}" enctype="multipart/form-data">
	@csrf
	<?php 
    $message=Session::get('message');
    if($message){
    ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php
                echo $message;
                Session::put('message','');
            ?>
        </div>
    <?php
    }
    ?>
<div class="row">
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-60"><font color="red">Customer Information</font></h4>
		                    <p class="mb-1"><b>Customer Name:</b> {{$pendingOrder->firstName}}</p>
		                    <p class="mb-0"><b>Customer Email:</b> {{$pendingOrder->email}}</p>
		                    <input type="hidden" name="customerEmail" value="{{$pendingOrder->email}}">
		                    <input type="hidden" name="orderNumber" value="{{$pendingOrder->orderNumber}}">
		                    <input type="hidden" name="id" value="{{$pendingOrder->id}}">
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Customer Phone:</b> {{$pendingOrder->countryCode.$pendingOrder->phone}}</p>
		            <p class="mb-0"><b>Customer Address:</b> {{$pendingOrder->address1}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-30"><font color="red">Order Information</font></h4>
		                    <p class="mb-1"><b>Address:</b> {{$pendingOrder->bill_address1}}</p>
		                    <p class="mb-1"><b>Country:</b> {{$pendingOrder->bill_country}}</p>
		                    <p class="mb-1"><b>State:</b> {{$pendingOrder->bill_state}}</p>
		                    <p class="mb-1"><b>City:</b> {{$pendingOrder->bill_city}}</p>
		                    <p class="mb-1"><b>Zip:</b> {{$pendingOrder->bill_zip}}</p>
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
                    <p class="mb-1"><b>Product Link:</b><a href="{{$pendingOrder->productLink}}" target="_blank">Click Here</a></p>
                    <p class="mb-1"><b>Product Name:</b> {{$pendingOrder->productName}}</p>
                    <p class="mb-1"><b>Product Quantity:</b> {{$pendingOrder->quantity}}</p>
                    <p class="mb-1"><b>Initial Paid:</b> {{$pendingOrder->initialPayment}}</p>
                    <p class="mb-1"><b>Order Date:</b> {{$pendingOrder->orderDate}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Order Number:</b> <input type="text" class="form-control" value="{{$pendingOrder->orderNumber}}" name="orderNumber" required placeholder="Order Number"/></p>
                    <p class="mb-1"><b>Product Name:</b> <input type="text" class="form-control" value="{{$pendingOrder->productName}}" name="productName"  /></p>
                    <p class="mb-1"><b>Quantity:</b> <input type="text" class="form-control" value="{{$pendingOrder->quantity}}" name="quantity"  /></p>
                    <p class="mb-1"><b>Product Link:</b> <input type="text" class="form-control" value="{{$pendingOrder->productLink}}" name="productLink"  /></p>
                    <p class="mb-1"><b>Product Description:</b></p>
                    <p class="mb-0">
                    	<textarea  class="form-control" name="productDescription">{{$pendingOrder->productDescription}}</textarea>
                    </p>
                    
                    
		        </div>

		        <div class="col-sm-6">
		            <p class="mb-1"><b>Package Id:</b> <input type="text" class="form-control" value="{{$pendingOrder->packageId}}" name="packageId"  /></p>
		            <p class="mb-1"><b>Package Size:</b> <input type="text" class="form-control" value="{{$pendingOrder->packageSize}}" name="packageSize"  /></p>
		            <p class="mb-1"><b>Service Cost:</b> <input type="text" class="form-control" value="{{$pendingOrder->serviceCost}}" name="serviceCost"  /></p>
		            <p class="mb-1"><b>Total Cost:</b> <input type="text" class="form-control" value="{{$pendingOrder->totalCost}}" name="totalCost"  /></p>

		            
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
    
	
	<div class="col-md-12">
		<h4>Product Image</h4>
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        
		        <div class="col-md-2">
                   <img  src="{{asset( $pendingOrder->userImage1 )}}" width="300" height="240" alt="product Image"> 
		        </div>
		        <div class="col-md-2">
		            <img  src="{{asset( $pendingOrder->userImage2 )}}" width="300" height="240" alt="product Image">
		        </div>
		        <div class="col-md-2">
                    <img  src="{{asset( $pendingOrder->userImage3 )}}" width="300" height="240" alt="product Image">
		        </div>
		        <div class="col-md-2">
		            <img  src="{{asset( $pendingOrder->userImage4 )}}" width="300" height="240" alt="product Image">
		        </div>
		        <div class="col-md-2">
                    <img  src="{{asset( $pendingOrder->userImage5 )}}" width="300" height="240" alt="product Image">
		        </div>
		        <div class="col-md-2">
                   
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>

	<div class="col-md-12">
		<h4>Admin Send Image To Customer</h4>
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        
		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Product Image</label>
					    <input type="file" name="adminImage1" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>
		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Product Image</label>
					    <input type="file" name="adminImage2" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>
		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Product Image</label>
					    <input type="file" name="adminImage3" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>

		        <div class="col-md-3">
                    <div class="form-group">
					    <label for="exampleFormControlFile1">Invoice Image</label>
					    <input type="file" name="invoiceImage" class="form-control-file" id="exampleFormControlFile1">
					</div> 
		        </div>
		        
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>

	<button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">Update Order</button>

</div>
</form>

@endsection