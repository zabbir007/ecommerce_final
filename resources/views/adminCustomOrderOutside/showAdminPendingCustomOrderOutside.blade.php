@extends('layouts.admin')

@section('title') Pending Order @endsection

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Custom Pending</h4>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">User Name</th>
                            <th class="text-center">Product Name</th>
                            <th class="text-center">Order Date</th>
                            <th class="text-center">Order Number</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($pendingOrder as $pending)
                        <tr>
                            <td class="text-center">{{$pending->firstName}}</td>
                            <td class="text-center">{{$pending->productName}}</td>
                            <td class="text-center">{{$pending->orderDate}}</td>
                            <td class="text-center">{{$pending->orderNumber}}</td>
                            <td class="text-center"><?php if($pending->status=='pending'){echo "Pending";}?></td>
                            <td class="text-center">
                                <a href="{{route('editAdminPendingCustomOrderOutside',[$pending->id])}}" target="_blank" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection