@extends('layouts.user')

@section('title') Ushopnship @endsection

@section('content')

    <div class="nav-2 allpadding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <div class="accordion-menu">
              <div id="accordion">
                <div class="card card-menu">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                    </h5>
                  </div>
                  <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body card-body-menu-popup">
                      <div class='cssmenuvv'>
                      <ul>
                         <li class='active'> <a href="{{route('welcome')}}"> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <div class="manu">
              <ul>
                <li>
                  <a href="{{route('userOrder')}}"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
            </div>
          </div>
        </div>
        </div>
      </div>
</section>

<!--Slider-------------------------------------------------->
<section class="slider mt-4 text-left">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img  src="{{asset('user/images/back.jpg')}}" alt="First slide">
          <div class="carousel-caption">
            <h2>Shop Authentic and <span> Geniune Products</span></h2>
            <p>Fine Picked Collection By Our experts</p>
            <a href="{{route('shop')}}"><button type="button" class="btn btn-danger w-12">Shop Now</button></a>
          </div>
        </div>
        <div class="carousel-item">
          <img  src="{{asset('user/images/for-Marketplace.jpg')}}" alt="Second slide">
          <div class="carousel-caption ">
            <h2>Shop Everything From Your <span>Favourite Marketplace</span> </h2>
            <p>Custom Order From Anywhere</p>
            <a href="{{route('showOrderPage')}}"><button type="button" class="btn btn-danger w-12">Order Now</button></a>
          </div>
        </div>
        <div class="carousel-item">
          <img  src="{{asset('user/images/for-Our-experts.jpg')}}" alt="Third slide">
          <div class="carousel-caption">
            <h2>Shop Globally Or Locally the way <span>you Like It</span> </h2>
            <p>Local And Intanational Shipping</p>
            <a href="{{route('showReshippingPage')}}">  <button type="button" class="btn btn-danger w-12">Start Now</button></a>
          </div>

        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
</section>

<section class="wrapper mt-5 allpadding">
  <div class="container-fluid">
    <?php 
      $language=Session::get('language');
      if ($language=='1') {
     ?>
    <h2 class="text-center"><span>আজ আমরা কীভাবে আপনাকে আশ্চর্য করতে পারি ?</span> </h2>
    <p class="text-center">এটি নিজের উপায়ে সম্পন্ন করুন।</p>
    <?php }else{?>
    <h2 class="text-center">How Can We Amaze You <span>Today ?</span> </h2>
    <p class="text-center">Get it done inyour way.</p>
    <?php } ?>
    <div class="row mt-5">
     <div class="col-md-3 col-sm-6 col-6">
       <div class="box1">
          <a href="{{route('shop')}}">
            <img src="{{asset('user/images/Shop.png')}}" class="img-fluid" alt="">
          </a>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-6">
       <div class="box1 box2">
          <a href="{{route('showOrderPage')}}">
            <img src="{{asset('user/images/Shop-From.png')}}" class="img-fluid" alt="">
          </a>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-6">
       <div class="box1 box3">
          <a href="{{route('showReshippingPage')}}">
            <img src="{{asset('user/images/ship-localy.png')}}"class="img-fluid" alt="">
          </a>
       </div>
       </div>
       <div class="col-md-3 col-sm-6 col-6">
         <div class="box1 box4">
            <a href="{{route('shop')}}">
              <img src="{{asset('user/images/Shipping-Globally.png')}}"class="img-fluid" alt="">
            </a>
         </div>
       </div>
   </div>
  </div>
</section>
<!---section 3----------------------------------->
<!---section 4----------------------------------->
<section class="client-slider mt-5">
  <div class="container">
    <div class="slideshow-container">
      <div class="quoteSlides autoplay">
        <img src="{{asset('user/images/bs-1.png')}}" alt="">
        <p class="author">- Richard S.</p>
        <p>Great experience buying a new car. Sean Liang was great. He helped me get the right vehicle at a reasonable price.</p>

      </div>
      <div class="quoteSlides autoplay">
        <img src="{{asset('user/images/bs-1.png')}}" alt="">
        <p class="author">- David M. </p>
        <p>This is just to say thank you very much. It was a pleasure shopping with you. Everything went smoothly and all my questions were answered promptly. I will definitely shop again!</p>

      </div>
      <div class="quoteSlides autoplay">
        <img src="{{asset('user/images/bs-1.png')}}" alt="">
        <p class="author">- David M. </p>
      <p>Great experience buying a new car. Sean Liang was great. He helped me get the right vehicle at a reasonable price.</p>
      </div>
    <a class="prev"  onclick="plusQuote(-1)">❮</a>
    <a class="next" onclick="plusQuote(1)">❯</a>

    <div class="dot-container">
      <div style="text-align:center">
      <span class="qtdot" onclick="currentQuote(1)"></span>
      <span class="qtdot" onclick="currentQuote(2)"></span>
      <span class="qtdot" onclick="currentQuote(3)"></span>
      </div>
    </div>
    </div>

  </div>
</section>
<!--hp-signup-------------------------------------->
<section class="hp-signup mt-5">
  <div class="container">
    <h2>Ready to Shop & Ship?</h2>
    <h5>All you need is a membership to instantly get your ushopnship address.</h5>
    <p>Helping local and international shoppers buy and ship what they love from anywhere in the world.</p><br>
     
  </div>
</section>

<!--Load More Button----------------------------------->

<script type="text/javascript">
$( document ).ready(function () {
$(".product-hidden").slice(0, 12).show();

  $("#loadMore").on('click', function (e) {
    e.preventDefault();
    $(".product-hidden:hidden").slice(0, 6).slideDown();
    if ($(".product-hidden:hidden").length == 0) {
      $("#loadMore").fadeOut('slow');
    }
  });
});
</script>

<script type="text/javascript">

    $(".addcart").click(function(){ 
        var counter = parseInt($("#hiddenVal").val());
        counter++;
        $("#hiddenVal").val(counter);
        $("#theCount").text(counter);
    });

</script>
@endsection

