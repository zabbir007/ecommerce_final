@extends('layouts.user')

@section('title') Profile @endsection

@section('content')

<div class="nav-2">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2">
          <div class="accordion-menu">
            <div id="accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                      <a href="" class="btn btn-link d-block" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down ml-5"></i></a>
                  </h5>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body card-body-menu">
                    <div class='cssmenuvv'>
                    <ul>
                       <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                      @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      
                       <li> <a href='#'> <span> About </span> </a>  </li>
                       <li class='last'> <a href='#'> <span> Contact </span></a> </li>
                    </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-6">
          <form method="post" action="{{route('searchProductByProductName')}}">
            @csrf
            <div class="search">
              <div class="opction">
                <select name="departmentId">
                  @foreach($departmentInfo as $department)
                  <option value="{{$department->id}}">{{$department->department_type}}</option>
                  @endforeach
                </select>
              </div>
              <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
              <button type="submit" class="searchButton">
               <i class="fa fa-search"></i>
              </button>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <div class="manu">
            <ul>
                <li>
                  <a href="#"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                <!-- <li>
                   <a href="#"> <i class="far fa-heart"></i> <span class="count"><?php //echo Cart::instance('wishlist')->count(); ?></span> </a>
                </li> -->
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
          </div>
        </div>
      </div>
      </div>
    </div>
</section>

<section class="user-profile">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="user-nav">
          <ul>
            <li> <a href="{{route('userOrder')}}">My Orders</a> </li>
          </ul>
          <ul>
            <li> <a href="#" >Manage My Account</a> </li>
            <ul>
              <li> <a href="{{route('userProfile')}}">Personal Information</a> </li>
              <li><a href="{{route('userBillingAddress')}}">Billing Address</a></li>
              <li><a href="{{route('userShippingAddress')}}">Shipping Address</a></li>
            </ul>
          </ul>
          <ul >
            <li> <a href="{{route('userSuiteAddress')}}" class="active">My Suite Address</a> </li>
          </ul>
          <ul>
            <li> <a href="{{route('userReshippingOrder')}}" >Re-Shipping Order</a> </li>
          </ul>
          <ul>
            <li> <a href="{{route('userCustomOrder')}}" >Custom Order</a> </li>
          </ul>
        </div>
      </div>

      <div class="col-md-9">
          <h4>Suite Address</h4>
        <div class="user-account">
            <table class="table">
            <thead>
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Phone </th>
                <th scope="col">Address 1</th>
                <th scope="col">Address 2</th>
                <th scope="col">Country</th>
                <th scope="col">City</th>
                <th scope="col">State</th>
                <th scope="col">Zip Code</th>
                <th scope="col">House && Road No</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{{$userInfo->firstName}} </td>
                <td>{{$suiteAddress->sweetPhone}}</td>
                <td>{{$suiteAddress->sweetAddress}}</td>
                <td>Suite- {{$userInfo->swiftNumber}}</td>
                <td>{{$suiteAddress->sweetCountry}}</td>
                <td>{{$suiteAddress->sweetCity}}</td>
                <td>{{$suiteAddress->sweetState}}</td>
                <td>{{$suiteAddress->sweetHouse}}</td>
                <td>{{$suiteAddress->sweetRoad}}</td>
              </tr>

              
            </tbody>
          </table>
        </div>
      </div>
      
    </div>
  </div>
</section>


<script type="text/javascript">
  $( document ).ready(function () {
  $(".product-hidden").slice(0, 12).show();

    $("#loadMore").on('click', function (e) {
      e.preventDefault();
      $(".product-hidden:hidden").slice(0, 6).slideDown();
      if ($(".product-hidden:hidden").length == 0) {
        $("#loadMore").fadeOut('slow');
      }
    });
  });
  </script>




@endsection