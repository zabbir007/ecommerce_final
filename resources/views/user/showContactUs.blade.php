@extends('layouts.user')

@section('title') E-Commerce @endsection

@section('content')
<div class="nav-2 allpadding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <div class="accordion-menu">
              <div id="accordion">
                <div class="card card-menu">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                    </h5>
                  </div>
                  <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body card-body-menu-popup">
                      <div class='cssmenuvv'>
                      <ul>
                         <li class='active'> <a href="{{route('welcome')}}"> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <div class="manu">
              <ul>
                <li>
                  <a href="{{route('userOrder')}}"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
            </div>
          </div>
        </div>
        </div>
      </div>
</section>
<!--Contact US------------------------------------------------------------------->
<section class="contact-address-area">
    <div class="container">
        <div class="sec-title-style1 text-center max-width">
            <div class="title">Contact Us</div>
            <div class="text"><div class="decor-left"><span></span></div><p>Contact with us</p><div class="decor-right"><span></span></div></div>
            <div class="bottom-text">
              <p>Fixyman is proud to be the name that nearly 1 million homeowners have trusted since 1996 for home improvement and repair, providing virtually any home repair.</p>
            </div>
        </div>
            <div class="contact-address-box row">
                <!--Start Single Contact Address Box-->
                <div class="col-sm-4 single-contact-address-box text-center">
                  <div class="icon-holder">
                    <span class="icon-clock-1">
                      <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span>
                    </span>
                  </div>
                    <h3>Lorem Ipsum</h3>
                    <h2>Lorem Ipsum is simply dummy</h2>
                </div>
                <!--End Single Contact Address Box-->
                <!--Start Single Contact Address Box-->
                <div class="col-sm-4 single-contact-address-box main-branch">
                    <h3>Full Address</h3>
                    <div class="inner">
                      <ul>
                        <li>
                          <div class="title">
                              <h4>Address:</h4>
                          </div>
                          <div class="text">
                              <br><p>48/2 <br>R. K Mission Road, Gopibag,<br> Dhaka- 1203   2nd floor</p>
                          </div>
                        </li>
                          <li>
                            <div class="title">
                                <h4>Ph & Email:</h4>
                            </div>
                            <div class="text">
                                <p>+880181167177 <br> hello@ushopnship.com</p>
                            </div>
                          </li>
                          <li>
                            <div class="title">
                                <h4>Office Hrs:</h4>
                            </div>
                            <div class="text">
                                <p>Mon-Fri: 9:30am - 6:30pm<br> Sat-Sun: Closed</p>
                            </div>
                          </li>
                      </ul>
                    </div>
                </div>
            <!--End Single Contact Address Box-->
            <!--Start Single Contact Address Box-->
            <div class="col-sm-4 single-contact-address-box text-center">
                <div class="icon-holder">
                    <span class="icon-question-2">
                      <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span>
                    </span>
                </div>
                <h3>Lorem Ipsum</h3>
                <h2>Lorem Ipsum is simply dummy</h2>
            </div>
            <!--End Single Contact Address Box-->
        </div>
        </div>
    </section>
<!--End Contact Address Area-->

<!--Start contact form area-->
<section class="contact-info-area allpadding">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-2">

        </div>
          <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
              <div class="contact-form">
                  <div class="row">
                    <div class="col-xl-12">
                      <div class="sec-title-style1 float-left">
                        <div class="title">Send Your Message</div>
                        <div class="text"><div class="decor-left"><span></span></div><p>Contact Form</p></div>
                      </div>
                        <div class="text-box float-right">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                        </div>
                    </div>
                  </div>
                  <div class="inner-box">
                      <form id="contact-form" name="contact_form" class="default-form" action="{{route('sendMessage')}}" method="post">
                        @csrf
                          <?php 
                            $message=Session::get('message');
                            if($message){
                          ?>
                              <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong><?php echo $message; Session::put('message',''); ?>
                              </div>
                            <?php
                              }
                            ?>
                          <div class="row">
                              <div class="col-xl-6 col-lg-12">
                                  <div class="row">
                                    <div class="col-xl-6">
                                      <div class="input-box">
                                        <input type="text" name="userName" value="" placeholder="Name" required="">
                                      </div>
                                    </div>
                                    <div class="col-xl-6">
                                      <div class="input-box">
                                        <input type="email" name="userEmail" value="" placeholder="Email" required="">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                     <div class="col-xl-12">
                                      <div class="input-box">
                                        <input type="text" name="mailSubject" value="" placeholder="Subject">
                                      </div>
                                    </div>
                                  </div>
                              </div>
                              <div class="col-xl-6 col-lg-12">
                                  <div class="input-box">
                                    <textarea name="message" placeholder="Your Message..." required="" ></textarea>
                                  </div>
                                  <div class="button-box">
                                      <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                      <button type="submit" data-loading-text="Please wait...">Send Message<span class="flaticon-next"></span></button>
                                  </div>
                              </div>
                          </div>
                      </form>
                  </div><!--inner box end------------->
              </div>
          </div>
          <div class="col-md-2">

         </div>
      </div>
  </div>
</section>

@endsection
