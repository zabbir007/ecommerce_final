@extends('layouts.user')

@section('title') Checkout @endsection

@section('content')  
<div class="nav-2 allpadding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <div class="accordion-menu">
              <div id="accordion">
                <div class="card card-menu">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                    </h5>
                  </div>
                  <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body card-body-menu-popup">
                      <div class='cssmenuvv'>
                      <ul>
                         <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <div class="manu">
              <ul>
                <li>
                  <a href="#"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
            </div>
          </div>
        </div>
        </div>
      </div>
</section>    
      <section class="checkout mt-5 allpadding">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-4">
              <div id="accordion">
                <div class="card  mb-5">
                  <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                      <div class="accordion-btn">
                        <a href="#"class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Have a coupon? Click here to enter your code</a>
                      </div>
                    </h5>
                  </div>
                  <div id="collapseTwo" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                      <label for="validationCustom04">If you have a coupon code, please apply it below.</label>
                      <input type="text" class="form-control" id="validationCustom04" placeholder="Coupon Code">


                      <div class="checkout-btn mt-2 mb-2">
                        <button class="btn btn-primary" type="submit">Coupon Apply</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <form class="needs-validation" novalidate method="post" action="{{route('saveEcommerceOrder')}}">
                @csrf
                <div class="form-row check-form">
                  <div class="col-md-12 mb-3">
                    <label for="validationCustom01" class="check-name">First name <span>*</span> </label>
                    <input type="text" class="form-control" id="validationCustom01" placeholder="First name" name="firstName" value="{{$userInfo->firstName}}"  required>
                    <div class="valid-feedback">
                      Looks good!
                    </div>
                  </div>
                  <div class="col-md-12 mb-3">
                    <label for="validationCustom02"class="check-name">Last name <span>*</span></label>
                    <input type="text" class="form-control" id="validationCustom02" value="{{$userInfo->lastName}}" name="lastName" placeholder="Last name"  required>
                    <div class="valid-feedback">
                      Looks good!
                    </div>
                  </div>
                  
                  <div class="col-md-12 mb-3">
                    <label for="validationCustom05"class="check-name">Phone  <span>*</span></label>
                    <input type="text" class="form-control" id="validationCustom05" placeholder="PHONE" value="{{$userInfo->phone}}" name="phone" required>
                  </div>
                  <div class="col-md-12 mb-3">
                    <label for="validationCustom05"class="check-name">Email<span>*</span></label>
                    <input type="text" class="form-control" id="validationCustom05" placeholder="EMAIL ADDRESS" value="{{$userInfo->email}}" name="email" required>
                  </div>

                  <div class="col-md-12 mb-3">
                    <label for="validationCustom04"class="check-name">Address</label>
                    <input type="text" class="form-control" id="autocomplete" onFocus="geolocate()" placeholder="Addreess" value="{{$userInfo->address1}}" name="address">
                    <input class="form-control" id="street_number" disabled="true" type="hidden">
                    <input class="form-control" id="route" disabled="true" type="hidden">
                  </div>
                  
                  <div class="col-md-12 mb-3">
                    <label for="validationCustom05"class="check-name">Country <span>*</span></label>
                    <input type="text" class="form-control" id="country" value="{{$userInfo->country}}" name="country" placeholder="Country Name" required>
                  </div>

                  <div class="col-md-12 mb-3">
                    <label for="validationCustom05"class="check-name">City <span>*</span></label>
                    <input type="text" class="form-control" name="city" id="locality" value="{{$userInfo->city}}" placeholder="CITY" required>
                  </div>

                  <div class="col-md-12 mb-3">
                    <label for="validationCustom05"class="check-name">State <span>*</span></label>
                    <input type="text" class="form-control" name="state" id="administrative_area_level_1" value="{{$userInfo->state}}" placeholder="State" required>
                  </div>

                  <div class="col-md-12 mb-3">
                    <label for="validationCustom05"class="check-name">Zip <span>*</span></label>
                    <input type="text" class="form-control" name="zip" id="postal_code" value="{{$userInfo->zip}}" placeholder="Zip" required>
                  </div>

                  <div class="col-md-12 mb-3">
                    <label>Shipping Address</label>
                      <textarea required class="form-control firstAddress" name="shippingAddress" id="autocomplete3">{{$userInfo->bill_address1.','.$userInfo->bill_country.','.$userInfo->bill_city.','.$userInfo->bill_state.','.$userInfo->bill_zip}}</textarea>
                  </div>


                  <div class="col-md-12 mb-3">
                    <label>Billing Address</label>
                      <textarea required class="form-control secondAddress" name="billingAddress" id="autocomplete3">{{$userInfo->address1.','.$userInfo->country.','.$userInfo->city.','.$userInfo->state.','.$userInfo->zip}}</textarea>
                  </div>


                </div>

            </div>
            <div class="col-md-4">
            
            <?php 

              $cartValue=Cart::count();

              if ($cartValue != '0') {
                
             ?>
            <div class="Yorder">
              <table>
                <tr>
                  <th colspan="2">Your order</th>
                </tr>
                <?php
                  $contents=Cart::content();
                  $productTotal=0;
                ?>

                @foreach($contents as $content)
                <tr>
                  <td>{{$content->name}} x {{$content->qty}}(Qty)</td>
                  <td>{{$content->subtotal}}</td>
                  <?php $productTotal += $content->subtotal; ?>
                  <input type="hidden" name="productName[]" value="{{$content->name}}">
                  <input type="hidden" name="productQun[]" value="{{$content->qty}}">
                  <input type="hidden" name="productPrice[]" value="{{$content->price}}">
                </tr>
                @endforeach 
                <tr>
                  <td>Shipping Cost</td>
                  <td>
                    <?php 
                      if ($shippingCost->type=='free') {
                        $shippingAmount=0;
                        echo "Free Shipping";
                      }else{
                        $shippingAmount=$shippingCost->amount;
                        echo $shippingCost->amount;
                      }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td>Total Amount</td>
                  <td><?php echo $productTotal+$shippingAmount;?></td>
                  <input type="hidden" name="totalAmount" value="<?php echo $productTotal+$shippingAmount;?>">
                  <input type="hidden" name="shippingAmount" value="<?php echo $shippingAmount;?>">
                </tr>

              </table><br>
                <div class="checkout-btn mt-2 mb-2">
                  <button class="btn btn-primary" type="submit">Place order</button>
                  
                </div>
                
              </div>
            <?php } ?>

            </form>
            </div><!-- Yorder -->
            </div>

          </div>

        </div>
      </section>


<script type="text/javascript">
    var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);

        //place suggest second box
        new google.maps.places.Autocomplete(
          (document.getElementById('autocomplete2')),
          {types: ['geocode']}
        );

        new google.maps.places.Autocomplete(
          (document.getElementById('autocomplete3')),
          {types: ['geocode']}
        );

        new google.maps.places.Autocomplete(
          (document.getElementById('autocomplete4')),
          {types: ['geocode']}
        );

      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
</script>
@endsection