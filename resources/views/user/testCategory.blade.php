<!-- <ul>
   <li class='active'><a href='index.html'><span>Home</span></a></li>
  @foreach($departmentInfo as $department)
  <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
      <ul>
        <?php 
          $subCategory=DB::table('sub_category as subc')
                            ->join('category as cat','subc.category_id','=','cat.id')
                            ->where('subc.department_id',$department->id)
                            ->get();
        ?>
        @foreach($subCategory as $sub)
         <li class='has-sub'><a href='#'><span>{{$sub->category_type}}</span></a>
            <ul>
               <li><a href='#'><span>{{$sub->subCategory_type}}</span></a></li>
            </ul>
         </li>
        @endforeach
      </ul>
   </li>
  @endforeach
   <li><a href='#'><span>About</span></a></li>
   <li class='last'><a href='#'><span>Contact</span></a></li>
</ul> -->


<ul>
   <li class='active'><a href='index.html'><span>Home</span></a></li>
  @foreach($departmentInfo as $department)
  <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
      <ul>
        @foreach($categoryInfo as $category)
         <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
            <ul>
              <?php 
                $subCat=DB::table('sub_category')
                          ->where('sub_category.department_id',$department->id)
                          ->where('sub_category.category_id',$category->id)
                          ->get();
              ?>
              @foreach($subCat as $sub)
               <li><a href='#'><span>{{$sub->subCategory_type}}</span></a></li>
               @endforeach
            </ul>
         </li>
        @endforeach
      </ul>
   </li>
  @endforeach
   <li><a href='#'><span>About</span></a></li>
   <li class='last'><a href='#'><span>Contact</span></a></li>
</ul>