@extends('layouts.user')

@section('title') E-Commerce @endsection

@section('content')
<link rel="stylesheet" href="{{asset('user/css/loginfrom.css')}}">
<div class="nav-2 allpadding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <div class="accordion-menu">
              <div id="accordion">
                <div class="card card-menu">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                    </h5>
                  </div>
                  <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body card-body-menu-popup">
                      <div class='cssmenuvv'>
                      <ul>
                         <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <div class="manu">
              <ul>
                <li>
                  <a href="#"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
            </div>
          </div>
        </div>
        </div>
      </div>
</section>

<div class="top-content">
    <div class="inner-bg">
        <div class="container">
            
            <?php
              if ($first=='1') {
            ?>
            <div class="row">
                <div class="col-sm-5">
                  <div class="form-box">
                    <div class="form-top">
                      <div class="form-top-left">
                        <h3>Forget Your Password</h3>
                          <p>Enter Your Email:</p>
                      </div>
                      <div class="form-top-right">
                        <i class="fa fa-key"></i>
                      </div>
                      </div>
                      <div class="form-bottom">
                    <form role="form" action="{{route('sendForgetVerifyCode')}}" method="post" class="login-form">
                      @csrf
                      <?php 
                        $message=Session::get('emailMatchError');
                        if($message){
                        ?>
                            <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                <?php
                                    echo $message;
                                    Session::put('emailMatchError','');
                                ?>
                            </div>
                        <?php   
                        }
                        ?>
                      <div class="form-group">
                        <label class="sr-only" for="form-username">Email</label>
                          <input type="text" name="userEmail" placeholder="Your Email" class="form-username form-control" id="form-username">
                        </div>
                        <!-- <a href="#" class="text-decoration-none"> <input type="submit" class="log-btn" name="" value="Log In"> </a> -->
                        <button type="submit" class="btn btn-danger mb-3">Verify</button>
                    </form>

                  </div>
                </div>
                </div>
            </div>
          <?php } ?>

            <?php
              if ($second=='1') {
            ?>

           <!--Secend--------------------------->
            <div class="row">
                <div class="col-sm-5">
                  <div class="form-box">
                    <div class="form-top">
                      <div class="form-top-left">
                        <h3>Login to our site</h3>
                          <p>Enter username and password to log on:</p>
                      </div>
                      <div class="form-top-right">
                        <i class="fa fa-key"></i>
                      </div>
                      </div>
                      <div class="form-bottom">
                    <form role="form" action="{{route('checkVerify')}}" method="post" class="login-form">
                      @csrf
                      <?php 
                        $message=Session::get('errorMes');
                        if($message){
                        ?>
                            <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                <?php
                                    echo $message;
                                    Session::put('errorMes','');
                                ?>
                            </div>
                        <?php   
                        }
                        ?>
                      <div class="form-group">
                        <!-- <label class="sr-only" for="form-username">Email</label> -->
                          <input type="text" readonly="" name="userEmail" placeholder="Email..." class="form-username form-control" id="form-username" value="<?php echo $email; ?>">
                        </div>
                        <div class="form-group">
                          <!-- <label class="sr-only" for="form-password">Password</label> -->
                          <input type="number" name="verifyCode" placeholder="verify code..." class="form-password form-control" id="form-password">
                        </div>
                          <div class="row">
                            <div class="col-md-6">
                              
                            </div>
                            <div class="col-md-6">
                              <button type="submit" class="btn btn-danger mb-3">Next</button>
                            </div>
                          </div>
                    </form>

                  </div>
                </div>
                </div>
            </div>

          <?php } ?>


          <?php
              if ($third=='1') {
            ?>

           <div class="row">
              <div class="col-sm-5">
                <div class="form-box">
                  <div class="form-top">
                    <div class="form-top-left">
                      <h3>Login to our site</h3>
                        <p>Enter username and password to log on:</p>
                    </div>
                    <div class="form-top-right">
                      <i class="fa fa-key"></i>
                    </div>
                    </div>
                    <div class="form-bottom">
                  <form role="form" action="{{route('savePassword')}}" method="post" class="login-form">
                    @csrf
                    <?php 
                      $message=Session::get('errorPass');
                      if($message){
                      ?>
                          <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              <?php
                                  echo $message;
                                  Session::put('errorPass','');
                              ?>
                          </div>
                      <?php   
                      }
                      ?>
                    <div class="form-group">
                        <input type="text" name="password" placeholder="Password..." class="form-username form-control" id="form-username" required="">
                        <input type="hidden" name="userEmail" value="{{$email}}">
                      </div>
                      <div class="form-group">
                          <input type="text" name="conPassword" placeholder="Retype password..." class="form-username form-control" id="form-username" required="">
                        </div>
                      <button type="submit" class="btn btn-danger mb-3">Send</button>
                  </form>

                </div>
              </div>
              </div>
            </div>

          <?php } ?>


            
        </div>
    </div>
</div>


@endsection