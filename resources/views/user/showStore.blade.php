@extends('layouts.user')

@section('title') Store @endsection

@section('content')
<div class="nav-2 allpadding">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2">
          <div class="accordion-menu">
            <div id="accordion">
              <div class="card card-menu">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                      <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                  </h5>
                </div>
                <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body card-body-menu-popup">
                    <div class='cssmenuvv'>
                    <ul>
                         <li class='active'> <a href="{{route('welcome')}}"> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
        </div>
        <div class="col-md-4">
          <div class="manu">
            <ul>
                <li>
                  <a href="{{route('userOrder')}}"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                <!-- <li>
                   <a href="#"> <i class="far fa-heart"></i> <span class="count"><?php //echo Cart::instance('wishlist')->count(); ?></span> </a>
                </li> -->
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
          </div>
        </div>
      </div>
      </div>
    </div>
</section>
<!--Category AREA------------------------------------------------------------------->
<section class="category-area allpadding">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-2">
        <h4 class="mt-3 text-dark">Set Your Budget</h4>
        <div class="category-border mb-5"></div>
        <div class="row">
           <div class="col-sm-12">
             <div id="slider-range"></div>
           </div>
         </div>
         <button type="submit" class="btn btn-danger mt-3">Filter</button>
         <div class="row slider-labels ml-5">
           <div class="col-xs-6 caption">
             <strong>Price:</strong> <span id="slider-range-value1"></span>
           </div>
           <div class="col-xs-6 text-right caption">
             <strong> — </strong> <span id="slider-range-value2"></span>
           </div>
         </div>
         <div class="row">
           <div class="col-sm-12">
             <form>
               <input type="hidden" name="min-value" id="slider-range-value1" value="">
               <input type="hidden" name="max-value" id="slider-range-value2" value="">
             </form>
           </div>
         </div>

        <div class="category">
          <h2 class="mt-3">Select Department</h2>
          <div class="category-border"></div>
			<?php if (isset($issetDepartment)) 
			{
				// echo $issetDepartment;
				// exit();
			?>
			    @foreach($departmentInfo as $department)
		          <div class="center">
		            <label> <a href="{{route('showDepartmentStore',[$department->id])}}"><input type="checkbox" onclick="window.location='{{route('showDepartmentStore',[$department->id])}}';" <?php if($department->id==$issetDepartment){echo "checked";} ?>  value=""> </a>   {{$department->department_type}} <span> </span> </label>
		          </div>
	            @endforeach
			<?php 
			}else{
			?>
           @foreach($departmentInfo as $department)
	          <div class="center">
	            <label> <a href="{{route('showDepartmentStore',[$department->id])}}"><input type="checkbox" onclick="window.location='{{route('showDepartmentStore',[$department->id])}}';" value=""> </a>   {{$department->department_type}} <span> </span> </label>
	          </div>
          @endforeach 

      	  <?php } ?>
            
        </div>

        <div class="category mt-3">
          <h2>Select Category</h2>
          <div class="category-border"></div>
          

          <?php if (isset($issetCategory)) 
			{
				// echo $issetDepartment;
				// exit();
			?>
			     @foreach($categoryInfo as $category)
		          <div class="center">
		            <label> <a href="{{route('showCategoryStore',[$category->id])}}"><input type="checkbox" onclick="window.location='{{route('showCategoryStore',[$category->id])}}';" <?php if($category->id==$issetCategory){echo "checked";} ?>  value=""> </a>   {{$category->category_type}} <span> </span> </label>
		          </div>
	            @endforeach
			<?php 
			}else{
			?>
            @foreach($categoryInfo as $category)
	          <div class="center">
	            <label> <a href="{{route('showCategoryStore',[$category->id])}}"><input type="checkbox" onclick="window.location='{{route('showCategoryStore',[$category->id])}}';" value=""> </a>   {{$category->category_type}} <span> </span> </label>
	          </div>
          @endforeach 

      	  <?php } ?>
            
        </div>
      </div>
      <div class="col-md-10">
        <!--PRODUCT------------------------------------------------------------------->
        <div class="product mt-5">
          <div class="container-fluid">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb brekcome">
                <li class="breadcrumb-item"><a href="#">Home </a></li>
                <li class="breadcrumb-item"><a href="#">Store</a></li>
                <?php if (isset($issetDepartment)) 
                {
                ?>
                <li class="breadcrumb-item active" aria-current="page">{{$departmentBread->department_type}}</li>
              <?php }elseif (isset($issetCategory)) {
              ?>
              <li class="breadcrumb-item active" aria-current="page">{{$categoryBread->category_type}}</li>
            <?php }elseif (isset($issetShowStore)) { ?>
              <li class="breadcrumb-item active" aria-current="page">All Product</li>
            <?php }elseif (isset($issetSearchSidebar)) { ?>
              <li class="breadcrumb-item active" aria-current="page">{{$bradeInfo->department_type}}</li>
              <li class="breadcrumb-item active" aria-current="page">{{$bradeInfo->category_type}}</li>
              <li class="breadcrumb-item active" aria-current="page">{{$bradeInfo->subCategory_type}}</li>
            <?php }elseif (isset($issetSearchName)) { ?>
              <li class="breadcrumb-item active" aria-current="page">{{$breadeInfo->department_type}}</li>
              <li class="breadcrumb-item active" aria-current="page">{{$productName}}</li>
            <?php } ?>
              </ol>
            </nav>
            <div class="title-box">
              <h4>LATEST ARRIVALS</h4>
            </div>

            <div class="row mt-2">


              @foreach($productInfo as $onsale)
	           <div class="col-md-2 col-sm-6 col-6">
	             <div class="p-grid product-hidden">
	                 <div class="product-image">
	                     <a href="{{route('singleProduct',[$onsale->id])}}"class="text-decoration-none">
	                         <img class="pic-1" src="{{asset( $onsale->image1 )}}">
	                         <img class="pic-2" src="{{asset( $onsale->image2 )}}">
	                     </a>
	                      <?php
	                        $checkDiscount=$onsale->discount;
	                        if ($checkDiscount!='') {
	                      ?>
	                     <span class="product-discount-label">-{{$onsale->discount}}%</span>
	                     <?php
	                      }else{
	                     ?>
	                     <span class="product-discount-label">-0%</span>
	                     <?php } ?>
	                     <ul class="social">
	                       <li>
	                         <button type="button"class="add-to-Wishlist" name="button"data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></button>
	                       </li>
	                       <li>
	                       <button type="button" class="add-to-Wishlist addcart" data-id="{{$onsale->id}}" name="button" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></button>
	                       </li>
	                     </ul>
	                 </div>
	                 <div class="product-content">
	                     <h3 class="title">
	                         <a href="{{route('singleProduct',[$onsale->id])}}"class="text-decoration-none">{{$onsale->productName}}</a>
	                     </h3>
	                     <span class="product-category">
	                         <a href="{{route('singleProduct',[$onsale->id])}}"class="text-decoration-none">{{$onsale->department_type.' - '.$onsale->category_type}}</a>
	                     </span>
	                     <?php
	                        $checkDiscount=$onsale->discount;
	                        if ($checkDiscount=='') {
	                      ?>
	                     <div class="price ">{{$onsale->currency_prefix.' '.$onsale->price}}</div>
	                      <?php }else{ ?>
	                     <div class="price">{{$onsale->currency_prefix.' '.$onsale->discountPrice}}
	                          <span>{{$onsale->currency_prefix.' '.$onsale->price}}</span>
	                      </div>
	                      <?php } ?>
	                     <button type="button" class="add-to-cart addcart" data-id="{{$onsale->id}}" name="button">ADD TO CART</button>
	                 </div>
	             </div>
	           </div>
	           @endforeach
              

         </div><!--row End------->
          </div>
          <div id="loadMore" class="mt-4 text-center">
            <button type="button" class="btn btn-danger">Load More</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--caregory wraper------------------------------------------->
<section class="category-wrapper">

</section>



<!--scrollup------------------------------------------------------------------->
<section class="scroll-top">
   <a href="#" class="scrollup"><i class="fas fa-chevron-up"></i></a>
</section>
<script src="{{asset('user/js/pricereange.js')}}"></script>
 <script type="text/javascript">
  $( document ).ready(function () {
  $(".product-hidden").slice(0, 12).show();

    $("#loadMore").on('click', function (e) {
      e.preventDefault();
      $(".product-hidden:hidden").slice(0, 6).slideDown();
      if ($(".product-hidden:hidden").length == 0) {
        $("#loadMore").fadeOut('slow');
      }
    });
  });
  </script>

  <script type="text/javascript">

    $(".addcart").click(function(){ 
        var counter = parseInt($("#hiddenVal").val());
        counter++;
        $("#hiddenVal").val(counter);
        $("#theCount").text(counter);
    });

</script>
@endsection