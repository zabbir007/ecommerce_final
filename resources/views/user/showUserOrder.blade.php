@extends('layouts.user')

@section('title') Profile @endsection

@section('content')

<div class="nav-2">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2">
          <div class="accordion-menu">
            <div id="accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                      <a href="" class="btn btn-link d-block" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down ml-5"></i></a>
                  </h5>
                </div>
                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body card-body-menu">
                    <div class='cssmenuvv'>
                    <ul>
                       <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                      @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      
                       <li> <a href='#'> <span> About </span> </a>  </li>
                       <li class='last'> <a href='#'> <span> Contact </span></a> </li>
                    </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-6">
          <form method="post" action="{{route('searchProductByProductName')}}">
            @csrf
            <div class="search">
              <div class="opction">
                <select name="departmentId">
                  @foreach($departmentInfo as $department)
                  <option value="{{$department->id}}">{{$department->department_type}}</option>
                  @endforeach
                </select>
              </div>
              <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
              <button type="submit" class="searchButton">
               <i class="fa fa-search"></i>
              </button>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <div class="manu">
            <ul>
                <li>
                  <a href="#"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                <!-- <li>
                   <a href="#"> <i class="far fa-heart"></i> <span class="count"><?php //echo Cart::instance('wishlist')->count(); ?></span> </a>
                </li> -->
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
          </div>
        </div>
      </div>
      </div>
    </div>
</section>
<!--Category AREA------------------------------------------------------------------->

            
      <!--user-profile------------------------------------------------->
      <section class="user-profile">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <div class="user-nav">
                <ul>
                  <li> <a href="{{route('userOrder')}}" class="active">My Orders</a> </li>
                </ul>
                <ul>
                  <li> <a href="#" >Manage My Account</a> </li>
                  <ul>
                    <li> <a href="{{route('userProfile')}}">Personal Information</a> </li>
                    <li><a href="{{route('userBillingAddress')}}">Billing Address</a></li>
                    <li><a href="{{route('userShippingAddress')}}">Shipping Address</a></li>
                  </ul>
                </ul>
                <ul >
                  <li> <a href="{{route('userSuiteAddress')}}" >My Swift Address</a> </li>
                </ul>
                <ul>
                  <li> <a href="{{route('userReshippingOrder')}}" >Re-Shipping Order</a> </li>
                </ul>
                <ul>
                  <li> <a href="{{route('userCustomOrder')}}" >Custom Order</a> </li>
                </ul>
              </div>
            </div>  

              <div class="col-md-8">
                <p>Ecommerce Information</p>
                <div class="user-account swift-table">

                    <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">Order Number</th>
                        <th scope="col">Paid Status</th>
                        <th scope="col">Order Status</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($orderInfo as $order)
                        <tr>
                          <th scope="row">{{$order->orderNumber}}</th>
                          <td><?php if($order->paidStatus=='0'){echo "UnPaid";}else{echo "Paid";} ?></td>
                          <td>{{$order->orderStatus}}</td>
                          <td>
                            <a href="{{route('viewEcommerceOrder',[$order->id])}}" class="action-icon"> <i class="fas fa-eye"></i></a>
                          </td>
                        </tr>
                      @endforeach                      
                    </tbody>
                  </table>
                </div>
                
                
              </div>

            </div>
          </div>
        </section>



<script type="text/javascript">
  $( document ).ready(function () {
  $(".product-hidden").slice(0, 12).show();

    $("#loadMore").on('click', function (e) {
      e.preventDefault();
      $(".product-hidden:hidden").slice(0, 6).slideDown();
      if ($(".product-hidden:hidden").length == 0) {
        $("#loadMore").fadeOut('slow');
      }
    });
  });
  </script>




@endsection