@extends('layouts.user')

@section('title') Custom Order @endsection

@section('content')

<div class="nav-2">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2">
          <div class="accordion-menu">
            <div id="accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                      <a href="" class="btn btn-link d-block" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down ml-5"></i></a>
                  </h5>
                </div>
                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body card-body-menu">
                    <div class='cssmenuvv'>
                    <ul>
                       <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                      @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      
                       <li> <a href='#'> <span> About </span> </a>  </li>
                       <li class='last'> <a href='#'> <span> Contact </span></a> </li>
                    </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-6">
          <form method="post" action="{{route('searchProductByProductName')}}">
            @csrf
            <div class="search">
              <div class="opction">
                <select name="departmentId">
                  @foreach($departmentInfo as $department)
                  <option value="{{$department->id}}">{{$department->department_type}}</option>
                  @endforeach
                </select>
              </div>
              <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
              <button type="submit" class="searchButton">
               <i class="fa fa-search"></i>
              </button>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <div class="manu">
            <ul>
                <li>
                  <a href="#"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                <!-- <li>
                   <a href="#"> <i class="far fa-heart"></i> <span class="count"><?php //echo Cart::instance('wishlist')->count(); ?></span> </a>
                </li> -->
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
          </div>
        </div>
      </div>
      </div>
    </div>
</section>
<!--Category AREA------------------------------------------------------------------->

<!--user-profile------------------------------------------------->
      <section class="user-profile">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2">
              <div class="user-nav">
                <ul>
                  <li> <a href="{{route('userOrder')}}">My Orders</a> </li>
                </ul>
                <ul>
                  <li> <a href="#" >Manage My Account</a> </li>
                  <ul>
                    <li> <a href="{{route('userProfile')}}">Personal Information</a> </li>
                    <li><a href="{{route('userBillingAddress')}}">Billing Address</a></li>
                    <li><a href="{{route('userShippingAddress')}}">Shipping Address</a></li>
                  </ul>
                </ul>
                <ul >
                  <li> <a href="{{route('userSuiteAddress')}}" >My Swift Address</a> </li>
                </ul>
                <ul >
                  <li> <a href="{{route('userReshippingOrder')}}">Re-Shipping Order</a> </li>
                </ul>
                <ul>
                  <li> <a href="{{route('userCustomOrder')}}" class="active">Custom Order</a> </li>
                </ul>
              </div>
            </div>  

              <div class="col-md-10">
                <p>Custom Order Information</p>
                <div class="row">
                  <h4>Inside Order</h4>
                @foreach($customOrderInfo as $custom)
               
                  <div class="col-md-12 mb-5">
                    <div class=" table-responsive  bg-white">
                      
                        <table class="table ">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">Order Number</th>
                            <th scope="col">Order Date</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Product Link</th>
                            <th scope="col">Product Description</th>
                            <th scope="col">Estimate Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Package Size</th>
                            <th scope="col">Shipping Address</th>
                            <th scope="col">Shipping Type</th>
                            <th scope="col">Order Status</th>
              							<th scope="col">Order Image</th>
              							<?php if($custom->orderStatus=='preprocessing'){ ?>
              								<th scope="col">Pay</th>
              							<?php } ?>

                            <?php if($custom->orderStatus=='preprocessing'){ ?>
                              <th scope="col">Cancel</th>
                            <?php } ?>

              							<?php if($custom->orderStatus=='userprocessing'){ ?>
              								<th scope="col">Complete Order</th>
              							<?php } ?>
                          </tr>
                        </thead>
                        <tbody>
                          
                            <tr>
                              <th scope="row">{{$custom->orderNumber}}</th>
                              <td>{{$custom->orderDate}}</td>
                              <td>{{$custom->productName}}</td>
                              <td><a href="{{$custom->productLink}}" target="_blank">View</a> </td>
                             <!--  <td>{{$custom->productDescription}}</td> -->
                              <td>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleMod{{$custom->id}}">
                                  View
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleMod{{$custom->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Product Description</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                       {{$custom->productDescription}}
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td>{{$custom->productEstimatePrice}}</td>
                              <td>{{$custom->productQuantity}}</td>
                              <td>{{$custom->packageSize}}</td>
                              <!-- <td>{{$custom->bill_address1 .' ,'.$custom->bill_address2.', '.$custom->bill_country.', '.$custom->bill_city.' ,'.$custom->bill_state.' ,'.$custom->bill_zip}}</td> -->
                              <td>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModa{{$custom->id}}">
                                  View
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModa{{$custom->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Shipping Address</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                       {{$custom->bill_address1 .' ,'.$custom->bill_address2.', '.$custom->bill_country.', '.$custom->bill_city.' ,'.$custom->bill_state.' ,'.$custom->bill_zip}}
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td>{{$custom->shippingType}}</td>
                              
                              <td>{{$custom->orderStatus}}</td>
                              <td><a target="_blank" href="{{route('showUserCustomOrderImage',[$custom->id])}}" class="action-icon">
                                <button type="button" class="btn btn-info"><?php if($custom->orderStatus=='userprocessing'){echo "Image";}else{echo "Show";} ?></button></a>
                              <?php if($custom->orderStatus=='preprocessing'){ ?>
                              	<td>
                  								<form method="post" action="{{route('paySecondPayment')}}">
                    									@csrf
                    									<input type="hidden" name="id" value="{{$custom->id}}">
                                  	<button type="submit" class="btn btn-success">Pay</button>
                                  </form>
                              	</td>
							                <?php } ?>
                              <?php if($custom->orderStatus=='preprocessing'){ ?>
                                <td>
                                  <form method="post" action="{{route('cancelCustomOrder')}}">
                                      @csrf
                                      <input type="hidden" name="id" value="{{$custom->id}}">
                                    <button type="submit" class="btn btn-danger float-right">Cancel</button>
                                  </form>
                                </td>
                              <?php } ?>
              							  <?php if($custom->orderStatus=='userprocessing'){ ?>
              							  	<td>
              							  		<a target="_blank" href="{{route('editCustomerPreProcessingCustomOrder',[$custom->id])}}" class="action-icon">
                                              <button type="button" class="btn btn-success">Complete</button></a>
              							  	</td>
              							  <?php } ?>
                            </tr>
                                           
                        </tbody>
                      </table>
					         <?php if($custom->orderStatus=='adminprocessing'){ ?>
	                    <h5 class="ml-3"><span class="statusShow">We Will reach your order within 48 hours.</span></h5>
	                  <?php } ?>

	                  <?php if($custom->orderStatus=='processing' || $custom->orderStatus=='waitingpayment' || $custom->orderStatus=='active' || $custom->orderStatus=='complete') { ?>
                      <table class="table">
                        <thead class="bg-info">
                          <tr>
                            <th scope="col">Order Number</th>
                            <th scope="col">Submit Date</th>
                            <th>Product Cost</th>
                            <th>Insurance Cost</th>
                            <th>Option Cost</th>
                            <th>Shipping Cost</th>
                            <th>Commition</th>
                            <th>Total Cost</th>
                            <th>Offer</th>
                            <th>Tracking </th>
                            <?php if($custom->orderStatus=='waitingpayment'){ ?>
                             <th>Pay </th>
                            <?php } ?> 
                            <?php if($custom->orderStatus=='waitingpayment'){ ?>
                             <th>Cancel </th>
                            <?php } ?>
                             <th>Invoice</th>
                          </tr>
                        </thead>
                          <tbody>
                            <tr>
                              <td>{{$custom->orderNumber}}</td>
                              <td>{{$custom->submitDate}}</td>
                              <td>{{$custom->adminProductFinalAmount}}</td>
                              <td>{{$custom->finalInsuranceAmount}}</td>
                              <td>{{$custom->finalTotalAmount}}</td>
                              <td>{{$custom->adminShippingPrice}}</td>
                              <td>{{$custom->adminCommitionPrice}}</td>
                              <td>{{$custom->adminSetProductFinalAmount}}</td>
                              <td>{{$custom->adminOfferPrice}}</td>
                              <td>{{$custom->trackingNumber}}</td>
                             
                              <?php if($custom->orderStatus=='waitingpayment'){ ?>
                                
                               <td>
                                <form method="post" action="{{route('updateCustomerWaitingPaymentCustomOrder')}}">
                                  @csrf
                                
                                <input type="hidden" name="id" value="{{$custom->id}}">
                                <button type="submit" class="btn btn-warning">Pay Now</button>
                                 </form>
                                </td>
                              
                               <?php } ?>

                               <?php if($custom->orderStatus=='waitingpayment'){ ?>
                                <td>
                                  <form method="post" action="{{route('cancelCustomOrder')}}">
                                      @csrf
                                      <input type="hidden" name="id" value="{{$custom->id}}">
                                    <button type="submit" class="btn btn-danger float-right">Cancel</button>
                                  </form>
                                </td>
                              <?php } ?>
                               <td><a target="_blank" href="{{route('showUserCustomOrderImage',[$custom->id])}}" class="action-icon">
                                <button type="button" class="btn btn-success">Show</button>
                               </a></td>
                            </tr>
                          </tbody>
                      </table>
                    <?php } ?>
                    </div>
                     
                  </div>

                  @endforeach

                  <h4>Outside Order</h4>
                  <div class="col-md-12 mb-5">
                    <div class=" table-responsive  bg-white">
                      @foreach($customOrderOutsideInfo as $customOrder)
                      <table class="table ">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">Order Number</th>
                            <th scope="col">Order Date</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Product Link</th>
                            <th scope="col">Product Description</th>
                            <th scope="col">Package Id</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Size</th>
                            <th scope="col">Shipping Address</th>
                            <th scope="col">Total Cost</th>
                            <th scope="col">Service Cost</th>
                            <th scope="col">Status</th>
                            <th scope="col">Show Image</th>
                            <?php if($customOrder->status=='waitingpayment'){?>
                            <th scope="col">Pay</th>
                          <?php } ?>
                          <?php if($customOrder->status=='waitingShippingPayment'){?>
                            <th scope="col">Pay</th>
                          <?php } ?>
                          </tr>
                        </thead>
                         <tbody>
                            <tr>
                              <td scope="row">{{$customOrder->orderNumber}}</td>
                              <td scope="row">{{$customOrder->orderDate}}</td>
                              <td scope="row">{{$customOrder->productName}}</td>
                              <td><a href="{{$customOrder->productLink}}" target="_blank">View</a> </td>
                              <td>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter{{$custom->id}}">
                                  View
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalCenter{{$custom->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Product Description</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                       {{$custom->productDescription}}
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td scope="row">{{$customOrder->packageId}}</td>
                              <td scope="row">{{$customOrder->quantity}}</td>
                              <td scope="row">{{$customOrder->packageSize}}</td>
                              <!-- <td>{{'Address: '.$customOrder->bill_address1 .' , Country: ' .$customOrder->bill_country.' , City: '.$customOrder->bill_city.' , State: '.$customOrder->bill_state.' , Zip: '.$customOrder->bill_zip}}</td> -->
                              <td>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$custom->id}}">
                                  View
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal{{$custom->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Shipping Address</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                       {{$custom->bill_address1 .' ,'.$custom->bill_address2.', '.$custom->bill_country.', '.$custom->bill_city.' ,'.$custom->bill_state.' ,'.$custom->bill_zip}}
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td scope="row">{{$customOrder->totalCost}}</td>
                              <td scope="row">{{$customOrder->serviceCost}}</td>
                              <td scope="row">{{$customOrder->status}}</td>
                              <td scope="row"><a target="_blank" href="{{route('showUserCustomOrderImageOutside',[$customOrder->id])}}" class="action-icon">
                                <button type="button" class="btn btn-success">Show</button>
                               </a></td>
                               <?php if($customOrder->status=='waitingpayment'){ ?>
                                <td>
                                <form method="post" action="{{route('paySecondPaymentOutside')}}">
                                  @csrf
                                  <input type="hidden" name="id" value="{{$customOrder->id}}">
                                      <button type="submit" class="btn btn-info">Pay</button>
                                </form>
                                </td>
                                <?php } ?>
                                <?php if($customOrder->status=='waitingShippingPayment'){ ?>
                                <td>
                                <form method="post" action="{{route('payThirdPaymentOutside')}}">
                                  @csrf
                                  <input type="hidden" name="id" value="{{$customOrder->id}}">
                                      <button type="submit" class="btn btn-info">Pay</button>
                                </form>
                                </td>
                                <?php } ?>
                            </tr>
                          </tbody>

                      </table>
                      @endforeach
                    </div>
                  </div>
                 
                </div>
                
                
                
              </div>

            </div>
          </div>
        </section>

@endsection