@extends('layouts.user')

@section('title') User Login @endsection

@section('content')
<div class="nav-2">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-5">
          <form>
            <div class="search">
              <div class="opction">
                <select >
                  @foreach($departmentInfo as $department)
                  <option value="{{$department->id}}">{{$department->department_type}}</option>
                  @endforeach
                </select>
              </div>
              <input type="text" class="searchTerm" placeholder="What are you looking for?">
              <button type="submit" class="searchButton">
               <i class="fa fa-search"></i>
              </button>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <div class="manu">
            <ul>
              <li>
                 <a href="#"> <i class="far fa-heart"></i> </a>
              </li>
              <li>
                 <a href="#"> <i class="fas fa-cart-plus"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      </div>
    </div>
</section>

<!--start body login page------------------------------------------------------------------->
<section class="login mt-5">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="img">
          <img src="{{asset('user/images/img.svg')}}"  alt="">
        </div>
      </div>
      <div class="col-md-6">
        <div class="login-container">
          <form method="post" action="{{route('saveRegister')}}">
          @csrf
          @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <?php 
              $message=Session::get('message');
              if($message){
              ?>
                  <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <?php
                          echo $message;
                          Session::put('message','');
                      ?>
                  </div>
              <?php
              }
              ?>

            <?php 
              $message=Session::get('messageWarning');
              if($message){
              ?>
                  <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      <?php
                          echo $message;
                          Session::put('messageWarning','');
                      ?>
                  </div>
              <?php   
              }
              ?>
            <img class="avatar" src="{{asset('user/images/avatar.svg')}}" alt="">
            <h2>Welcome</h2>
            <div class="input-div one focus">
              <div class="i">
                <i class="fas fa-user"></i>
              </div>
              <div >
                <input class="input" type="text" name="userName" placeholder="Name" value="" required>
              </div>
            </div>
            <div class="input-div one focus">
              <div class="i">
                <i class="fas fa-envelope"></i>
              </div>
              <div >
                <input class="input" type="email" name="userEmail" id="checkUserEmail" placeholder="Email" value="" required>
              </div>
            </div>


            <div class="input-div one focus">
              <div class="i">
                <i class="fas fa-mobile-alt"></i>
              </div>
              <div >
                <input class="input" type="number" name="userPhone" id="checkUserPhone" placeholder="Phone" value="" required>
              </div>
            </div>

            <div class="input-div two focus">
              <div class="i">
                <i class="fas fa-lock"></i>
              </div>
              <div>
                <input class="input" type="password" placeholder="Password" name="userPassword" value="" required>
              </div>
            </div>

            <div class="input-div two focus">
              <div class="i">
                <i class="fas fa-lock"></i>
              </div>
              <div>
                <input class="input" type="password"placeholder="Retype password" name="userRePassword" value="" required>
              </div>
            </div>
            <!-- <a href="#"> Forgot Password</a> -->
             <a href="#" class="text-decoration-none"> <input type="submit"class="log-btn" id="userRegister" name="" value="Sign me up"> </a>
          </form>
        </div>
      </div>
    </div>
  </div>

</section>

@endsection