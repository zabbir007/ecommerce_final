@extends('layouts.user')

@section('title') User Login @endsection

@section('content')
<link rel="stylesheet" href="{{asset('user/css/loginfrom.css')}}">
<div class="nav-2 allpadding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <div class="accordion-menu">
              <div id="accordion">
                <div class="card card-menu">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                    </h5>
                  </div>
                  <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body card-body-menu-popup">
                      <div class='cssmenuvv'>
                      <ul>
                         <li class='active'> <a href="{{route('welcome')}}"> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <div class="manu">
              <ul>
                <li>
                  <a href="#"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
               <!--  <li>
                   <a href="#"> <i class="far fa-heart"></i> <span class="count"><?php //echo Cart::instance('wishlist')->count(); ?></span> </a>
                </li> -->
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
            </div>
          </div>
        </div>
        </div>
      </div>
</section>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<?php 
  $message=Session::get('message');
  if($message){
  ?>
      <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          <?php
              echo $message;
              Session::put('message','');
          ?>
      </div>
  <?php   
  }
  ?>

  <?php 
    $message=Session::get('messageWarning');
    if($message){
    ?>
        <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            <?php
                echo $message;
                Session::put('messageWarning','');
            ?>
        </div>
    <?php   
    }
    ?>

  <?php 
  $message=Session::get('messageVerifyR');
  if($message){
  ?>
      <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          <?php
              echo $message;
              Session::put('messageVerifyR','');
          ?>
      </div>
  <?php   
  }
  ?>

  <?php 
  $message=Session::get('messageVerifyU');
  if($message){
  ?>
      <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          <?php
              echo $message;
              Session::put('messageVerifyU','');
          ?>
      </div>
  <?php   
  }
  ?>

  <?php 
  $message=Session::get('messageVerifyS');
  if($message){
  ?>
      <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
          <?php
              echo $message;
              Session::put('messageVerifyS','');
          ?>
      </div>
  <?php
  }
  ?>
<div class="top-content">
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                  <div class="form-box">
                    <div class="form-top">
                      <div class="form-top-left">
                        <h3>Login to our site</h3>
                          <p>Enter Email and password to log on:</p>
                      </div>
                      <div class="form-top-right">
                        <i class="fa fa-key"></i>
                      </div>
                      </div>
                      <div class="form-bottom">
                    <form role="form" action="{{route('loginUser')}}" method="post" class="login-form">
                      @csrf
                      <div class="input-div one focus">
                        <div class="i">
                          <i class="fas fa-user"></i>
                        </div>
                        <div >
                          <input class="input" type="text" name="emailOrPhone" placeholder="Email" value="" required>
                        </div>
                      </div>
                      <div class="input-div two focus">
                        <div class="i">
                          <i class="fas fa-lock"></i>
                        </div>
                        <div>
                          <input class="input" type="password"placeholder="Password" name="password" value="" required>
                        </div>
                      </div>
                        <button type="submit" class="btn btn-danger mb-3">Log In</button>
                        <a href="{{route('showForgetPassword')}}">Forgot Password?</a>
                    </form>
                  </div>
                </div>
                </div>
                <div class="col-sm-1 middle-border"></div>
                <div class="col-sm-1"></div>
                <div class="col-sm-5">
                  <div class="form-box">
                    <div class="form-top">
                      <div class="form-top-left">
                        <h3>Sign up now</h3>
                          <p>Fill in the form below to get instant access:</p>
                      </div>
                      <div class="form-top-right">
                        <i class="fa fa-pencil"></i>
                      </div>
                      </div>
                      <div class="form-bottom">
                    <form role="form" action="{{route('saveRegister')}}" method="post" class="registration-form">
                      @csrf
                      <div class="input-div one focus">
                          <div class="i">
                            <i class="fas fa-user"></i>
                          </div>
                          <div >
                            <input class="input" type="text" name="userName" placeholder="Name" value="" required>
                          </div>
                        </div>
                        <div class="input-div one focus">
                          <div class="i">
                            <i class="fas fa-envelope"></i>
                          </div>
                          <div >
                            <input class="input" type="email" name="userEmail" id="checkUserEmail" placeholder="Email" value="" required>
                          </div>
                        </div>


                        <div class="input-div one focus">
                          <div class="i">
                            <i class="fas fa-mobile-alt"></i>
                          </div>
                          <div >
                            <input class="input" type="number" name="userPhone" id="checkUserPhone" placeholder="Phone" value="" required>
                          </div>
                        </div>

                        <div class="input-div two focus">
                          <div class="i">
                            <i class="fas fa-lock"></i>
                          </div>
                          <div>
                            <input class="input" type="password" placeholder="Password" name="userPassword" value="" required>
                          </div>
                        </div>

                        <div class="input-div two focus">
                          <div class="i">
                            <i class="fas fa-lock"></i>
                          </div>
                          <div>
                            <input class="input" type="password"placeholder="Retype password" name="userRePassword" value="" required>
                          </div>
                        </div>
                        <button type="submit" class="btn btn-danger">Sign me up</button>
                    </form>
                  </div>
                  </div>

                </div>
            </div>

        </div>
    </div>

</div>

@endsection