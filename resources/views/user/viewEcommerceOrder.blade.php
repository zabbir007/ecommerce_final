@extends('layouts.user')

@section('title') Order @endsection

@section('content')
<link href="{{asset('admin/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  @media print {
    body * {
      visibility: hidden;
    }
    
    #printInvoice, #printInvoice * {
      visibility: visible;
    }
    #notPrint, #notPrint *{
      visibility: hidden;
    }
    #searchBirthProtilipi, #searchBirthProtilipi *{
      visibility: hidden;
    }
    #printInvoice {
      position: absolute;
      left: 0;
      top: 0;
    }
  }
</style>
<div class="nav-2" id="notPrint">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2">
          <div class="accordion-menu">
            <div id="accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                      <a href="" class="btn btn-link d-block" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down ml-5"></i></a>
                  </h5>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body card-body-menu">
                    <div class='cssmenuvv'>
                    <ul>
                       <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                      @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                    </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-6">
          <form method="post" action="{{route('searchProductByProductName')}}">
            @csrf
            <div class="search">
              <div class="opction">
                <select name="departmentId">
                  @foreach($departmentInfo as $department)
                  <option value="{{$department->id}}">{{$department->department_type}}</option>
                  @endforeach
                </select>
              </div>
              <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
              <button type="submit" class="searchButton">
               <i class="fa fa-search"></i>
              </button>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <div class="manu">
            <ul>
              <li>
                <a href="#"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
              </li>
              <li>
                 <a href="#"> <i class="fas fa-cart-plus"></i> <span class="count"><?php echo Cart::count();?></span> </a>
              </li>

            </ul>
          </div>
        </div>
      </div>
      </div>
    </div>
</section>
 <div class="container" id="printInvoice">
                        
   

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <!-- Logo & title -->
                <div class="clearfix">
                    <div class="float-left">
                        <img src="{{asset('admin/img/2019120213100756012.png')}}" alt="" height="20">
                    </div>
                    <div class="float-right">
                        <h4 class="m-0 d-print-none">Invoice</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="mt-3">
                            <p><b>Hello, {{$orderInfo->firstName}}</b></p>
                            <p class="text-muted">Thanks a lot because you keep purchasing our products. Our company
                                promises to provide high quality products for you as well as outstanding
                                customer service for every transaction. </p>
                        </div>

                    </div><!-- end col -->
                    <div class="col-md-4 offset-md-2">
                        <div class="mt-3 float-right">
                            <p class="m-b-10"><strong>Order Date : </strong> <span class="float-right"> &nbsp;&nbsp;&nbsp;&nbsp; {{$orderInfo->orderDate}}</span></p>
                            <p class="m-b-10"><strong>Order Status : </strong> <span class="float-right"><?php if($orderInfo->paidStatus=='0'){
                            ?>
							<span class="badge badge-danger">Unpaid</span>
							<?php

                            }else{ ?>
							<span class="badge badge-success">Paid</span>
							<?php } ?>
                            </span></p>
                            <p class="m-b-10"><strong>Order No. : </strong> <span class="float-right">{{$orderInfo->orderNumber}} </span></p>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                <div class="row mt-3">
                    <div class="col-sm-6">
                        <h6>Billing Address</h6>
                        <address>
                            {{$orderInfo->billingAddress}}
                        </address>
                    </div> <!-- end col -->

                    <div class="col-sm-6">
                        <h6>Shipping Address</h6>
                        <address>
                            {{$orderInfo->shippingAddress}}
                        </address>
                    </div> <!-- end col -->
                </div> 
                <!-- end row -->

                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table mt-4 table-centered">
                                <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th style="width: 10%">Quantity</th>
                                    <th style="width: 10%">Price</th>
                                </tr></thead>
                                <tbody>
							<?php
				                $pkgCount=count($name);
				                for($i=0;$i<$pkgCount;$i++){
				             ?>

                                <tr>
                                    <td>
                                    	<?php echo $name[$i]; ?>
                                    </td>
                                    <td>
                                       <?php echo $qun[$i]; ?>
                                    </td>
                                    <td>
                                    	<?php echo $price[$i]; ?>
                                    </td>
                                    
                                </tr>

                            <?php } ?>

                                <tr>
                                	<td>
                                		Shipping Charge
                                	</td>
                                	<td>
                                		
                                	</td>
                                	<td>
                                		{{$orderInfo->shippingAmount}}
                                	</td>
                                </tr>

                                </tbody>
                            </table>
                        </div> <!-- end table-responsive -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

                <div class="row">
                    <div class="col-sm-6">
                        <div class="clearfix pt-5">
                            <h6 class="text-muted">Notes:</h6>

                            <small class="text-muted">
                                All accounts are to be paid within 7 days from receipt of
                                invoice. To be paid by cheque or credit card or direct payment
                                online. If account is not paid within 7 days the credits details
                                supplied as confirmation of work undertaken will be charged the
                                agreed quoted fee noted above.
                            </small>
                        </div>
                    </div> <!-- end col -->
                    <div class="col-sm-6">
                        <div class="float-right">
                            <p><b>Total :</b> <span class="float-right">{{$orderInfo->totalAmount}}</span></p>
                            <!-- <p><b>Discount (10%):</b> <span class="float-right"> &nbsp;&nbsp;&nbsp; $459.75</span></p>
                            <h3>$4137.75 USD</h3> -->
                        </div>
                        <div class="clearfix"></div>
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

                <div class="mt-4 mb-1" id="notPrint">
                    <div class="text-right d-print-none">
                        <a href="" class="btn btn-primary waves-effect waves-light" onclick="printPage()"><i class="mdi mdi-printer mr-1"></i> Print</a>
                        <input type="hidden" name="" id="customer_name" value="{{$userInfo->firstName}}">
                        <input type="hidden" name="" id="mobile" value="{{$userInfo->phone}}">
                        <input type="hidden" name="" id="email" value="{{$userInfo->email}}">
                        <input type="hidden" name="" id="address" value="{{$userInfo->bill_address1}}">
                        <input type="hidden" name="" id="total_amount" value="{{$orderInfo->totalAmount}}">
                        
                        <button class="btn btn-primary btn-lg btn-block" id="sslczPayBtn"
                          token="if you have any token validation"
                          postdata="your javascript arrays or objects which requires in backend"
                          order="If you already have the transaction generated for current order"
                          endpoint="{{ url('/pay-via-ajax') }}"> Pay Now
                        </button>
                    </div>
                </div>
            </div> <!-- end card-box -->
        </div> <!-- end col -->
    </div>
    <!-- end row --> 
    
</div> <!-- container -->

 <!-- Vendor js -->
<script src="{{asset('admin/assets/js/vendor.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('admin/assets/js/app.min.js')}}"></script>

<script>
    
function printPage() {
  window.print();

}
</script>

<!-- If you want to use the popup integration, -->
<script>
    var obj = {};
    obj.cus_name = $('#customer_name').val();
    obj.cus_phone = $('#mobile').val();
    obj.cus_email = $('#email').val();
    obj.cus_addr1 = $('#address').val();
    obj.amount = $('#total_amount').val();

    $('#sslczPayBtn').prop('postdata', obj);

    (function (window, document) {
        var loader = function () {
            var script = document.createElement("script"), tag = document.getElementsByTagName("script")[0];
            // script.src = "https://seamless-epay.sslcommerz.com/embed.min.js?" + Math.random().toString(36).substring(7); // USE THIS FOR LIVE
            script.src = "https://sandbox.sslcommerz.com/embed.min.js?" + Math.random().toString(36).substring(7); // USE THIS FOR SANDBOX
            tag.parentNode.insertBefore(script, tag);
        };

        window.addEventListener ? window.addEventListener("load", loader, false) : window.attachEvent("onload", loader);
    })(window, document);
</script>

@endsection