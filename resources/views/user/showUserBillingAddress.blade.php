@extends('layouts.user')

@section('title') Profile @endsection

@section('content')

<div class="nav-2">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2">
          <div class="accordion-menu">
            <div id="accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                      <a href="" class="btn btn-link d-block" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down ml-5"></i></a>
                  </h5>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body card-body-menu">
                    <div class='cssmenuvv'>
                    <ul>
                       <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                      @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      
                       <li> <a href='#'> <span> About </span> </a>  </li>
                       <li class='last'> <a href='#'> <span> Contact </span></a> </li>
                    </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-6">
          <form method="post" action="{{route('searchProductByProductName')}}">
            @csrf

            <div class="search">
              <div class="opction">
                <select name="departmentId">
                  @foreach($departmentInfo as $department)
                  <option value="{{$department->id}}">{{$department->department_type}}</option>
                  @endforeach
                </select>
              </div>
              <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
              <button type="submit" class="searchButton">
               <i class="fa fa-search"></i>
              </button>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <div class="manu">
            <ul>
                <li>
                  <a href="#"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                <!-- <li>
                   <a href="#"> <i class="far fa-heart"></i> <span class="count"><?php //echo Cart::instance('wishlist')->count(); ?></span> </a>
                </li> -->
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
          </div>
        </div>
      </div>
      </div>
    </div>
</section>


              <!--user-profile------------------------------------------------->
              <section class="user-profile">
                <div class="container">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="user-nav">
                        <ul>
                          <li> <a href="{{route('userOrder')}}">My Orders</a> </li>
                        </ul>
                        <ul>
                          <li> <a href="#" >Manage My Account</a> </li>
                          <ul>
                            <li> <a href="{{route('userProfile')}}">Personal Information</a> </li>
                            <li><a href="{{route('userBillingAddress')}}"class="active">Billing Address</a></li>
                            <li><a href="{{route('userShippingAddress')}}">Shipping Address</a></li>
                          </ul>
                        </ul>
                        <ul >
                          <li> <a href="{{route('userSuiteAddress')}}" >My Swift Address</a> </li>
                        </ul>
                        <ul>
                          <li> <a href="{{route('userReshippingOrder')}}" >Re-Shipping Order</a> </li>
                        </ul>
                        <ul>
                          <li> <a href="{{route('userCustomOrder')}}" >Custom Order</a> </li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-8">
                        <h4>Billing Address</h4>
                      <div class="user-account">
                        <div class="card-body">
                          <form  method="post" action="{{route('updateCustomerAddress')}}">
                            @csrf
                            <?php 
                                $message=Session::get('messagePer');
                                if($message){
                                ?>
                                  <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                      <?php
                                          echo $message;
                                          Session::put('messagePer','');
                                      ?>
                                  </div>
                                <?php
                                }
                                ?>

                                <?php 
                                $message=Session::get('messageWarningPer');
                                if($message){
                                ?>
                                    <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        <?php
                                            echo $message;
                                            Session::put('messageWarningPer','');
                                        ?>
                                    </div>
                                <?php   
                                }
                                ?>

                                <?php 
                                  $message=Session::get('messageOrder');
                                  if($message){
                                  ?>
                                      <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                              </button>
                                          <?php
                                              echo $message;
                                              Session::put('messageOrder','');
                                          ?>
                                      </div>
                                <?php   
                                }
                                ?>
                            <div class="form-row">
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom01" >Name </label>
                                <input type="text" name="firstName" value="{{$userInfo->firstName}}" class="form-control" id="validationCustom01" placeholder="Name" required="">
                                <div class="valid-feedback">
                                  Looks good!
                                </div>
                              </div>
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom01" >Address 1 </label>
                                <input type="text" name="address1" value="{{$userInfo->address1}}" class="form-control" id="validationCustom02" placeholder="Street,district" required>
                                <div class="valid-feedback">
                                  Looks good!
                                </div>
                              </div>
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom01" >Address 2 </label>
                                <input type="text" name="address2" value="{{$userInfo->address2}}" class="form-control" id="validationCustom02" placeholder="Suit, building, flat" required>
                                <div class="valid-feedback">
                                  Looks good!
                                </div>
                              </div>
                              <div class="col-md-6 mb-3">
                                <div class="form-group">
                                  <label for="validationCustom05">Country</label>
                                  <select class="form-control field-validate" name="country" required="">
                                     <?php
                                       $length = count($countries);
                                          for ($i = 0; $i < $length; $i++) {
                                     ?>
                                     <option value="<?php echo $countries[$i]; ?>" <?php if($countries[$i]==$userInfo->country) {echo "selected";}?> ><?php echo $countries[$i]; ?></option>
                                     <?php
                                       }
                                     ?>
                                   </select>
                                </div>
                              </div>


                              <div class="col-md-6 mb-3">
                                <label for="validationCustom01" >City </label>
                                <input type="text" class="form-control" name="city" value="{{$userInfo->city}}" id="validationCustom04" placeholder="City" required>
                                <div class="valid-feedback">
                                  Looks good!
                                </div>
                              </div>
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom03">State </label>
                                <input type="text" class="form-control " name="state" value="{{$userInfo->state}}" id="validationCustom05" placeholder="State" required>
                                <div class="invalid-feedback">
                                  Please provide a valid city.
                                </div>

                              </div>
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom04">Zip/Postal Code</label>
                                <input type="text" class="form-control" name="zip" value="{{$userInfo->zip}}" id="validationCustom06" placeholder="Zip or pastal code" required>

                              </div>



                            </div>
                            <div class="checkout-btn ">
                              <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                          </form>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>


<script type="text/javascript">
  $( document ).ready(function () {
  $(".product-hidden").slice(0, 12).show();

    $("#loadMore").on('click', function (e) {
      e.preventDefault();
      $(".product-hidden:hidden").slice(0, 6).slideDown();
      if ($(".product-hidden:hidden").length == 0) {
        $("#loadMore").fadeOut('slow');
      }
    });
  });
  </script>




@endsection