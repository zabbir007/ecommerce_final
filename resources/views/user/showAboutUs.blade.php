@extends('layouts.user')

@section('title') E-Commerce @endsection

@section('content')
<div class="nav-2 allpadding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <div class="accordion-menu">
              <div id="accordion">
                <div class="card card-menu">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                    </h5>
                  </div>
                  <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body card-body-menu-popup">
                      <div class='cssmenuvv'>
                      <ul>
                         <li class='active'> <a href="{{route('welcome')}}"> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <div class="manu">
              <ul>
                <li>
                  <a href="{{route('userOrder')}}"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
            </div>
          </div>
        </div>
        </div>
      </div>
</section>
<!--about-us------------------------------------------------------------------->
<section class="about-us">

</section>
<!--what-we-do------------------------------------------------------------------->
<section class="what-we-do">
  <div class="what-we-do-overley">
  </div>
  <div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="what-we">
        <h1 class="text-center wow bounceInUp data-wow-duration="2s data-wow-delay=".2s">What <span>We Do</span> </h1>
      </div>
    </div>
  </div>
    <div class="row">
      <div class="col-md-6">
        <div class="story ">
          <p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
             Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
          </p>
          <p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
             The point of using Lorem Ipsum is that it has a more-or-less normal distribution</p>

          <p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s"> Contrary to popular belief, Lorem Ipsum is not simply random text.
            It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin</p>
        </div>
        </div>
        <div class="col-md-6">
          <div class="story">
            <p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
               Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
            </p>
            <p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
               The point of using Lorem Ipsum is that it has a more-or-less normal distribution</p>

            <p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">Contrary to popular belief, Lorem Ipsum is not simply random text.
              It has roots in a piece of classical Latin literature from 45 BC, </p>
          </div>
        </div>
      </div>
    </div>
</section>
<!--Our Promise------------------------------------------------------------------->
<section class="our-promise">
  <div class="container">
    <div class="what-we">
      <h1 class="text-center">Our <span>Promise</span> </h1>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="promise-img ">
          <img src="{{asset('user/images/pexels-photo-1595385.jpeg')}}" class="img-fluid" alt="">
        </div>
      </div>
      <div class="col-md-6">
        <ul class="list-unstyled mt-5">
          <li class="media">
            <img class="mr-3" src="{{asset('user/images/cart.jpg')}}"class="img-fluid" alt="Generic placeholder image">

            <div class="media-body">
              <h5 class="mt-0 mb-1">List-based media object</h5>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
            </div>
          </li>
          <li class="media my-4">
            <img class="mr-3" src="{{asset('user/images/delivery.png')}}" alt="Generic placeholder image">
            <div class="media-body">
              <h5 class="mt-0 mb-1">Fast Delivery</h5>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
            </div>
          </li>
          <li class="media">
            <img class="mr-3" src="{{asset('user/images/product.png')}}" alt="Generic placeholder image">
            <div class="media-body">
              <h5 class="mt-0 mb-1">List-based media object</h5>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
            </div>
          </li>

          <li class="media my-4">
            <img class="mr-3" src="{{asset('user/images/prices.jpg')}}" alt="Generic placeholder image">
            <div class="media-body">
              <h5 class="mt-0 mb-1">Best Price</h5>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!--Our Values------------------------------------------------------------------->
<section class="our-values">
  <div class="container">
    <div class="what-we">
      <h1 class="text-center mt-5">Our <span>Values</span> </h1>
    </div>
    <div class="row">
      <div class="col-md-6">
        <ul class="list-unstyled mt-5">
          <li class="media">
            <img class="mr-3" src="{{asset('user/images/embracing.jpg')}}"class="img-fluid" alt="Generic placeholder image">

            <div class="media-body">
              <h5 class="mt-0 mb-1">Embracing</h5>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
            </div>
          </li>
          <li class="media my-4">
            <img class="mr-3" src="{{asset('user/images/team.png')}}" alt="Generic placeholder image">
            <div class="media-body">
              <h5 class="mt-0 mb-1">Team Work</h5>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
            </div>
          </li>
          <li class="media">
            <img class="mr-3" src="{{asset('user/images/customer.png')}}" alt="Generic placeholder image">
            <div class="media-body">
              <h5 class="mt-0 mb-1">Customer Commitment</h5>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
            </div>
          </li>

          <li class="media my-4">
            <img class="mr-3" src="{{asset('user/images/ownershi.png')}}" alt="Generic placeholder image">
            <div class="media-body">
              <h5 class="mt-0 mb-1">Ownership</h5>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
            </div>
          </li>
        </ul>
      </div>

      <div class="col-md-6">
        <div class="our-team mt-5">
          <img src="{{asset('user/images/our team.jpg')}}" class="img-fluid" alt="">
        </div>
      </div>
    </div>
  </div>
</section>

@endsection