@extends('layouts.user')

@section('title') Store @endsection

@section('content')

<div class="nav-2">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2">
          <div class="accordion-menu">
            <div id="accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                      <a href="" class="btn btn-link d-block" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down ml-5"></i></a>
                  </h5>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body card-body-menu">
                    <div class='cssmenuvv'>
                    <ul>
                       <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                      @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                    </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-6">
          <form method="post" action="{{route('searchProductByProductName')}}">
            @csrf
            <div class="search">
              <div class="opction">
                <select name="departmentId">
                  @foreach($departmentInfo as $department)
                  <option value="{{$department->id}}">{{$department->department_type}}</option>
                  @endforeach
                </select>
              </div>
              <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
              <button type="submit" class="searchButton">
               <i class="fa fa-search"></i>
              </button>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <div class="manu">
            <ul>
              <li>
                <a href="#"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
              </li>
              <li>
                 <a href="#"> <i class="fas fa-cart-plus"></i> <span class="count"><?php echo Cart::count();?></span> </a>
              </li>

            </ul>
          </div>
        </div>
      </div>
      </div>
    </div>
</section>


<!--cart main body start here-->

<section class="shaping">
  <div class="container">
    <div class="what-we">
      <h1 class="text-center wow bounceInLeft data-wow-duration="2s" data-wow-delay=".2s"">My <span>Cart</span> </h1>
    </div>
  </div>
</section>
<!--HOW TO RESHIPPING------------------------------------------------------------------->
<section class="my-cart">
  <div class="container">
    <div class="row">


      <div class="col-md-12">
        <table id="cart" class="table table-hover table-condensed">
            <thead>
              <tr>
                <th style="width:50%">Product</th>
                <th style="width:10%">Price</th>
                <th style="width:18%">Quantity</th>
                <th style="width:12%" class="text-center">Subtotal</th>
                <th style="width:10%">Action</th>
              </tr>
            </thead>
            <tbody>
            	<?php

            		$contents=Cart::content();
            	?>
              @foreach($contents as $content)
              <tr>
                <td data-th="Product">
                  <div class="row">
                    <div class="col-sm-3 hidden-xs mycart-img">
                      <img src="{{asset($content->options->image)}}"class="img-fluid" alt="">
                    </div>
                    <div class="col-sm-9">
                      <h4 class="nomargin">{{$content->name}}</h4>
                    </div>
                  </div>
                </td>
                <td data-th="Price">{{$content->price}}</td>
                <td data-th="Quantity">
                  <form method="post" action="{{route('updateCartValue')}}">
                    @csrf
                    <div class="row">
                      <div class="col-md-6">
                        <span>
                          <input type="number" min="1" max="100000" step="1" class="form-control text-center" name="qty" value="{{$content->qty}}">
                          <input type="hidden" name="rowId" value="{{$content->rowId}}">
                        </span>
                      </div>
                      <div class="col-md-6">
                        <span>
                          <button class="btn btn-dark btn-sm" type="submit"><i class="fas fa-pen-square"></i></button>
                        </span>
                      </div>
                    </div> 
                  </form>                                             
                </td>
                <td data-th="Subtotal" class="text-center">{{$content->subtotal}}</td>
                <td class="actions" data-th="">
                  <a href="{{route('deleteCartValue',[$content->rowId])}}">
                    <button class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></button>
                  </a>
                                 
                </td>
              </tr>
               @endforeach 
            </tbody>
			
		       <tfoot>
              <tr>
                <td><a href="{{route('shop')}}" class="btn btn-warning"> <i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                <td colspan="2" class="hidden-xs"></td>
                <td class="hidden-xs text-center"><strong>Total {{Cart::total()}}</strong></td>
                <td><a href="{{route('showEcommerceOrderCheckout')}}" class="btn btn-success btn-mycart">Checkout</a></td>
              </tr>
            </tfoot>

          </table>
      </div>
     

    </div>
  </div>
</section>

<!--cart main body end here-->

<script type="text/javascript">
  $( document ).ready(function () {
  $(".product-hidden").slice(0, 12).show();

    $("#loadMore").on('click', function (e) {
      e.preventDefault();
      $(".product-hidden:hidden").slice(0, 6).slideDown();
      if ($(".product-hidden:hidden").length == 0) {
        $("#loadMore").fadeOut('slow');
      }
    });
  });
  </script>
@endsection