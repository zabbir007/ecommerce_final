@extends('layouts.user')

@section('title') Profile @endsection

@section('content')

<div class="nav-2">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2">
          <div class="accordion-menu">
            <div id="accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                      <a href="" class="btn btn-link d-block" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down ml-5"></i></a>
                  </h5>
                </div>
                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body card-body-menu">
                    <div class='cssmenuvv'>
                    <ul>
                       <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                      @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      
                       <li> <a href='#'> <span> About </span> </a>  </li>
                       <li class='last'> <a href='#'> <span> Contact </span></a> </li>
                    </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-6">
          <form method="post" action="{{route('searchProductByProductName')}}">
            @csrf
            <div class="search">
              <div class="opction">
                <select name="departmentId">
                  @foreach($departmentInfo as $department)
                  <option value="{{$department->id}}">{{$department->department_type}}</option>
                  @endforeach
                </select>
              </div>
              <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
              <button type="submit" class="searchButton">
               <i class="fa fa-search"></i>
              </button>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <div class="manu">
            <ul>
                <li>
                  <a href="#"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                <!-- <li>
                   <a href="#"> <i class="far fa-heart"></i> <span class="count"><?php //echo Cart::instance('wishlist')->count(); ?></span> </a>
                </li> -->
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
          </div>
        </div>
      </div>
      </div>
    </div>
</section>
<!--Category AREA------------------------------------------------------------------->

            
      <!--user-profile------------------------------------------------->
      <section class="user-profile">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2">
              <div class="user-nav">
                <ul>
                  <li> <a href="{{route('userOrder')}}">My Orders</a> </li>
                </ul>
                <ul>
                  <li> <a href="#" >Manage My Account</a> </li>
                  <ul>
                    <li> <a href="{{route('userProfile')}}">Personal Information</a> </li>
                    <li><a href="{{route('userBillingAddress')}}">Billing Address</a></li>
                    <li><a href="{{route('userShippingAddress')}}">Shipping Address</a></li>
                  </ul>
                </ul>
                <ul >
                  <li> <a href="{{route('userSuiteAddress')}}" >My Swift Address</a> </li>
                </ul>
                <ul >
                  <li> <a href="{{route('userReshippingOrder')}}" class="active">Re-Shipping Order</a> </li>
                </ul>
                <ul>
                  <li> <a href="{{route('userCustomOrder')}}" >Custom Order</a> </li>
                </ul>
              </div>
            </div>  

              <div class="col-md-10">
                <p>Reshipping Information</p>
                <div class="row">
                @foreach($reshippingOrderInfo as $reshippingOrder)
               
                  <div class="col-md-12 mb-5">
                    <div class=" table-responsive  bg-white">
                      
                        <table class="table ">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">Tracking Number</th>
                            <th scope="col">Order Date</th>
                            <th scope="col">Product Type</th>
                            <th scope="col">Invoice Number</th>
                            <th scope="col">Product Description</th>
                            <th scope="col">Package Id</th>
                            <th scope="col">Product From</th>
                            <th scope="col">Product Weight</th>
                            <th scope="col">Sent To</th>
                            <th scope="col">Product Quantity</th>
                            <th scope="col">Order Status</th>
                            <?php if ($reshippingOrder->status=='pending') {
                              ?>
                            <th scope="col">Action</th>
                          <?php }?>
                          </tr>
                        </thead>
                        <tbody>
                          
                            <tr>
                              <th scope="row">{{$reshippingOrder->trackingNumber}}</th>
                              <td>{{$reshippingOrder->createTime}}</td>
                              <td>{{$reshippingOrder->productType}}</td>
                              <td>{{$reshippingOrder->invoiceNumber}}</td>
                             <!--  <td>{{$reshippingOrder->productDescription}}</td> -->
                              <td>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleMod{{$reshippingOrder->id}}">
                                  View
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleMod{{$reshippingOrder->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Product Description</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                       {{$reshippingOrder->productDescription}}
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td>{{$reshippingOrder->packageId}}</td>
                              <td>{{$reshippingOrder->from}}</td>
                              <td>{{$reshippingOrder->weight}}</td>
                              <td>{{$reshippingOrder->sentTo}}</td>
                              <td>{{$reshippingOrder->quantity}}</td>
                              <td>{{$reshippingOrder->status}}</td>
                              <?php if ($reshippingOrder->status=='pending') {
                              ?>
                              <td>
                                <!-- <a href="{{route('editCustomerPendingShippingOrder',[$reshippingOrder->id])}}" class="action-icon"> <i class="fa fa-edit"></i></a> -->
                                <a href="{{route('editCustomerPendingShippingOrder',[$reshippingOrder->id])}}" class="action-icon">
                                <button type="button" class="btn btn-success">Complete Order</button>
                               </a>
                              </td>
                            <?php } ?>
                           
                            </tr>
                                           
                        </tbody>
                      </table>

                      <?php if($reshippingOrder->status=='waitingpayment' || $reshippingOrder->status=='active' || $reshippingOrder->status=='complete'){ ?>
                      <table class="table">
                        <thead class="bg-info">
                          <tr>
                            <th scope="col">Order Number</th>
                            <th>Shipment Cost</th>
                            <th>Insurance Cost</th>
                            <th>Additional Cost</th>
                            <th>Service Cost</th>
                            <th>Shipping Cost</th>
                            <th>Total Cost</th>
                            <th>Discount</th>
                            <th>Tracking </th>
                            <?php if($reshippingOrder->status=='waitingpayment'){ ?>
                             <th>Pay </th>
                            <?php } ?> 
                            <?php if($reshippingOrder->status=='waitingpayment'){ ?>
                             <th>Cancel</th>
                            <?php } ?>
                             <th>Image</th>
                          </tr>
                        </thead>
                          <tbody>
                            <tr>
                              <td>{{$reshippingOrder->orderNumber}}</td>
                              <td>{{$reshippingOrder->adminShipmentCharge}}</td>
                              <td>{{$reshippingOrder->insuranceAmount}}</td>
                              <td>{{$reshippingOrder->customerSetTotalAmount}}</td>
                              <td>{{$reshippingOrder->adminServiceCharge}}</td>
                              <td>{{$reshippingOrder->adminShippingCharge}}</td>
                              <td>{{$reshippingOrder->adminFinalAmount}}</td>
                              <td>{{$reshippingOrder->adminDiscountAmount}}</td>
                              <td>{{$reshippingOrder->trackingNumber}}</td>
                              <?php if($reshippingOrder->status=='waitingpayment'){ ?>
                                
                               <td>
                                <form method="post" action="{{route('updateCustomerWaitingPaymentShippingOrder')}}">
                                  @csrf
                                <input type="hidden" name="id" value="{{$reshippingOrder->id}}">
                                <input type="hidden" name="userEmail" value="{{$reshippingOrder->userEmail}}">
                                <input type="hidden" name="bill_address1" value="{{$reshippingOrder->bill_address1}}">
                                <input type="hidden" name="bill_address2" value="{{$reshippingOrder->bill_address2}}">
                                <input type="hidden" name="bill_country" value="{{$reshippingOrder->bill_country}}">
                                <input type="hidden" name="bill_city" value="{{$reshippingOrder->bill_city}}">
                                <input type="hidden" name="bill_state" value="{{$reshippingOrder->bill_state}}">
                                <input type="hidden" name="bill_zip" value="{{$reshippingOrder->bill_zip}}">
                                <input type="hidden" name="shippingCost" value="{{$reshippingOrder->adminShippingCharge}}">
                                <input type="hidden" name="serviceCost" value="{{$reshippingOrder->adminServiceCharge}}">
                                <input type="hidden" name="weight" value="{{$reshippingOrder->weight}}">
                                <input type="hidden" name="totalAmount" value="{{$reshippingOrder->adminFinalAmount}}">
                                
                                <button type="submit" class="btn btn-warning">Pay Now</button>
                                 </form>
                                </td>
                              
                               <?php } ?>

                               <?php if($reshippingOrder->status=='waitingpayment'){ ?>
                                
                               <td>
                                <form method="post" action="{{route('cancelReshippingOrder')}}">
                                  @csrf
                                <input type="hidden" name="id" value="{{$reshippingOrder->id}}">
                                <button type="submit" class="btn btn-danger">Cancel</button>
                                 </form>
                                </td>
                              
                               <?php } ?>
                               <td><a target="_blank" href="{{route('editCustomerWaitingPaymentShippingOrder',[$reshippingOrder->id])}}" class="action-icon">
                                <button type="button" class="btn btn-success">Show</button>
                               </a></td>
                            </tr>
                          </tbody>
                      </table>
                    <?php } ?>
                    </div>
                  </div>

                  @endforeach
                 
                </div>
                
                
                
              </div>

            </div>
          </div>
        </section>



<script type="text/javascript">
  $( document ).ready(function () {
  $(".product-hidden").slice(0, 12).show();

    $("#loadMore").on('click', function (e) {
      e.preventDefault();
      $(".product-hidden:hidden").slice(0, 6).slideDown();
      if ($(".product-hidden:hidden").length == 0) {
        $("#loadMore").fadeOut('slow');
      }
    });
  });
  </script>




@endsection