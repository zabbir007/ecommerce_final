@extends('layouts.reshipping')

@section('title') E-Commerce @endsection

@section('content')

<!-- Start Content-->
<div class="container-fluid">

  <section class="checkout">
<!-- <div class="container"> -->
  <div class="what-we">
    <h1 class="text-center wow bounceInLeft data-wow-duration="2s" data-wow-delay=".2s"">Inside Shipping<span>Address</span> </h1>
  </div>
  <form class="needs-validation" novalidate method="post" action="{{route('saveInsideShippingAddress')}}">
  	@csrf
  	@if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <?php 
              $message=Session::get('message');
              if($message){
              ?>
                  <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <?php
                          echo $message;
                          Session::put('message','');
                      ?>
                  </div>
              <?php
              }
              ?>

            <?php 
              $message=Session::get('messageWarning');
              if($message){
              ?>
                  <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      <?php
                          echo $message;
                          Session::put('messageWarning','');
                      ?>
                  </div>
              <?php   
              }
              ?>

              <?php 
              $message=Session::get('messageWarningAdd');
              if($message){
              ?>
                  <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      <?php
                          echo $message;
                          Session::put('messageWarningAdd','');
                      ?>
                  </div>
              <?php   
              }
              ?>
  <div class="inside-form">
  <div class="row">
    <div class="col-md-6">
        <div class="form-row">
          <div class="col-md-12 mb-3">
            <label for="validationCustom02"class="check-name">Primary Address <span>*</span></label>
            <input type="text" name="address1" class="form-control" id="autocomplete" onFocus="geolocate()" placeholder="Address"  required>
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
          <div class="col-md-12 mb-3">
            <label for="validationCustom02"class="check-name">Secondary Address<span></span></label>
            <input type="text" name="address2" class="form-control" id="autocomplete2" placeholder="Address" >
            <input class="form-control" id="street_number" disabled="true" type="hidden">
            <input class="form-control" id="route" disabled="true" type="hidden">
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
          <div class="col-md-12 mb-3">
            <label for="validationCustom02"class="check-name">Country <span>*</span></label>
            <input type="text" name="country" class="form-control" id="country" placeholder="Country"  required>
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
          <div class="col-md-12 mb-3">
            <label for="validationCustom03"class="check-name">City <span>*</span></label>
            <input type="text" name="city" class="form-control" id="locality" placeholder="City"required >
            <div class="invalid-feedback">
              Please provide a valid city.
            </div>
          </div>

          <div class="col-md-12 mb-3">
            <label for="validationCustom05"class="check-name">State<span>*</span></label>
            <input type="text" name="street" class="form-control" id="administrative_area_level_1" placeholder="State" required>
          </div>

        <div class="form-group">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
            <label class="form-check-label" for="invalidCheck">
              Create an account?
            </label>
            <div class="invalid-feedback">
              You must agree before submitting.
            </div>
          </div>
        </div>
      <!-- <button class="btn btn-primary" type="submit">Submit form</button> -->
    </div>
  </div>
    <div class="col-md-6">

      <div class="col-md-12 mb-3">
        <label for="validationCustom05"class="check-name">Zip Code<span>*</span></label>
        <input type="text" name="town" class="form-control" id="postal_code" placeholder="Zip Code" required>
      </div>
      <div class="col-md-12 mb-3">
        <label for="validationCustom04"class="check-name">Company Name</label>
        <input type="text" name="company" class="form-control" id="validationCustom04" placeholder="COMPANY NAME">
      </div>
      <div class="col-md-12 mb-3">
        <label for="validationCustom05"class="check-name">PHONE  <span>*</span></label>
        <input type="text" name="phone" class="form-control" id="validationCustom05" placeholder="PHONE " required>
      </div>
      <div class="col-md-12 mb-3">
        <label for="validationCustom05"class="check-name">EMAIL ADDRESS   <span>*</span></label>
        <input type="text" class="form-control" name="email" id="validationCustom05" placeholder="EMAIL ADDRESS" required>
      </div>
      <input type="hidden" name="userId" value="{{Session::get('reshippingUserId')}}">

      <div class="checkout-btn mt-2 mb-2">
        <button class="btn btn-primary" type="submit">Submit</button>
        <button class="">Cancel</button>
      </div>
    </div>

  </div>
</form>
</div>
<script type="text/javascript">
    var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
              (document.getElementById('autocomplete')),
            {types: ['geocode']});
        autocomplete.addListener('place_changed', fillInAddress);

        //place suggest second box
        new google.maps.places.Autocomplete(
          (document.getElementById('autocomplete2')),
          {types: ['geocode']}
        );

      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

       
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
</script>

@endsection