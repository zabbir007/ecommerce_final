@extends('layouts.reshipping')

@section('title') reshipping @endsection

@section('content')

<div class="container-fluid mt-3">
    <form action="{{route('saveUserInsideShipping')}}" class="parsley-examples" method="post">
      @csrf
      <?php 
        $message=Session::get('message');
        if($message){
        ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php
                    echo $message;
                    Session::put('message','');
                ?>
            </div>
        <?php
        }
        ?>
      <div class="row">
        <div class="col-md-6">
          
          <!--Active Your Address------------------------------------------------>
          <div class="card">
            <div class="card-header bg-danger py-2 text-white">
              <div class="card-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                <a data-toggle="collapse" href="#cardCollpase4" role="button" aria-expanded="true" aria-controls="cardCollpase2" class=""><i class="mdi mdi-minus"></i></a>
                <!-- <a href="#" data-toggle="remove"><i class="mdi mdi-close"></i></a> -->
              </div>
                <h4 class="card-title text-center mb-0 text-white">Shipping Address</h4>
            </div>
          <div id="cardCollpase4" class="collapse show" style="">
            <div class="card-body">
              <div class="info-box text-center">
                <h3>Your Shipping Address</h3>
                <div class="table-responsive">
                  <table class="table mb-0">
                      <thead>
                      <tr>
                        <th>Ship To Country</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>Street</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Order Date</th>
                        <th>Delivery Date</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                          <th scope="row">{{$reshippingOrderInfo->country}}</th>
                          <td>{{$reshippingOrderInfo->address1}}</td>
                          <td>{{$reshippingOrderInfo->city}}</td>
                          <td>{{$reshippingOrderInfo->street}}</td>
                          <td>{{$reshippingOrderInfo->email}}</td>
                          <td>{{$reshippingOrderInfo->phone}}</td>
                          <td>{{$reshippingOrderInfo->createTime}}</td>
						              <td>{{$reshippingOrderInfo->deliveryDate}}</td>
                      </tr>
                      </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          </div>
          <!--Total Price--------------------------------------------------->
          <div class="card">
            <div class="card-header bg-danger py-2 text-white">
                <div class="card-widgets">
                    <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                    <a data-toggle="collapse" href="#cardCollpase6" role="button" aria-expanded="true" aria-controls="cardCollpase2" class=""><i class="mdi mdi-minus"></i></a>
                    <!-- <a href="#" data-toggle="remove"><i class="mdi mdi-close"></i></a> -->
                </div>
                <h5 class="card-title mb-0 text-center text-white">Total Price</h5>
            </div>
            <div id="cardCollpase6" class="collapse show" style="">
                <div class="card-body">
                  <div class="info-box text-center">
                    <h3>Total Price</h3>
                    <div class="table-responsive">
                      <table class="table mb-0">
                          <thead>
                          <tr>
                            <th>Product</th>
                            <th>Price</th>
                          </tr>
                          </thead>
                          <tbody>
                          <tr>
                            <th scope="row">Services Charge</th>
                            <td>{{$reshippingOrderInfo->serviceCharge}}</td>
                          </tr>
                          <tr>
                            <th scope="row">Sipping Charge</th>
                            <td>{{$reshippingOrderInfo->shippingCharge}}</td>
                          </tr>
                          <tr>
                            <th scope="row">Box Price</th>
                            <td>
                              {{$reshippingOrderInfo->boxPrice}}
                            </td>
                          </tr>
                          <tr>
                            <th scope="row">Delivery Price</th>
                            <td>
                              {{$reshippingOrderInfo->deliveryPriceTotal}}
                            </td>
                          </tr>
                          <tr>
                            <th scope="row">Total</th>
                            <td>
								              {{$reshippingOrderInfo->totalAmount}}
                            </td>
                          </tr>
                          
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>
            </div>
        </div>


        </div>
        <!------------------------------------------------------->
        <div class="col-md-6">
          <div class="card">
          <div class="card-header bg-danger py-2 text-white">
              <div class="card-widgets">
                  <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                  <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="true" aria-controls="cardCollpase2" class=""><i class="mdi mdi-minus"></i></a>
                  <!-- <a href="#" data-toggle="remove"><i class="mdi mdi-close"></i></a> -->
              </div>
              <h5 class="card-title mb-0 text-center text-white">Card title</h5>
          </div>
          <div id="cardCollpase7" class="collapse show" style="">
              <div class="card-body">
                <div class="card-box">

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label>Reshipping Tracking Number</label>
                        <input type="text" class="form-control" required placeholder="Order Number" readonly value="{{$reshippingOrderInfo->insideShippingOrderNumber}}" />
                    </div>
                    <div class="form-group">
                        <label>Item Name</label>
                        <input type="text" class="form-control" name="itemName" required placeholder="Item Name" value="{{$reshippingOrderInfo->itemName}}" readonly/>
                    </div>

                    <div class="form-group">
                        <label>Item ULR</label>
                        <div>
                            <input type="text" class="form-control" name="itemUrl" value="{{$reshippingOrderInfo->itemUrl}}" placeholder="Item ULR" readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Size</label>
                        <div>
                            <input type="text" class="form-control" name="size" value="{{$reshippingOrderInfo->size}}" required placeholder="Size" readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Quanty</label>
                        <div>
                            <input data-parsley-type="digits" type="text" name="quantity" value="{{$reshippingOrderInfo->quantity}}" class="form-control" required placeholder="Quanty" readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Color</label>
                        <div>
                            <input type="text"class="form-control" name="color" required placeholder="Color" value="{{$reshippingOrderInfo->color}}" readonly/>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-6">

                    <!-- <div class="card-box"> -->
                    <div class="form-group">
                        <label>Order Number</label>
                        <input type="text" class="form-control" name="orderNumber" value="{{$reshippingOrderInfo->orderNumber}}" required placeholder="Order Number" readonly/>
                    </div>

                    <div class="form-group">
                        <label>Price</label>
                        <div>
                            <input type="text"class="form-control" name="price" value="{{$reshippingOrderInfo->price}}" required placeholder="Current Price" readonly/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Any Other</label>
                        <div>
                            <input data-parsley-type="alphanum" type="text"class="form-control" value="{{$reshippingOrderInfo->anyOther}}" required placeholder="Any Other" readonly/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Instruction</label>
                        <div>
                            <input data-parsley-type="alphanum" value="{{$reshippingOrderInfo->instruction}}" type="text"class="form-control" required placeholder="Instruction" readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="heard">Box Type</label><br>
                        <?php
		                    $pkgCount=count($amount);
		                    for($i=0;$i<$pkgCount;$i++){
		                       
		                        $testAmountInfo=DB::table('inside_box_price')
								                  ->where('id',$amount[$i])
								                  ->get();
									foreach($testAmountInfo as $te){
							           echo $te->boxSize.' , ';
							        }
							         
		                    }
	                  
		            	?>
                    </div>
                    <div class="form-group">
                        <label for="heard">Delivery Type</label><br>
                        <?php 
                        	if ($reshippingOrderInfo->deliveryType=='1') {
                        		echo "Regular Delivery";
                        	}elseif ($reshippingOrderInfo->deliveryType=='2') {
                        		echo "Urgent Delivery";
                        	}
                        ?>
                    </div>
                  </form>
                   </div>
                  </div>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection