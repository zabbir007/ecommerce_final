@extends('layouts.reshipping')

@section('title') reshipping @endsection

@section('content')

<div class="container-fluid mt-3">
    <form action="{{route('saveUserInsideShipping')}}" class="parsley-examples" method="post">
      @csrf
      <?php 
        $message=Session::get('message');
        if($message){
        ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php
                    echo $message;
                    Session::put('message','');
                ?>
            </div>
        <?php
        }
        ?>
      <div class="row">
        <div class="col-md-6">
          <div class="card">
            <div class="card-header bg-danger py-2 text-white">
              <div class="card-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="true" aria-controls="cardCollpase2" class=""><i class="mdi mdi-minus"></i></a>
                <!-- <a href="#" data-toggle="remove"><i class="mdi mdi-close"></i></a> -->
              </div>
                <h4 class="card-title mb-0 text-center text-white">Shipping Charge</h4>
            </div>
          <div id="cardCollpase5" class="collapse show" style="">
            <div class="card-body">
              <div class="info-box text-center">
                <h3>Shipping Charge</h3>
              <div class="table-responsive">
                  <table class="table mb-0">
                      <thead>
                      <tr>
                          <th>Service Charge</th>
                          <th>Shipping Charge</th>
                          <th>additional photo change</th>
                          <th>Normal Delivery Time</th>
                          <th>Fast Delivery Time</th>
                          <th>Normal Delivery Price</th>
                          <th>Fast Delivery Price</th>
                          <th>Product Stay In House</th>
                          <th>Next Per Day Price</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                          <th>{{$insideShippingPrice->serviceCharge}}</th>
                          <td>{{$insideShippingPrice->shippingCharge}}</td>
                          <td>{{$insideShippingPrice->additionalPhotoCharge}}</td>
                          <td>{{$insideShippingPrice->normalDeliveryTime}}</td>
                          <td>{{$insideShippingPrice->fastDeliveryTime}}</td>
                          <td>{{$insideShippingPrice->normalDeliveryPrice}}</td>
                          <td>{{$insideShippingPrice->fastDeliveryPrice}}</td>
                          <td>{{$insideShippingPrice->stayHouse}}</td>
                          <td>{{$insideShippingPrice->perDayPrice}}</td>
                      </tr>
                      </tbody>
                  </table>
              </div>
            </div>
            </div>
          </div>
          </div>
          <!--Active Your Address------------------------------------------------>
          <div class="card">
            <div class="card-header bg-danger py-2 text-white">
              <div class="card-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                <a data-toggle="collapse" href="#cardCollpase4" role="button" aria-expanded="true" aria-controls="cardCollpase2" class=""><i class="mdi mdi-minus"></i></a>
                <!-- <a href="#" data-toggle="remove"><i class="mdi mdi-close"></i></a> -->
              </div>
                <h4 class="card-title text-center mb-0 text-white">Active Your Address</h4>
            </div>
          <div id="cardCollpase4" class="collapse show" style="">
            <div class="card-body">
              <div class="info-box text-center">
                <h3>Active Your Address</h3>
                <div class="table-responsive">
                  <table class="table mb-0">
                      <thead>
                      <tr>
                        <th>Ship To Cuntry</th>
                        <th>Address</th>
                        <th>Address 2</th>
                        <th>City</th>
                        <th>Street</th>
                        <th>Email</th>
                        <th>Phone</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                          <th scope="row">{{$insideActiveAddress->country}}</th>
                          <td>{{$insideActiveAddress->address1}}</td>
                          <td>{{$insideActiveAddress->address2}}</td>
                          <td>{{$insideActiveAddress->city}}</td>
                          <td>{{$insideActiveAddress->street}}</td>
                          <td>{{$insideActiveAddress->email}}</td>
                          <td>{{$insideActiveAddress->phone}}</td>
                      </tr>
                      </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          </div>
          <!--Total Price--------------------------------------------------->
          <div class="card">
            <div class="card-header bg-danger py-2 text-white">
                <div class="card-widgets">
                    <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                    <a data-toggle="collapse" href="#cardCollpase6" role="button" aria-expanded="true" aria-controls="cardCollpase2" class=""><i class="mdi mdi-minus"></i></a>
                    <!-- <a href="#" data-toggle="remove"><i class="mdi mdi-close"></i></a> -->
                </div>
                <h5 class="card-title mb-0 text-center text-white">Total Price</h5>
            </div>
            <div id="cardCollpase6" class="collapse show" style="">
                <div class="card-body">
                  <div class="info-box text-center">
                    <h3>Total Price</h3>
                    <div class="table-responsive">
                      <table class="table mb-0">
                          <thead>
                          <tr>
                            <th>Product</th>
                            <th>Price</th>
                          </tr>
                          </thead>
                          <tbody>
                          <tr>
                            <th scope="row">Services Charge</th>
                            <td>{{$insideShippingPrice->serviceCharge}}</td>
                            <input type="hidden" name="serviceCharge" id="serviceCharge" value="{{$insideShippingPrice->serviceCharge}}">
                            <input type="hidden" name="" id="showTotalAmount2" value="{{$insideShippingPrice->serviceCharge + $insideShippingPrice->shippingCharge}}">
                          </tr>
                          <tr>
                            <th scope="row">Sipping Charge</th>
                            <td>{{$insideShippingPrice->shippingCharge}}</td>
                            <input type="hidden" name="shippingCharge" id="shippingCharge" value="{{$insideShippingPrice->shippingCharge}}">
                          </tr>
                          <tr>
                            <th scope="row">Box Price</th>
                            <td id="boxPrice">
                              
                            </td>
                          </tr>
                          <tr>
                            <th scope="row">Delivery Price</th>
                            <td id="deliveryPrice">
                              
                            </td>
                          </tr>
                          <tr>
                            <th scope="row">Total</th>
                            <td id="showTotal">

                            </td>
                            
                            <div id="showTotalAmount">
                    
                            </div>
                          </tr>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>
            </div>
        </div>


        </div>
        <!------------------------------------------------------->

        <div class="col-md-6">


        <div class="card">
            <div class="card-header bg-danger py-2 text-white">
              <div class="card-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                <a data-toggle="collapse" href="#cardCollpase4" role="button" aria-expanded="true" aria-controls="cardCollpase2" class=""><i class="mdi mdi-minus"></i></a>
                <!-- <a href="#" data-toggle="remove"><i class="mdi mdi-close"></i></a> -->
              </div>
                <h4 class="card-title text-center mb-0 text-white">Select Swift Address</h4>
            </div>
          <div id="cardCollpase4" class="collapse show" style="">
            <div class="card-body">
              <div class="info-box text-center">
                <h3>Swift Address</h3>
                <div class="table-responsive">
                  <table class="table mb-0">
                      <thead>
                      <tr>
                        <th>Select</th>
                        <th>Address</th>
                        <th>Country</th>
                        <th>City</th>
                        <th>State</th>
                        <th>House No</th>
                        <th>Road No</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($sweetInfo as $sweet)
                      <tr>
 
                          <th scope="row"><input type="radio" name="sweet" checked value="{{$sweet->id}}" /></th>
                          <td>{{$sweet->sweetAddress}}</td>
                          <td>{{$sweet->sweetCountry}}</td>
                          <td>{{$sweet->sweetCity}}</td>
                          <td>{{$sweet->sweetState}}</td>
                          <td>{{$sweet->sweetHouse}}</td>
                          <td>{{$sweet->sweetRoad}}</td>
                      </tr>
                      @endforeach
                      </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>


          <div class="card">
          <div class="card-header bg-danger py-2 text-white">
              <div class="card-widgets">
                  <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                  <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="true" aria-controls="cardCollpase2" class=""><i class="mdi mdi-minus"></i></a>
                  <!-- <a href="#" data-toggle="remove"><i class="mdi mdi-close"></i></a> -->
              </div>
              <h5 class="card-title mb-0 text-center text-white">Order Information</h5>
          </div>
          <div id="cardCollpase7" class="collapse show" style="">
              <div class="card-body">
                <div class="card-box">

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label>Order Number</label>
                        <input type="text" class="form-control" name="orderNumber" required placeholder="Order Number"/>
                    </div>
                    <div class="form-group">
                        <label>Item Name</label>
                        <input type="text" class="form-control" name="itemName" required placeholder="Item Name"/>
                    </div>

                    <div class="form-group">
                        <label>Item ULR</label>
                        <div>
                            <input type="text" class="form-control" name="itemUrl" placeholder="Item ULR"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Size</label>
                        <div>
                            <input type="text" class="form-control" name="size" required placeholder="Size"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Quanty</label>
                        <div>
                            <input data-parsley-type="digits" type="text" name="quantity" class="form-control" required placeholder="Quanty"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Color</label>
                        <div>
                            <input type="text"class="form-control" name="color" required placeholder="Color"/>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-6">

                    <!-- <div class="card-box"> -->
                    <div class="form-group">
                        <label>Price</label>
                        <div>
                            <input type="text"class="form-control" name="price" required placeholder="Current Price"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Any Other</label>
                        <div>
                            <input data-parsley-type="alphanum" name="anyOther" type="text"class="form-control" required placeholder="Any Other"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Instruction</label>
                        <div>
                            <input data-parsley-type="alphanum" name="instruction" type="text"class="form-control" required placeholder="Instruction"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="heard">Select Box</label><br>
                         @foreach($insideBoxPrice as $inside)
                          <input class="my-activity" type="checkbox" name="box[]" value="{{$inside->id}}"> {{$inside->boxSize.' Price'.'= '.$inside->boxPrice}}<br/>
                         @endforeach
                    </div>
                    <div class="form-group">
                        <label for="heard">Select Delivery Type</label><br>
                        <input class="my-activity2" type="radio" name="normal" value="{{$insideShippingPrice->normalDeliveryPrice}}">NormalDelivery <br/>
                        <input class="my-activity2" type="radio" name="normal" value="{{$insideShippingPrice->fastDeliveryPrice}}">FastDelivery
                    </div>
                    <div class="form-group mb-0">
                        <div>
                             
                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                Cancel
                            </button>
                        </div>
                    </div>
                  </form>
                   </div>
                  </div>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

@endsection