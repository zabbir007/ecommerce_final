<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('user/css/normalize.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/main.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/responsive.css')}}">
		    <link rel="stylesheet" href="{{asset('user/css/bootstrap.min.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    </head>
    <body>
  <!--nav-menu------------------------------------------------------------------->
  <section class="top-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <p>SHOP NOW & GET UP TO <span>65% OFF!</span>    Call Us:<span>+880 1960 403 393</span> </p>
        </div>
        <div class="col-md-6">
          <ul class="right-nav">
            <li> <a href="#"> <i class="fab fa-facebook-f"></i> </a> </li>
            <li> <a href="#"> <i class="fab fa-twitter"></i> </a> </li>
            <li> <a href="#"> <i class="fab fa-google-plus-g"></i> </a> </li>
            <li> <a href="#"> <i class="fab fa-youtube"></i> </a> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
    <!--main nav------------------------------------------------------------------->
    <section class="main-nav">
      <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="#"> <img src="images/Logo.png"class="img-fluid" alt="logo"> </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
           <ul class="navbar-nav  font-weight-bold">
            <li class="nav-item">
              <a class="nav-link " href="{{route('showAboutUs')}}">About Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " href="{{route('showContactUs')}}">Contact Us</a>
            </li>
          </ul>
        </div>
      </nav>

      
    </section>

<!--start body login page------------------------------------------------------------------->
<section class="login mt-5">
  <div class="container">
    <div class="row">
      <div class="col-md-4">

        <div class="login-container">
          <form method="post" action="{{route('reshippingUserLogin')}}">
            @csrf
             @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <?php 
              $message=Session::get('message');
              if($message){
              ?>
                  <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      <?php
                          echo $message;
                          Session::put('message','');
                      ?>
                  </div>
              <?php   
              }
              ?>
              <?php 
              $message=Session::get('messageVerifyR');
              if($message){
              ?>
                  <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      <?php
                          echo $message;
                          Session::put('messageVerifyR','');
                      ?>
                  </div>
              <?php   
              }
              ?>

              <?php 
              $message=Session::get('messageVerifyU');
              if($message){
              ?>
                  <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      <?php
                          echo $message;
                          Session::put('messageVerifyU','');
                      ?>
                  </div>
              <?php   
              }
              ?>

              <?php 
              $message=Session::get('messageVerifyS');
              if($message){
              ?>
                  <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <?php
                          echo $message;
                          Session::put('messageVerifyS','');
                      ?>
                  </div>
              <?php
              }
              ?>
            <img class="avatar" src="{{asset('user/images/avatar.svg')}}" alt="">
            <h2>Login</h2>
            <div class="input-div one focus">
              <div class="i">
                <i class="fas fa-user"></i>
              </div>
              <div >
                <input class="input" type="text" name="emailOrPhone" placeholder="UserEmail" value="" required>
              </div>
            </div>
            <div class="input-div two focus">
              <div class="i">
                <i class="fas fa-lock"></i>
              </div>
              <div>
                <input class="input" type="password"placeholder="Password" name="password" value="" required>
              </div>
            </div>
            <a href="#"> Forgot Password</a>
            <a href="#" class="text-decoration-none"> <input type="submit"class="log-btn" name="" value="Login"> </a>
          </form>
        </div>
      </div>
      <div class="col-md-4">
        <div class="img">
          <img src="{{asset('user/images/img.svg')}}"  alt="">
        </div>
      </div>
      <div class="col-md-4">
        <div class="login-container">
          <form method="post" action="{{route('reshippingUserRegister')}}">
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <?php 
              $message=Session::get('messagere');
              if($message){
              ?>
                  <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <?php
                          echo $message;
                          Session::put('messagere','');
                      ?>
                  </div>
              <?php
              }
              ?>

            <?php 
              $message=Session::get('messageWarning');
              if($message){
              ?>
                  <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      <?php
                          echo $message;
                          Session::put('messageWarning','');
                      ?>
                  </div>
              <?php   
              }
              ?>
            <img class="avatar" src="{{asset('user/images/avatar.svg')}}" alt="">
            <h2>Register</h2>
            <div class="input-div one focus">
              <div class="i">
                <i class="fas fa-user"></i>
              </div>
              <div >
                <input class="input" type="text" name="name" placeholder="Name" value="" required>
              </div>
            </div>
            <div class="input-div one focus">
              <div class="i">
                <i class="fas fa-envelope"></i>
              </div>
              <div >
                <input class="input" type="mail" name="email" placeholder="Email" value="" required>
              </div>
            </div>


            <div class="input-div one focus">
              <div class="i">
                <i class="fas fa-mobile-alt"></i>
              </div>
              <div >
                <input class="input" type="number" name="phone"placeholder="Phone" value="" required>
              </div>
            </div>

            <div class="input-div two focus">
              <div class="i">
                <i class="fas fa-lock"></i>
              </div>
              <div>
                <input class="input" type="password"placeholder="Password" name="password" value="" required>
              </div>
            </div>

            <div class="input-div two focus">
              <div class="i">
                <i class="fas fa-lock"></i>
              </div>
              <div>
                <input class="input" type="password"placeholder="Retype password" name="rePassword" value="" required>
              </div>
            </div>
            <!-- <a href="#"> Forgot Password</a> -->
             <a href="#" class="text-decoration-none"> <input type="submit"class="log-btn" name="" value="Sign me up"> </a>
          </form>
        </div>
      </div>
    </div>
  </div>

</section>

<!--scrollup------------------------------------------------------------------->
<section class="scroll-top">
   <a href="#" class="scrollup"><i class="fas fa-chevron-up"></i></a>
</section>
<!--FOOTER------------------------------------------------------------------->
<footer class="site-footer mt-5">
   <div class="container-fluid">
     <div class="row">
       <div class="col-sm-12 col-md-3">
         <h6>Customer Care</h6>
         <ul class="footer-links">
           <li> <a href="#">Help Center</a> </li>
           <li> <a href="#">How to Buy</a> </li>
           <li> <a href="#">Track Your Order</a> </li>
           <li> <a href="#">Returns & Refunds</a> </li>
           <li> <a href="#">Contact Us</a> </li>

         </ul>

         <ul class="social-icons">
           <li> <a class="facebook" href="#"> <img src="images/1.png" alt="footer-img"> </a> </li>
           <li> <a class="twitter" href="#"> <img src="images/2.png" alt="footer-img"> </a> </li>
           <li> <a class="dribbble" href="#"> <img src="images/3.png" alt="footer-img"> </a> </li>
           <li> <a class="linkedin" href="#"> <img src="images/4.png" alt="footer-img"> </a> </li>
         </ul>
       </div>

       <div class="col-xs-6 col-md-3">
         <h6>Categories</h6>
         <ul class="footer-links">
           <li> <a href="#">C</a> </li>
           <li> <a href="#">UI Design</a> </li>
           <li> <a href="#">PHP</a> </li>
           <li> <a href="#">Java</a> </li>
           <li> <a href="#">Android</a> </li>
           <li> <a href="#">Templates</a> </li>
         </ul>
       </div>

       <div class="col-xs-6 col-md-3">
         <h6>Quick Links</h6>
         <ul class="footer-links">
           <li> <a href="#">About Us</a> </li>
           <li> <a href="#">Contact Us</a> </li>
           <li> <a href="#">Contribute</a> </li>
           <li> <a href="#">Privacy Policy</a> </li>
           <li> <a href="#">Sitemap</a> </li>
         </ul>
       </div>

       <div class="col-xs-6 col-md-3">
         <div class="left-content">
          <h3 class="title">Subscribe Our Newsletter</h3>
    			<div class="description">Sign up to our newsletter to get updates &amp; offers along with product support and new inventory.</div>
            <p>Email Address:</p>
			  </div>
         <form class="needs-validation" novalidate>
        <div class="form-row">
          <input type="text" class="form-control subscribe" id="validationCustom03" placeholder="" required>
          <div class="subscribe-btn ml-2">
            <button class="btn btn-primary" type="submit">Subscribe</button>
          </div>
        </form>
       </div>

       <div class="rounded-social-buttons">
          <a class="social-button facebook" href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a>
          <a class="social-button twitter" href="https://www.twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a>
          <a class="social-button linkedin" href="https://www.linkedin.com/" target="_blank"><i class="fab fa-linkedin"></i></a>
          <a class="social-button youtube" href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube"></i></a>
          <a class="social-button instagram" href="https://www.instagram.com/" target="_blank"><i class="fab fa-instagram"></i></a>
      </div>
     </div>
     <hr>
   </div>
   <div class="container-fluid">
     <div class="row">
       <div class="col-md-8 col-sm-6 col-xs-12">
         <div class="store-img">
           <!-- <a href="#"> <img src="images/apple.jpg" class="img-fluid" alt="footer-img"> </a>
           <a href="#"> <img src="images/google.jpg" class="img-fluid"> </a> -->
         </div>
       </div>

       <div class="col-md-4 col-sm-6 col-xs-12">

       </div>
     </div>
   </div>
</footer>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="{{asset('user/js/vendor/modernizr-3.5.0.min.js')}}"></script>
  <script src="{{asset('user/js/main.js')}}"></script>
  <script src="{{asset('user/js/jquery-3.4.1.min.js')}}"></script>
  <script src="{{asset('user/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('user/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('user/js/wow.min.js')}}"></script>
  <script>
  new WOW().init();
  </script>
</body>
</html>
