@extends('layouts.reshipping')

@section('title') reshipping @endsection

@section('content')

<!-- Start Content-->
<div class="container-fluid">

  <section class="checkout">
<!-- <div class="container"> -->
  <div class="what-we">
    <h1 class="text-center wow bounceInLeft data-wow-duration="2s" data-wow-delay=".2s"">Inside Shipping<span>Address</span> </h1>
  </div>
  <form class="needs-validation" novalidate method="post" action="{{route('updateInsideShippingAddress')}}">
  	@csrf
  	@if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <?php 
              $message=Session::get('message');
              if($message){
              ?>
                  <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <?php
                          echo $message;
                          Session::put('message','');
                      ?>
                  </div>
              <?php
              }
              ?>

            <?php 
              $message=Session::get('messageWarning');
              if($message){
              ?>
                  <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      <?php
                          echo $message;
                          Session::put('messageWarning','');
                      ?>
                  </div>
              <?php   
              }
              ?>
  <div class="inside-form">
  <div class="row">
    <div class="col-md-6">
        <div class="form-row">
          <div class="col-md-12 mb-3">
            <label for="validationCustom02"class="check-name">Primary Address <span>*</span></label>
            <input type="text" name="address1" value="{{$singleAddressInfo->address1}}" class="form-control" id="autocomplete" onFocus="geolocate()" placeholder="Address"  required>
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
          <div class="col-md-12 mb-3">
            <label for="validationCustom02"class="check-name">Secondary Address<span></span></label>
            <input type="text" name="address2" value="{{$singleAddressInfo->address2}}" class="form-control" id="autocomplete2" placeholder="Address" >
            <input class="form-control" id="street_number" disabled="true" type="hidden">
            <input class="form-control" id="route" disabled="true" type="hidden">
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
          <div class="col-md-12 mb-3">
            <label for="validationCustom01" class="check-name">Ship To Cuntry <span>*</span> </label>
            <input type="text" name="country" value="{{$singleAddressInfo->country}}" class="form-control" id="country" placeholder="Country"required >
            <div class="invalid-feedback">
              Please provide a valid city.
            </div>
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
          
          <div class="col-md-12 mb-3">
            <label for="validationCustom03"class="check-name">City <span>*</span></label>
            <input type="text" name="city" value="{{$singleAddressInfo->city}}" class="form-control" id="locality" placeholder="City"required >
            <div class="invalid-feedback">
              Please provide a valid city.
            </div>
          </div>

          <div class="col-md-12 mb-3">
            <label for="validationCustom05"class="check-name">State<span>*</span></label>
            <input type="text" name="street" value="{{$singleAddressInfo->street}}" class="form-control" id="administrative_area_level_1" placeholder="State" required>
          </div>

          

        <div class="form-group">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
            <label class="form-check-label" for="invalidCheck">
              Create an account?
            </label>
            <div class="invalid-feedback">
              You must agree before submitting.
            </div>
          </div>
        </div>
      <!-- <button class="btn btn-primary" type="submit">Submit form</button> -->
    </div>
  </div>
    <div class="col-md-6">
      <div class="col-md-12 mb-3">
        <label for="validationCustom05"class="check-name">Zip Code<span>*</span></label>
        <input type="text" name="town" value="{{$singleAddressInfo->town}}" class="form-control" id="postal_code" placeholder="Zip Code" required>
      </div>
      <div class="col-md-12 mb-3">
        <label for="validationCustom04"class="check-name">COMPANY NAME (OPTIONAL)</label>
        <input type="text" name="company" value="{{$singleAddressInfo->company}}" class="form-control" id="validationCustom04" placeholder="COMPANY NAME">
      </div>
      <div class="col-md-12 mb-3">
        <label for="validationCustom05"class="check-name">PHONE  <span>*</span></label>
        <input type="text" name="phone" class="form-control" value="{{$singleAddressInfo->phone}}" id="validationCustom05" placeholder="PHONE " required>
      </div>
      <div class="col-md-12 mb-3">
        <label for="validationCustom05"class="check-name">EMAIL ADDRESS   <span>*</span></label>
        <input type="text" class="form-control" name="email" value="{{$singleAddressInfo->email}}" id="validationCustom05" placeholder="EMAIL ADDRESS" required>
      </div>
      <div class="col-md-12 mb-3">
        <label for="validationCustom01" class="check-name">Change Status <span>*</span> </label>
        <select class="form-control" name="status" id="validationCustom05" required>
            
          <option value="1" <?php if($singleAddressInfo->status==1)echo "selected"; ?> >Active</option>
          <option value="0" <?php if($singleAddressInfo->status==0)echo "selected"; ?> >De-Active</option> 
        </select>
        <div class="valid-feedback">
          Looks good!
        </div>
      </div>
      <input type="hidden" name="id" value="{{$id}}">

      <div class="checkout-btn mt-2 mb-2">
        <button class="btn btn-primary" type="submit">Update</button>
      </div>
    </div>

  </div>
</form>
</div>

@endsection