<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('user/css/normalize.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/main.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/responsive.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/bootstrap.min.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    </head>
    <body>
      <section class="top-header">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6">
              <p class="ml-5">Shop and Ship anywhere in the world! <span>Wellcome to Ushopnship!</span>    Need Help? Call Us:<span>  01841-167177,01841-167178</span> </p>
            </div>
            <div class="col-md-6">
              <ul class="right-nav mr-5">
                <li> <a href="#"> <i class="fab fa-facebook-f"></i> </a> </li>
                <li> <a href="#"> <i class="fab fa-twitter"></i> </a> </li>
                <li> <a href="#"> <i class="fab fa-youtube"></i> </a> </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
        <!--main nav------------------------------------------------------------------->
<section class="main-nav">
  <nav class="navbar navbar-expand-lg navbar-light">
    <?php 
      $companyInfo=DB::table('company_setting')
                        ->where('status',1)
                        ->first();
    ?>
    <a class="navbar-brand allpadding" href="{{route('welcome')}}"> <img src="{{$companyInfo->companyLogo}}"class="img-fluid" alt="logo"> </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav  font-weight-bold">
          <li class="nav-item active">
            <a class="nav-link" href="{{route('welcome')}}">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{route('showStore')}}">Store</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{route('showAboutUs')}}">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="{{route('showContactUs')}}">Contact Us</a>
          </li>
        </ul>
        <ul class="main-nav-right ml-auto">
           <?php 
            if (Session::get('userId')) {
          ?>
          <li>
            <div class="btn-group dropleft">
              <a class="btn dropdown-toggle" href="profile.html" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i> Profile
              </a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="{{route('userProfile')}}">Profile</a>
                <a class="dropdown-item" href="{{route('userLogout')}}">Logout</a>
              </div>
            </div>
          </li>
          <?php
            }else{
          ?>
            <li> <a href="{{route('showUserLogin')}}"> Login or  </a> </li>
            <li> <a href="{{route('showUserLogin')}}"> Register </a> </li>
          <?php 
            }
          ?>
        </ul>
      </div>
  </nav>

  <div class="nav-2 allpadding">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2">
          <div class="accordion-menu">
            <div id="accordion1">
              <div class="card card-menu">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                      <a href="" class="btn btn-link side-menu-btn btn-link d-block accordionClick" id="accordionClick" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fas fa-bars"></i>  all Departments <i class="fas fa-chevron-down"></i></a>
                  </h5>
                </div>
                <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordion1">
                  <div class="card-body card-body-menu-popup">
                    <div class='cssmenuvv'>
                    <ul>
                         <li class='active'> <a href='index.html'> <span>Home</span> </a> </li>
                         @foreach($departmentInfo as $department)
                        <li class='has-sub'><a href='#'><span>{{$department->department_type}}</span></a>
                            <ul>
                              @foreach($categoryInfo as $category)
                               <li class='has-sub'><a href='#'><span>{{$category->category_type}}</span></a>
                                  <ul>
                                    <?php 
                                      $subCat=DB::table('sub_category')
                                                ->where('sub_category.department_id',$department->id)
                                                ->where('sub_category.category_id',$category->id)
                                                ->get();
                                    ?>
                                    @foreach($subCat as $sub)
                                     <li><a href="{{route('searchProductSidebar',[$sub->id,$department->id,$category->id])}}"><span>{{$sub->subCategory_type}}</span></a></li>
                                     @endforeach
                                  </ul>
                               </li>
                              @endforeach
                            </ul>
                         </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <form method="post" action="{{route('searchProductByProductName')}}">
              @csrf
              <div class="search">
                <div class="opction">
                  <select name="departmentId">
                    @foreach($departmentInfo as $department)
                    <option value="{{$department->id}}">{{$department->department_type}}</option>
                    @endforeach
                  </select>
                </div>
                <input type="text" class="searchTerm" name="productName" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                 <i class="fa fa-search"></i>
                </button>
              </div>
            </form>
        </div>
        <div class="col-md-4">
          <div class="manu">
            <ul>
                <li>
                  <a href="#"> <i class="fas fa-truck"></i> <span class="text-track">Track Your Order</span> </a>
                </li>
                <!-- <li>
                   <a href="#"> <i class="far fa-heart"></i> <span class="count"><?php //echo Cart::instance('wishlist')->count(); ?></span> </a>
                </li> -->
                <li>
                  <a href="{{route('showCartPage')}}"> <i class="fas fa-cart-plus"></i> <span class="count" id="theCount"><?php echo Cart::count();?></span> </a>
                  <input type="hidden" id="hiddenVal" value="<?php echo Cart::count();?>">
                </li>
              </ul>
          </div>
        </div>
      </div>
      </div>
    </div>
</section>
<section class="shipping-banner mt-3">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="banner-text">
          <h2>Reshipping System</h2>
          <p>A Nice And Safe Way. International Shipping And Local Shipping (Bangladesh Only)</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!--reshipping-box1-------------------------------->
<section class="reshipping-box1 mt-5">
  <div class="container">
    <div class="row">
      <div class="col-md-3">

      </div>
      <div class="col-md-6">
        <h3 class="re-border py-2 px-2 mb-5 text-center bg-danger">Get Your Order In Bangladesh</h3>
      </div>
      <div class="col-md-3">

      </div>
    </div>

    <div class="row">
    <div class="col-md-5 mb-5">
      <div class="card re-box bg-dark text-white">
        <div class="card-body">
          <h5 class="card-title"></h5>
          <p class="card-text">Create an account and get multiple address from Dhaka.</p>
        </div>
      </div>
    </div>
    <div class="col-md-2">
      <div class="arrow-top  arrow-down text-center">
        <img src="{{asset('user/images/down.png')}}" class="img-fluid" alt="">
      </div>
      <div class="arrow-top dis-off">
        <img src="{{asset('user/images/Red-Arrow.png')}}" class="img-fluid" alt="">
      </div>
    </div>
    <div class="col-md-5 mb-5">
      <div class="card re-box bg-success text-white">
        <div class="card-body">
          <h5 class="card-title"></h5>
          <p class="card-text">Any E-commerce Website In Bangladesh You Can Using Multiple Address For Shipping.</p>

        </div>
      </div>
      <div class="arrow-top dis-off text-center">
        <img src="{{asset('user/images/down.png')}}" class="img-fluid" alt="">
      </div>
      <div class="arrow-top arrow-down text-center">
        <img src="{{asset('user/images/down.png')}}" class="img-fluid" alt="">
      </div>
    </div>
    <div class="col-md-5 mt-3">
      <div class="card re-box bg-danger text-white">
        <div class="card-body">
          <h5 class="card-title"></h5>
          <p class="card-text">We Are Accept Your Shipment And safely We Setup In Multple Address, Then We Infrom You.</p>

        </div>
      </div>
    </div>
    <div class="col-md-2">
      <div class="arrow-top dis-off">
        <img src="{{asset('user/images/Red-Arrow_left.png')}}" class="img-fluid" alt="">
      </div>
      <div class="arrow-top dis-off arrow-down text-center">
        <img src="{{asset('user/images/down.png')}}" class="img-fluid" alt="">
      </div>

    </div>
    <div class="col-md-5 mt-3">
      <div class="card re-box bg-warning text-white">
        <div class="card-body">
          <h5 class="card-title"></h5>
          <p class="card-text">Order Us For Your Local Or International Shipping and Pay Us. Then We Will Complete Your Order.</p>

        </div>
      </div>
    </div>
    <div class="col-md-3">

    </div>
    <div class="col-md-5 mt-5">
      <div class="card re-box bg-info text-white">
        <div class="card-body">
          <h5 class="card-title"></h5>
          <p class="card-text">First 1,000 Coustomer Will Get Free Multiple Address.</p>

        </div>
      </div>
    </div>
    <div class="col-md-3">

    </div>
  </div>
  </div>

</section>


<section class="footer-1">
  <div class="container">
    <div class="footer-header">
        <h4>Need help? Call our award-winning support team 24/7 at +880 2-9511236</h4>
    </div>
  </div>
</section>
<!--FOOTER------------------------------------------------------------------->
<footer class="site-footer">
   <div class="container-fluid">
     <div class="row">
       <div class="col-md-9">
         <div class="row">
           <div class="col-md-3 col-12">
            <?php 
              $companyInfo=DB::table('company_setting')
                                ->where('status',1)
                                ->first();
            ?>
             <h6 class=" uShopnShip"> {{$companyInfo->companyName}}</h6>
             <ul class="footer-links ">
               <li> <strong>Visit Us</strong> </li>
               <li>{{$companyInfo->companyAddress}}</li>
               <li> <strong>Call Us 24/7</strong>  </li>
               <li> {{$companyInfo->companyPhone}}</li>
               <li> <strong>Drop us a line</strong> </li>
               <li>{{$companyInfo->companyEmail}}</li>
               
               <li class="mb-2"> Copyright 2019 | {{$companyInfo->companyName}} </li>
             </ul>

           </div>
           <div class="col-md-3 col-6 help">
             <h6>Help & Support</h6>
             <ul class="footer-links">
               <li> <a href="{{route('showFaqPage')}}">FAQ’s</a> </li>
               <li> <a href="{{route('showForgetPassword')}}">Lost password?</a> </li>
               <li> <a href="{{route('showSupportPage')}}">Support & Service</a> </li>
               <li> <a href="{{route('showReportPage')}}">Report An Issue</a> </li>
             </ul>
           </div>
           <div class="col-md-3 col-6 information">
             <h6>Information</h6>
             <ul class="footer-links">
               <li> <a href="{{route('showAboutUs')}}">About Us</a> </li>
               <li> <a href="{{route('showContactUs')}}">Contact Us</a> </li>
               <li> <a href="{{route('showStoreLocationPage')}}">Store Locator</a> </li>
             </ul>
           </div>

           <div class="col-md-3 col-6 legal">
             <h6>Legal</h6>
             <ul class="footer-links">
               <li> <a href="{{route('showPrivacyPage')}}">Privacy Policy</a> </li>
               <li> <a href="{{route('showTermsPage')}}">Terms Of Service</a> </li>
               <li> <a href="{{route('showWarrantyPage')}}">Warranty Policy</a> </li>
               <li> <a href="{{route('showRefundPage')}}">Refund & Return Policy</a> </li>
             </ul>
           </div>
         </div>
         <ul class="social-icons ">
           <li> <a class="facebook" href="#"> <img src="{{asset('user/images/1.png')}}" alt="footer-img"> </a> </li>
           <li> <a class="twitter" href="#"> <img src="{{asset('user/images/2.png')}}" alt="footer-img"> </a> </li>
           <li> <a class="dribbble" href="#"> <img src="{{asset('user/images/3.png')}}" alt="footer-img"> </a> </li>
           <li> <a class="linkedin" href="#"> <img src="{{asset('user/images/4.png')}}" alt="footer-img"> </a> </li>
         </ul>
       </div><!---end col-8-------------->

       <div class="col-md-3 subscribe">
         <div class="left-content">
          <h3 class="title">Subscribe Our Newsletter</h3>
          <div class="description">Sign up to our newsletter to get updates &amp; offers along with product support and new inventory.</div>
            <p>Email Address:</p>
        </div>
        <form method="post">
          <div class="form-row">
            <input type="text" class="form-control subscribe" name="subscribeUserEmail" placeholder="Your Email Address" required>
            <div class="subscribe-btn ml-2">
              <button class="btn btn-primary insertSubscribeUser" type="button">Subscribe</button>
          </div>
        </form>
       </div>

       <div class="rounded-social-buttons">
          <a class="social-button facebook" href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a>
          <a class="social-button twitter" href="https://www.twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a>
          <a class="social-button linkedin" href="https://www.linkedin.com/" target="_blank"><i class="fab fa-linkedin"></i></a>
          <a class="social-button youtube" href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube"></i></a>
          <a class="social-button instagram" href="https://www.instagram.com/" target="_blank"><i class="fab fa-instagram"></i></a>
       </div>
       </div>
     <hr>
   </div>
   <div class="container-fluid">
     <div class="row">
       <div class="col-md-8 col-sm-6 col-xs-12">
         <div class="store-img">
           <!-- <a href="#"> <img src="images/apple.jpg" class="img-fluid" alt="footer-img"> </a>
           <a href="#"> <img src="images/google.jpg" class="img-fluid"> </a> -->
         </div>
       </div>

       <div class="col-md-4 col-sm-6 col-xs-12">
       </div>
     </div>
   </div>
</footer>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="{{asset('user/js/vendor/modernizr-3.5.0.min.js')}}"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
  <script src="{{asset('user/js/main.js')}}"></script>
  <script src="{{asset('user/js/jquery-3.4.1.min.js')}}"></script>
  <script src="{{asset('user/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('user/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('user/js/progressbar.js')}}"></script>
  <script src="{{asset('user/js/wow.min.js')}}"></script>




  <script>
  new WOW().init();
  </script>
</body>
</html>
