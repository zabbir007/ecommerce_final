@extends('layouts.reshipping')

@section('title') E-Commerce @endsection

@section('content')

<div class="container-fluid">
    <div class="row">

        <div class="col-md-12">
            <div class="card">
              <div class="what-we">
                <h1 class="text-center mt-3"> <span>Show</span>Address </h1>
              </div>

                <div class="card-body">
                    <table id="basic-datatable" class="table dt-responsive nowrap">
                        <thead>
                            <tr style="background:#38414A; color:#fff;">
                                <th>Country</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>street</th>
                                <th>Zip Code</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>


                        <tbody>
                            @foreach($addressInfo as $address)
                            <tr>
                                <td>{{$address->country}}</td>
                                <td>{{$address->address1}}</td>
                                <td>{{$address->city}}</td>
                                <td>{{$address->street}}</td>
                                <td>{{$address->town}}</td>
                                <td>
                                    <?php if($address->status=='1'){ ?>
                                        <span class="badge badge-success">Active</span>
                                    
                                <?php }else{ ?>
                                    <span class="badge badge-danger">De-Active</span>

                                <?php }?>
                                </td>
                                <td>
                                  <a href="{{route('editInsideShippingAddress',[$address->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                  <a href="#" id="{{$address->id}}" class="action-icon btnuserInsideShippingDelete"> <i class="mdi mdi-delete"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            


                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->

    </div>
  </div>

  @endsection