@extends('layouts.reshipping')

@section('title') Reshipping @endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="what-we">
            <h1 class="text-center mt-3"> <span>Show</span>Shipping </h1>
          </div>
            <div class="card-body">
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr style="background:#38414A; color:#fff;">
                            <th>Order Number</th>
                            <th>Item Name</th>
                            <th>Delivery Type</th>
                            <th>Order Date</th>
                            <th>Delivery Date</th>
                            <th>Total Amount</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

						@foreach($reshippingInfo as $reshipping)
                        <tr>
                            <td>{{$reshipping->insideShippingOrderNumber}}</td>
                            <td>{{$reshipping->itemName}}</td>
                            <td><?php if($reshipping->deliveryType=='1'){echo "Regular Delivery";}elseif($reshipping->deliveryType=='2'){echo "Fast Delivery";} ?></td>
                            <td>{{$reshipping->createTime}}</td>
                            <td>{{$reshipping->deliveryDate}}</td>
                            <td>{{$reshipping->totalAmount}}</td>
                            <td> 

                            	<?php if($reshipping->status=='pending'){ ?> 
                            	<span class="badge badge-danger">Pending</span>
                            	<?php }elseif($reshipping->status=='active'){ ?>
								<span class="badge badge-warning">Active</span>
								<?php }elseif($reshipping->status=='delivery done'){?>
								<span class="badge badge-success">Delivery Done</span>
								<?php }elseif($reshipping->status=='cancel'){?>
								<span class="badge badge-danger">Cancel</span>
								<?php } ?>
                        	</td>
                            <td>
                            <a href="{{route('viewUserInsideShipping',[$reshipping->id])}}" class="action-icon"> <i class="mdi mdi-eye"></i></a>
                            </td>
                        </tr>
						@endforeach

                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection