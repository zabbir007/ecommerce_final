@extends('layouts.admin')

@section('title') Dashboard @endsection

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Product</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addProduct')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add New Product</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Product Name</th>
                            <th class="text-center">Subcategory</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($productInfo as $product)
                        <tr>
                            <td class="text-center">{{$product->productName}}</td>
                            <td class="text-center">{{$product->subCategory_type}}</td>
                            <td class="text-center">{{$product->price}}</td>
                            <td class="text-center">{{$product->status}}</td>
                            <td class="text-center">
                                <a href="{{route('editProduct',[$product->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnProductDelete" id="{{$product->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection