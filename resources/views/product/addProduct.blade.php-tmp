@extends('layouts.admin')

@section('title') Add Product @endsection

@section('content')

<!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">Add Product</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

<form method="post" action="{{route('saveProduct')}}" enctype="multipart/form-data" accept-charset="utf-8" data-parsley-validate="">
@csrf



<?php
$message = Session::get('message');
if ($message) {
	?>
	<div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	            </button>
	<?php
	echo $message;
	Session::put('message', '');
	?>
	</div>
	<?php
}
?>


<?php
$message = Session::get('messageWarning');
if ($message) {
	?>
	<div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
	                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	<?php
	echo $message;
	Session::put('messageWarning', '');
	?>
	</div>
	<?php
}
?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <h5 class="text-uppercase bg-light p-2 mt-0 mb-3">General</h5>

                <div class="form-group mb-3">
                    <label for="product-name">Product Name <span class="text-danger">*</span></label>
                    <input type="text" id="product-name" name="productName" class="form-control" placeholder="e.g : Apple iMac" required="">
                </div>

                <div class="form-group mb-3">
                    <label for="product-description">Product Description <span class="text-danger">*</span></label>
                    <textarea class="form-control" id="product-description" rows="5" placeholder="Please enter description" required="" name="productDescription"></textarea>
                </div>

                <div class="form-group mb-3">
                    <label for="product-summary">Product Summary <span class="text-danger">*</span></label>
                    <textarea class="form-control" id="product-description1" rows="3" placeholder="Please enter summary" required="" name="productSummary"></textarea>
                </div>

                <div class="form-group mb-3">
                    <label for="product-category">Department<span class="text-danger">*</span></label>
                    <select class="form-control select2" id="department_id" name="department_id" required="">
                        <option value="">Select</option>
                        @foreach($departmentInfo as $department)
                        <option value="{{$department->id}}">{{$department->department_type}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group mb-3">
                    <label for="product-category">Categories<span class="text-danger">*</span></label>
                    <select class="form-control select2" id="category_id" name="category_id" required="">
                        <option>Select</option>
                        @foreach($categoryInfo as $category)
                        <option value="{{$category->id}}">{{$category->category_type}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group mb-3" style="display: none;" id="subCategoryShow">



                </div>

                <div class="form-group mb-3">
                    <label for="product-price">Price <span class="text-danger">*</span></label>
                    <input type="text" onkeypress="return isNumberKey(event)" class="form-control" id="total_price" name="total_price" placeholder="Enter amount" required="">
                </div>

                <div class="form-group mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-widgets">
                                <a data-toggle="collapse" href="#cardCollpase1" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                            </div>
                            <h5 class="card-title mb-0">Add Discount(only input number)</h5>

                            <div id="cardCollpase1" class="collapse pt-3 show">
                                <label for="product-price">Discount %</label>
                                <input onkeypress="return isNumberKey(event)" type="text" class="form-control" id="discount_percent" placeholder="20" name="discount_percent">
                                <div id="discount_price"></div>


                            </div>
                        </div>
                    </div> <!-- end card-->
                </div>

                <div class="form-group mb-3">
                    <label class="mb-2">Status <span class="text-danger">*</span></label>
                    <br/>
                    <div class="radio form-check-inline">
                        <input type="radio" id="inlineRadio1" value="online" name="status" checked="">
                        <label for="inlineRadio1"> Online </label>
                    </div>
                    <div class="radio form-check-inline">
                        <input type="radio" id="inlineRadio2" value="offline" name="status">
                        <label for="inlineRadio2"> Offline </label>
                    </div>
                    <div class="radio form-check-inline">
                        <input type="radio" id="inlineRadio3" value="draft" name="status">
                        <label for="inlineRadio3"> Draft </label>
                    </div>
                </div>

                <div class="form-group mb-0">
                    <label>Comment</label>
                    <textarea class="form-control" rows="3" placeholder="Please enter comment" name="comment"></textarea>
                </div>
            </div> <!-- end card-box -->
        </div> <!-- end col -->

        <div class="col-lg-6">

            <div class="card-box">
                <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">Product Images</h5>
                <div class="mt-3">
                    <input required="" type="file" class="dropify" data-max-file-size="1M" name="image1" />
                    <p class="text-muted text-center mt-2 mb-0">Product Image-1 <span class="text-danger">*</span></p>
                </div>
                <div class="mt-3">
                    <input type="file" class="dropify" data-max-file-size="1M" name="image2"/>
                    <p class="text-muted text-center mt-2 mb-0">Product Image-2</p>
                </div>
                <div class="mt-3">
                    <input type="file" class="dropify" data-max-file-size="1M" name="image3"/>
                    <p class="text-muted text-center mt-2 mb-0">Product Image-3</p>
                </div>
            </div> <!-- end col-->

            <div class="card-box">
                <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">Meta Data</h5>

                <div class="form-group mb-3">
                    <label for="product-meta-title">Meta title</label>
                    <input type="text" class="form-control" id="product-meta-title" placeholder="Enter title" name="metaTitle">
                </div>

                <div class="form-group mb-3">
                    <label for="product-meta-keywords">Meta Keywords</label>
                    <input type="text" class="form-control" id="product-meta-keywords" placeholder="Enter keywords" name="metaKey">
                </div>

                <div class="form-group mb-0">
                    <label for="product-meta-description">Meta Description </label>
                    <textarea class="form-control" rows="5" id="product-meta-description" placeholder="Please enter description" name="metaDes"></textarea>
                </div>
            </div> <!-- end card-box -->

        </div> <!-- end col-->
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-12">
            <div class="text-left mb-3">
                <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save</button>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->
</form>
    <script type="text/javascript">
        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

    </script>

@endsection