@extends('layouts.admin')

@section('title') Edit Package Price @endsection

@section('content')

<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <form class="needs-validation" method="post" action="{{route('updateOutsideBoxPrice')}}" novalidate>
            @csrf
            	<?php 
                $message=Session::get('message');
                if($message){
                ?>
	                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                    <?php
	                        echo $message;
	                        Session::put('message','');
	                    ?>
	                </div>
                <?php
                }
                ?>

                <?php 
                $message=Session::get('messageWarning');
                if($message){
                ?>
                    <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        <?php
                            echo $message;
                            Session::put('messageWarning','');
                        ?>
                    </div>
                <?php   
                }
                ?>
               
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Package Name</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Please Enter Package Name" required name="boxSize" value="{{$boxInfo->boxSize}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Package Name.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Package Price</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Please Enter Package Price" required name="boxPrice" value="{{$boxInfo->boxPrice}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Package Price.
                    </div>
                </div>
                <input type="hidden" name="id" value="{{$id}}">

                <button class="btn btn-primary" type="submit">Update Package</button>
            </form>
        </div>
    </div>
</div>

@endsection