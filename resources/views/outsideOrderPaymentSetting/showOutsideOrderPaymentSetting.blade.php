@extends('layouts.admin')

@section('title') Show Outside Payment @endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Outside Order Payment</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addOutsideOrderPaymentSetting')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add New</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Service Charge</th>
                            <th class="text-center">Shipping Charge</th>
                            <th class="text-center">Insurance Amount</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($outsideOrderPaymentInfo as $outsideOrder)
                        <tr>
                            <td class="text-center">{{$outsideOrder->serviceCharge}}</td>
                            <td class="text-center">{{$outsideOrder->shippingCharge}}</td>
                            <td class="text-center">{{$outsideOrder->insuranceAmount}}</td>
                            <td class="text-center"><?php if($outsideOrder->status=='1'){echo "Active";}else if($outsideOrder->status=='0'){echo "De-Active";} ?></td>
                            <td class="text-center">
                                <a href="{{route('editOutsideOrderPaymentSetting',[$outsideOrder->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnOutsideOrderPaymentSettingDelete" id="{{$outsideOrder->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection