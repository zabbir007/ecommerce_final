@extends('layouts.admin')

@section('title') Edit Outside Order Payment @endsection

@section('content')

<form class="needs-validation" method="post" action="{{route('updateOutsideOrderPaymentSetting')}}" novalidate>
    @csrf
        <?php 
        $message=Session::get('message');
        if($message){
        ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php
                    echo $message;
                    Session::put('message','');
                ?>
            </div>
        <?php
        }
        ?>

        <?php 
        $message=Session::get('messageWarning');
        if($message){
        ?>
            <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                <?php
                    echo $message;
                    Session::put('messageWarning','');
                ?>
            </div>
        <?php   
        }
        ?>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Service Charge</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Please Enter Service Charge" required name="serviceCharge" value="{{$outsideOrderPaymentInfo->serviceCharge}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Service Charge.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Shipping Charge</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Please Enter Shipping Charge" required name="shippingCharge" value="{{$outsideOrderPaymentInfo->shippingCharge}}">
                    <input type="hidden" name="id" value="{{$id}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Shipping Charge.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Bute Cost</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Please Enter Bute Cost" required name="buteCost" value="{{$outsideOrderPaymentInfo->buteCost}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Bute Cost.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
               
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Fragile Sticker Amount</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Please Enter Fragile Sticker Amount" required name="fragileStickerAmount" value="{{$outsideOrderPaymentInfo->fragileStickerAmount}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Fragile Sticker Amount.
                    </div>
                </div>
                
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Cancel Amount/ Initial Amount</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="When Customer Cancel Any Order then which Amount Cut From Main Amount or Initial Payment for Order" required name="cancelAmount" value="{{$outsideOrderPaymentInfo->cancelAmount}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Cancel Amount.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Insurance Amount</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Insurance Amount For User" required name="insuranceAmount" value="{{$outsideOrderPaymentInfo->insuranceAmount}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Insurance Amount.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Insurance Range</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Insurance Range Like 100 or 200 it's also calculate for less than." required name="insuranceRange" value="{{$outsideOrderPaymentInfo->insuranceRange}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Insurance Range.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Status</label>
                    <select class="selectpicker" data-live-search="true"  data-style="btn-light" name="status">
                        <option value="">Select Status</option>
                        <option value="1" <?php if($outsideOrderPaymentInfo->status=='1'){echo "selected";} ?> >Active</option>
                        <option value="0" <?php if($outsideOrderPaymentInfo->status=='0'){echo "selected";} ?> >De-Active</option>
                    </select>
                </div>

                <button class="btn btn-primary" type="submit">Update Setting</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection