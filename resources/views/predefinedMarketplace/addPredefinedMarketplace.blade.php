@extends('layouts.admin')

@section('title') Predefined Marketplace @endsection

@section('content')

<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <form class="needs-validation" method="post" action="{{route('savePredefinedMarketplace')}}" novalidate>
            @csrf
            	<?php 
                $message=Session::get('message');
                if($message){
                ?>
	                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                    <?php
	                        echo $message;
	                        Session::put('message','');
	                    ?>
	                </div>
                <?php
                }
                ?>

                <?php 
                $message=Session::get('messageWarning');
                if($message){
                ?>
                    <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        <?php
                            echo $message;
                            Session::put('messageWarning','');
                        ?>
                    </div>
                <?php   
                }
                ?>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Market Place Name</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Amazon, Ali express etc.." required name="marketPlaceName">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Market Place Name.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Market Place Url</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Please Enter Parket Place Url" required name="marketPlaceUrl">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Market Place Url.
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Add Marketplace</button>
            </form>
        </div>
    </div>
</div>

@endsection