@extends('layouts.admin')

@section('title') Predefined Marketplace @endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Predefined Marketplace</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addPredefinedMarketplace')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add Marketplace</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Marketplace Name</th>
                            <th class="text-center">Marketplace Url</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($predefinedMarketplaceInfo as $predefined)
                        <tr>
                            <td class="text-center">{{$predefined->marketPlaceName}}</td>
                            <td class="text-center">{{$predefined->marketPlaceUrl}}</td>
                            <td class="text-center">
                                <a href="{{route('editPredefinedMarketplace',[$predefined->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnPredefinedMarketplaceDelete" id="{{$predefined->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection