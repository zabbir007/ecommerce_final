@extends('layouts.admin')

@section('title') Show Company Setting @endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Company Setting</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addCompanySetting')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add Company Setting</button>
                    </a>
                </p>
                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Comapany Name</th>
                            <th class="text-center">Company Email</th>
                            <th class="text-center">Company Phone</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($companySettingInfo as $companySetting)
                        <tr>
                            <td class="text-center">{{$companySetting->companyName}}</td>
                            <td class="text-center">{{$companySetting->companyEmail}}</td>
                            <td class="text-center">{{$companySetting->companyPhone}}</td>
                            <td class="text-center"><?php if($companySetting->status=='1'){echo "Active";}else if($companySetting->status=='0'){echo "De-Active";} ?></td>
                            <td class="text-center">
                                <a href="{{route('editCompanySetting',[$companySetting->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnCompanySettingDelete" id="{{$companySetting->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection