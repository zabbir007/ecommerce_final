@extends('layouts.admin')

@section('title') Show Box Price @endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Customer Message</h4>
                

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Name</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Subject</th>
                            <th class="text-center">Send Date</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($customerMessageInfo as $customerMessage)
                        <tr>
                            <td class="text-center">{{$customerMessage->userName}}</td>
                            <td class="text-center">{{$customerMessage->userEmail}}</td>
                            <td class="text-center">{{$customerMessage->mailSubject}}</td>
                            <td class="text-center">{{$customerMessage->createTime}}</td>
                            <td class="text-center">
                                <a href="{{route('viewCustomerMessage',[$customerMessage->id])}}" class="action-icon"> <i class="mdi mdi-eye"></i></a>
                                <a href="#" class="action-icon btnCustomerMessageDelete" id="{{$customerMessage->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection