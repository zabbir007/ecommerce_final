@extends('layouts.admin')

@section('title') Edit Company Setting @endsection

@section('content')

<form class="needs-validation" method="post" action="{{route('updateCompanySetting')}}" novalidate enctype="multipart/form-data">
    @csrf
        <?php 
        $message=Session::get('message');
        if($message){
        ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php
                    echo $message;
                    Session::put('message','');
                ?>
            </div>
        <?php
        }
        ?>

        <?php 
        $message=Session::get('messageWarning');
        if($message){
        ?>
            <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                <?php
                    echo $message;
                    Session::put('messageWarning','');
                ?>
            </div>
        <?php   
        }
        ?>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Company Name</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Tomattos" required name="companyName" value="{{$companySettingInfo->companyName}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Company Name.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Company Email</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="akash@tomattos.com" required name="companyEmail" value="{{$companySettingInfo->companyEmail}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Company Email.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Company Logo</label>
                    <input type="file" class="dropify" data-max-file-size="1M" name="image" data-default-file="{{asset( $companySettingInfo->companyLogo )}}"/>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Company Phone</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="01613722564" required name="companyPhone" value="{{$companySettingInfo->companyPhone}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Company Phone.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Company Address</label>
                    <textarea required="" class="form-control" placeholder="Dhaka,Bangladesh" name="companyAddress">{{$companySettingInfo->companyAddress}}</textarea>
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Company Address.
                    </div>
                </div>
                <input type="hidden" name="id" value="{{$companySettingInfo->id}}">
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Status</label>
                    <select class="selectpicker" data-live-search="true"  data-style="btn-light" name="status" id="">
                        <option value="">Select Status</option>
                        <option value="1" <?php if($companySettingInfo->status=='1'){echo "selected";} ?> >Active</option>
                        <option value="0" <?php if($companySettingInfo->status=='0'){echo "selected";} ?> >De-Active</option>
                    </select>
                </div>
                <button class="btn btn-primary" type="submit">Update Company Setting</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection