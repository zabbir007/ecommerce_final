@extends('layouts.admin')

@section('title') Add Company Setting @endsection

@section('content')

<form class="needs-validation" method="post" action="{{route('saveCompanySetting')}}" novalidate enctype="multipart/form-data">
    @csrf
        <?php 
        $message=Session::get('message');
        if($message){
        ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php
                    echo $message;
                    Session::put('message','');
                ?>
            </div>
        <?php
        }
        ?>

        <?php 
        $message=Session::get('messageWarning');
        if($message){
        ?>
            <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                <?php
                    echo $message;
                    Session::put('messageWarning','');
                ?>
            </div>
        <?php   
        }
        ?>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Company Name</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Tomattos" required name="companyName">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Company Name.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Company Email</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="akash@tomattos.com" required name="companyEmail">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Company Email.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Company Logo</label>
                    <input required="" type="file" class="dropify" data-max-file-size="1M" name="image" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Company Phone</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="01613722564" required name="companyPhone">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Company Phone.
                    </div>
                </div>
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Company Address</label>
                    <textarea required="" class="form-control" placeholder="Dhaka,Bangladesh" name="companyAddress"></textarea>
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Company Address.
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Add Company Setting</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection