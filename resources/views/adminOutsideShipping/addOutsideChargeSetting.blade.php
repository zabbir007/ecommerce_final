@extends('layouts.admin')

@section('title') Outside Shipping @endsection

@section('content')

<form class="needs-validation" method="post" action="{{route('saveOutsideChargeSetting')}}" novalidate>
    @csrf
        <?php 
        $message=Session::get('message');
        if($message){
        ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php
                    echo $message;
                    Session::put('message','');
                ?>
            </div>
        <?php
        }
        ?>

        <?php 
        $message=Session::get('messageWarning');
        if($message){
        ?>
            <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                <?php
                    echo $message;
                    Session::put('messageWarning','');
                ?>
            </div>
        <?php   
        }
        ?>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Outside Shipping Setting</h4>
                <p class="sub-header">Provide All Setting And Charge for Outside Shipping</p>
                <div class="form-group mb-3">
                    <label for="validationCustom01">Service Charge</label>
                    <input type="text" name="serviceCharge" class="form-control" id="validationCustom01" placeholder="Service Charge" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="form-group mb-3">
                    <label for="validationCustom01">Shipping Charge</label>
                    <input type="text" name="shippingCharge" class="form-control" id="validationCustom01" placeholder="Shipping Charge" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="form-group mb-3">
                    <label for="validationCustom01">Fragile Sticker Amount</label>
                    <input type="text" name="fragileStickerAmount" class="form-control" id="validationCustom01" placeholder="Fragile Sticker Amount" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>

                <div class="form-group mb-3">
                    <label for="validationCustom01">Insurance Amount</label>
                    <input type="text" name="insuranceAmount" class="form-control" id="validationCustom01" placeholder="Insurance Amount" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group mb-3">
                    <label for="validationCustom01">Insurance Range</label>
                    <input type="text" name="insuranceRange" class="form-control" id="validationCustom01" placeholder="Insurance Range" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>

                <div class="form-group mb-3">
                    <label for="validationCustom01">Fast Delivery Day</label>
                    <input type="text" name="fastDeliveryTime" class="form-control" id="validationCustom01" placeholder="Fast Delivery Day" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                
                <div class="form-group mb-3">
                    <label for="validationCustom01">Fast Delivery Price</label>
                    <input type="text" name="fastDeliveryPrice" class="form-control" id="validationCustom01" placeholder="Fast Delivery Price" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Add Setting</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection