@extends('layouts.admin')

@section('title') Custom Pending Order @endsection

@section('content')
<form method="post" action="{{route('updateAdminPendingCustomOrder')}}" enctype="multipart/form-data">
	@csrf
	<?php 
    $message=Session::get('message');
    if($message){
    ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php
                echo $message;
                Session::put('message','');
            ?>
        </div>
    <?php
    }
    ?>
<div class="row">
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-60"><font color="red">Customer Information</font></h4>
		                    <p class="mb-1"><b>Customer Name:</b> {{$pendingOrder->firstName}}</p>
		                    <p class="mb-0"><b>Customer Email:</b> {{$pendingOrder->email}}</p>
		                    <input type="hidden" name="customerEmail" value="{{$pendingOrder->email}}">
		                    <input type="hidden" name="orderNumber" value="{{$pendingOrder->orderNumber}}">
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Customer Phone:</b> {{$pendingOrder->countryCode.$pendingOrder->phone}}</p>
		            <p class="mb-0"><b>Customer Address:</b> {{$pendingOrder->address1}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Product Price:</b> <input type="text" class="form-control" value="{{$pendingOrder->productEstimatePrice}}" id="getProductPrice" name="updateProductPrice" required placeholder="Product Price"/></p>
                    <p class="mb-1"><b>Product Quantity:</b> <input type="text" class="form-control" value="{{$pendingOrder->productQuantity}}" id="getNumber" name="updateQuantity" required placeholder="Quantity"/></p>
                    <p class="mb-1"><b>Package Size:</b></p>
                    @foreach($packageInfo as $package)
						<input type="radio" name="packageSize" value="{{$package->id}}"> {{$package->boxSize}}
                    @endforeach
                    
		        </div>

		        <div class="col-sm-6">
		            <p class="mb-1"><b>Order Description:</b></p>
                    <p class="mb-0">
                    	<textarea required class="form-control" name="adminOrderDescription"></textarea>
                    </p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
    <div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-12">
		            <div class="card-body">
	                  <div class="info-box text-center">
	                    <h3>Total Price</h3>
	                    <div class="table-responsive">
	                      <table class="table mb-0">
	                          <thead>
	                          <tr>
	                            <th>Product</th>
	                            <th>Price</th>
	                          </tr>
	                          </thead>
	                          <tbody>
	                          
	                          <tr>
	                            <th scope="row">Product Price</th>
	                            <td id="productPrice">
	                            	
	                            </td>
	                            
	                          </tr>
	                         
	                          </tbody>
	                      </table>
	                    </div>
	                  </div>
	                </div>
		        </div>
		        <input type="hidden" name="id" value="{{$pendingOrder->id}}">
		        
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
		
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-30"><font color="red">Order Information</font></h4>
		                    <p class="mb-1"><b>Address:</b> {{$pendingOrder->bill_address1}}</p>
		                    <p class="mb-1"><b>Country:</b> {{$pendingOrder->bill_country}}</p>
		                    <p class="mb-1"><b>State:</b> {{$pendingOrder->bill_state}}</p>
		                    <p class="mb-1"><b>City:</b> {{$pendingOrder->bill_city}}</p>
		                    <p class="mb-1"><b>Zip:</b> {{$pendingOrder->bill_zip}}</p>
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
                    <p class="mb-1"><b>Product Link:</b><a href="{{$pendingOrder->productLink}}" target="_blank">Click Here</a></p>
                    <p class="mb-1"><b>Product Name:</b> {{$pendingOrder->productName}}</p>
                    <p class="mb-1"><b>Product Estimate Price:</b> {{$pendingOrder->productEstimatePrice}}</p>
                    <p class="mb-1"><b>Product Quantity:</b> {{$pendingOrder->productQuantity}}</p>
                    <p class="mb-1"><b>Initial Paid:</b> {{$pendingOrder->initialAmount}}</p>
                    <p class="mb-1"><b>Order Date:</b> {{$pendingOrder->orderDate}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
	<div class="col-md-12">
		<h4>Product Image</h4>
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        
		        
		        <div class="col-md-2">
			        <div class="grid-item grid-item--width2 transition">
			          <a href="{{asset( $pendingOrder->userImage1 )}}" target="_blank" data-lightbox="example-set">
			            <img  src="{{asset( $pendingOrder->userImage1 )}}" class="img-fluid" alt="">
			          </a>
			        </div>
			    </div>
		        <div class="col-md-2">
			        <div class="grid-item grid-item--width2 transition">
			          <a href="{{asset( $pendingOrder->userImage2 )}}" target="_blank" data-lightbox="example-set">
			            <img  src="{{asset( $pendingOrder->userImage2 )}}" class="img-fluid" alt="">
			          </a>
			        </div>
			    </div>
		        <div class="col-md-2">
			        <div class="grid-item grid-item--width2 transition">
			          <a href="{{asset( $pendingOrder->userImage3 )}}" target="_blank" data-lightbox="example-set">
			            <img  src="{{asset( $pendingOrder->userImage3 )}}" class="img-fluid" alt="">
			          </a>
			        </div>
			    </div>
		        
		        <div class="col-md-2">
			        <div class="grid-item grid-item--width2 transition">
			          <a href="{{asset( $pendingOrder->userImage4 )}}" target="_blank" data-lightbox="example-set">
			            <img  src="{{asset( $pendingOrder->userImage4 )}}" class="img-fluid" alt="">
			          </a>
			        </div>
			    </div>
		        
		         <div class="col-md-2">
			        <div class="grid-item grid-item--width2 transition">
			          <a href="{{asset( $pendingOrder->userImage5 )}}" target="_blank" data-lightbox="example-set">
			            <img  src="{{asset( $pendingOrder->userImage5 )}}" class="img-fluid" alt="">
			          </a>
			        </div>
			    </div>
		        <div class="col-md-2">
                   
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>

	

	<button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">Update Order</button>

</div>
</form>

@endsection