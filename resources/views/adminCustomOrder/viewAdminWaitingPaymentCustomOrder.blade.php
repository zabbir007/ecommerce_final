@extends('layouts.admin')

@section('title') Custom Waiting Payment Order @endsection

@section('content')

<div class="row">
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-60"><font color="red">Customer Information</font></h4>
		                    <p class="mb-1"><b>Customer Name:</b> {{$pendingOrder->firstName}}</p>
		                    <p class="mb-0"><b>Customer Email:</b> {{$pendingOrder->email}}</p>
		                    <input type="hidden" name="customerEmail" value="{{$pendingOrder->email}}">
		                    <input type="hidden" name="orderNumber" value="{{$pendingOrder->orderNumber}}">
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Customer Phone:</b> {{$pendingOrder->countryCode.$pendingOrder->phone}}</p>
		            <p class="mb-0"><b>Customer Address:</b> {{$pendingOrder->address1}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-60"><font color="red">Product Information</font></h4>
		                    <p class="mb-1"><b>Product Price:</b> {{$pendingOrder->productEstimatePrice}}</p>
		                    <p class="mb-0"><b>Product Quantity:</b> {{$pendingOrder->productQuantity}}</p>
		                    <input type="hidden" name="customerEmail" value="{{$pendingOrder->email}}">
		                    <input type="hidden" name="orderNumber" value="{{$pendingOrder->orderNumber}}">
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Package Size:</b> {{$pendingOrder->packageSize}}</p>
		            <p class="mb-0"><b>Order Description:</b> {{$pendingOrder->adminOrderDescription}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-60"><font color="red">User Send Information</font></h4>
		                    <p class="mb-1"><b>Additional Picture :</b> {{$pendingOrder->additionalPicture}}</p>
		                    <p class="mb-1"><b>Box Condition :</b> {{$pendingOrder->boxCondition}}</p>
		                    
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
		            <p class="mb-1"><b>Use Sticker :</b> <?php if($pendingOrder->useSticker=='1'){echo "Yes";}else{echo "No";} ?></p>
		            <p class="mb-1"><b>Personal use :</b> <?php if($pendingOrder->personalUse=='1'){echo "Yes";}else{echo "No";} ?></p>
		            <p class="mb-1"><b>Urgent Ship :</b> <?php if($pendingOrder->useUrgent=='1'){echo "Yes";}else{echo "No";} ?></p>
		            <p class="mb-1"><b>Shipping Type :</b> {{$pendingOrder->shippingSubCategory}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
    
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-6">
		            <div class="media">
		                <div class="media-body">
		                	<h4 class="mt-0 mb-2 font-30"><font color="red">Order Information</font></h4>
		                    <p class="mb-1"><b>Address:</b> {{$pendingOrder->bill_address1}}</p>
		                    <p class="mb-1"><b>Country:</b> {{$pendingOrder->bill_country}}</p>
		                    <p class="mb-1"><b>State:</b> {{$pendingOrder->bill_state}}</p>
		                    <p class="mb-1"><b>City:</b> {{$pendingOrder->bill_city}}</p>
		                    <p class="mb-1"><b>Zip:</b> {{$pendingOrder->bill_zip}}</p>
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6">
                    <p class="mb-1"><b>Product Link:</b><a href="{{$pendingOrder->productLink}}" target="_blank">Click Here</a></p>
                    <p class="mb-1"><b>Product Name:</b> {{$pendingOrder->productName}}</p>
                    <p class="mb-1"><b>Product Estimate Price:</b> {{$pendingOrder->productEstimatePrice}}</p>
                    <p class="mb-1"><b>Product Quantity:</b> {{$pendingOrder->productQuantity}}</p>
                    <p class="mb-1"><b>Initial Paid:</b> {{$pendingOrder->initialAmount}}</p>
                    <p class="mb-1"><b>Order Date:</b> {{$pendingOrder->orderDate}}</p>
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>
	<div class="col-md-6">
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        <div class="col-sm-12">
		            <div class="card-body">
	                  <div class="info-box text-center">
	                    <h3>Total Price</h3>
	                    <div class="table-responsive">
	                      <table class="table mb-0">
	                          <thead>
	                          <tr>
	                            <th>Product</th>
	                            <th>Price</th>
	                          </tr>
	                          </thead>
	                          <tbody>
	                          
	                          <tr>
	                            <th scope="row">Order Number</th>
	                            <td id="">
	                            	<span>{{$pendingOrder->orderNumber}}</span>
	                            </td>
	                            
	                          </tr>
	                          <tr>
	                            <th scope="row">Product Price</th>
	                            <td id="">
	                            	<span>{{$pendingOrder->adminProductFinalAmount}}</span>
	                            </td>
	                            
	                          </tr>

	                          

	                          <tr>
	                            <th scope="row">Insurance Price</th>
	                            <td id="">
	                            	<span>{{$pendingOrder->finalInsuranceAmount}}</span>
	                            </td>
	                            
	                          </tr>

	                          <tr>
	                            <th scope="row">Option Price</th>
	                            <td id="">
	                            	<span>{{$pendingOrder->finalTotalAmount}}</span>
	                            </td>
	                            
	                          </tr>

	                          <tr>
	                            <th scope="row">Shipping Cost</th>
	                            <td id="">
	                            	<span>{{$pendingOrder->adminShippingPrice}}</span>
	                            </td>
	                            
	                          </tr>

	                          <tr>
	                            <th scope="row">Our Commition</th>
	                            <td id="">
	                            	<span>{{$pendingOrder->adminCommitionPrice}}</span>
	                            </td>
	                            
	                          </tr>

	                          <tr>
	                            <th scope="row">Total Cost</th>
	                            <td id="">
	                            	<span>{{$pendingOrder->adminSetProductFinalAmount}}</span>
	                            </td>
	                            
	                          </tr>

	                          <tr>
	                            <th scope="row">Offer</th>
	                            <td id="">
	                            	<span>{{$pendingOrder->adminOfferPrice}}</span>
	                            </td>
	                            
	                          </tr>

	                          <tr>
	                            <th scope="row">Tracking Number</th>
	                            <td id="">
	                            	
	                            	<span>{{$pendingOrder->trackingNumber}}</span>
	                            </td>
	                            
	                          </tr>

	                          </tbody>
	                      </table>
	                    </div>
	                  </div>
	                </div>
		        </div>
		        <input type="hidden" name="id" value="{{$pendingOrder->id}}">
		        
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
		
	</div>
	<div class="col-md-12">
		<h4>Product Image</h4>
		<div class="card-box mb-2">
		    <div class="row align-items-center">
		        
		        <div class="col-md-2">
                   <img  src="{{asset( $pendingOrder->userImage1 )}}" width="300" height="240" alt="product Image"> 
		        </div>
		        <div class="col-md-2">
		            <img  src="{{asset( $pendingOrder->userImage2 )}}" width="300" height="240" alt="product Image">
		        </div>
		        <div class="col-md-2">
                    <img  src="{{asset( $pendingOrder->userImage3 )}}" width="300" height="240" alt="product Image">
		        </div>
		        <div class="col-md-2">
		            <img  src="{{asset( $pendingOrder->userImage4 )}}" width="300" height="240" alt="product Image">
		        </div>
		        <div class="col-md-2">
                    <img  src="{{asset( $pendingOrder->userImage5 )}}" width="300" height="240" alt="product Image">
		        </div>
		        <div class="col-md-2">
                   
		        </div>
		    </div> <!-- end row -->
		</div> <!-- end card-box-->
	</div>

</div>

@endsection