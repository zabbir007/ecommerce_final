@extends('layouts.admin')

@section('title') Show Inside Payment @endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Inside Order Payment</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addInsideOrderPaymentSetting')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add New</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Fast Delivery Day</th>
                            <th class="text-center">Fast Delivery Price</th>
                            <th class="text-center">Insurance Amount</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($insideOrderPaymentInfo as $insideOrder)
                        <tr>
                            <td class="text-center">{{$insideOrder->fastDeliveryTime}}</td>
                            <td class="text-center">{{$insideOrder->fastDeliveryPrice}}</td>
                            <td class="text-center">{{$insideOrder->insuranceAmount}}</td>
                            <td class="text-center"><?php if($insideOrder->status=='1'){echo "Active";}else if($insideOrder->status=='0'){echo "De-Active";} ?></td>
                            <td class="text-center">
                                <a href="{{route('editInsideOrderPaymentSetting',[$insideOrder->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnInsideOrderPaymentSettingDelete" id="{{$insideOrder->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection