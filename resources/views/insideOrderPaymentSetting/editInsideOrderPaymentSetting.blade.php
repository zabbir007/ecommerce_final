@extends('layouts.admin')

@section('title') Edit Inside Order Payment @endsection

@section('content')

<form class="needs-validation" method="post" action="{{route('updateInsideOrderPaymentSetting')}}" novalidate>
    @csrf
        <?php 
        $message=Session::get('message');
        if($message){
        ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php
                    echo $message;
                    Session::put('message','');
                ?>
            </div>
        <?php
        }
        ?>

        <?php 
        $message=Session::get('messageWarning');
        if($message){
        ?>
            <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                <?php
                    echo $message;
                    Session::put('messageWarning','');
                ?>
            </div>
        <?php   
        }
        ?>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Fast Delivery Price</label>
                    <input type="text" class="form-control" value="{{$insideOrderPaymentInfo->fastDeliveryPrice}}" id="validationTooltip01" placeholder="Please Enter Delivery Price" required name="fastDeliveryPrice">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Delivery Price
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Fast Delivery Day</label>
                    <input type="text" class="form-control" value="{{$insideOrderPaymentInfo->fastDeliveryTime}}" id="validationTooltip01" placeholder="Please Enter Delivery Day" required name="fastDeliveryTime">
                    <input type="hidden" name="id" value="{{$insideOrderPaymentInfo->id}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Delivery Day
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
               

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Fragile Sticker Amount</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Please Enter Fragile Sticker Amount" required name="fragileStickerAmount" value="{{$insideOrderPaymentInfo->fragileStickerAmount}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Fragile Sticker Amount.
                    </div>
                </div>
                
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Cancel Amount/ Initial Amount</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="When Customer Cancel Any Order then which Amount Cut From Main Amount or Initial Payment for Order" required name="cancelAmount" value="{{$insideOrderPaymentInfo->cancelAmount}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Cancel Amount.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Insurance Amount</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Insurance Amount For User" required name="insuranceAmount" value="{{$insideOrderPaymentInfo->insuranceAmount}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Insurance Amount.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Insurance Range</label>
                    <input type="text" class="form-control" id="validationTooltip01" placeholder="Insurance Range Like 100 or 200 it's also calculate for less than." required name="insuranceRange" value="{{$insideOrderPaymentInfo->insuranceRange}}">
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                    <div class="invalid-tooltip">
                        Please Enter Insurance Range.
                    </div>
                </div>

                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Status</label>
                    <select class="selectpicker" data-live-search="true"  data-style="btn-light" name="status">
                        <option value="">Select Status</option>
                        <option value="1" <?php if($insideOrderPaymentInfo->status=='1'){echo "selected";} ?> >Active</option>
                        <option value="0" <?php if($insideOrderPaymentInfo->status=='0'){echo "selected";} ?> >De-Active</option>
                    </select>
                </div>

                <button class="btn btn-primary" type="submit">Update Setting</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection