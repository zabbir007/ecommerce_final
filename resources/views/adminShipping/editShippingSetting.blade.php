@extends('layouts.admin')

@section('title') Shipping @endsection

@section('content')

<form class="needs-validation" method="post" action="{{route('updateShippingSetting')}}" novalidate>
    @csrf
        <?php 
        $message=Session::get('message');
        if($message){
        ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php
                    echo $message;
                    Session::put('message','');
                ?>
            </div>
        <?php
        }
        ?>

        <?php 
        $message=Session::get('messageWarning');
        if($message){
        ?>
            <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                <?php
                    echo $message;
                    Session::put('messageWarning','');
                ?>
            </div>
        <?php   
        }
        ?>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="product-description">How to Shipping<span class="text-danger">*</span></label>
                    <textarea class="form-control" id="product-description" rows="5" placeholder="Please Describe How To Shipping This Website" name="howShipping"><?php echo $shippingSettingInfo->howShipping; ?></textarea>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="product-description1">Shipping Address<span class="text-danger">*</span></label>
                    <textarea class="form-control" id="product-description1" rows="5" placeholder="Please enter Shipping Address" name="shippingAddress"><?php echo $shippingSettingInfo->shippingAddress; ?></textarea>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="validationTooltip01">Status</label>
                    <select class="selectpicker" data-live-search="true"  data-style="btn-light" name="status" id="category_id">
                        <option value="">Select Status</option>
                        <option value="1" <?php if($shippingSettingInfo->status=='1'){echo "selected";} ?> >Active</option>
                        <option value="0" <?php if($shippingSettingInfo->status=='0'){echo "selected";} ?> >De-Active</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="product-description">Description Inside Shipping<span class="text-danger">*</span></label>
                    <textarea class="form-control" id="product-description2" rows="5" placeholder="Please enter description of inside shipping" name="insideShipping"><?php echo $shippingSettingInfo->insideShipping; ?></textarea>
                </div>
                <div class="card-body">
                <div class="form-group position-relative mb-3">
                    <label for="product-description">Description Outside Shipping<span class="text-danger">*</span></label>
                    <textarea class="form-control" id="product-description3" rows="5" placeholder="Please enter description of outside shipping" name="outsideShipping"><?php echo $shippingSettingInfo->outsideShipping; ?></textarea>
                </div>
                <input type="hidden" name="id" value="{{$id}}">
            </div>
                <button class="btn btn-primary" type="submit">Update Shipping Setting</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection