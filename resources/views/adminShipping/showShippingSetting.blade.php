@extends('layouts.admin')

@section('title') Shipping @endsection

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Setting</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addShippingSetting')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add New</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Shipping Address</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($shippingSettingInfo as $shippingSetting)
                        <tr>
                            <td class="text-center"><?php echo $shippingSetting->shippingAddress ?></td>
                            <td class="text-center"><?php if($shippingSetting->status=='1'){echo "Active";}else if($shippingSetting->status=='0'){echo "De-Active";} ?></td>
                            <td class="text-center">
                                <a href="{{route('editShippingSetting',[$shippingSetting->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnShippingSettingDelete" id="{{$shippingSetting->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection