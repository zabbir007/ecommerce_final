@extends('layouts.admin')

@section('title') Dashboard @endsection

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Customer</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addCustomer')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add New Customer</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">First Name</th>
                            <th class="text-center">Last Name</th>
                            <th class="text-center">Address</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($customerInfo as $customer)
                        <tr>
                            <td class="text-center">{{$customer->firstName}}</td>
                            <td class="text-center">{{$customer->lastName}}</td>
                            <td class="text-center">{{$customer->address1}}</td>
                            <td class="text-center">{{$customer->email}}</td>
                            <td class="text-center"><?php if($customer->status=='1'){echo "Active";}else{echo "De-Active";} ?></td>
                            <td class="text-center">
                                <a href="{{route('adminEditCustomer',[$customer->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnAdminCustomerDelete" id="{{$customer->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection