@extends('layouts.admin')

@section('title') Add Customer @endsection

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript" ></script>
<form class="needs-validation parsley-examples" novalidate method="post" action="{{route('saveCustomer')}}" enctype="multipart/form-data" accept-charset="utf-8">
@csrf
	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<?php 
    $message=Session::get('message');
    if($message){
    ?>
        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php
                echo $message;
                Session::put('message','');
            ?>
        </div>
    <?php
    }
    ?>


    <?php 
    $message=Session::get('messageWarning');
    if($message){
    ?>
        <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            <?php
                echo $message;
                Session::put('messageWarning','');
            ?>
        </div>
    <?php   
    }
    ?>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Add New Customer</h4>
					<div class="form-group position-relative mb-3">
                        <label for="validationTooltip03">First Name<span class="text-danger">*</span></label>
                        <input type="text" name="firstName" class="form-control" id="validationTooltip03" placeholder="First Name" required="">
                        <div class="invalid-tooltip">
                            Please provide a valid First Name.
                        </div>
                    </div>
                    <div class="form-group position-relative mb-3">
                        <label for="validationTooltip04">Last Name<span class="text-danger">*</span></label>
                        <input type="text" name="lastName" class="form-control" id="validationTooltip04" placeholder="Last Name" required>
                        <div class="invalid-tooltip">
                            Please provide a valid Last Name.
                        </div>
                    </div>
                    <div class="form-group position-relative mb-3">
                        <label for="validationTooltip05">First Address<span class="text-danger">*</span></label>
                        <input type="text" id="autocomplete" onFocus="geolocate()"  name="address1" class="form-control" placeholder="First Address" required>
                        <div class="invalid-tooltip">
                            Please provide a valid Address.
                        </div>
                    </div>
                    <div class="form-group position-relative mb-3">
                        <label for="validationTooltip05">Second Address</label>
                        <input type="text" name="address2" class="form-control" id="autocomplete2" placeholder="Second Address">
                        <input class="form-control" id="street_number" disabled="true" type="hidden">
                        <input class="form-control" id="route" disabled="true" type="hidden">
                    </div>
                    <div class="form-group position-relative mb-3">
                        <label for="validationTooltip05">Country<span class="text-danger">*</span></label>
                        <select class="form-control field-validate" name="country">
                         <?php
                           $length = count($countries);
                              for ($i = 0; $i < $length; $i++) {
                         ?>
                         <option value="<?php echo $countries[$i]; ?>" ><?php echo $countries[$i]; ?></option>
                         <?php
                           }
                         ?>
                       </select>
                    </div>
                    <div class="form-group position-relative mb-3">
                        <label for="validationTooltip05">City<span class="text-danger">*</span></label>
                        <input type="text" name="city" id="locality" class="form-control" placeholder="City" required="">
                        <div class="invalid-tooltip">
                            Please provide a valid City.
                        </div>
                    </div>
                    <div class="form-group position-relative mb-3">
                        <label for="validationTooltip05">State<span class="text-danger">*</span></label>
                        <input type="text" name="state" id="administrative_area_level_1" class="form-control" placeholder="State" required="">
                        <div class="invalid-tooltip">
                            Please provide a valid State.
                        </div>
                    </div>
                    

            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                    <div class="form-group position-relative mb-3">
                        <label for="validationTooltip05">Zip Code<span class="text-danger">*</span></label>
                        <input type="number" id="postal_code" name="zip" class="form-control" placeholder="Zip Code" required="">
                        <div class="invalid-tooltip">
                            Please provide a valid Zip Code.
                        </div>
                    </div>

					<div class="form-group position-relative mb-3">
                        <label for="validationTooltip05">Phone Number<span class="text-danger">*</span></label>
                        <input type="number" id="adminCheckCustomerPhone" name="phone" class="form-control" placeholder="Phone Number" required="">
                        <div class="invalid-tooltip">
                            Please provide a valid Phone Number.
                        </div>
                    </div>
                    <div class="form-group position-relative mb-3">
                        <label for="validationTooltip05">Customer Email<span class="text-danger">*</span></label>
                        <input type="text" id="adminCheckCustomerEmail" name="email" class="form-control" placeholder="Email" required="">
                        <div class="invalid-tooltip">
                            Please provide a valid Email.
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="pass1" class="">Password<span class="text-danger">*</span></label>
                        <div class="">
                            <input id="pass1" type="password" placeholder="Password" required
                                   class="form-control" name="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pass2" class="">Confirm Password
                            <span class="text-danger">*</span></label>
                        <div class="">
                            <input data-parsley-equalto="#pass1" type="password" required
                                   placeholder="Password" class="form-control" id="pass2">
                        </div>
                    </div>
                    
                    
                    <button class="btn btn-primary" type="submit" id="customerRegister">Save Customer</button>
            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col-->

</div>
<!-- end row -->
</form>


<script type="text/javascript">
    var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);

        //place suggest second box
        new google.maps.places.Autocomplete(
          (document.getElementById('autocomplete2')),
          {types: ['geocode']}
        );

        new google.maps.places.Autocomplete(
          (document.getElementById('autocomplete3')),
          {types: ['geocode']}
        );

        new google.maps.places.Autocomplete(
          (document.getElementById('autocomplete4')),
          {types: ['geocode']}
        );

      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
</script>
@endsection