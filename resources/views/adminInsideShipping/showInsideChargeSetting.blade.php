@extends('layouts.admin')

@section('title') Show Inside Charge @endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">Show All Charge</h4>
                <p class="text-muted font-13 mb-4">
                    <a href="{{route('addInsideChargeSetting')}}">
                    <button type="button" class="btn btn-outline-success waves-effect waves-light">Add New</button>
                    </a>
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Shipment Charge</th>
                            <th class="text-center">Insurance Amount</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                
                
                    <tbody>
                        @foreach($insideChargeInfo as $insideCharge)
                        <tr>
                            <td class="text-center">{{$insideCharge->serviceCharge}}</td>
                            <td class="text-center">{{$insideCharge->insuranceAmount}}</td>
                            <td class="text-center"><?php if($insideCharge->status=='1'){echo "Active";}else if($insideCharge->status=='0'){echo "De-Active";} ?></td>
                            <td class="text-center">
                                <a href="{{route('editInsideChargeSetting',[$insideCharge->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" class="action-icon btnInsideChargeSettingDelete" id="{{$insideCharge->id}}"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@endsection